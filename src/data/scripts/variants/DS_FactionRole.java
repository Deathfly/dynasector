package data.scripts.variants;

public class DS_FactionRole {

    public final String role;
    public final float val;

    public DS_FactionRole(String role, float val) {
        this.role = role;
        this.val = val;
    }
}
