package data.scripts.variants;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.SettingsAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.loading.RoleEntryAPI;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import data.scripts.DSModPlugin;
import data.scripts.variants.DS_VariantRandomizer.Archetype;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Note: the player_npc faction MUST be last in load order!
public class DS_Database {

    public static final Map<String, Map<String, List<DS_FactionRole>>> factionRoles = new HashMap<>(30);
    public static final Map<String, Map<String, List<DS_FactionVariant>>> factionVariants = new HashMap<>(30);
    public static final Map<String, Map<SlotType, Float>> factionWeaponAverages = new LinkedHashMap<>(30);
    public static final Map<String, Map<String, Float>> factionWeapons = new LinkedHashMap<>(30);
    public static final Map<String, DS_WeaponEntry> masterWeaponList = new LinkedHashMap<>(300);
    public static final Map<String, Map<String, EnumMap<RandomizerWeaponType, Float>>> shipSlotCoefficients = new HashMap<>(500); // Ranged 0 to ~1, biased around 0.5
    public static final Map<String, Float> variantQuality = new HashMap<>(500); // Actually mostly ship hull IDs

    private static final String NEXERELIN_SETTINGS = "nexerelinOptions.json";
    private static final String PLAYER_FACTION = "player_npc";

    private static final Map<String, Map<String, Float>> factionTechMap = new HashMap<>(30);
    private static final Set<String> invalidShipHeaders = new HashSet<>(32);
    private static final Set<String> invalidWeaponHeaders = new HashSet<>(16);
    private static final Logger log = Global.getLogger(DS_Database.class);
    private static final Map<String, Float> playerWeightMap = new HashMap<>(30);
    private static final Set<String> roleHeaders = new HashSet<>(29);

    static final Map<Archetype, Set<String>> ARCHETYPE_HULLMOD_BLOCK = new EnumMap<>(Archetype.class);
    static final Map<String, Set<String>> FACTION_HULLMOD_BLOCK = new HashMap<>(30);
    static final Set<String> FIGHTER_IGNORE = new HashSet<>(28);
    static final Set<String> HULL_IGNORE = new HashSet<>(0);
    static final Map<String, Integer> SHIELD_QUALITY = new HashMap<>(500);
    static final Set<String> WEAPON_IGNORE = new HashSet<>(0);

    static {
        log.setLevel(Level.ALL);
    }

    static {
        FIGHTER_IGNORE.add("ssp_boss_wasp_wing");
        FIGHTER_IGNORE.add("SCY_lamia_armorFront_wing");
        FIGHTER_IGNORE.add("SCY_lamia_armorLeft_wing");
        FIGHTER_IGNORE.add("SCY_lamia_armorRight_wing");
        FIGHTER_IGNORE.add("SCY_stheno_armorBL_wing");
        FIGHTER_IGNORE.add("SCY_stheno_armorBR_wing");
        FIGHTER_IGNORE.add("SCY_stheno_armorFL_wing");
        FIGHTER_IGNORE.add("SCY_stheno_armorFR_wing");
        FIGHTER_IGNORE.add("SCY_stheno_armorNose_wing");
        FIGHTER_IGNORE.add("SCY_erymanthianBoar_armorBL_wing");
        FIGHTER_IGNORE.add("SCY_erymanthianBoar_armorBR_wing");
        FIGHTER_IGNORE.add("SCY_erymanthianBoar_armorFL_wing");
        FIGHTER_IGNORE.add("SCY_erymanthianBoar_armorFR_wing");
        FIGHTER_IGNORE.add("SCY_erymanthianBoar_armorL_wing");
        FIGHTER_IGNORE.add("SCY_erymanthianBoar_armorR_wing");
        FIGHTER_IGNORE.add("SCY_khalkotauroiPodsA_wing");
        FIGHTER_IGNORE.add("SCY_khalkotauroiPodsB_wing");
        FIGHTER_IGNORE.add("SCY_khalkotauroiPodsC_wing");
        FIGHTER_IGNORE.add("SCY_khalkotauroiPodsD_wing");
        FIGHTER_IGNORE.add("SCY_khalkotauroiPodsE_wing");
        FIGHTER_IGNORE.add("SCY_khalkotauroiPodsF_wing");
        FIGHTER_IGNORE.add("SCY_NL_armorBL_wing");
        FIGHTER_IGNORE.add("SCY_NL_armorBR_wing");
        FIGHTER_IGNORE.add("SCY_NL_armorFL_wing");
        FIGHTER_IGNORE.add("SCY_NL_armorFR_wing");
        FIGHTER_IGNORE.add("SCY_keto_armorBL_wing");
        FIGHTER_IGNORE.add("SCY_keto_armorBR_wing");
        FIGHTER_IGNORE.add("SCY_keto_armorFL_wing");
        FIGHTER_IGNORE.add("SCY_keto_armorFR_wing");

        HULL_IGNORE.add("ssp_cristarium_Hull");
        HULL_IGNORE.add("ssp_archangel_Hull");
        HULL_IGNORE.add("ssp_ultron_Hull");
        HULL_IGNORE.add("ssp_hyperzero_Hull");
        HULL_IGNORE.add("ssp_superhyperion_Hull");
        HULL_IGNORE.add("ssp_oberon_Hull");
        HULL_IGNORE.add("ssp_zeus_Hull");
        HULL_IGNORE.add("ssp_ezekiel_Hull");
        HULL_IGNORE.add("ssp_zero_Hull");
        HULL_IGNORE.add("ssp_superzero_Hull");
        HULL_IGNORE.add("ii_boss_olympus_Hull");
        HULL_IGNORE.add("ii_boss_dominus_Hull");
        HULL_IGNORE.add("ii_boss_praetorian_Hull");
        HULL_IGNORE.add("ms_boss_charybdis_Hull");
        HULL_IGNORE.add("ms_boss_mimir_Hull");
        HULL_IGNORE.add("msp_boss_potniaBis_Hull");
        HULL_IGNORE.add("ssp_boss_doom_Hull");
        HULL_IGNORE.add("ssp_boss_onslaught_Hull");
        HULL_IGNORE.add("ssp_boss_mule_Hull");
        HULL_IGNORE.add("ssp_boss_hammerhead_Hull");
        HULL_IGNORE.add("ssp_boss_aurora_Hull");
        HULL_IGNORE.add("ssp_boss_medusa_Hull");
        HULL_IGNORE.add("ssp_boss_sunder_Hull");
        HULL_IGNORE.add("ssp_boss_lasher_r_Hull");
        HULL_IGNORE.add("ssp_boss_falcon_Hull");
        HULL_IGNORE.add("ssp_boss_shade_Hull");
        HULL_IGNORE.add("ssp_boss_tarsus_Hull");
        HULL_IGNORE.add("ssp_boss_euryale_Hull");
        HULL_IGNORE.add("ssp_boss_brawler_Hull");
        HULL_IGNORE.add("ssp_boss_eagle_Hull");
        HULL_IGNORE.add("ssp_boss_cerberus_Hull");
        HULL_IGNORE.add("ssp_boss_dominator_Hull");
        HULL_IGNORE.add("ssp_boss_afflictor_Hull");
        HULL_IGNORE.add("ssp_boss_odyssey_Hull");
        HULL_IGNORE.add("ssp_boss_phaeton_Hull");
        HULL_IGNORE.add("ssp_boss_hyperion_Hull");
        HULL_IGNORE.add("ssp_boss_atlas_Hull");
        HULL_IGNORE.add("ssp_boss_lasher_b_Hull");
        HULL_IGNORE.add("ssp_boss_paragon_Hull");
        HULL_IGNORE.add("ssp_boss_beholder_Hull");
        HULL_IGNORE.add("ssp_boss_dominator_luddic_path_Hull");
        HULL_IGNORE.add("ssp_boss_onslaught_luddic_path_Hull");
        HULL_IGNORE.add("ssp_boss_astral_Hull");
        HULL_IGNORE.add("ssp_boss_conquest_Hull");
        HULL_IGNORE.add("tem_boss_archbishop_Hull");
        HULL_IGNORE.add("tem_boss_paladin_Hull");
        HULL_IGNORE.add("tiandong_boss_wuzhang_Hull");
        HULL_IGNORE.add("pack_bulldog_bullseye_Hull");
        HULL_IGNORE.add("pack_pitbull_bullseye_Hull");
        HULL_IGNORE.add("pack_komondor_bullseye_Hull");
        HULL_IGNORE.add("pack_schnauzer_bullseye_Hull");
        HULL_IGNORE.add("diableavionics_IBBgulf_Hull");
        HULL_IGNORE.add("fox_meatship_Hull");
        HULL_IGNORE.add("fox_meatship_internal_Hull");
        HULL_IGNORE.add("fox_meatship2_Hull");
        HULL_IGNORE.add("fox_meatship3_Hull");
        HULL_IGNORE.add("fox_troll_Hull");
        HULL_IGNORE.add("ii_titan_Hull");
        HULL_IGNORE.add("ii_mirv_Hull");
        HULL_IGNORE.add("junk_pirates_onslaught_overdrive_Hull");
        HULL_IGNORE.add("SCY_deepSpaceOutpost_Hull");
        HULL_IGNORE.add("TAR_meatshipHT_Hull");
        HULL_IGNORE.add("TAR_meatshipLT_Hull");
        HULL_IGNORE.add("TAR_meatshipMT_Hull");

        WEAPON_IGNORE.add("megapulse");
        WEAPON_IGNORE.add("omegaorb2");
        WEAPON_IGNORE.add("wraithii");
        WEAPON_IGNORE.add("zeroannihilator");
        WEAPON_IGNORE.add("zeroannihilator2");
        WEAPON_IGNORE.add("zeroannihilator3");
        WEAPON_IGNORE.add("zerobeam");
        WEAPON_IGNORE.add("zerocannon");
        WEAPON_IGNORE.add("empbomb");
        WEAPON_IGNORE.add("godmode");
        WEAPON_IGNORE.add("tiandong_boss_liberty_flare");
        WEAPON_IGNORE.add("fox_idiot_main");
    }

    static {
        for (Archetype archetype : Archetype.values()) {
            ARCHETYPE_HULLMOD_BLOCK.put(archetype, new HashSet<String>(12));
        }
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARCADE).add("flight_deck_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARCADE).add("cargo_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARCADE).add("additional_crew_quarters");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARCADE).add("supply_conservation_program");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARCADE).add("maximized_ordinance");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARCADE).add("fuel_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARCADE).add("shieldbypass");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARTILLERY).add("cargo_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARTILLERY).add("additional_crew_quarters");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARTILLERY).add("fuel_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ARTILLERY).add("safetyoverrides");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ASSAULT).add("flight_deck_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ASSAULT).add("cargo_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ASSAULT).add("additional_crew_quarters");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ASSAULT).add("fuel_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ASSAULT).add("supply_conservation_program");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ASSAULT).add("SCY_lightArmor");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.BALANCED).add("maximized_ordinance");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.BALANCED).add("shieldbypass");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.BALANCED).add("SCY_lightArmor");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.CLOSE_SUPPORT).add("cargo_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.CLOSE_SUPPORT).add("additional_crew_quarters");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.CLOSE_SUPPORT).add("fuel_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.CLOSE_SUPPORT).add("safetyoverrides");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("flight_deck_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("cargo_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("additional_crew_quarters");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("fuel_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("supply_conservation_program");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("shieldbypass");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("blast_doors");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("dedicated_targeting_core");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("solar_shielding");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ELITE).add("unstable_injector");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ESCORT).add("shieldbypass");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ESCORT).add("safetyoverrides");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ESCORT).add("SCY_lightArmor");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ESCORT).add("maximized_ordinance");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("maximized_ordinance");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("shieldbypass");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("brtarget");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("advancedoptics");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("eccm");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("magazines");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("missleracks");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("fluxcoil");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("hardened_subsystems");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("pointdefenseai");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("safetyoverrides");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.FLEET).add("unstable_injector");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SKIRMISH).add("flight_deck_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SKIRMISH).add("cargo_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SKIRMISH).add("additional_crew_quarters");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SKIRMISH).add("fuel_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SKIRMISH).add("heavyarmor");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SKIRMISH).add("ii_energized_armor");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SKIRMISH).add("ii_fire_control");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.STRIKE).add("flight_deck_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.STRIKE).add("cargo_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.STRIKE).add("additional_crew_quarters");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.STRIKE).add("fuel_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.STRIKE).add("ii_fire_control");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SUPPORT).add("flight_deck_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SUPPORT).add("maximized_ordinance");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SUPPORT).add("shieldbypass");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.SUPPORT).add("safetyoverrides");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ULTIMATE).add("cargo_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ULTIMATE).add("additional_crew_quarters");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ULTIMATE).add("fuel_expansion");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ULTIMATE).add("supply_conservation_program");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ULTIMATE).add("shieldbypass");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ULTIMATE).add("blast_doors");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ULTIMATE).add("dedicated_targeting_core");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ULTIMATE).add("solar_shielding");
        ARCHETYPE_HULLMOD_BLOCK.get(Archetype.ULTIMATE).add("unstable_injector");

        FACTION_HULLMOD_BLOCK.put(Factions.DIKTAT, new HashSet<String>(1));
        FACTION_HULLMOD_BLOCK.get(Factions.DIKTAT).add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.put(Factions.HEGEMONY, new HashSet<String>(12));
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("maximized_ordinance");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("advanced_ai_core");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("brassaultop");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("brtarget");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("brdrive");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("ii_energized_armor");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("ii_fire_control");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("safetyoverrides");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("pointdefenseai");
        FACTION_HULLMOD_BLOCK.get(Factions.HEGEMONY).add("unstable_injector");
        FACTION_HULLMOD_BLOCK.put(Factions.INDEPENDENT, new HashSet<String>(0));
        FACTION_HULLMOD_BLOCK.put(Factions.KOL, new HashSet<String>(10));
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("advanced_ai_core");
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("flight_deck_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("cargo_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("additional_crew_quarters");
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("fuel_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get(Factions.KOL).add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.put(Factions.LIONS_GUARD, new HashSet<String>(7));
        FACTION_HULLMOD_BLOCK.get(Factions.LIONS_GUARD).add("flight_deck_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.LIONS_GUARD).add("cargo_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.LIONS_GUARD).add("additional_crew_quarters");
        FACTION_HULLMOD_BLOCK.get(Factions.LIONS_GUARD).add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get(Factions.LIONS_GUARD).add("fuel_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.LIONS_GUARD).add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get(Factions.LIONS_GUARD).add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.put(Factions.LUDDIC_CHURCH, new HashSet<String>(12));
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("advanced_ai_core");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("pointdefenseai");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("reinforcedhull");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("targetingunit");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("augmentedengines");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("frontshield");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("safetyoverrides");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("brassaultop");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("brtarget");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("brdrive");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("ii_energized_armor");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_CHURCH).add("ii_fire_control");
        FACTION_HULLMOD_BLOCK.put(Factions.LUDDIC_PATH, new HashSet<String>(21));
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("advanced_ai_core");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("pointdefenseai");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("targetingunit");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("augmentedengines");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("frontshield");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("flight_deck_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("cargo_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("additional_crew_quarters");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("fuel_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("autorepair");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("fluxcoil");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("fluxdistributor");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("advancedshieldemitter");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("extendedshieldemitter");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("frontemitter");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("hardenedshieldemitter");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("adaptiveshields");
        FACTION_HULLMOD_BLOCK.get(Factions.LUDDIC_PATH).add("stabilizedshieldemitter");
        FACTION_HULLMOD_BLOCK.put(Factions.PIRATES, new HashSet<String>(5));
        FACTION_HULLMOD_BLOCK.get(Factions.PIRATES).add("flight_deck_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.PIRATES).add("cargo_expansion");
        FACTION_HULLMOD_BLOCK.get(Factions.PIRATES).add("additional_crew_quarters");
        FACTION_HULLMOD_BLOCK.get(Factions.PIRATES).add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get(Factions.PIRATES).add("fuel_expansion");
        FACTION_HULLMOD_BLOCK.put(Factions.TRITACHYON, new HashSet<String>(9));
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("safetyoverrides");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("unstable_injector");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("dedicated_targeting_core");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("blast_doors");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("brassaultop");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("brtarget");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("brdrive");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("ii_energized_armor");
        FACTION_HULLMOD_BLOCK.get(Factions.TRITACHYON).add("ii_fire_control");
        FACTION_HULLMOD_BLOCK.put("cabal", new HashSet<String>(9));
        FACTION_HULLMOD_BLOCK.get("cabal").add("dedicated_targeting_core");
        FACTION_HULLMOD_BLOCK.get("cabal").add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get("cabal").add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get("cabal").add("flight_deck_expansion");
        FACTION_HULLMOD_BLOCK.get("cabal").add("cargo_expansion");
        FACTION_HULLMOD_BLOCK.get("cabal").add("additional_crew_quarters");
        FACTION_HULLMOD_BLOCK.get("cabal").add("fuel_expansion");
        FACTION_HULLMOD_BLOCK.put("interstellarimperium", new HashSet<String>(7));
        FACTION_HULLMOD_BLOCK.get("interstellarimperium").add("safetyoverrides");
        FACTION_HULLMOD_BLOCK.get("interstellarimperium").add("unstable_injector");
        FACTION_HULLMOD_BLOCK.get("interstellarimperium").add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.get("interstellarimperium").add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get("interstellarimperium").add("brassaultop");
        FACTION_HULLMOD_BLOCK.get("interstellarimperium").add("brtarget");
        FACTION_HULLMOD_BLOCK.get("interstellarimperium").add("brdrive");
        FACTION_HULLMOD_BLOCK.put("citadeldefenders", new HashSet<String>(1));
        FACTION_HULLMOD_BLOCK.get("citadeldefenders").add("shieldbypass");
        FACTION_HULLMOD_BLOCK.put("blackrock_driveyards", new HashSet<String>(7));
        FACTION_HULLMOD_BLOCK.get("blackrock_driveyards").add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get("blackrock_driveyards").add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get("blackrock_driveyards").add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get("blackrock_driveyards").add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get("blackrock_driveyards").add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.get("blackrock_driveyards").add("pointdefenseai");
        FACTION_HULLMOD_BLOCK.get("blackrock_driveyards").add("unstable_injector");
        FACTION_HULLMOD_BLOCK.put("exigency", new HashSet<String>(12));
        FACTION_HULLMOD_BLOCK.get("exigency").add("maximized_ordinance");
        FACTION_HULLMOD_BLOCK.get("exigency").add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get("exigency").add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get("exigency").add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get("exigency").add("brassaultop");
        FACTION_HULLMOD_BLOCK.get("exigency").add("brtarget");
        FACTION_HULLMOD_BLOCK.get("exigency").add("brdrive");
        FACTION_HULLMOD_BLOCK.get("exigency").add("ii_energized_armor");
        FACTION_HULLMOD_BLOCK.get("exigency").add("ii_fire_control");
        FACTION_HULLMOD_BLOCK.get("exigency").add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.get("exigency").add("frontshield");
        FACTION_HULLMOD_BLOCK.get("exigency").add("unstable_injector");
        FACTION_HULLMOD_BLOCK.get("exigency").add("dedicated_targeting_core");
        FACTION_HULLMOD_BLOCK.get("exigency").add("safetyoverrides");
        FACTION_HULLMOD_BLOCK.put("exipirated", new HashSet<String>(6));
        FACTION_HULLMOD_BLOCK.get("exipirated").add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get("exipirated").add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.get("exipirated").add("cargo_expansion");
        FACTION_HULLMOD_BLOCK.get("exipirated").add("additional_crew_quarters");
        FACTION_HULLMOD_BLOCK.get("exipirated").add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get("exipirated").add("fuel_expansion");
        FACTION_HULLMOD_BLOCK.put("templars", new HashSet<String>(16));
        FACTION_HULLMOD_BLOCK.get("templars").add("maximized_ordinance");
        FACTION_HULLMOD_BLOCK.get("templars").add("flight_deck_expansion");
        FACTION_HULLMOD_BLOCK.get("templars").add("cargo_expansion");
        FACTION_HULLMOD_BLOCK.get("templars").add("additional_crew_quarters");
        FACTION_HULLMOD_BLOCK.get("templars").add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get("templars").add("fuel_expansion");
        FACTION_HULLMOD_BLOCK.get("templars").add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get("templars").add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get("templars").add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.get("templars").add("brassaultop");
        FACTION_HULLMOD_BLOCK.get("templars").add("brtarget");
        FACTION_HULLMOD_BLOCK.get("templars").add("brdrive");
        FACTION_HULLMOD_BLOCK.get("templars").add("ii_energized_armor");
        FACTION_HULLMOD_BLOCK.get("templars").add("ii_fire_control");
        FACTION_HULLMOD_BLOCK.get("templars").add("blast_doors");
        FACTION_HULLMOD_BLOCK.get("templars").add("dedicated_targeting_core");
        FACTION_HULLMOD_BLOCK.get("templars").add("safetyoverrides");
        FACTION_HULLMOD_BLOCK.get("templars").add("unstable_injector");
        FACTION_HULLMOD_BLOCK.put("shadow_industry", new HashSet<String>(3));
        FACTION_HULLMOD_BLOCK.get("shadow_industry").add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get("shadow_industry").add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get("shadow_industry").add("safetyoverrides");
        FACTION_HULLMOD_BLOCK.get("shadow_industry").add("brassaultop");
        FACTION_HULLMOD_BLOCK.get("shadow_industry").add("brtarget");
        FACTION_HULLMOD_BLOCK.get("shadow_industry").add("brdrive");
        FACTION_HULLMOD_BLOCK.put("mayorate", new HashSet<String>(2));
        FACTION_HULLMOD_BLOCK.get("mayorate").add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get("mayorate").add("shieldbypass");
        FACTION_HULLMOD_BLOCK.put("junk_pirates", new HashSet<String>(5));
        FACTION_HULLMOD_BLOCK.get("junk_pirates").add("cargo_expansion");
        FACTION_HULLMOD_BLOCK.get("junk_pirates").add("additional_crew_quarters");
        FACTION_HULLMOD_BLOCK.get("junk_pirates").add("supply_conservation_program");
        FACTION_HULLMOD_BLOCK.get("junk_pirates").add("fuel_expansion");
        FACTION_HULLMOD_BLOCK.get("junk_pirates").add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.put("pack", new HashSet<String>(5));
        FACTION_HULLMOD_BLOCK.get("pack").add("maximized_ordinance");
        FACTION_HULLMOD_BLOCK.get("pack").add("brassaultop");
        FACTION_HULLMOD_BLOCK.get("pack").add("brtarget");
        FACTION_HULLMOD_BLOCK.get("pack").add("brdrive");
        FACTION_HULLMOD_BLOCK.get("pack").add("ii_energized_armor");
        FACTION_HULLMOD_BLOCK.get("pack").add("ii_fire_control");
        FACTION_HULLMOD_BLOCK.get("pack").add("shieldbypass");
        FACTION_HULLMOD_BLOCK.put("syndicate_asp", new HashSet<String>(6));
        FACTION_HULLMOD_BLOCK.get("syndicate_asp").add("brassaultop");
        FACTION_HULLMOD_BLOCK.get("syndicate_asp").add("brtarget");
        FACTION_HULLMOD_BLOCK.get("syndicate_asp").add("brdrive");
        FACTION_HULLMOD_BLOCK.get("syndicate_asp").add("ii_energized_armor");
        FACTION_HULLMOD_BLOCK.get("syndicate_asp").add("ii_fire_control");
        FACTION_HULLMOD_BLOCK.get("syndicate_asp").add("maximized_ordinance");
        FACTION_HULLMOD_BLOCK.get("syndicate_asp").add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get("syndicate_asp").add("safetyoverrides");
        FACTION_HULLMOD_BLOCK.put("SCY", new HashSet<String>(6));
        FACTION_HULLMOD_BLOCK.get("SCY").add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get("SCY").add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get("SCY").add("ii_energized_armor");
        FACTION_HULLMOD_BLOCK.get("SCY").add("ii_fire_control");
        FACTION_HULLMOD_BLOCK.get("SCY").add("shieldbypass");
        FACTION_HULLMOD_BLOCK.get("SCY").add("brassaultop");
        FACTION_HULLMOD_BLOCK.get("SCY").add("brtarget");
        FACTION_HULLMOD_BLOCK.get("SCY").add("brdrive");
        FACTION_HULLMOD_BLOCK.put("tiandong", new HashSet<String>(1));
        FACTION_HULLMOD_BLOCK.get("tiandong").add("SCY_lightArmor");
        FACTION_HULLMOD_BLOCK.put("diableavionics", new HashSet<String>(5));
        FACTION_HULLMOD_BLOCK.get("diableavionics").add("ilk_SensorSuite");
        FACTION_HULLMOD_BLOCK.get("diableavionics").add("ilk_AICrew");
        FACTION_HULLMOD_BLOCK.get("diableavionics").add("ii_energized_armor");
        FACTION_HULLMOD_BLOCK.get("diableavionics").add("ii_fire_control");
        FACTION_HULLMOD_BLOCK.get("diableavionics").add("brassaultop");
        FACTION_HULLMOD_BLOCK.get("diableavionics").add("brtarget");
        FACTION_HULLMOD_BLOCK.get("diableavionics").add("brdrive");

        // -1 = expendable
        // 0 = crap
        // 1 = okay (default)
        // 2 = good
        SHIELD_QUALITY.put("dominator", -1);
        SHIELD_QUALITY.put("onslaught", -1);
        SHIELD_QUALITY.put("ssp_sidecar", -1);
        SHIELD_QUALITY.put("ssp_punisher", -1);
        SHIELD_QUALITY.put("ssp_infernalmachine", -1);
        SHIELD_QUALITY.put("ssp_boss_dominator", -1);
        SHIELD_QUALITY.put("ssp_boss_onslaught", -1);
        SHIELD_QUALITY.put("ssp_boss_dominator_luddic_path", -1);
        SHIELD_QUALITY.put("ssp_boss_onslaught_luddic_path", -1);
        SHIELD_QUALITY.put("ssp_boss_lasher_r", -1);
        SHIELD_QUALITY.put("ii_caesar", -1);
        SHIELD_QUALITY.put("ii_dictator", -1);
        SHIELD_QUALITY.put("junk_pirates_boxenstein", -1);
        SHIELD_QUALITY.put("junk_pirates_boxer", -1);
        SHIELD_QUALITY.put("tiandong_xu", -1);
        SHIELD_QUALITY.put("tiandong_hanzhong", -1);
        SHIELD_QUALITY.put("tiandong_nanzhong", -1);
        SHIELD_QUALITY.put("diableavionics_draft_Hull", -1);
        SHIELD_QUALITY.put("prometheus", 0);
        SHIELD_QUALITY.put("buffalo2", 0);
        SHIELD_QUALITY.put("cerberus", 0);
        SHIELD_QUALITY.put("hound", 0);
        SHIELD_QUALITY.put("conquest", 0);
        SHIELD_QUALITY.put("hermes", 0);
        SHIELD_QUALITY.put("dram", 0);
        SHIELD_QUALITY.put("tarsus", 0);
        SHIELD_QUALITY.put("phaeton", 0);
        SHIELD_QUALITY.put("shepherd", 0);
        SHIELD_QUALITY.put("condor", 0);
        SHIELD_QUALITY.put("enforcer", 0);
        SHIELD_QUALITY.put("mule", 0);
        SHIELD_QUALITY.put("venture", 0);
        SHIELD_QUALITY.put("atlas", 0);
        SHIELD_QUALITY.put("mudskipper", 0);
        SHIELD_QUALITY.put("nebula", 0);
        SHIELD_QUALITY.put("starliner", 0);
        SHIELD_QUALITY.put("ssp_torch", 0);
        SHIELD_QUALITY.put("ssp_vindicator", 0);
        SHIELD_QUALITY.put("ssp_barbarian", 0);
        SHIELD_QUALITY.put("ssp_renegade", 0);
        SHIELD_QUALITY.put("ssp_shark", 0);
        SHIELD_QUALITY.put("ssp_venom", 0);
        SHIELD_QUALITY.put("ssp_boar", 0);
        SHIELD_QUALITY.put("ssp_mongrel", 0);
        SHIELD_QUALITY.put("ssp_amalgam", 0);
        SHIELD_QUALITY.put("ssp_boss_conquest", 0);
        SHIELD_QUALITY.put("ssp_boss_phaeton", 0);
        SHIELD_QUALITY.put("ssp_boss_tarsus", 0);
        SHIELD_QUALITY.put("ssp_boss_mule", 0);
        SHIELD_QUALITY.put("ssp_boss_atlas", 0);
        SHIELD_QUALITY.put("ssp_boss_cerberus", 0);
        SHIELD_QUALITY.put("fox_dfrigate", 0);
        SHIELD_QUALITY.put("Fox_Minelayer", 0);
        SHIELD_QUALITY.put("Fox_catamaran", 0);
        SHIELD_QUALITY.put("Fox_LDestroyer", 0);
        SHIELD_QUALITY.put("Fox_Carrier", 0);
        SHIELD_QUALITY.put("Fox_Dreadnaught", 0);
        SHIELD_QUALITY.put("ii_auriga", 0);
        SHIELD_QUALITY.put("ii_basileus_f", 0);
        SHIELD_QUALITY.put("ii_basileus", 0);
        SHIELD_QUALITY.put("ii_jupiter", 0);
        SHIELD_QUALITY.put("msp_thresher", 0);
        SHIELD_QUALITY.put("exipirated_azryel", 0);
        SHIELD_QUALITY.put("exipirated_gehenna", 0);
        SHIELD_QUALITY.put("exipirated_harinder", 0);
        SHIELD_QUALITY.put("exipirated_kafziel", 0);
        SHIELD_QUALITY.put("exipirated_rauwel", 0);
        SHIELD_QUALITY.put("brdy_mantis", 0);
        SHIELD_QUALITY.put("brdy_locust", 0);
        SHIELD_QUALITY.put("brdy_typheus", 0);
        SHIELD_QUALITY.put("brdy_stenos", 0);
        SHIELD_QUALITY.put("brdy_convergence", 0);
        SHIELD_QUALITY.put("brdy_eschaton", 0);
        SHIELD_QUALITY.put("brdy_gonodactylus", 0);
        SHIELD_QUALITY.put("junk_pirates_stoatB", 0);
        SHIELD_QUALITY.put("junk_pirates_stoatA", 0);
        SHIELD_QUALITY.put("junk_pirates_scythe", 0);
        SHIELD_QUALITY.put("junk_pirates_kraken", 0);
        SHIELD_QUALITY.put("junk_pirates_the_reaper", 0);
        SHIELD_QUALITY.put("junk_pirates_mandarine", 0);
        SHIELD_QUALITY.put("junk_pirates_satsuma", 0);
        SHIELD_QUALITY.put("pack_sharpei", 0);
        SHIELD_QUALITY.put("SCY_lamiaArmored", 0);
        SHIELD_QUALITY.put("SCY_sthenoArmored", 0);
        SHIELD_QUALITY.put("SCY_erymanthianBoarArmored", 0);
        SHIELD_QUALITY.put("SCY_nemeanLion", 0);
        SHIELD_QUALITY.put("tiandong_luo_yang", 0);
        SHIELD_QUALITY.put("tiandong_xiakou", 0);
        SHIELD_QUALITY.put("tiandong_wuzhang", 0);
        SHIELD_QUALITY.put("tiandong_dingjun", 0);
        SHIELD_QUALITY.put("tiandong_tianshui", 0);
        SHIELD_QUALITY.put("tiandong_chengdu", 0);
        SHIELD_QUALITY.put("tiandong_guan_du", 0);
        SHIELD_QUALITY.put("tiandong_wujun", 0);
        SHIELD_QUALITY.put("tiandong_lao_hu", 0);
        SHIELD_QUALITY.put("tiandong_tuolu", 0);
        SHIELD_QUALITY.put("tiandong_boss_wuzhang", 0);
        SHIELD_QUALITY.put("diableavionics_haze_Hull", 0);
        SHIELD_QUALITY.put("diableavionics_pandemonium_Hull", 0);
        SHIELD_QUALITY.put("hyperion", 2);
        SHIELD_QUALITY.put("omen", 2);
        SHIELD_QUALITY.put("monitor", 2);
        SHIELD_QUALITY.put("sunder", 2);
        SHIELD_QUALITY.put("medusa", 2);
        SHIELD_QUALITY.put("apogee", 2);
        SHIELD_QUALITY.put("aurora", 2);
        SHIELD_QUALITY.put("astral", 2);
        SHIELD_QUALITY.put("paragon", 2);
        SHIELD_QUALITY.put("odyssey", 2);
        SHIELD_QUALITY.put("tempest", 2);
        SHIELD_QUALITY.put("ssp_sunder_u", 2);
        SHIELD_QUALITY.put("ssp_cathedral", 2);
        SHIELD_QUALITY.put("ssp_circe", 2);
        SHIELD_QUALITY.put("ssp_arachne", 2);
        SHIELD_QUALITY.put("ssp_venomx", 2);
        SHIELD_QUALITY.put("ssp_vortex", 2);
        SHIELD_QUALITY.put("swp_chronos", 2);
        SHIELD_QUALITY.put("ssp_boss_sunder", 2);
        SHIELD_QUALITY.put("ssp_boss_medusa", 2);
        SHIELD_QUALITY.put("ssp_boss_hyperion", 2);
        SHIELD_QUALITY.put("ssp_boss_paragon", 2);
        SHIELD_QUALITY.put("ssp_boss_aurora", 2);
        SHIELD_QUALITY.put("ssp_boss_odyssey", 2);
        SHIELD_QUALITY.put("ssp_boss_lasher_b", 2);
        SHIELD_QUALITY.put("ssp_boss_astral", 2);
        SHIELD_QUALITY.put("Fox_Tank", 2);
        SHIELD_QUALITY.put("Fox_Drone", 2);
        SHIELD_QUALITY.put("ii_maximus", 2);
        SHIELD_QUALITY.put("ii_jupiter", 2);
        SHIELD_QUALITY.put("ms_morningstar", 2);
        SHIELD_QUALITY.put("ms_tartarus", 2);
        SHIELD_QUALITY.put("ms_elysium", 2);
        SHIELD_QUALITY.put("ilk_lilith", 2);
        SHIELD_QUALITY.put("ilk_cimeterre", 2);
        SHIELD_QUALITY.put("ilk_del_azarchel", 2);
        SHIELD_QUALITY.put("ilk_tiamat", 2);
        SHIELD_QUALITY.put("junk_pirates_clam", 2);
        SHIELD_QUALITY.put("pack_ridgeback", 2);
        SHIELD_QUALITY.put("pack_ridgeback_x", 2);
        SHIELD_QUALITY.put("syndicate_asp_diamondback", 2);
        SHIELD_QUALITY.put("syndicate_asp_gigantophis", 2);
        SHIELD_QUALITY.put("SCY_tisiphone", 2);
        SHIELD_QUALITY.put("SCY_stymphalian", 2);
        SHIELD_QUALITY.put("SCY_euryale", 2);
        SHIELD_QUALITY.put("SCY_manticore", 2);
        SHIELD_QUALITY.put("SCY_manticoreMirv", 2);
        SHIELD_QUALITY.put("SCY_manticorePhase", 2);
        SHIELD_QUALITY.put("SCY_manticoreCarrier", 2);
        SHIELD_QUALITY.put("SCY_khalkotauroi", 2);
        SHIELD_QUALITY.put("SCY_siren", 2);
        SHIELD_QUALITY.put("diableavionics_derecho_Hull", 2);
        SHIELD_QUALITY.put("diableavionics_vapor_Hull", 2);
        SHIELD_QUALITY.put("afflictor", 2); // phase
        SHIELD_QUALITY.put("shade", 2); // phase
        SHIELD_QUALITY.put("ssp_excelsior", 2); // phase
        SHIELD_QUALITY.put("ssp_revenant", 2); // phase
        SHIELD_QUALITY.put("ssp_scythe", 2); // phase
        SHIELD_QUALITY.put("doom", 2); // phase
        SHIELD_QUALITY.put("ssp_boss_afflictor", 2); // phase
        SHIELD_QUALITY.put("ssp_boss_doom", 2); // phase
        SHIELD_QUALITY.put("ssp_boss_euryale", 2); // phase
        SHIELD_QUALITY.put("ssp_boss_shade", 2); // phase
        SHIELD_QUALITY.put("brdy_imaginos", 2); // phase
        SHIELD_QUALITY.put("brdy_asura", 2); // phase
        SHIELD_QUALITY.put("brdy_morpheus", 2); // phase
        SHIELD_QUALITY.put("Fox_Battleship", 2); // phase
        SHIELD_QUALITY.put("ms_shamash", 2); // phase
        SHIELD_QUALITY.put("ms_scylla", 2); // phase
        SHIELD_QUALITY.put("junk_pirates_turbot", 2); // phase
        SHIELD_QUALITY.put("SCY_megaera", 2); // phase
    }

    static {
        invalidShipHeaders.add("name");
        invalidShipHeaders.add("id");
        invalidShipHeaders.add("qfMin");
        invalidShipHeaders.add("qfMax");
        invalidShipHeaders.add("rare");
        invalidShipHeaders.add("interceptor");
        invalidShipHeaders.add("fighter");
        invalidShipHeaders.add("bomber");
        invalidShipHeaders.add("fastAttack");
        invalidShipHeaders.add("escortSmall");
        invalidShipHeaders.add("escortMedium");
        invalidShipHeaders.add("combatSmall");
        invalidShipHeaders.add("combatMedium");
        invalidShipHeaders.add("combatLarge");
        invalidShipHeaders.add("combatCapital");
        invalidShipHeaders.add("combatFreighterSmall");
        invalidShipHeaders.add("combatFreighterMedium");
        invalidShipHeaders.add("combatFreighterLarge");
        invalidShipHeaders.add("civilianRandom");
        invalidShipHeaders.add("carrierSmall");
        invalidShipHeaders.add("carrierMedium");
        invalidShipHeaders.add("carrierLarge");
        invalidShipHeaders.add("freighterSmall");
        invalidShipHeaders.add("freighterMedium");
        invalidShipHeaders.add("freighterLarge");
        invalidShipHeaders.add("tankerSmall");
        invalidShipHeaders.add("tankerMedium");
        invalidShipHeaders.add("tankerLarge");
        invalidShipHeaders.add("personnelSmall");
        invalidShipHeaders.add("personnelMedium");
        invalidShipHeaders.add("personnelLarge");
        invalidShipHeaders.add("linerSmall");
        invalidShipHeaders.add("linerMedium");
        invalidShipHeaders.add("linerLarge");
        invalidShipHeaders.add("tug");
        invalidShipHeaders.add("crig");
        invalidShipHeaders.add("utility");
        invalidShipHeaders.add("miningSmall");
        invalidShipHeaders.add("miningMedium");
        invalidShipHeaders.add("miningLarge");
        invalidShipHeaders.add("techType");
        invalidWeaponHeaders.add("name");
        invalidWeaponHeaders.add("id");
        invalidWeaponHeaders.add("builtin");
        invalidWeaponHeaders.add("size");
        invalidWeaponHeaders.add("type");
        invalidWeaponHeaders.add("tier");
        invalidWeaponHeaders.add("OPs");
        invalidWeaponHeaders.add("limitedAmmo");
        invalidWeaponHeaders.add("damageType");
        invalidWeaponHeaders.add("attack");
        invalidWeaponHeaders.add("standoff");
        invalidWeaponHeaders.add("alpha");
        invalidWeaponHeaders.add("defense");
        invalidWeaponHeaders.add("closeRange");
        invalidWeaponHeaders.add("longRange");
        invalidWeaponHeaders.add("disable");
        invalidWeaponHeaders.add("techType");
        invalidWeaponHeaders.add("leetness");
        roleHeaders.add("interceptor");
        roleHeaders.add("fighter");
        roleHeaders.add("bomber");
        roleHeaders.add("fastAttack");
        roleHeaders.add("escortSmall");
        roleHeaders.add("escortMedium");
        roleHeaders.add("combatSmall");
        roleHeaders.add("combatMedium");
        roleHeaders.add("combatLarge");
        roleHeaders.add("combatCapital");
        roleHeaders.add("combatFreighterSmall");
        roleHeaders.add("combatFreighterMedium");
        roleHeaders.add("combatFreighterLarge");
        roleHeaders.add("civilianRandom");
        roleHeaders.add("carrierSmall");
        roleHeaders.add("carrierMedium");
        roleHeaders.add("carrierLarge");
        roleHeaders.add("freighterSmall");
        roleHeaders.add("freighterMedium");
        roleHeaders.add("freighterLarge");
        roleHeaders.add("tankerSmall");
        roleHeaders.add("tankerMedium");
        roleHeaders.add("tankerLarge");
        roleHeaders.add("personnelSmall");
        roleHeaders.add("personnelMedium");
        roleHeaders.add("personnelLarge");
        roleHeaders.add("linerSmall");
        roleHeaders.add("linerMedium");
        roleHeaders.add("linerLarge");
        roleHeaders.add("tug");
        roleHeaders.add("crig");
        roleHeaders.add("utility");
        roleHeaders.add("miningSmall");
        roleHeaders.add("miningMedium");
        roleHeaders.add("miningLarge");
    }

    public static void checkForUnusedFighters(SettingsAPI settings) {
        for (String wingId : Global.getSector().getAllFighterWingIds()) {
            if (FIGHTER_IGNORE.contains(wingId)) {
                continue;
            }

            Float quality = variantQuality.get(wingId);
            if (quality == null) {
                log.warn("No fighter definition for " + wingId);
            }
        }
    }

    public static void checkForUnusedHulls(SettingsAPI settings) {
        for (String hullId : Global.getSector().getAllEmptyVariantIds()) {
            if (HULL_IGNORE.contains(hullId)) {
                continue;
            }

            Float quality = variantQuality.get(hullId);
            if (quality == null) {
                ShipVariantAPI variant = Global.getSettings().getVariant(hullId);
                log.warn("No hull definition for " + variant.getHullSpec().getHullId());
            }
        }
    }

    public static void checkForUnusedWeapons(SettingsAPI settings) {
        Set<String> warned = new HashSet<>(100);
        for (String weaponId : Global.getSector().getAllWeaponIds()) {
            if (WEAPON_IGNORE.contains(weaponId)) {
                continue;
            }

            WeaponSpecAPI weaponSpec = settings.getWeaponSpec(weaponId);
            if (weaponSpec.getType() == WeaponType.DECORATIVE) {
                continue;
            }

            boolean system = (weaponSpec.getAIHints().contains(AIHints.SYSTEM) || weaponSpec.getType() == WeaponType.SYSTEM);

            DS_WeaponEntry weapon = masterWeaponList.get(weaponId);
            if (weapon == null) {
                warned.add(weaponId);
                if (system) {
                    log.warn("No weapon definition for " + weaponSpec.getSize().getDisplayName() + " " + weaponSpec.getType().getDisplayName() + " (system): " +
                            weaponId + " (" + weaponSpec.getWeaponName() + ")");
                } else {
                    log.warn("No weapon definition for " + weaponSpec.getSize().getDisplayName() + " " + weaponSpec.getType().getDisplayName() + ": " +
                            weaponId + " (" + weaponSpec.getWeaponName() + ")");
                }
            } else if (weapon.builtIn && !system) {
                warned.add(weaponId);
                log.warn(weaponSpec.getSize().getDisplayName() + " " + weaponSpec.getType().getDisplayName() + " weapon marked as built-in but is modular: " +
                        weaponId + " (" + weaponSpec.getWeaponName() + ")");
            }
        }
        for (String hullId : Global.getSector().getAllEmptyVariantIds()) {
            ShipVariantAPI variant = Global.getSettings().getVariant(hullId);
            for (String slot : variant.getFittedWeaponSlots()) {
                WeaponSpecAPI weaponSpec = variant.getWeaponSpec(slot);
                String weaponId = weaponSpec.getWeaponId();
                if (warned.contains(weaponId)) {
                    continue;
                }

                if (WEAPON_IGNORE.contains(weaponId)) {
                    continue;
                }

                boolean system = (weaponSpec.getAIHints().contains(AIHints.SYSTEM) || weaponSpec.getType() == WeaponType.SYSTEM);

                DS_WeaponEntry weapon = masterWeaponList.get(weaponId);
                if (weapon == null) {
                    warned.add(weaponId);
                    if (system) {
                        log.warn("No weapon definition for " + weaponSpec.getSize().getDisplayName() + " " + weaponSpec.getType().getDisplayName() +
                                " (system, built-in): " + weaponId + " (" + weaponSpec.getWeaponName() + ")");
                    } else {
                        log.warn("No weapon definition for " + weaponSpec.getSize().getDisplayName() + " " + weaponSpec.getType().getDisplayName() +
                                " (built-in): " + weaponId + " (" + weaponSpec.getWeaponName() + ")");
                    }
                } else if (!weapon.builtIn && system) {
                    warned.add(weaponId);
                    log.warn(
                            weaponSpec.getSize().getDisplayName() + " " + weaponSpec.getType().getDisplayName() + " weapon is built-in but marked as modular: " +
                            weaponId + " (" + weaponSpec.getWeaponName() + ")");
                }
            }
        }
    }

    public static void generateSlotCoefficients(SettingsAPI settings) {
        /* Map variants to hull */
        Map<String, List<ShipVariantAPI>> junctionTable = new HashMap<>(500);
        for (String variantId : settings.getAllVariantIds()) {
            ShipVariantAPI variant = settings.getVariant(variantId);

            // It's important that fighters are skipped!
            if (variant == null || variant.isEmptyHullVariant() || variant.isFighter() || !variant.isStockVariant()) {
                continue;
            }
            String hullId = variant.getHullSpec().getHullId();
            List<ShipVariantAPI> shipVariants = junctionTable.get(hullId);
            if (shipVariants == null) {
                shipVariants = new ArrayList<>(10);
                junctionTable.put(hullId, shipVariants);
            }
            shipVariants.add(variant);
        }

        /* Get a coefficient for each weapon type, to avoid over-/under-representation of certain types */
        EnumMap<RandomizerWeaponType, Float> masterCoefficients = new EnumMap<>(RandomizerWeaponType.class);
        for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
            masterCoefficients.put(type, 0f);
        }
        int totalWeapons = 0;
        for (DS_WeaponEntry weaponEntry : masterWeaponList.values()) {
            if (weaponEntry.builtIn) {
                continue;
            }

            if (weaponEntry.alpha) {
                float curr = masterCoefficients.get(RandomizerWeaponType.ALPHA);
                masterCoefficients.put(RandomizerWeaponType.ALPHA, curr + 1f);
            }
            if (weaponEntry.attack) {
                float curr = masterCoefficients.get(RandomizerWeaponType.ATTACK);
                masterCoefficients.put(RandomizerWeaponType.ATTACK, curr + 1f);
            }
            if (weaponEntry.defense) {
                float curr = masterCoefficients.get(RandomizerWeaponType.DEFENSE);
                masterCoefficients.put(RandomizerWeaponType.DEFENSE, curr + 1f);
            }
            if (weaponEntry.disable) {
                float curr = masterCoefficients.get(RandomizerWeaponType.DISABLE);
                masterCoefficients.put(RandomizerWeaponType.DISABLE, curr + 1f);
            }
            if (weaponEntry.standoff) {
                float curr = masterCoefficients.get(RandomizerWeaponType.STANDOFF);
                masterCoefficients.put(RandomizerWeaponType.STANDOFF, curr + 1f);
            }
            if (weaponEntry.closeRange) {
                float curr = masterCoefficients.get(RandomizerWeaponType.CLOSE_RANGE);
                masterCoefficients.put(RandomizerWeaponType.CLOSE_RANGE, curr + 1f);
            }
            if (weaponEntry.longRange) {
                float curr = masterCoefficients.get(RandomizerWeaponType.LONG_RANGE);
                masterCoefficients.put(RandomizerWeaponType.LONG_RANGE, curr + 1f);
            }
//            if (weaponEntry.frontOnly) {
//                float curr = masterCoefficients.get(RandomizerWeaponType.FRONT_ONLY);
//                masterCoefficients.put(RandomizerWeaponType.FRONT_ONLY, curr + 1f);
//            }
//            if (weaponEntry.turretOnly) {
//                float curr = masterCoefficients.get(RandomizerWeaponType.TURRET_ONLY);
//                masterCoefficients.put(RandomizerWeaponType.TURRET_ONLY, curr + 1f);
//            }
            totalWeapons++;
        }
        float totalWeaponCoeff = 0f;
        for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
            float curr = (float) Math.sqrt(Math.max(1f, masterCoefficients.get(type)));
            float coeff = totalWeapons / curr;
            totalWeaponCoeff += coeff;
        }
        totalWeaponCoeff /= RandomizerWeaponType.values().length;
        for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
            float curr = (float) Math.sqrt(Math.max(1f, masterCoefficients.get(type)));
            float coeff = (totalWeapons / curr) / totalWeaponCoeff;
            masterCoefficients.put(type, coeff);
            log.info("Master coefficient for " + type.toString() + ": " + coeff);
        }

        for (String hullVariantId : variantQuality.keySet()) {
            String hullId = hullVariantId;
            if (hullId.endsWith("_Hull")) {
                hullId = hullId.replace("_Hull", "");
            }
            List<ShipVariantAPI> variants = junctionTable.get(hullId);
            if (variants == null || variants.size() <= 0) {
                // No variants to scan, no data to find
                continue;
            }

            ShipHullSpecAPI hullSpec = variants.get(0).getHullSpec();
            List<WeaponSlotAPI> slots = hullSpec.getAllWeaponSlotsCopy();

            Map<String, EnumMap<RandomizerWeaponType, Float>> shipSlotsDef = new HashMap<>(slots.size());
            for (WeaponSlotAPI slot : slots) {
                if (slot.isBuiltIn() || slot.isDecorative() || slot.isSystemSlot() || slot.getWeaponType() == WeaponType.LAUNCH_BAY) {
                    continue;
                }

                EnumMap<RandomizerWeaponType, Float> slotCoefficients = new EnumMap<>(RandomizerWeaponType.class);
                for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
                    slotCoefficients.put(type, masterCoefficients.get(type));
                }

                for (ShipVariantAPI variant : variants) {
                    WeaponSpecAPI weapon = variant.getWeaponSpec(slot.getId());
                    if (weapon == null) {
                        // Don't add anything; coefficients will drop across the board for this slot
                        continue;
                    }

                    DS_WeaponEntry weaponEntry = masterWeaponList.get(weapon.getWeaponId());
                    if (weaponEntry == null) {
                        // No info on this weapon; assume average
                        for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
                            float curr = slotCoefficients.get(type);
                            slotCoefficients.put(type, curr + 0.5f * masterCoefficients.get(type));
                        }
                        continue;
                    }

                    if (weaponEntry.alpha) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.ALPHA);
                        slotCoefficients.put(RandomizerWeaponType.ALPHA, curr + masterCoefficients.get(RandomizerWeaponType.ALPHA));
                    }
                    if (weaponEntry.attack) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.ATTACK);
                        slotCoefficients.put(RandomizerWeaponType.ATTACK, curr + masterCoefficients.get(RandomizerWeaponType.ATTACK));
                    }
                    if (weaponEntry.defense) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.DEFENSE);
                        slotCoefficients.put(RandomizerWeaponType.DEFENSE, curr + masterCoefficients.get(RandomizerWeaponType.DEFENSE));
                    }
                    if (weaponEntry.disable) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.DISABLE);
                        slotCoefficients.put(RandomizerWeaponType.DISABLE, curr + masterCoefficients.get(RandomizerWeaponType.DISABLE));
                    }
                    if (weaponEntry.standoff) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.STANDOFF);
                        slotCoefficients.put(RandomizerWeaponType.STANDOFF, curr + masterCoefficients.get(RandomizerWeaponType.STANDOFF));
                    }
                    if (weaponEntry.closeRange) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.CLOSE_RANGE);
                        slotCoefficients.put(RandomizerWeaponType.CLOSE_RANGE, curr + masterCoefficients.get(RandomizerWeaponType.CLOSE_RANGE));
                    }
                    if (weaponEntry.longRange) {
                        float curr = slotCoefficients.get(RandomizerWeaponType.LONG_RANGE);
                        slotCoefficients.put(RandomizerWeaponType.LONG_RANGE, curr + masterCoefficients.get(RandomizerWeaponType.LONG_RANGE));
                    }
//                    if (weaponEntry.frontOnly) {
//                        float curr = slotCoefficients.get(RandomizerWeaponType.FRONT_ONLY);
//                        slotCoefficients.put(RandomizerWeaponType.FRONT_ONLY, curr + masterCoefficients.get(RandomizerWeaponType.FRONT_ONLY));
//                    }
//                    if (weaponEntry.turretOnly) {
//                        float curr = slotCoefficients.get(RandomizerWeaponType.TURRET_ONLY);
//                        slotCoefficients.put(RandomizerWeaponType.TURRET_ONLY, curr + masterCoefficients.get(RandomizerWeaponType.TURRET_ONLY));
//                    }
                }

                for (RandomizerWeaponType type : RandomizerWeaponType.values()) {
                    float total = slotCoefficients.get(type);
                    float avg = total / (variants.size() + 1);
                    slotCoefficients.put(type, avg);
                    if (DSModPlugin.SHOW_DEBUG_INFO) {
                        log.info("Coefficient for " + hullId + " " + slot.getId() + " " + type.toString() + ": " + avg);
                    }
                }

                shipSlotsDef.put(slot.getId(), slotCoefficients);
            }

            shipSlotCoefficients.put(hullId, shipSlotsDef);
        }
    }

    public static void init() {
        List<String> urls = DS_Database.readFactionsCSV(Global.getSettings(), "data/factions/factions.csv");
        for (String json : urls) {
            JSONObject nexerelin = null;
            JSONObject body = null;

            try {
                body = Global.getSettings().loadJSON(json);
                nexerelin = Global.getSettings().loadJSON(NEXERELIN_SETTINGS);
            } catch (IOException | JSONException e) {
                log.log(Level.ERROR, "JSON Loading Failed! " + e.getMessage());
            }

            if (body == null || nexerelin == null) {
                continue;
            }

            try {
                String factionID = body.getString("FactionID");

                playerWeightMap.put(factionID, (float) nexerelin.optDouble(factionID + "FactionTech", 0f));
            } catch (JSONException e) {
                log.log(Level.ERROR, "Weapon Loading Failed! " + e.getMessage());
            }
        }

        readWeaponsCSV(Global.getSettings(), "data/factions/weapon_categories.csv");
        readAllFactions(Global.getSettings(), urls);
        readShipsCSV(Global.getSettings(), "data/factions/ship_roles.csv");

        // Save some memory
        for (Entry<String, Map<String, List<DS_FactionRole>>> e0 : factionRoles.entrySet()) {
            for (Entry<String, List<DS_FactionRole>> e1 : e0.getValue().entrySet()) {
                ((ArrayList) e1.getValue()).trimToSize();
            }
        }
        for (Entry<String, Map<String, List<DS_FactionVariant>>> e0 : factionVariants.entrySet()) {
            for (Entry<String, List<DS_FactionVariant>> e1 : e0.getValue().entrySet()) {
                ((ArrayList) e1.getValue()).trimToSize();
            }
        }

        if (DSModPlugin.Module_FleetIntegration) {
            rebuildRoleEntries(Global.getSettings());
        }

        if (DSModPlugin.Module_ProceduralVariants) {
            generateSlotCoefficients(Global.getSettings());
        }
    }

    public static boolean isHullModBlocked(String hullModId, Archetype archetype, String faction) {
        Set<String> blockedMods = ARCHETYPE_HULLMOD_BLOCK.get(archetype);
        if (blockedMods != null) {
            if (blockedMods.contains(hullModId)) {
                return true;
            }
        }
        blockedMods = FACTION_HULLMOD_BLOCK.get(faction);
        if (blockedMods != null) {
            if (blockedMods.contains(hullModId)) {
                return true;
            }
        }
        return false;
    }

    public static void readAllFactions(SettingsAPI settings, List<String> urls) {
        for (String json : urls) {
            readFactionJSON(settings, json);
        }
    }

    public static void readFactionJSON(SettingsAPI settings, String urlToJSON) {
        String factionID;

        JSONObject body = null;

        try {
            body = settings.loadJSON(urlToJSON);
        } catch (IOException | JSONException e) {
            log.log(Level.ERROR, "JSON Loading Failed! " + e.getMessage());
        }

        if (body == null) {
            return;
        }

        try {
            factionID = body.getString("FactionID");

            JSONObject techWeightsWeapon = body.getJSONObject("TechWeightsWeapon");

            Map<String, Float> weightsWeapon = new HashMap<>(techWeightsWeapon.length());
            Iterator weightsIteratorWeapon = techWeightsWeapon.keys();
            while (weightsIteratorWeapon.hasNext()) {
                String key = (String) weightsIteratorWeapon.next();
                weightsWeapon.put(key, (float) techWeightsWeapon.optDouble(key, 1.0));
            }

            JSONObject weaponTiers = body.getJSONObject("WeaponTiers");

            float tier[] = new float[6];
            tier[0] = (float) weaponTiers.optDouble("0", 0.0);
            tier[1] = (float) weaponTiers.optDouble("1", 0.0);
            tier[2] = (float) weaponTiers.optDouble("2", 0.0);
            tier[3] = (float) weaponTiers.optDouble("3", 0.0);
            tier[4] = (float) weaponTiers.optDouble("4", 0.0);
            tier[5] = (float) weaponTiers.optDouble("5", 0.0);

            Map<String, Float> thisFactionWeapons = new LinkedHashMap<>(300);
            Map<SlotType, Float> thisFactionWeaponAverages = new EnumMap<>(SlotType.class);
            Map<SlotType, Float> thisFactionWeaponTotalWeights = new EnumMap<>(SlotType.class);

            for (Entry<String, DS_WeaponEntry> entry : masterWeaponList.entrySet()) {
                DS_WeaponEntry weaponEntry = entry.getValue();

                float weight = 0f;
                if (weaponEntry.factionWeights.containsKey(factionID)) {
                    weight = tier[weaponEntry.tier] * weaponEntry.factionWeights.get(factionID);
                }
                if (!weaponEntry.techType.isEmpty()) {
                    weight *= weightsWeapon.get(weaponEntry.techType);
                }

                if (weight > 0f) {
                    thisFactionWeapons.put(entry.getKey(), weight);
                } else {
                    continue;
                }

                List<SlotType> slots = SlotType.getSlotTypesForWeapon(weaponEntry.size, weaponEntry.type);
                for (SlotType slot : slots) {
                    Float total = thisFactionWeaponAverages.get(slot);
                    Float totalWeight = thisFactionWeaponTotalWeights.get(slot);
                    if (total == null) {
                        total = 0f;
                    }
                    if (totalWeight == null) {
                        totalWeight = 0f;
                    }
                    total += weaponEntry.OP * weight;
                    totalWeight += weight;
                    thisFactionWeaponAverages.put(slot, total);
                    thisFactionWeaponTotalWeights.put(slot, totalWeight);
                }
            }

            for (SlotType slot : SlotType.values()) {
                Float total = thisFactionWeaponAverages.get(slot);
                Float totalWeight = thisFactionWeaponTotalWeights.get(slot);
                if (total != null && totalWeight != null && totalWeight > 0f) {
                    float average = total / totalWeight;
                    thisFactionWeaponAverages.put(slot, average);
                    log.info(factionID + " " + slot.toString() + " average OP: " + String.format("%.1f", average));
                } else {
                    if (slot.size == WeaponSize.SMALL) {
                        thisFactionWeaponAverages.put(slot, 5f);
                    } else if (slot.size == WeaponSize.MEDIUM) {
                        thisFactionWeaponAverages.put(slot, 10f);
                    } else if (slot.size == WeaponSize.LARGE) {
                        thisFactionWeaponAverages.put(slot, 20f);
                    }
                }
            }

            factionWeapons.put(factionID, thisFactionWeapons);
            factionWeaponAverages.put(factionID, thisFactionWeaponAverages);

            JSONObject techWeightsShip = body.getJSONObject("TechWeightsShip");

            Map<String, Float> weightsShip = new HashMap<>(techWeightsShip.length());
            Iterator weightsIteratorShip = techWeightsShip.keys();
            while (weightsIteratorShip.hasNext()) {
                String key = (String) weightsIteratorShip.next();
                weightsShip.put(key, (float) techWeightsShip.optDouble(key, 1.0));
            }
            factionTechMap.put(factionID, weightsShip);

            Map<String, List<DS_FactionVariant>> factionVariantMap = new HashMap<>(300);
            for (String role : roleHeaders) {
                factionVariantMap.put(role, new ArrayList<DS_FactionVariant>(50));
            }
            factionVariants.put(factionID, factionVariantMap);

            Map<String, List<DS_FactionRole>> factionRoleMap = new HashMap<>(500);
            factionRoles.put(factionID, factionRoleMap);
        } catch (JSONException e) {
            log.log(Level.ERROR, "Weapon Loading Failed! " + e.getMessage());
        }
    }

    public static List<String> readFactionsCSV(SettingsAPI settings, String SettingID) {
        JSONArray listURLS;
        List<String> tempList = new ArrayList<>(30);

        try {
            listURLS = settings.loadCSV(SettingID);

            for (int count = 0; count < listURLS.length(); count++) {
                tempList.add(listURLS.getJSONObject(count).getString("url"));
            }
        } catch (IOException | JSONException e) {
            log.log(Level.ERROR, "Faction CSV Loading Failed! " + e.getMessage());
        }

        return tempList;
    }

    public static void readShipsCSV(SettingsAPI settings, String SettingID) {
        JSONArray rows;

        try {
            rows = settings.loadCSV(SettingID);
            for (int i = 0; i < rows.length(); i++) {
                JSONObject row = rows.getJSONObject(i);
                String id = row.optString("id");
                if (id.endsWith("_Hull")) {
                    id = id.replace("_Hull", "");
                }

                String techType = row.optString("techType");
                if (!id.isEmpty()) {
                    String idStripped = id;
                    if (!idStripped.endsWith("_wing") && !idStripped.endsWith("_Hull")) {
                        idStripped += "_Hull";
                    }

                    if (!idStripped.endsWith("_wing")) {
                        try {
                            ShipVariantAPI variant = settings.getVariant(idStripped);
                        } catch (Exception ex) {
                            continue;
                        }
                    }

                    float qualityFactorMin = (float) row.optDouble("qfMin", 0.25);
                    float qualityFactorMax = (float) row.optDouble("qfMax", 0.75);
                    variantQuality.put(idStripped, (qualityFactorMin + qualityFactorMax) / 2f);
                    float rarity = (float) row.optDouble("rare", 0.0);

                    int count = 0;
                    float playerWeight = 0f;
                    for (int k = 0; k < row.length(); k++) {
                        String factionID = row.names().getString(k);
                        if (!invalidShipHeaders.contains(factionID)) {
                            count++;
                            Map<String, List<DS_FactionVariant>> factionVariantMap = factionVariants.get(factionID);
                            if (factionVariantMap == null) {
                                continue;
                            }

                            float baseWeight = (float) row.optDouble(factionID, 0.0);
                            if (baseWeight <= 0f) {
                                continue;
                            }

                            Map<String, Float> techWeights = factionTechMap.get(factionID);
                            Float techWeight = techWeights.get(techType);
                            if (techWeight != null) {
                                baseWeight *= techWeight;
                            }

                            float pWeight = baseWeight * (playerWeightMap.containsKey(factionID) ? playerWeightMap.get(factionID) : 0f);
                            if (pWeight > 0f) {
                                playerWeight += pWeight;
                            }

                            Map<String, List<DS_FactionRole>> factionRoleMap = factionRoles.get(factionID);
                            List<DS_FactionRole> factionRoleList = new ArrayList<>(roleHeaders.size());
                            if (factionRoleMap != null) {
                                factionRoleMap.put(id, factionRoleList);
                            }

                            float totalRoleWeight = 0f;
                            for (int j = 0; j < row.length(); j++) {
                                String shipRole = row.names().getString(j);
                                if (roleHeaders.contains(shipRole)) {
                                    List<DS_FactionVariant> factionVariantRoleList = factionVariantMap.get(shipRole);
                                    if (factionVariantRoleList == null) {
                                        continue;
                                    }

                                    float roleWeight = (float) row.optDouble(shipRole, 0.0);
                                    totalRoleWeight += roleWeight;

                                    float weight = baseWeight * roleWeight;
                                    if (weight <= 0f) {
                                        continue;
                                    }

                                    factionVariantRoleList.add(new DS_FactionVariant(idStripped, techType, weight, qualityFactorMin, qualityFactorMax, rarity));
                                }
                            }

                            for (int j = 0; j < row.length(); j++) {
                                String shipRole = row.names().getString(j);
                                if (roleHeaders.contains(shipRole)) {
                                    List<DS_FactionVariant> factionVariantRoleList = factionVariantMap.get(shipRole);
                                    if (factionVariantRoleList == null) {
                                        continue;
                                    }

                                    float roleWeight = (float) row.optDouble(shipRole, 0.0);
                                    if (roleWeight > 0.0001f) {
                                        factionRoleList.add(new DS_FactionRole(shipRole, roleWeight / totalRoleWeight));
                                    }
                                }
                            }
                        }
                    }
                    if (playerWeight > 0f) {
                        playerWeight /= count;
                        Map<String, List<DS_FactionVariant>> factionVariantMap = factionVariants.get(PLAYER_FACTION);
                        if (factionVariantMap == null) {
                            continue;
                        }

                        Map<String, List<DS_FactionRole>> factionRoleMap = factionRoles.get(PLAYER_FACTION);
                        List<DS_FactionRole> factionRoleList = new ArrayList<>(roleHeaders.size());
                        if (factionRoleMap != null) {
                            factionRoleMap.put(id, factionRoleList);
                        }

                        float totalRoleWeight = 0f;
                        for (int j = 0; j < row.length(); j++) {
                            String shipRole = row.names().getString(j);
                            if (roleHeaders.contains(shipRole)) {
                                List<DS_FactionVariant> factionVariantRoleList = factionVariantMap.get(shipRole);
                                if (factionVariantRoleList == null) {
                                    continue;
                                }

                                float roleWeight = (float) row.optDouble(shipRole, 0.0);
                                totalRoleWeight += roleWeight;

                                float weight = playerWeight * roleWeight;
                                if (weight <= 0f) {
                                    continue;
                                }

                                factionVariantRoleList.add(new DS_FactionVariant(idStripped, techType, weight, qualityFactorMin, qualityFactorMax, rarity));
                            }
                        }

                        for (int j = 0; j < row.length(); j++) {
                            String shipRole = row.names().getString(j);
                            if (roleHeaders.contains(shipRole)) {
                                List<DS_FactionVariant> factionVariantRoleList = factionVariantMap.get(shipRole);
                                if (factionVariantRoleList == null) {
                                    continue;
                                }

                                float roleWeight = (float) row.optDouble(shipRole, 0.0);
                                if (roleWeight > 0.0001f) {
                                    factionRoleList.add(new DS_FactionRole(shipRole, roleWeight / totalRoleWeight));
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException | JSONException e) {
            log.log(Level.ERROR, "Ship CSV Loading Failed! " + e.getMessage());
        }
    }

    public static void readWeaponsCSV(SettingsAPI settings, String SettingID) {
        JSONArray rows;

        try {
            rows = settings.loadCSV(SettingID);
            for (int i = 0; i < rows.length(); i++) {
                JSONObject row = rows.getJSONObject(i);
                String id = row.optString("id");

                if (!id.isEmpty()) {
                    WeaponSpecAPI spec = null;
                    try {
                        spec = Global.getSettings().getWeaponSpec(id);
                    } catch (Exception e) {
                    }
                    if (spec == null) {
                        continue;
                    }

                    boolean builtIn = row.optBoolean("builtin", false);

                    String sizeString = row.optString("size", spec.getSize().getDisplayName().toLowerCase());
                    WeaponSize size;
                    switch (sizeString) {
                        case "small":
                            size = WeaponSize.SMALL;
                            break;
                        case "medium":
                            size = WeaponSize.MEDIUM;
                            break;
                        case "large":
                            size = WeaponSize.LARGE;
                            break;
                        default:
                            continue;
                    }
                    String typeString = row.optString("type", spec.getType().getDisplayName().toLowerCase());
                    WeaponType type;
                    switch (typeString) {
                        case "ballistic":
                            type = WeaponType.BALLISTIC;
                            break;
                        case "missile":
                            type = WeaponType.MISSILE;
                            break;
                        case "energy":
                            type = WeaponType.ENERGY;
                            break;
                        case "universal":
                            type = WeaponType.UNIVERSAL;
                            break;
                        case "hybrid":
                            type = WeaponType.HYBRID;
                            break;
                        case "synergy":
                            type = WeaponType.SYNERGY;
                            break;
                        case "composite":
                            type = WeaponType.COMPOSITE;
                            break;
                        case "system":
                            type = WeaponType.SYSTEM;
                            break;
                        case "decorative":
                            type = WeaponType.DECORATIVE;
                            break;
                        case "launch_bay":
                            type = WeaponType.LAUNCH_BAY;
                            break;
                        case "built_in":
                            type = WeaponType.BUILT_IN;
                            break;
                        default:
                            continue;
                    }
                    int tier = row.optInt("tier", spec.getTier());
                    int OP = row.optInt("OPs", (int) spec.getOrdnancePointCost(null));
                    String damageTypeString = row.optString("damageType");
                    boolean limitedAmmo = row.optBoolean("limitedAmmo", false);
                    boolean frontOnly = row.optBoolean("frontOnly", false);
                    boolean turretOnly = row.optBoolean("turretOnly", false);
                    DamageType damageType;
                    switch (damageTypeString) {
                        case "HIGH_EXPLOSIVE":
                            damageType = DamageType.HIGH_EXPLOSIVE;
                            break;
                        case "KINETIC":
                            damageType = DamageType.KINETIC;
                            break;
                        case "ENERGY":
                            damageType = DamageType.ENERGY;
                            break;
                        case "FRAGMENTATION":
                            damageType = DamageType.FRAGMENTATION;
                            break;
                        case "OTHER":
                            damageType = DamageType.OTHER;
                            break;
                        default:
                            continue;
                    }
                    boolean attack = row.optBoolean("attack", false);
                    boolean standoff = row.optBoolean("standoff", false);
                    boolean alpha = row.optBoolean("alpha", false);
                    boolean defense = row.optBoolean("defense", false);
                    boolean closeRange = row.optBoolean("closeRange", false);
                    boolean longRange = row.optBoolean("longRange", false);
                    boolean disable = row.optBoolean("disable", false);
                    String techType = row.optString("techType", "");
                    float leetness = (float) row.optDouble("leetness", 1f);

                    DS_WeaponEntry weaponEntry = new DS_WeaponEntry(builtIn, size, type, tier, OP, limitedAmmo, frontOnly, turretOnly, damageType, attack,
                                                                    standoff, alpha, defense, closeRange, longRange, disable, techType, leetness);

                    int count = 0;
                    float playerWeight = 0f;
                    for (int j = 0; j < row.length(); j++) {
                        String factionID = row.names().getString(j);
                        if (!invalidWeaponHeaders.contains(factionID)) {
                            count++;
                            float weight = (float) row.optDouble(factionID, 0.0);
                            if (weight > 0f) {
                                weaponEntry.factionWeights.put(factionID, weight);
                            }
                            weight *= playerWeightMap.containsKey(factionID) ? playerWeightMap.get(factionID) : 0f;
                            if (weight > 0f) {
                                playerWeight += weight;
                            }
                        }
                    }
                    if (playerWeight > 0f) {
                        playerWeight /= count;
                        weaponEntry.factionWeights.put(PLAYER_FACTION, playerWeight);
                    }

                    masterWeaponList.put(id, weaponEntry);
                }
            }
        } catch (IOException | JSONException e) {
            log.log(Level.ERROR, "Weapon CSV Loading Failed! " + e.getMessage());
        }
    }

    public static void rebuildRoleEntries(SettingsAPI settings) {
        Map<String, List<ShipVariantAPI>> junctionTable = new HashMap<>(500);
        for (String variantId : settings.getAllVariantIds()) {
            ShipVariantAPI variant = settings.getVariant(variantId);

            // It's important that fighters are skipped!
            if (variant == null || variant.isEmptyHullVariant() || variant.isFighter() || !variant.isStockVariant()) {
                continue;
            }
            String hullId = variant.getHullSpec().getHullId();
            List<ShipVariantAPI> shipVariants = junctionTable.get(hullId);
            if (shipVariants == null) {
                shipVariants = new ArrayList<>(10);
                junctionTable.put(hullId, shipVariants);
            }
            shipVariants.add(variant);
        }

        List<String> idsToAdd = new ArrayList<>(10);
        for (Entry<String, Map<String, List<DS_FactionVariant>>> e0 : factionVariants.entrySet()) {
            String faction = e0.getKey();
            try {
                for (String role : roleHeaders) {
                    settings.getEntriesForRole(faction, role);
                }
            } catch (Exception ex) {
                continue;
            }

            for (Entry<String, List<DS_FactionVariant>> e1 : e0.getValue().entrySet()) {
                String role = e1.getKey();
                List<DS_FactionVariant> variants = e1.getValue();

                List<RoleEntryAPI> entries = new ArrayList<>(settings.getEntriesForRole(faction, role));
                for (RoleEntryAPI entry : entries) {
                    settings.removeEntryForRole(faction, role, entry.getVariantId());
                }

                for (DS_FactionVariant factionVariant : variants) {
                    ShipVariantAPI variant = null;
                    try {
                        variant = settings.getVariant(factionVariant.variantId);
                    } catch (Exception ex) {
                    }

                    idsToAdd.clear();
                    if (variant == null) {
                        idsToAdd.add(factionVariant.variantId);
                    } else {
                        String hullId = factionVariant.variantId;
                        if (hullId.endsWith("_Hull")) {
                            hullId = hullId.replace("_Hull", "");
                        }
                        List<ShipVariantAPI> shipVariants = junctionTable.get(hullId);
                        if (shipVariants == null) {
                            idsToAdd.add(factionVariant.variantId);
                        } else {
                            for (ShipVariantAPI shipVariant : shipVariants) {
                                idsToAdd.add(shipVariant.getHullVariantId());
                            }
                        }
                    }
                    for (String variantId : idsToAdd) {
                        try {
                            settings.addEntryForRole(faction, role, variantId, factionVariant.weight / idsToAdd.size());
                        } catch (Exception ex) {
                        }
                    }
                }

                for (RoleEntryAPI entry : settings.getEntriesForRole(faction, role)) {
                    if (entry.isFighterWing()) {
                        entry.setQuality(variantQuality.get(entry.getVariantId()));
                    } else {
                        ShipVariantAPI variant = settings.getVariant(entry.getVariantId());
                        String shipId = variant.getHullSpec().getHullId() + "_Hull";
                        entry.setQuality(variantQuality.get(shipId));
                    }
                }
            }
        }
    }

    public static int shieldQuality(String id) {
        Integer quality = SHIELD_QUALITY.get(id);
        if (quality == null) {
            return 1;
        } else {
            return quality;
        }
    }

    public static enum SlotType {

        LARGE_UNIVERSAL(WeaponSize.LARGE, WeaponType.UNIVERSAL),
        MEDIUM_UNIVERSAL(WeaponSize.MEDIUM, WeaponType.UNIVERSAL),
        SMALL_UNIVERSAL(WeaponSize.SMALL, WeaponType.UNIVERSAL),
        LARGE_HYBRID(WeaponSize.LARGE, WeaponType.HYBRID, LARGE_UNIVERSAL),
        MEDIUM_HYBRID(WeaponSize.MEDIUM, WeaponType.HYBRID, MEDIUM_UNIVERSAL),
        SMALL_HYBRID(WeaponSize.SMALL, WeaponType.HYBRID, SMALL_UNIVERSAL),
        LARGE_SYNERGY(WeaponSize.LARGE, WeaponType.SYNERGY, LARGE_UNIVERSAL),
        MEDIUM_SYNERGY(WeaponSize.MEDIUM, WeaponType.SYNERGY, MEDIUM_UNIVERSAL),
        SMALL_SYNERGY(WeaponSize.SMALL, WeaponType.SYNERGY, SMALL_UNIVERSAL),
        LARGE_COMPOSITE(WeaponSize.LARGE, WeaponType.COMPOSITE, LARGE_UNIVERSAL),
        MEDIUM_COMPOSITE(WeaponSize.MEDIUM, WeaponType.COMPOSITE, MEDIUM_UNIVERSAL),
        SMALL_COMPOSITE(WeaponSize.SMALL, WeaponType.COMPOSITE, SMALL_UNIVERSAL),
        LARGE_BALLISTIC(WeaponSize.LARGE, WeaponType.BALLISTIC, LARGE_UNIVERSAL, LARGE_HYBRID, LARGE_COMPOSITE),
        MEDIUM_BALLISTIC(WeaponSize.MEDIUM, WeaponType.BALLISTIC, MEDIUM_UNIVERSAL, MEDIUM_HYBRID, MEDIUM_COMPOSITE),
        SMALL_BALLISTIC(WeaponSize.SMALL, WeaponType.BALLISTIC, SMALL_UNIVERSAL, SMALL_HYBRID, SMALL_COMPOSITE),
        LARGE_MISSILE(WeaponSize.LARGE, WeaponType.MISSILE, LARGE_UNIVERSAL, LARGE_SYNERGY, LARGE_COMPOSITE),
        MEDIUM_MISSILE(WeaponSize.MEDIUM, WeaponType.MISSILE, MEDIUM_UNIVERSAL, MEDIUM_SYNERGY, MEDIUM_COMPOSITE),
        SMALL_MISSILE(WeaponSize.SMALL, WeaponType.MISSILE, SMALL_UNIVERSAL, SMALL_SYNERGY, SMALL_COMPOSITE),
        LARGE_ENERGY(WeaponSize.LARGE, WeaponType.ENERGY, LARGE_UNIVERSAL, LARGE_HYBRID, LARGE_SYNERGY),
        MEDIUM_ENERGY(WeaponSize.MEDIUM, WeaponType.ENERGY, MEDIUM_UNIVERSAL, MEDIUM_HYBRID, MEDIUM_SYNERGY),
        SMALL_ENERGY(WeaponSize.SMALL, WeaponType.ENERGY, SMALL_UNIVERSAL, SMALL_HYBRID, SMALL_SYNERGY);

        final WeaponSize size;
        final WeaponType type;
        final List<SlotType> compatibleSlots;

        SlotType(WeaponSize size, WeaponType type, SlotType... compatibleSlots) {
            this.size = size;
            this.type = type;
            this.compatibleSlots = new ArrayList<>(compatibleSlots.length + 1);
            this.compatibleSlots.addAll(Arrays.asList(compatibleSlots));
            init();
        }

        private void init() {
            this.compatibleSlots.add(this);
        }

        public static SlotType getSlotType(WeaponSize size, WeaponType type) {
            for (SlotType slot : SlotType.values()) {
                if (slot.size == size && slot.type == type) {
                    return slot;
                }
            }
            return null;
        }

        public static List<SlotType> getSlotTypesForWeapon(WeaponSize size, WeaponType type) {
            for (SlotType slot : SlotType.values()) {
                if (slot.size == size && slot.type == type) {
                    return Collections.unmodifiableList(slot.compatibleSlots);
                }
            }
            return null;
        }
    }

    public static enum RandomizerWeaponType {

        ALPHA, ATTACK, DEFENSE, DISABLE, STANDOFF,
        CLOSE_RANGE, LONG_RANGE,
        //FRONT_ONLY, TURRET_ONLY
    }
}
