package data.scripts.variants;

public class DS_FactionVariant {

    public final float qualityFactorMax;
    public final float qualityFactorMin;
    public final float rarity;
    public final String techType;
    public final String variantId;
    public final float weight;

    public DS_FactionVariant(String variantId, String techType, float weight, float qualityFactorMin, float qualityFactorMax, float rarity) {
        this.variantId = variantId;
        this.weight = weight;
        this.qualityFactorMin = qualityFactorMin;
        this.qualityFactorMax = qualityFactorMax;
        this.techType = techType;
        this.rarity = rarity;
    }
}
