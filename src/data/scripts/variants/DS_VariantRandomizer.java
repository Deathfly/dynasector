package data.scripts.variants;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.ShieldAPI.ShieldType;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.loading.VariantSource;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.campaign.fleets.DS_FleetInjector;
import data.scripts.variants.DS_Database.RandomizerWeaponType;
import data.scripts.variants.DS_WeaponGrouper.WeaponGroupConfig;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import static data.scripts.variants.DS_WeaponGrouper.weaponTypeOverride;

public class DS_VariantRandomizer {

    private static final Logger log = Global.getLogger(DS_Database.class);

    public static ShipVariantAPI createVariant(FleetMemberAPI ship, String inputFaction, String otherFaction, float otherFactionWeight,
                                               MutableCharacterStatsAPI stats, Archetype archetype, float qualityFactor, float bonusOP, boolean noobFriendly) {
        String faction = inputFaction;
        if (!DS_Database.factionWeapons.containsKey(faction)) {
            faction = Factions.INDEPENDENT;
        }
        String factionAlt = otherFaction;
        if (factionAlt != null && !DS_Database.factionWeapons.containsKey(factionAlt)) {
            factionAlt = Factions.INDEPENDENT;
        }

        if (ship.isFighterWing()) {
            return ship.getVariant();
        }

        ShipVariantAPI newVariant = ship.getVariant().clone();
        newVariant.setSource(VariantSource.REFIT);
        ShipHullSpecAPI hullSpec = newVariant.getHullSpec();

        Map<String, Float> weaponData, weaponDataAlt;
        if (faction.contentEquals("player") || (faction.contentEquals("SCY") && !hullSpec.getHullId().startsWith("SCY_"))) {
            weaponData = DS_Database.factionWeapons.get(Factions.INDEPENDENT);
        } else {
            weaponData = DS_Database.factionWeapons.get(faction);
        }
        if (factionAlt != null && (factionAlt.contentEquals("player") || (factionAlt.contentEquals("SCY") && !hullSpec.getHullId().startsWith("SCY_")))) {
            weaponDataAlt = DS_Database.factionWeapons.get(Factions.INDEPENDENT);
        } else {
            weaponDataAlt = DS_Database.factionWeapons.get(factionAlt);
        }

        float attack, minAttack;
        float standoff, minStandoff;
        float alpha, minAlpha;
        float defense, minDefense;
        float closeRange, minCloseRange;
        float longRange, minLongRange;
        float disable, minDisable;
        float desiredHE, desiredK;
        boolean highTier;
        int allowedWeaponsOP;

        int originalOP = hullSpec.getOrdnancePoints(null);
        int totalOP;
        if (stats == null) {
            totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
        } else {
            totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP * (stats.getShipOrdnancePointBonus().percentMod + bonusOP) /
                              100f) * stats.getShipOrdnancePointBonus().mult);
        }

        int totalHE = 0;
        int totalK = 0;
        int totalE = 0;
        int totalAttack = 0;
        int totalStandoff = 0;
        int totalAlpha = 0;
        int totalDefense = 0;
        int totalCloseRange = 0;
        int totalLongRange = 0;
        int totalDisable = 0;

        int sizeScalar;
        if (hullSpec.getHullSize() == HullSize.CAPITAL_SHIP) {
            sizeScalar = 100;
        } else if (hullSpec.getHullSize() == HullSize.CRUISER) {
            sizeScalar = 60;
        } else if (hullSpec.getHullSize() == HullSize.DESTROYER) {
            sizeScalar = 40;
        } else {
            sizeScalar = 20;
        }

        float targetWeaponsOP = 0;
        for (WeaponSlotAPI slot : ship.getHullSpec().getAllWeaponSlotsCopy()) {
            targetWeaponsOP += getOPTargetForSlot(faction, slot.getSlotSize(), slot.getWeaponType());
        }

        if (archetype == Archetype.ARCADE) {
            attack = 1f;
            minAttack = 0f;

            standoff = 1f;
            minStandoff = 0f;

            alpha = 1f;
            minAlpha = 0f;

            defense = 1f;
            minDefense = 0f;

            closeRange = 1f;
            minCloseRange = 0f;

            longRange = 1f;
            minLongRange = 0f;

            disable = 1f;
            minDisable = 0f;

            desiredHE = 0f;
            desiredK = 0f;

            highTier = false;

            totalOP = (int) (hullSpec.getOrdnancePoints(null) * 1.3f);
            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.8f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            newVariant.setVariantDisplayName("Arcade");
        } else if (archetype == Archetype.BALANCED) {
            attack = 1f;
            minAttack = 0.25f;

            standoff = 1f;
            minStandoff = 0.25f;

            alpha = 0.75f;
            minAlpha = 0f;

            defense = 0.75f;
            minDefense = 0.1f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 0.5f;
            minLongRange = 0f;

            disable = 0.25f;
            minDisable = 0f;

            desiredHE = 0.2f;
            desiredK = 0.3f;

            highTier = false;

            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                    !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction)) {
                if (0.4f >= (1f - (float) Math.random() * (float) Math.random() * (float) Math.random())) {
                    newVariant.addMod("maximized_ordinance");
                    totalOP = hullSpec.getOrdnancePoints(stats) - newVariant.computeOPCost(stats);
                }
            }

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.75f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case Factions.KOL:
                case Factions.LUDDIC_CHURCH:
                case Factions.LUDDIC_PATH:
                    name = "Luddic";
                    break;
                case "exipirated":
                case Factions.PIRATES:
                    name = "Stolen";
                    break;
                case "templars":
                    name = "Estendre";
                    break;
                case "SCY":
                    name = "Combat";
                    break;
                case "diableavionics":
                case "cabal":
                    if (qualityFactor < 0.25f) {
                        name = "Outdated";
                    } else {
                        if (Math.random() > 0.5) {
                            name = "Multirole";
                        } else {
                            name = "Standard";
                        }
                    }
                    break;
                default:
                    if (qualityFactor < 0.25f) {
                        name = "Outdated";
                    } else {
                        if (Math.random() > 0.5) {
                            name = "Balanced";
                        } else {
                            name = "Standard";
                        }
                    }
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.STRIKE) {
            attack = 0.5f;
            minAttack = 0f;

            standoff = 0.5f;
            minStandoff = 0f;

            alpha = 3f;
            minAlpha = 0.6f;

            defense = 0.5f;
            minDefense = 0f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 0f;
            minLongRange = 0f;

            disable = 0.5f;
            minDisable = 0f;

            desiredHE = 0f;
            desiredK = 0f;

            highTier = false;

            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                    !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction)) {
                if (0.6f >= (1f - (float) Math.random() * (float) Math.random() * (float) Math.random())) {
                    newVariant.addMod("maximized_ordinance");
                    totalOP = hullSpec.getOrdnancePoints(stats) - newVariant.computeOPCost(stats);
                }
            }

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.75f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case Factions.KOL:
                case Factions.LUDDIC_CHURCH:
                case Factions.LUDDIC_PATH:
                    name = "Smiter";
                    break;
                case "exipirated":
                case "mayorate":
                case Factions.PIRATES:
                    name = "Lancer";
                    break;
                case "templars":
                    name = "Mittere";
                    break;
                case "SCY":
                    name = "Brawler";
                    break;
                case "diableavionics":
                case "cabal":
                    name = "Blitz";
                    break;
                default:
                    name = "Strike";
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.ELITE) {
            attack = 1f;
            minAttack = 0.2f;

            standoff = 0.75f;
            minStandoff = 0.1f;

            alpha = 1f;
            minAlpha = 0.2f;

            defense = 0.75f;
            minDefense = 0.1f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 0.25f;
            minLongRange = 0f;

            disable = 1f;
            minDisable = 0f;

            desiredHE = 0f;
            desiredK = 0f;

            highTier = true;

            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                    !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction)) {
                if (0.6f >= (1f - (float) Math.random() * (float) Math.random() * (float) Math.random())) {
                    newVariant.addMod("maximized_ordinance");
                    totalOP = hullSpec.getOrdnancePoints(stats) - newVariant.computeOPCost(stats);
                }
            }

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.75f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case Factions.KOL:
                case Factions.LUDDIC_CHURCH:
                case Factions.LUDDIC_PATH:
                case "cabal":
                    name = "Holy";
                    break;
                case "exipirated":
                case "mayorate":
                case Factions.PIRATES:
                case "tiandong":
                    name = "Modified";
                    break;
                case "templars":
                    name = "Eligere";
                    break;
                case "SCY":
                case "diableavionics":
                    name = "Veteran";
                    break;
                default:
                    if (qualityFactor < 0.25f) {
                        name = "Standard";
                    } else {
                        name = "Elite";
                    }
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.ASSAULT) {
            attack = 3f;
            minAttack = 0.5f;

            standoff = 0.25f;
            minStandoff = 0f;

            alpha = 0.5f;
            minAlpha = 0f;

            defense = 0.75f;
            minDefense = 0.1f;

            closeRange = 0.75f;
            minCloseRange = 0f;

            longRange = 0.25f;
            minLongRange = 0f;

            disable = 0.25f;
            minDisable = 0f;

            desiredHE = 0.35f;
            desiredK = 0.2f;

            highTier = false;

            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                    !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction)) {
                if (0.5f >= (1f - (float) Math.random() * (float) Math.random() * (float) Math.random())) {
                    newVariant.addMod("maximized_ordinance");
                    totalOP = hullSpec.getOrdnancePoints(stats) - newVariant.computeOPCost(stats);
                }
            }

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.85f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case Factions.KOL:
                case Factions.LUDDIC_CHURCH:
                case Factions.LUDDIC_PATH:
                    name = "Evangelist";
                    break;
                case "exigency":
                case "mayorate":
                case Factions.PIRATES:
                case "shadow_industry":
                case Factions.TRITACHYON:
                case "SCY":
                    name = "Attack";
                    break;
                case "citadeldefenders":
                case "cabal":
                    name = "Breaker";
                    break;
                case "templars":
                    name = "Saltare";
                    break;
                case "tiandong":
                    if (Math.random() > 0.5) {
                        name = "Assault";
                    } else {
                        name = "Brawler";
                    }
                    break;
                case "diableavionics":
                    name = "Frontline";
                    break;
                default:
                    name = "Assault";
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.SKIRMISH) {
            attack = 0.75f;
            minAttack = 0.15f;

            standoff = 0.75f;
            minStandoff = 0.15f;

            alpha = 0.5f;
            minAlpha = 0f;

            defense = 0.5f;
            minDefense = 0f;

            closeRange = 0f;
            minCloseRange = 0f;

            longRange = 1f;
            minLongRange = 0.2f;

            disable = 0.75f;
            minDisable = 0f;

            desiredHE = 0.1f;
            desiredK = 0.3f;

            highTier = false;

            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                    !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction)) {
                if (0.5f >= (1f - (float) Math.random() * (float) Math.random() * (float) Math.random())) {
                    newVariant.addMod("maximized_ordinance");
                    totalOP = hullSpec.getOrdnancePoints(stats) - newVariant.computeOPCost(stats);
                }
            }

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.65f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case "blackrock_driveyards":
                case Factions.INDEPENDENT:
                case Factions.TRITACHYON:
                    name = "Fast Attack";
                    break;
                case "exigency":
                case "exipirated":
                case "mayorate":
                case Factions.PIRATES:
                    name = "Raider";
                    break;
                case Factions.HEGEMONY:
                case "interstellarimperium":
                case Factions.DIKTAT:
                    name = "Patrol";
                    break;
                case "citadeldefenders":
                    name = "Mobile";
                    break;
                case "templars":
                    name = "Agilis";
                    break;
                case "SCY":
                    name = "Recon";
                    break;
                case "diableavionics":
                case "cabal":
                    name = "Harasser";
                    break;
                default:
                    name = "Skirmish";
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.ARTILLERY) {
            attack = 0.25f;
            minAttack = 0f;

            standoff = 0.5f;
            minStandoff = 0f;

            alpha = 0.25f;
            minAlpha = 0f;

            defense = 0.75f;
            minDefense = 0f;

            closeRange = 0f;
            minCloseRange = 0f;

            longRange = 3f;
            minLongRange = 0.6f;

            disable = 0.25f;
            minDisable = 0f;

            desiredHE = 0f;
            desiredK = 0.25f;

            highTier = false;

            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                    !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction)) {
                if (0.4f >= (1f - (float) Math.random() * (float) Math.random() * (float) Math.random())) {
                    newVariant.addMod("maximized_ordinance");
                    totalOP = hullSpec.getOrdnancePoints(stats) - newVariant.computeOPCost(stats);
                }
            }

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.8f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case "citadeldefenders":
                case "interstellarimperium":
                case Factions.KOL:
                case Factions.LUDDIC_CHURCH:
                case Factions.LUDDIC_PATH:
                    name = "Siege";
                    break;
                case "exipirated":
                case "mayorate":
                case Factions.PIRATES:
                case "cabal":
                    name = "Bombardment";
                    break;
                case Factions.HEGEMONY:
                case Factions.INDEPENDENT:
                case "shadow_industry":
                case Factions.TRITACHYON:
                case "tiandong":
                    if (Math.random() > 0.5) {
                        name = "Standoff";
                    } else {
                        name = "Fire Support";
                    }
                    break;
                case "templars":
                    name = "Atirier";
                    break;
                default:
                    name = "Fire Support";
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.CLOSE_SUPPORT) {
            attack = 1f;
            minAttack = 0.1f;

            standoff = 3f;
            minStandoff = 0.6f;

            alpha = 0.25f;
            minAlpha = 0f;

            defense = 1f;
            minDefense = 0.1f;

            closeRange = 0f;
            minCloseRange = 0f;

            longRange = 0.75f;
            minLongRange = 0f;

            disable = 0.75f;
            minDisable = 0f;

            desiredHE = 0f;
            desiredK = 0.5f;

            highTier = false;

            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                    !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction)) {
                if (0.4f >= (1f - (float) Math.random() * (float) Math.random() * (float) Math.random())) {
                    newVariant.addMod("maximized_ordinance");
                    totalOP = hullSpec.getOrdnancePoints(stats) - newVariant.computeOPCost(stats);
                }
            }

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.75f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case "templars":
                    name = "Fusiller";
                    break;
                default:
                    name = "Close Support";
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.SUPPORT) {
            attack = 0.25f;
            minAttack = 0f;

            standoff = 1f;
            minStandoff = 0.1f;

            alpha = 0.25f;
            minAlpha = 0f;

            defense = 1f;
            minDefense = 0.1f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 1f;
            minLongRange = 0.1f;

            disable = 2f;
            minDisable = 0.3f;

            desiredHE = 0f;
            desiredK = 0.2f;

            highTier = false;

            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                    !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction)) {
                if (0.3f >= (1f - (float) Math.random() * (float) Math.random() * (float) Math.random())) {
                    newVariant.addMod("maximized_ordinance");
                    totalOP = hullSpec.getOrdnancePoints(stats) - newVariant.computeOPCost(stats);
                }
            }

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.75f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case "exipirated":
                case Factions.PIRATES:
                case "cabal":
                    name = "Retrofitted";
                    break;
                case "templars":
                    name = "Portare";
                    break;
                case "diableavionics":
                    name = "Relief";
                    break;
                default:
                    name = "Support";
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.ESCORT) {
            attack = 0.5f;
            minAttack = 0f;

            standoff = 1f;
            minStandoff = 0.1f;

            alpha = 0f;
            minAlpha = 0f;

            defense = 3f;
            minDefense = 0.6f;

            closeRange = 0.5f;
            minCloseRange = 0f;

            longRange = 0f;
            minLongRange = 0f;

            disable = 1f;
            minDisable = 0.1f;

            desiredHE = 0f;
            desiredK = 0f;

            highTier = false;

            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                    !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction)) {
                if (0.3f >= (1f - (float) Math.random() * (float) Math.random() * (float) Math.random())) {
                    newVariant.addMod("maximized_ordinance");
                    totalOP = hullSpec.getOrdnancePoints(stats) - newVariant.computeOPCost(stats);
                }
            }

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.65f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case Factions.KOL:
                case Factions.LUDDIC_CHURCH:
                case Factions.LUDDIC_PATH:
                case "cabal":
                    name = "Protector";
                    break;
                case "exipirated":
                case Factions.PIRATES:
                    name = "Watchdog";
                    break;
                case "templars":
                    name = "Defensa";
                    break;
                case "citadeldefenders":
                    name = "Guardian";
                    break;
                case "SCY":
                    name = "Guard";
                    break;
                case "diableavionics":
                    name = "Reinforcement";
                    break;
                default:
                    name = "Escort";
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.FLEET) {
            attack = 0.25f;
            minAttack = 0f;

            standoff = 0.5f;
            minStandoff = 0f;

            alpha = 0.25f;
            minAlpha = 0f;

            defense = 2f;
            minDefense = 0.25f;

            closeRange = 0f;
            minCloseRange = 0f;

            longRange = 1f;
            minLongRange = 0f;

            disable = 1f;
            minDisable = 0f;

            desiredHE = 0f;
            desiredK = 0f;

            highTier = false;

            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.2f + 0.3f) + (totalOP - sizeScalar)) / 2f);

            String name;
            switch (faction) {
                case "templars":
                    name = "Loger";
                    break;
                case "blackrock_driveyards":
                case "citadeldefenders":
                case "exigency":
                case "shadow_industry":
                case Factions.TRITACHYON:
                    name = "Corporate";
                    break;
                case "SCY":
                    name = "Support";
                    break;
                case "diableavionics":
                case "cabal":
                    name = "Reserve";
                    break;
                default:
                    name = "Fleet";
            }
            newVariant.setVariantDisplayName(name);
        } else if (archetype == Archetype.ULTIMATE) {
            attack = 1f;
            minAttack = 0.2f;

            standoff = 1f;
            minStandoff = 0.2f;

            alpha = 1f;
            minAlpha = 0.2f;

            defense = 0.75f;
            minDefense = 0f;

            closeRange = 0.25f;
            minCloseRange = 0f;

            longRange = 0.25f;
            minLongRange = 0f;

            disable = 0.5f;
            minDisable = 0f;

            desiredHE = 0.2f;
            desiredK = 0.2f;

            highTier = true;

            if (stats == null) {
                totalOP = (int) (originalOP + originalOP * (bonusOP + 30f) / 100f);
            } else {
                totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP * (stats.getShipOrdnancePointBonus().percentMod +
                                                                                                           bonusOP + 30f) / 100f) *
                                 stats.getShipOrdnancePointBonus().mult);
            }
            allowedWeaponsOP = (int) ((totalOP * ((float) Math.random() * 0.1f + 0.75f) + Math.max(totalOP - sizeScalar, totalOP / 2)) / 2f);
            allowedWeaponsOP = (int) Math.max(allowedWeaponsOP, (allowedWeaponsOP + targetWeaponsOP) / 2f);

            String name;
            switch (faction) {
                case "blackrock_driveyards":
                case "cabal":
                    name = "Prototype";
                    break;
                case Factions.INDEPENDENT:
                case "tiandong":
                    name = "Master";
                    break;
                case "interstellarimperium":
                    if (qualityFactor < 0.5f) {
                        name = "Imperial";
                    } else {
                        name = "Maximum";
                    }
                    break;
                case Factions.KOL:
                case Factions.LUDDIC_CHURCH:
                case Factions.LUDDIC_PATH:
                    name = "Exalted";
                    break;
                case "exipirated":
                case "mayorate":
                case Factions.PIRATES:
                    name = "Heavily Modified";
                    break;
                case "citadeldefenders":
                case "exigency":
                case Factions.TRITACHYON:
                    name = "Super";
                    break;
                case "shadow_industry":
                    name = "Shadow";
                    break;
                case "templars":
                    name = "Magister";
                    break;
                case "SCY":
                    name = "Army";
                    break;
                case "diableavionics":
                    name = "Experimental";
                    break;
                default:
                    if (qualityFactor < 0.25f) {
                        name = "Elite";
                    } else {
                        name = "Ultimate";
                    }
            }
            newVariant.setVariantDisplayName(name);
        } else {
            return newVariant;
        }

        if (faction.contentEquals(Factions.LUDDIC_PATH)) {
            allowedWeaponsOP *= 0.75f;
            targetWeaponsOP *= 0.75f;
        }

        if (faction.contentEquals("diableavionics")) {
            allowedWeaponsOP += Math.round((totalOP - allowedWeaponsOP) * 0.33f);
            targetWeaponsOP += (totalOP - targetWeaponsOP) * 0.33f;
        }

        switch (hullSpec.getBaseHullId()) {
            case "SCY_orthrus":
                allowedWeaponsOP += Math.round((totalOP - allowedWeaponsOP) * 0.33f);
                targetWeaponsOP += (totalOP - targetWeaponsOP) * 0.33f;
                break;
            default:
                break;
        }

        if (allowedWeaponsOP <= 0) {
            allowedWeaponsOP = 1;
        }

        if (DSModPlugin.SHOW_DEBUG_INFO) {
            log.info("Creating " + hullSpec.getHullName() + " " + archetype.toString() + " variant" +
                    " (Total OP: " + totalOP + ")" +
                    " (Allowed Weapons OP: " + allowedWeaponsOP + ")" +
                    " (Target Weapons OP: " + targetWeaponsOP + ")"
            );
        }

        WeaponPreference preference;
        switch (hullSpec.getBaseHullId()) {
            case "sunder":
            case "aurora":
            case "odyssey":
            case "ssp_sunder_u":
            case "ssp_excelsior":
            case "ssp_tiger":
            case "ssp_nautilus":
            case "ssp_hecate":
            case "ssp_boss_sunder":
            case "ssp_boss_aurora":
            case "ssp_boss_odyssey":
            case "ssp_boss_paragon":
            case "ssp_boss_tempest":
            case "ssp_boss_afflictor":
            case "ii_interrex":
            case "ii_barrus":
            case "fox_dfrigate":
            case "ms_inanna":
            case "ilk_lilith":
            case "junk_pirates_langoustine":
            case "junk_pirates_kraken":
                preference = WeaponPreference.ENERGY;
                break;
            case "lasher":
            case "brawler":
            case "hammerhead":
            case "ssp_vanguard":
            case "ssp_scythe":
            case "ssp_boss_hammerhead":
            case "ssp_boss_brawler":
            case "ssp_boss_dominator":
            case "ii_invictus":
            case "ii_praetorian":
            case "ii_boss_praetorian":
            case "ii_dictator":
            case "Fox_Frigate":
            case "pack_komondor":
            case "junk_pirates_dugong":
            case "junk_pirates_boss_dugong":
            case "SCY_alecto":
            case "SCY_lamia":
            case "SCY_lamiaArmored":
            case "tiandong_xiakou":
            case "tiandong_nanzhong":
            case "tiandong_lao_hu":
            case "tiandong_boss_wuzhang":
                preference = WeaponPreference.BALLISTIC;
                break;
            case "vigilance":
            case "condor":
            case "venture":
            case "doom":
            case "gryphon":
            case "ssp_archer":
            case "ssp_vindicator":
            case "ssp_dragon":
            case "ssp_mongrel":
            case "ssp_boss_doom":
            case "ii_lynx":
            case "exigency_compactfrigate":
            case "ilk_del_azarchel":
            case "junk_pirates_hammer":
                preference = WeaponPreference.MISSILE;
                break;
            case "hyperion":
            case "ssp_boss_hyperion":
            case "brdy_stenos":
            case "SCY_megaera":
            case "SCY_tisiphone":
            case "SCY_orthrus":
            case "diableavionics_haze":
                preference = WeaponPreference.STRIKE;
                break;
            case "diableavionics_versant":
                preference = WeaponPreference.PD;
                break;
            default:
                preference = null;
                break;
        }

        boolean asymmetric;
        switch (hullSpec.getBaseHullId()) {
            case "brawler":
            case "hyperion":
            case "kite":
            case "centurion":
            case "harbinger":
            case "wayfarer":
            case "scarab":
            case "ssp_venom":
            case "ssp_venomx":
            case "ssp_boss_hyperion":
            case "ssp_boss_shade":
            case "ii_decurion":
            case "tiandong_luo_yang":
            case "tiandong_hanzhong":
            case "tiandong_hujing":
            case "diableavionics_vapor":
                asymmetric = true;
                break;
            default:
                asymmetric = false;
                break;
        }

        boolean broadside;
        switch (hullSpec.getBaseHullId()) {
            case "junk_pirates_dugong":
            case "junk_pirates_boss_dugong":
            case "conquest":
            case "ssp_boss_conquest":
            case "ssp_torch":
            case "ms_inanna":
            case "ms_elysium":
            case "syndicate_asp_gigantophis":
            case "junk_pirates_kraken":
            case "tiandong_xu":
                broadside = true;
                break;
            default:
                broadside = false;
                break;
        }

        if (faction.contentEquals("SCY")) {
            minDefense = (minDefense < 0.5f) ? (minDefense + 0.5f) / 2f : minDefense;
        }

        Map<Float, String> slotsMapSB = new HashMap<>(20);
        Map<Float, String> slotsMapMB = new HashMap<>(10);
        Map<Float, String> slotsMapLB = new HashMap<>(5);
        Map<Float, String> slotsMapSM = new HashMap<>(20);
        Map<Float, String> slotsMapMM = new HashMap<>(10);
        Map<Float, String> slotsMapLM = new HashMap<>(5);
        Map<Float, String> slotsMapSE = new HashMap<>(20);
        Map<Float, String> slotsMapME = new HashMap<>(10);
        Map<Float, String> slotsMapLE = new HashMap<>(5);
        Map<Float, String> slotsMapSU = new HashMap<>(20);
        Map<Float, String> slotsMapMU = new HashMap<>(10);
        Map<Float, String> slotsMapLU = new HashMap<>(5);
        Map<Float, String> slotsMapSH = new HashMap<>(20);
        Map<Float, String> slotsMapMH = new HashMap<>(10);
        Map<Float, String> slotsMapLH = new HashMap<>(5);
        Map<Float, String> slotsMapSS = new HashMap<>(20);
        Map<Float, String> slotsMapMS = new HashMap<>(10);
        Map<Float, String> slotsMapLS = new HashMap<>(5);
        Map<Float, String> slotsMapSC = new HashMap<>(20);
        Map<Float, String> slotsMapMC = new HashMap<>(10);
        Map<Float, String> slotsMapLC = new HashMap<>(5);

        List<WeaponSlotAPI> slots = ship.getHullSpec().getAllWeaponSlotsCopy();
        for (WeaponSlotAPI slot : slots) {
            if (slot.isDecorative() || slot.isSystemSlot() || slot.getWeaponType().getDisplayName().contentEquals("Launch Bay")) {
                continue;
            }

            String slotId = slot.getId();

            if (slot.isBuiltIn()) {
                if (newVariant.getWeaponId(slotId) == null) {
                    continue;
                }

                DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(newVariant.getWeaponId(slotId));

                if (weapon == null) {
                    continue;
                }

                totalHE += weapon.damageType == DamageType.HIGH_EXPLOSIVE ? weapon.OP : 0;
                totalK += weapon.damageType == DamageType.KINETIC ? weapon.OP : 0;
                totalE += weapon.damageType == DamageType.ENERGY ? weapon.OP : 0;
                totalAttack += weapon.attack ? weapon.OP : 0;
                totalStandoff += weapon.standoff ? weapon.OP : 0;
                totalAlpha += weapon.alpha ? weapon.OP : 0;
                totalDefense += weapon.defense ? weapon.OP : 0;
                totalCloseRange += weapon.closeRange ? weapon.OP : 0;
                totalLongRange += weapon.longRange ? weapon.OP : 0;
                totalDisable += weapon.disable ? weapon.OP : 0;

                continue;
            }

            float normalizedAngle = 0;
            if (slot.getAngle() > 180) {
                normalizedAngle = 360 - slot.getAngle();
            } else if (slot.getAngle() > 0) {
                normalizedAngle = slot.getAngle();
            } else if (slot.getAngle() < 0) {
                normalizedAngle = -slot.getAngle();
            }
            float uniqueKey = normalizedAngle * 360f + slot.getArc();

            int currentWeaponsOP = newVariant.computeOPCost(stats);

            SlotType slotType;
            boolean slotFront = Math.abs(slot.getAngle()) - slot.getArc() / 2f <= 0f || slot.getArc() >= 315f;
            boolean slotTurret = slot.getArc() >= 45f;
            if (slot.getArc() <= 120f && slot.getArc() >= 45f && Math.abs(normalizedAngle) <= slot.getArc() / 2f - 22.5f) {
                slotType = SlotType.FRONT;
            } else if (slot.getArc() <= 225f && slot.getArc() >= 45f && Math.abs(normalizedAngle) >= 135f) {
                slotType = SlotType.BACK;
            } else if (slot.getArc() <= 120f && slot.getArc() >= 45f) {
                slotType = SlotType.SIDE;
            } else if (slot.getArc() <= 225 && slot.getArc() >= 120f) {
                slotType = SlotType.HEMI;
            } else if (slot.getArc() >= 225f) {
                slotType = SlotType.WIDE;
            } else if (normalizedAngle >= 22.5f) {
                slotType = SlotType.CROOKED;
            } else {
                slotType = SlotType.BORESIGHT;
            }

            float modAttack = attack;
            if (totalAttack < (int) (allowedWeaponsOP * minAttack)) {
                modAttack *= allowedWeaponsOP / (float) (allowedWeaponsOP + 10 + totalAttack - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minAttack)) {
                    modAttack /= Math.max(1f - minAttack, 0.1f);
                }
            }
            float modStandoff = standoff;
            if (totalStandoff < (int) (allowedWeaponsOP * minStandoff)) {
                modStandoff *= allowedWeaponsOP / (float) (allowedWeaponsOP + 10 + totalStandoff - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minStandoff)) {
                    modStandoff /= Math.max(1f - minStandoff, 0.1f);
                }
            }
            float modAlpha = alpha;
            if (totalAlpha < (int) (allowedWeaponsOP * minAlpha)) {
                modAlpha *= allowedWeaponsOP / (float) (allowedWeaponsOP + 2 + totalAlpha - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minAlpha)) {
                    modAlpha /= Math.max(1f - minAlpha, 0.1f);
                }
            }
            float modDefense = defense;
            if (totalDefense < (int) (allowedWeaponsOP * minDefense)) {
                modDefense *= allowedWeaponsOP / (float) (allowedWeaponsOP + 4 + totalDefense - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minDefense)) {
                    modDefense /= Math.max(1f - minDefense, 0.1f);
                }
            }
            float modCloseRange = closeRange;
            if (totalCloseRange < (int) (allowedWeaponsOP * minCloseRange)) {
                modCloseRange *= allowedWeaponsOP / (float) (allowedWeaponsOP + 4 + totalCloseRange - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minCloseRange)) {
                    modCloseRange /= Math.max(1f - minCloseRange, 0.1f);
                }
            }
            float modLongRange = longRange;
            if (totalLongRange < (int) (allowedWeaponsOP * minLongRange)) {
                modLongRange *= allowedWeaponsOP / (float) (allowedWeaponsOP + 6 + totalLongRange - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minLongRange)) {
                    modLongRange /= Math.max(1f - minLongRange, 0.1f);
                }
            }
            float modDisable = disable;
            if (totalDisable < (int) (allowedWeaponsOP * minDisable)) {
                modDisable *= allowedWeaponsOP / (float) (allowedWeaponsOP + 8 + totalDisable - currentWeaponsOP);
                if (currentWeaponsOP < (int) (allowedWeaponsOP * minDisable)) {
                    modDisable /= Math.max(1f - minDisable, 0.1f);
                }
            }

            DamageType preferredDamageType = null;
            float modDesiredHE = desiredHE;
            if (totalHE + totalE / 2 < (int) (allowedWeaponsOP * desiredHE)) {
                modDesiredHE *= allowedWeaponsOP / (float) (allowedWeaponsOP + 10 + totalHE + totalE / 2 - currentWeaponsOP);
            }
            float modDesiredK = desiredK;
            if (totalK + totalE / 2 < (int) (allowedWeaponsOP * desiredK)) {
                modDesiredK *= allowedWeaponsOP / (float) (allowedWeaponsOP + 10 + totalK + totalE / 2 - currentWeaponsOP);
            }
            if (modDesiredHE > modDesiredK && modDesiredHE >= 0.4f) {
                preferredDamageType = DamageType.HIGH_EXPLOSIVE;
            } else if (modDesiredK > modDesiredHE && modDesiredK >= 0.4f) {
                preferredDamageType = DamageType.KINETIC;
            }

            Map<Float, String> slotsMap = null;
            switch (slot.getSlotSize()) {
                case SMALL:
                    switch (slot.getWeaponType()) {
                        case BALLISTIC:
                            slotsMap = slotsMapSB;
                            break;
                        case ENERGY:
                            slotsMap = slotsMapSE;
                            break;
                        case MISSILE:
                            slotsMap = slotsMapSM;
                            break;
                        case UNIVERSAL:
                            slotsMap = slotsMapSU;
                            break;
                        case HYBRID:
                            slotsMap = slotsMapSH;
                            break;
                        case SYNERGY:
                            slotsMap = slotsMapSS;
                            break;
                        case COMPOSITE:
                            slotsMap = slotsMapSC;
                            break;
                        default:
                            break;
                    }
                    break;
                case MEDIUM:
                    switch (slot.getWeaponType()) {
                        case BALLISTIC:
                            slotsMap = slotsMapMB;
                            break;
                        case ENERGY:
                            slotsMap = slotsMapME;
                            break;
                        case MISSILE:
                            slotsMap = slotsMapMM;
                            break;
                        case UNIVERSAL:
                            slotsMap = slotsMapMU;
                            break;
                        case HYBRID:
                            slotsMap = slotsMapMH;
                            break;
                        case SYNERGY:
                            slotsMap = slotsMapMS;
                            break;
                        case COMPOSITE:
                            slotsMap = slotsMapMC;
                            break;
                        default:
                            break;
                    }
                    break;
                case LARGE:
                    switch (slot.getWeaponType()) {
                        case BALLISTIC:
                            slotsMap = slotsMapLB;
                            break;
                        case ENERGY:
                            slotsMap = slotsMapLE;
                            break;
                        case MISSILE:
                            slotsMap = slotsMapLM;
                            break;
                        case UNIVERSAL:
                            slotsMap = slotsMapLU;
                            break;
                        case HYBRID:
                            slotsMap = slotsMapLH;
                            break;
                        case SYNERGY:
                            slotsMap = slotsMapLS;
                            break;
                        case COMPOSITE:
                            slotsMap = slotsMapLC;
                            break;
                        default:
                            break;
                    }
                    break;
            }

            if (slotsMap == null) {
                continue;
            }

            // Can't really go higher than 2x price (~10/20/40 OP)
            float weaponsBudgetFactor = Math.min(Math.max((allowedWeaponsOP - currentWeaponsOP) / Math.max(targetWeaponsOP, 1f), 0.25f), 2f);

            String weaponId = null;
            if (slotsMap.containsKey(uniqueKey)) {
                weaponId = slotsMap.get(uniqueKey);
            }

            Map<String, EnumMap<RandomizerWeaponType, Float>> shipSlotDef = DS_Database.shipSlotCoefficients.get(hullSpec.getHullId());
            EnumMap<RandomizerWeaponType, Float> coefficients;
            if (shipSlotDef == null) {
                coefficients = null;
            } else {
                coefficients = shipSlotDef.get(slotId);
            }

            weaponId = chooseWeapon(faction, coefficients, weaponData, weaponDataAlt, otherFactionWeight, weaponId, slotType, slotFront, slotTurret, preference,
                                    slot.getSlotSize(), slot.getWeaponType(), preferredDamageType, (highTier ? ((1f + qualityFactor) / 2f) : qualityFactor),
                                    (allowedWeaponsOP - currentWeaponsOP), (totalOP - currentWeaponsOP), modAttack, modStandoff, modAlpha, modDefense,
                                    modCloseRange, modLongRange, modDisable, weaponsBudgetFactor, asymmetric, broadside, noobFriendly);

            if (weaponId != null) {
                DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(weaponId);

                totalHE += weapon.damageType == DamageType.HIGH_EXPLOSIVE ? weapon.OP : 0;
                totalK += weapon.damageType == DamageType.KINETIC ? weapon.OP : 0;
                totalE += weapon.damageType == DamageType.ENERGY ? weapon.OP : 0;
                totalAttack += weapon.attack ? weapon.OP : 0;
                totalStandoff += weapon.standoff ? weapon.OP : 0;
                totalAlpha += weapon.alpha ? weapon.OP : 0;
                totalDefense += weapon.defense ? weapon.OP : 0;
                totalCloseRange += weapon.closeRange ? weapon.OP : 0;
                totalLongRange += weapon.longRange ? weapon.OP : 0;
                totalDisable += weapon.disable ? weapon.OP : 0;

                slotsMap.put(uniqueKey, weaponId);

                newVariant.addWeapon(slotId, weaponId);
            }

            targetWeaponsOP -= getOPTargetForSlot(faction, slot.getSlotSize(), slot.getWeaponType());
        }

        newVariant = doUpgrades(ship, newVariant, faction, stats, archetype, false, qualityFactor, bonusOP);

        return newVariant;
    }

    public static ShipVariantAPI doUpgrades(FleetMemberAPI ship, ShipVariantAPI variant, String inputFaction, MutableCharacterStatsAPI stats,
                                            Archetype archetype, boolean onlyUpgrade, float qualityFactor, float bonusOP) {
        String faction = inputFaction;
        if (!DS_Database.factionWeapons.containsKey(faction)) {
            faction = Factions.INDEPENDENT;
        }

        if (ship.isFighterWing()) {
            return variant;
        }

        int beamMounts = 0;
        int missMounts = 0;
        int ituMounts = 0;
        int fcsMounts = 0;
        int trackingMounts = 0;
        int ipdaiMounts = 0; // Does not include PD or Strike
        int limitedAmmoMounts = 0;
        int limitedAmmoMissMounts = 0;
        int turretMounts = 0;
        ShipVariantAPI newVariant = variant.clone();
        newVariant.setSource(VariantSource.REFIT);

        for (WeaponSlotAPI slot : ship.getHullSpec().getAllWeaponSlotsCopy()) {
            String weaponId = newVariant.getWeaponId(slot.getId());
            DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(weaponId);
            if (weapon == null) {
                continue;
            }

            if (slot.getArc() >= 45f) {
                turretMounts += 1;
            }

            EnumSet<AIHints> hints = Global.getSettings().getWeaponSpec(weaponId).getAIHints();
            if (slot.getArc() >= 45f && (weapon.type == WeaponType.BALLISTIC || weapon.type == WeaponType.ENERGY) && !weapon.alpha && !weaponTypeOverride.get(
                    "Sustained Beam").contains(weaponId) && !weaponTypeOverride.get("Burst Beam").contains(weaponId)) {
                if (weapon.size == WeaponSize.SMALL) {
                    trackingMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    trackingMounts += 2;
                } else {
                    trackingMounts += 4;
                }
            }
            if (slot.getArc() >= 45f && (weapon.type == WeaponType.BALLISTIC || weapon.type == WeaponType.ENERGY) && !hints.contains(AIHints.PD) &&
                    !hints.contains(AIHints.PD_ONLY) && !hints.contains(AIHints.STRIKE) && weapon.size == WeaponSize.SMALL) {
                ipdaiMounts += 1;
            }
            if (weaponTypeOverride.get("Sustained Beam").contains(weaponId) || weaponTypeOverride.get("Burst Beam").contains(weaponId)) {
                if (weapon.size == WeaponSize.SMALL) {
                    beamMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    beamMounts += 2;
                } else {
                    beamMounts += 4;
                }
            }
            if (weapon.type == WeaponType.MISSILE) {
                if (weapon.size == WeaponSize.SMALL) {
                    missMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    missMounts += 2;
                } else {
                    missMounts += 4;
                }
            }
            if (weapon.limitedAmmo && weapon.type == WeaponType.MISSILE && Global.getSettings().getWeaponSpec(weaponId).getMaxAmmo() > 1) {
                if (weapon.size == WeaponSize.SMALL) {
                    limitedAmmoMissMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    limitedAmmoMissMounts += 2;
                } else {
                    limitedAmmoMissMounts += 4;
                }
            }
            if (weapon.limitedAmmo && weapon.type != WeaponType.MISSILE) {
                if (weapon.size == WeaponSize.SMALL) {
                    limitedAmmoMounts += 1;
                } else if (weapon.size == WeaponSize.MEDIUM) {
                    limitedAmmoMounts += 2;
                } else {
                    limitedAmmoMounts += 4;
                }
            }
            if (weapon.type == WeaponType.BALLISTIC || weapon.type == WeaponType.ENERGY) {
                ituMounts += 1;
            }
            if ((weapon.type == WeaponType.BALLISTIC || weapon.type == WeaponType.ENERGY) &&
                    !weaponTypeOverride.get("Sustained Beam").contains(weaponId) && !weaponTypeOverride.get("Burst Beam").contains(weaponId)) {
                fcsMounts += 1;
            }
        }

        ShipHullSpecAPI hullSpec = newVariant.getHullSpec();
        String hullId = hullSpec.getBaseHullId();

        int originalOP = hullSpec.getOrdnancePoints(null);
        int totalOP;
        if (stats == null) {
            totalOP = (int) (originalOP + originalOP * bonusOP / 100f);
        } else {
            totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP * (stats.getShipOrdnancePointBonus().percentMod + bonusOP) /
                              100f) * stats.getShipOrdnancePointBonus().mult);
        }

        float logisticsFactor;
        float eliteFactor;
        float shieldFactor;
        float armorFactor;
        float defenseFactor;
        float fluxFactor;
        float enginesFactor;
        float weaponsFactor;
        float rangeFactor;
        float capacitorFactor;
        float ventFactor;
        float sizeFactor;

        switch (hullSpec.getHullSize()) {
            default:
            case FRIGATE:
                sizeFactor = 0.5f;
                break;
            case DESTROYER:
                sizeFactor = 1f;
                break;
            case CRUISER:
                sizeFactor = 1.5f;
                break;
            case CAPITAL_SHIP:
                sizeFactor = 2.5f;
                break;
        }

        if (archetype == Archetype.ARCADE) {
            logisticsFactor = 0f;
            eliteFactor = 1f;
            shieldFactor = 1f;
            armorFactor = 1f;
            defenseFactor = 1f;
            fluxFactor = 1f;
            enginesFactor = 1f;
            weaponsFactor = 1f;
            rangeFactor = 1f;
            capacitorFactor = 1f;
            ventFactor = 1f;
        } else if (archetype == Archetype.BALANCED) {
            logisticsFactor = 1f;
            eliteFactor = 1f;
            shieldFactor = 1f;
            armorFactor = 1f;
            defenseFactor = 1f;
            fluxFactor = 1f;
            enginesFactor = 1f;
            weaponsFactor = 1f;
            rangeFactor = 1f;
            capacitorFactor = 1f;
            ventFactor = 1f;
        } else if (archetype == Archetype.STRIKE) {
            logisticsFactor = 0.25f;
            eliteFactor = 1f;
            shieldFactor = 1f;
            armorFactor = 1f;
            defenseFactor = 0.5f;
            fluxFactor = 1.5f;
            enginesFactor = 1.5f;
            weaponsFactor = 1.5f;
            rangeFactor = 1f;
            capacitorFactor = 1.5f;
            ventFactor = 1f;
        } else if (archetype == Archetype.ELITE) {
            logisticsFactor = 0f;
            eliteFactor = 2f;
            shieldFactor = 1f;
            armorFactor = 1f;
            defenseFactor = 1f;
            fluxFactor = 1f;
            enginesFactor = 1f;
            weaponsFactor = 1f;
            rangeFactor = 1f;
            capacitorFactor = 1f;
            ventFactor = 1f;
        } else if (archetype == Archetype.ASSAULT) {
            logisticsFactor = 0.5f;
            eliteFactor = 0.5f;
            shieldFactor = 0.5f;
            armorFactor = 1.5f;
            defenseFactor = 0.5f;
            fluxFactor = 1f;
            enginesFactor = 0.5f;
            weaponsFactor = 2f;
            rangeFactor = 1.25f;
            capacitorFactor = 0.75f;
            ventFactor = 1.5f;
        } else if (archetype == Archetype.SKIRMISH) {
            logisticsFactor = 0.5f;
            eliteFactor = 0.75f;
            shieldFactor = 1f;
            armorFactor = 0.5f;
            defenseFactor = 1f;
            fluxFactor = 0.5f;
            enginesFactor = 3f;
            weaponsFactor = 0.5f;
            rangeFactor = 1f;
            capacitorFactor = 0.75f;
            ventFactor = 0.75f;
        } else if (archetype == Archetype.ARTILLERY) {
            logisticsFactor = 1f;
            eliteFactor = 0.5f;
            shieldFactor = 1f;
            armorFactor = 1f;
            defenseFactor = 1f;
            fluxFactor = 0.5f;
            enginesFactor = 0.25f;
            weaponsFactor = 1.5f;
            rangeFactor = 5f;
            capacitorFactor = 0.75f;
            ventFactor = 0.5f;
        } else if (archetype == Archetype.CLOSE_SUPPORT) {
            logisticsFactor = 1f;
            eliteFactor = 0.5f;
            shieldFactor = 1f;
            armorFactor = 1f;
            defenseFactor = 1f;
            fluxFactor = 1f;
            enginesFactor = 0.5f;
            weaponsFactor = 1f;
            rangeFactor = 1.5f;
            capacitorFactor = 1.25f;
            ventFactor = 0.75f;
        } else if (archetype == Archetype.SUPPORT) {
            logisticsFactor = 0.75f;
            eliteFactor = 0.25f;
            shieldFactor = 1f;
            armorFactor = 0.5f;
            defenseFactor = 1f;
            fluxFactor = 0.5f;
            enginesFactor = 1f;
            weaponsFactor = 0.5f;
            rangeFactor = 1f;
            capacitorFactor = 0.75f;
            ventFactor = 0.75f;
        } else if (archetype == Archetype.ESCORT) {
            logisticsFactor = 1f;
            eliteFactor = 0.25f;
            shieldFactor = 1.5f;
            armorFactor = 1f;
            defenseFactor = 3f;
            fluxFactor = 0.75f;
            enginesFactor = 1.5f;
            weaponsFactor = 1f;
            rangeFactor = 2f;
            capacitorFactor = 1.25f;
            ventFactor = 0.5f;
        } else if (archetype == Archetype.FLEET) {
            logisticsFactor = 5f;
            eliteFactor = 0f;
            shieldFactor = 1f;
            armorFactor = 1f;
            defenseFactor = 0.5f;
            fluxFactor = 0.25f;
            enginesFactor = 2f;
            weaponsFactor = 0.5f;
            rangeFactor = 0.5f;
            capacitorFactor = 0.25f;
            ventFactor = 0.25f;
        } else if (archetype == Archetype.ULTIMATE) {
            if (stats == null) {
                totalOP = (int) (originalOP + originalOP * (bonusOP + 30f) / 100f);
            } else {
                totalOP = (int) ((stats.getShipOrdnancePointBonus().flatBonus + originalOP + originalOP * (stats.getShipOrdnancePointBonus().percentMod +
                                                                                                           bonusOP + 30f) / 100f) *
                                 stats.getShipOrdnancePointBonus().mult);
            }
            logisticsFactor = 0f;
            eliteFactor = 2f;
            shieldFactor = 1f;
            armorFactor = 1f;
            defenseFactor = 1f;
            fluxFactor = 1f;
            enginesFactor = 1f;
            weaponsFactor = 1f;
            rangeFactor = 1f;
            capacitorFactor = 1f;
            ventFactor = 1f;
        } else {
            return newVariant;
        }
        float missileFactor = weaponsFactor;

        switch (faction) {
            case Factions.HEGEMONY:
                armorFactor *= 1.5f;
                defenseFactor *= 1.5f;
                break;
            case Factions.DIKTAT:
                defenseFactor *= 1.5f;
                missileFactor *= 1.5f;
                break;
            case Factions.INDEPENDENT:
                logisticsFactor *= 1.5f;
                break;
            case Factions.LIONS_GUARD:
                eliteFactor *= 1.5f;
                enginesFactor *= 1.5f;
                break;
            case Factions.KOL:
                eliteFactor *= 1.5f;
                enginesFactor *= 1.5f;
                weaponsFactor *= 1.5f;
                break;
            case Factions.LUDDIC_CHURCH:
                logisticsFactor *= 1.5f;
                enginesFactor *= 0.5f;
                rangeFactor *= 1.5f;
                capacitorFactor *= 1.5f;
                break;
            case Factions.LUDDIC_PATH:
                logisticsFactor *= 0.5f;
                eliteFactor = 2f;
                enginesFactor = 2f;
                fluxFactor *= 3f;
                rangeFactor *= 0.5f;
                capacitorFactor *= 0.5f;
                ventFactor *= 0.75f;
                break;
            case Factions.PIRATES:
                logisticsFactor *= 0.5f;
                weaponsFactor *= 1.5f;
                break;
            case Factions.TRITACHYON:
                fluxFactor *= 1.5f;
                shieldFactor *= 1.5f;
                enginesFactor *= 1.5f;
                break;
            case "cabal":
                fluxFactor *= 1.5f;
                shieldFactor *= 1.5f;
                enginesFactor *= 1.5f;
                weaponsFactor *= 1.5f;
                break;
            case "interstellarimperium":
                armorFactor *= 2f;
                rangeFactor *= 2f;
                missileFactor *= 1.5f;
                break;
            case "citadeldefenders":
                fluxFactor *= 1.5f;
                enginesFactor *= 1.5f;
                weaponsFactor *= 1.5f;
                break;
            case "blackrock_driveyards":
                armorFactor *= 1.5f;
                fluxFactor *= 1.5f;
                enginesFactor *= 1.5f;
                weaponsFactor *= 1.5f;
                break;
            case "exigency":
                fluxFactor *= 1.5f;
                missileFactor *= 2f;
                break;
            case "exipirated":
                missileFactor *= 1.5f;
                break;
            case "templars":
                logisticsFactor *= 0f;
                fluxFactor *= 1.5f;
                rangeFactor *= 3f;
                capacitorFactor *= 1.5f;
                ventFactor *= 1.5f;
                break;
            case "shadow_industry":
                fluxFactor *= 1.5f;
                enginesFactor *= 1.5f;
                rangeFactor *= 1.5f;
                break;
            case "mayorate":
                enginesFactor *= 1.5f;
                weaponsFactor *= 2f;
                break;
            case "junk_pirates":
                logisticsFactor *= 0.5f;
                eliteFactor *= 2f;
                weaponsFactor *= 2f;
                break;
            case "pack":
                weaponsFactor *= 2f;
                break;
            case "syndicate_asp":
                logisticsFactor *= 2f;
                eliteFactor *= 0.5f;
                break;
            case "SCY":
                armorFactor *= 1.5f;
                fluxFactor *= 1.5f;
                enginesFactor *= 0.5f;
                ventFactor *= 1.5f;
                break;
            case "tiandong":
                armorFactor *= 1.5f;
                fluxFactor *= 1.5f;
                enginesFactor *= 1.5f;
                ventFactor *= 2f;
                break;
            case "diableavionics":
                armorFactor *= 2f;
                shieldFactor *= 2f;
                rangeFactor *= 2f;
                missileFactor *= 1.5f;
                capacitorFactor *= 1.75f;
                ventFactor *= 1.75f;
                break;
            default:
                break;
        }

        int weapOPcost = newVariant.computeWeaponOPCost(stats);
        int freeOP = (totalOP - newVariant.computeOPCost(stats));

        if (!newVariant.getFittedWeaponSlots().isEmpty()) {
            switch (hullId) {
                case "brdy_morpheus":
                    newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, WeaponGroupConfig.MORPHEUS);
                    break;
                case "tem_jesuit":
                case "tem_martyr":
                case "tem_crusader":
                case "tem_paladin":
                case "tem_archbishop":
                case "tem_boss_paladin":
                case "tem_boss_archbishop":
                    newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, WeaponGroupConfig.LINKED_STRIKE);
                    break;
                case "exigency_carrier":
                case "exigency_compactfrigate":
                case "exigency_cruiser":
                case "exigency_destroyer":
                case "exigency_indra":
                case "exigency_lightcarrier":
                case "exigency_yria":
                    missileFactor *= 3f;
                    newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, WeaponGroupConfig.EXIGENCY);
                    break;
                case "exipirated_azryel":
                case "exipirated_gehenna":
                case "exipirated_harinder":
                case "exipirated_kafziel":
                case "exipirated_rauwel":
                    missileFactor *= 2f;
                    newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, WeaponGroupConfig.EXIGENCY);
                    break;
                case "junk_pirates_dugong":
                case "junk_pirates_boss_dugong":
                    missileFactor *= 3f;
                    newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, WeaponGroupConfig.MISSILE_BROADSIDE);
                    break;
                case "conquest":
                case "ssp_torch":
                case "ssp_boss_conquest":
                case "ms_inanna":
                case "ms_elysium":
                case "syndicate_asp_gigantophis":
                case "junk_pirates_kraken":
                case "tiandong_xu":
                    newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, WeaponGroupConfig.BROADSIDE);
                    break;
                case "buffalo2":
                case "venture":
                case "gryphon":
                case "ssp_archer":
                case "ssp_mongrel":
                case "ii_basileus":
                case "ii_lynx":
                case "ilk_del_azarchel":
                case "Fox_Cruiser":
                case "ssp_vindicator":
                case "junk_pirates_hammer":
                case "SCY_lealaps":
                case "SCY_orthrus":
                case "SCY_erymanthianBoar":
                case "SCY_erymanthianBoarArmored":
                case "tiandong_dingjun":
                case "tiandong_tuolu":
                    missileFactor *= 3f;
                    newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, WeaponGroupConfig.MISSILE);
                    break;
                case "diableavionics_calm":
                case "diableavionics_storm":
                case "diableavionics_draft":
                    missileFactor *= 1.5f;
                    newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, WeaponGroupConfig.MISSILE);
                    break;
                default:
                    newVariant = DS_WeaponGrouper.generateWeaponGroups(newVariant, WeaponGroupConfig.STANDARD);
                    break;
            }
        }

        if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("logistical_oversight") >= 10) &&
                !DS_Database.isHullModBlocked("maximized_ordinance", archetype, faction) &&
                !newVariant.getHullMods().contains("maximized_ordinance") && !onlyUpgrade) {
            if (Math.pow(weapOPcost / (double) totalOP, 1.0 / (eliteFactor + 0.1)) >= (1f - (float) Math.random() * (float) Math.random() *
                                                                                       (float) Math.random())) {
                newVariant.addMod("maximized_ordinance");
                freeOP = totalOP - newVariant.computeOPCost(stats);
            }
        }

        if (freeOP > 0) {
            Collection<String> builtIn = getBuiltInHullMods(ship);

            // The AI has no reason to use this
//            if ((stats != null && stats.getSkillLevel("logistical_oversight") >= 7) && faction.contentEquals(Factions.LUDDIC_CHURCH) && !builtIn.contains(
//                    "mobile_resupplier") && !builtIn.contains("mobile_headquarters") && !builtIn.contains("mobile_starbase") && !builtIn.contains(
//                            "mobile_autofactory") && logisticsFactor > (float) Math.random()) {
//                if (!builtIn.contains("supply_conservation_program")) {
//                    newVariant.addMod("supply_conservation_program");
//                }
//                if (newVariant.computeOPCost(stats) >= totalOP) {
//                    newVariant.removeMod("supply_conservation_program");
//                }
//
//                freeOP = totalOP - newVariant.computeOPCost(stats);
//            }
            int maxVents;
            if (hullSpec.getHullSize() == HullSize.CAPITAL_SHIP) {
                maxVents = 50;
            } else if (hullSpec.getHullSize() == HullSize.CRUISER) {
                maxVents = 30;
            } else if (hullSpec.getHullSize() == HullSize.DESTROYER) {
                maxVents = 20;
            } else {
                maxVents = 10;
            }
            if ((stats == null && bonusOP >= 30f) || (stats != null && stats.getSkillLevel("flux_dynamics") >= 10)) {
                if (DSModPlugin.hasStarsectorPlus) {
                    maxVents += 10;
                } else {
                    maxVents *= 2;
                }
            }

            int maxCapacitors;
            if (hullSpec.getHullSize() == HullSize.CAPITAL_SHIP) {
                maxCapacitors = 50;
            } else if (hullSpec.getHullSize() == HullSize.CRUISER) {
                maxCapacitors = 30;
            } else if (hullSpec.getHullSize() == HullSize.DESTROYER) {
                maxCapacitors = 20;
            } else {
                maxCapacitors = 10;
            }
            if ((stats == null && bonusOP >= 15f) || (stats != null && stats.getSkillLevel("flux_dynamics") >= 5)) {
                if (DSModPlugin.hasStarsectorPlus) {
                    maxCapacitors += 10;
                } else {
                    maxCapacitors *= 2;
                }
            }

            boolean neverVent = hullId.contentEquals("ssp_excelsior");

            if (neverVent) {
                capacitorFactor += ventFactor;
                ventFactor = 0f;
            }

            int capacitors = Math.min((int) (freeOP / 6f * ((float) Math.random() + 1f) * capacitorFactor), maxCapacitors);
            int vents = Math.min((int) (freeOP / 3f * ((float) Math.random() + 1f) * ventFactor - capacitors), maxVents);
            if (capacitors + vents > freeOP) {
                capacitors = (int) Math.floor(capacitors * (float) (freeOP / (capacitors + vents)));
                vents = (int) Math.floor(vents * (float) (freeOP / (capacitors + vents)));
            }
            newVariant.setNumFluxCapacitors(Math.max(capacitors, newVariant.getNumFluxCapacitors()));
            newVariant.setNumFluxVents(Math.max(vents, newVariant.getNumFluxVents()));

            boolean shields = ((hullSpec.getDefenseType().equals(ShieldType.FRONT)) || (newVariant.getHullSpec().getDefenseType().equals(ShieldType.OMNI)));
            boolean phase = (hullSpec.getDefenseType().equals(ShieldType.PHASE));
            boolean repulsor = hullId.startsWith("exigency_");
            boolean priwen = hullId.startsWith("tem_");
            int shieldQuality = DS_Database.shieldQuality(hullId);
            boolean goodShields = (shieldQuality == 2);
            boolean badShields = (shieldQuality == 0 || shieldQuality == -1);
            boolean expendableShields = (shieldQuality == -1);
            boolean lotsOfBeamMounts = (beamMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 3f);
            boolean lotsOfMissMounts = (missMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 2f);
            boolean lotsOfTrackingMounts = (trackingMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 3f);
            boolean lotsOfLimitedAmmoMounts = (limitedAmmoMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 3f);
            boolean lotsOfLimitedAmmoMissMounts = (limitedAmmoMissMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 2f);
            boolean lotsOfTurretMounts = (turretMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 1.5f);
            boolean lotsOfITUMounts = (ituMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 2f);
            boolean lotsOfFCSMounts = (fcsMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 2.5f);
            float shieldArc = hullSpec.getShieldSpec().getArc();

            boolean lotsOfIPDAIMounts;
            if (faction.contentEquals("SCY")) {
                lotsOfIPDAIMounts = (ipdaiMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 4f);
            } else {
                lotsOfIPDAIMounts = (ipdaiMounts >= (newVariant.getNonBuiltInWeaponSlots().size()) / 3f);
            }

            int bays = 0;
            for (WeaponSlotAPI slot : ship.getHullSpec().getAllWeaponSlotsCopy()) {
                if (slot.getWeaponType() == WeaponType.LAUNCH_BAY) {
                    bays++;
                }
            }
            boolean isCarrier = false;
            boolean isLiteCarrier = false;
            if ((ship.isFrigate() || ship.isDestroyer()) && bays > 0) {
                isCarrier = true;
            }
            if (ship.isCruiser()) {
                if (bays > 1) {
                    isCarrier = true;
                } else if (bays > 0) {
                    isLiteCarrier = true;
                }
            }
            if (ship.isCapital()) {
                if (bays > 2) {
                    isCarrier = true;
                } else if (bays > 0) {
                    isLiteCarrier = true;
                }
            }

            if (repulsor) {
                shields = false;
                phase = false;
                goodShields = false;
                badShields = true;
            }

            if (priwen) {
                shields = false;
                phase = false;
                goodShields = false;
                badShields = true;
            }

            if (phase) {
                goodShields = false;
                badShields = true;
            }

            if (DSModPlugin.hasSWP && expendableShields && shields && !DS_Database.isHullModBlocked("shieldbypass", archetype, faction)) {
                float target = 1f;
                if (ship.isFrigate()) {
                    target = 20f / (hullSpec.getFluxDissipation() * 0.3f);
                }
                if (ship.isDestroyer()) {
                    target = 40f / (hullSpec.getFluxDissipation() * 0.3f);
                }
                if (ship.isCruiser()) {
                    target = 60f / (hullSpec.getFluxDissipation() * 0.3f);
                }
                if (ship.isCapital()) {
                    target = 100f / (hullSpec.getFluxDissipation() * 0.3f);
                }

                if ((float) (Math.random() * Math.random()) >= target && !neverVent) {
                    boolean added = false;
                    if (!builtIn.contains("shieldbypass")) {
                        newVariant.addMod("shieldbypass");
                        added = true;
                        shields = false;
                    }
                    if (newVariant.computeOPCost(stats) >= totalOP) {
                        newVariant.removeMod("shieldbypass");
                        added = false;
                        shields = true;
                    }
                    if (added) {
                        shieldFactor = 0f;
                        armorFactor *= 3f;
                    }
                }
            }

            WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
            if (shields) {
                if (hullSpec.getDefenseType().equals(ShieldType.OMNI) && shieldArc < 360f) {
                    if (stats == null || stats.getSkillLevel("mechanical_engineering") >= 5) {
                        float weight = 1f * shieldFactor;
                        if (weight > 0.01f) {
                            picker.add("frontemitter", weight);
                        }
                    }
                }
                if (hullSpec.getDefenseType().equals(ShieldType.FRONT)) {
                    if (stats == null || stats.getSkillLevel("mechanical_engineering") >= 10) {
                        float weight = 2f * shieldFactor;
                        if (weight > 0.01f) {
                            picker.add("adaptiveshields", weight);
                        }
                    }
                }
                if (shieldArc < 360f) {
                    if (stats == null || stats.getSkillLevel("mechanical_engineering") >= 4) {
                        float weight = 2f * shieldFactor;
                        if (weight > 0.01f) {
                            picker.add("extendedshieldemitter", weight);
                        }
                    }
                }
                if (goodShields) {
                    if (stats == null || stats.getSkillLevel("applied_physics") >= 10) {
                        float weight = 4f * shieldFactor;
                        if (weight > 0.01f) {
                            picker.add("hardenedshieldemitter", weight);
                        }
                    }
                    float weight = 1f * shieldFactor;
                    if (weight > 0.01f) {
                        picker.add("advancedshieldemitter", weight);
                    }
                } else if (badShields) {
                    if (stats == null || stats.getSkillLevel("applied_physics") >= 10) {
                        float weight = 1f * shieldFactor;
                        if (weight > 0.01f) {
                            picker.add("hardenedshieldemitter", weight);
                        }
                    }
                    float weight = 0.25f * shieldFactor;
                    if (weight > 0.01f) {
                        picker.add("advancedshieldemitter", weight);
                    }
                } else {
                    if (stats == null || stats.getSkillLevel("applied_physics") >= 10) {
                        float weight = 2f * shieldFactor;
                        if (weight > 0.01f) {
                            picker.add("hardenedshieldemitter", weight);
                        }
                    }
                    float weight = 0.5f * shieldFactor;
                    if (weight > 0.01f) {
                        picker.add("advancedshieldemitter", weight);
                    }
                }
                if ((stats == null || stats.getSkillLevel("applied_physics") >= 1) && !neverVent) {
                    float weight = 2f * shieldFactor * fluxFactor;
                    if (weight > 0.01f) {
                        picker.add("stabilizedshieldemitter", weight);
                    }
                }
            }
            if (phase) {
                if (stats == null || stats.getSkillLevel("flux_dynamics") >= 3) {
                    float weight = 4f * fluxFactor * armorFactor;
                    if (weight > 0.01f) {
                        picker.add("fluxbreakers", weight);
                    }
                }
            } else if (builtIn.contains("safetyoverrides")) {
                if (stats == null || stats.getSkillLevel("flux_dynamics") >= 3) {
                    float weight = 1f * armorFactor;
                    if (weight > 0.01f) {
                        picker.add("fluxbreakers", weight);
                    }
                }
            } else {
                if (stats == null || stats.getSkillLevel("flux_dynamics") >= 3) {
                    float weight = 2f * fluxFactor * armorFactor;
                    if (weight > 0.01f) {
                        picker.add("fluxbreakers", weight);
                    }
                }
            }
            if (!shields && !phase && !repulsor && !priwen) {
                if ((stats == null || stats.getSkillLevel("applied_physics") >= 3) && newVariant.getHullMods().contains("shieldbypass") == false &&
                        !builtIn.contains("brimaginosregen")) {
                    float weight = 3f * shieldFactor;
                    if (weight > 0.01f) {
                        picker.add("frontshield", weight);
                    }
                }
            }
            if (!goodShields || phase) {
                if (badShields) {
                    float wt = 1f * armorFactor * logisticsFactor;
                    if (wt > 0.01f) {
                        picker.add("blast_doors", wt);
                    }
                    if (stats == null || stats.getSkillLevel("construction") >= 5) {
                        float weight = 3f * armorFactor;
                        if (weight > 0.01f) {
                            picker.add("reinforcedhull", weight);
                        }
                    }
                    if ((stats == null || stats.getSkillLevel("construction") >= 1) && !builtIn.contains("brimaginosregen")) {
                        float weight = 2f * armorFactor * weaponsFactor;
                        if (weight > 0.01f) {
                            picker.add("armoredweapons", weight);
                        }
                    }
                    if (stats == null || stats.getSkillLevel("damage_control") >= 5) {
                        float weight = 3f * armorFactor * eliteFactor / (0.5f + logisticsFactor);
                        if (weight > 0.01f) {
                            picker.add("autorepair", weight);
                        }
                    }
                    if ((stats == null || stats.getSkillLevel("construction") >= 7) && !builtIn.contains("brimaginosregen")) {
                        float weight = 4f * armorFactor;
                        if (weight > 0.01f) {
                            picker.add("heavyarmor", weight);
                        }
                    }
                    if (stats == null || stats.getSkillLevel("construction") >= 4) {
                        float weight = 2f * armorFactor * enginesFactor;
                        if (weight > 0.01f) {
                            picker.add("insulatedengine", weight);
                        }
                    }
                    if (DSModPlugin.imperiumExists && hullId.startsWith("ii_") && (stats == null || stats.getSkillLevel("flux_dynamics") >= 10)) {
                        float weight = 4f * armorFactor * fluxFactor * (float) Math.sqrt(sizeFactor);
                        if (faction.contentEquals("interstellarimperium")) {
                            weight *= 2f;
                        }
                        if (weight > 0.01f) {
                            picker.add("ii_energized_armor", weight);
                        }
                    }
                } else {
                    float wt = 1f * armorFactor * logisticsFactor;
                    if (wt > 0.01f) {
                        picker.add("blast_doors", wt);
                    }
                    if (stats == null || stats.getSkillLevel("construction") >= 5) {
                        float weight = 2f * armorFactor;
                        if (weight > 0.01f) {
                            picker.add("reinforcedhull", weight);
                        }
                    }
                    if ((stats == null || stats.getSkillLevel("construction") >= 1) && !builtIn.contains("brimaginosregen")) {
                        float weight = 1f * armorFactor * weaponsFactor;
                        if (weight > 0.01f) {
                            picker.add("armoredweapons", weight);
                        }
                    }
                    if (stats == null || stats.getSkillLevel("damage_control") >= 5) {
                        float weight = 2f * armorFactor * eliteFactor / (0.5f + logisticsFactor);
                        if (weight > 0.01f) {
                            picker.add("autorepair", weight);
                        }
                    }
                    if ((stats == null || stats.getSkillLevel("construction") >= 7) && !builtIn.contains("brimaginosregen")) {
                        float weight = 2f * armorFactor;
                        if (weight > 0.01f) {
                            picker.add("heavyarmor", weight);
                        }
                    }
                    if (stats == null || stats.getSkillLevel("construction") >= 4) {
                        float weight = 2f * armorFactor * enginesFactor;
                        if (weight > 0.01f) {
                            picker.add("insulatedengine", weight);
                        }
                    }
                    if (DSModPlugin.imperiumExists && hullId.startsWith("ii_") && (stats == null || stats.getSkillLevel("flux_dynamics") >= 10)) {
                        float weight = 2f * armorFactor * fluxFactor * (float) Math.sqrt(sizeFactor);
                        if (faction.contentEquals("interstellarimperium")) {
                            weight *= 2f;
                        }
                        if (weight > 0.01f) {
                            picker.add("ii_energized_armor", weight);
                        }
                    }
                }
            } else {
                float wt = 0.5f * armorFactor * logisticsFactor;
                if (wt > 0.01f) {
                    picker.add("blast_doors", wt);
                }
                if (stats == null || stats.getSkillLevel("construction") >= 5) {
                    float weight = 1f * armorFactor;
                    if (weight > 0.01f) {
                        picker.add("reinforcedhull", weight);
                    }
                }
                if ((stats == null || stats.getSkillLevel("construction") >= 1) && !builtIn.contains("brimaginosregen")) {
                    float weight = 0.5f * armorFactor * weaponsFactor;
                    if (weight > 0.01f) {
                        picker.add("armoredweapons", weight);
                    }
                }
                if (stats == null || stats.getSkillLevel("damage_control") >= 5) {
                    float weight = 1f * armorFactor * eliteFactor / (0.5f + logisticsFactor);
                    if (weight > 0.01f) {
                        picker.add("autorepair", weight);
                    }
                }
                if ((stats == null || stats.getSkillLevel("construction") >= 7) && !builtIn.contains("brimaginosregen")) {
                    float weight = 1f * armorFactor;
                    if (weight > 0.01f) {
                        picker.add("heavyarmor", weight);
                    }
                }
                if (stats == null || stats.getSkillLevel("construction") >= 4) {
                    float weight = 1f * armorFactor * enginesFactor;
                    if (weight > 0.01f) {
                        picker.add("insulatedengine", weight);
                    }
                }
                if (DSModPlugin.imperiumExists && hullId.startsWith("ii_") && (stats == null || stats.getSkillLevel("flux_dynamics") >= 10)) {
                    float weight = 0.5f * armorFactor * fluxFactor * (float) Math.sqrt(sizeFactor);
                    if (faction.contentEquals("interstellarimperium")) {
                        weight *= 2f;
                    }
                    if (weight > 0.01f) {
                        picker.add("ii_energized_armor", weight);
                    }
                }
            }
            if (DSModPlugin.scyExists && (stats == null || stats.getSkillLevel("construction") >= 2)) {
                float weight = 1f * enginesFactor / (1f + armorFactor);
                if (goodShields && !phase) {
                    weight *= 2f;
                } else if (badShields && !phase) {
                    weight *= 0.5f;
                } else if (!shields) {
                    weight *= 0.25f;
                }
                if (weight > 0.01f) {
                    picker.add("SCY_lightArmor", weight);
                }
            }
            if (stats == null || stats.getSkillLevel("construction") >= 10) {
                float weight = 0.5f * armorFactor;
                switch (faction) {
                    case Factions.DIKTAT:
                        weight *= 3f;
                        break;
                    case "templars":
                        weight *= 1.5f;
                        break;
                }
                if (weight > 0.01f) {
                    picker.add("solar_shielding", weight);
                }
            }
            if ((stats == null || stats.getSkillLevel("mechanical_engineering") >= 2) && !builtIn.contains("SCY_engineering")) {
                float weight = 3f * enginesFactor;
                if (weight > 0.01f) {
                    picker.add("auxiliarythrusters", weight);
                }
            }
            if (lotsOfBeamMounts) {
                if (stats == null || stats.getSkillLevel("applied_physics") >= 7) {
                    float weight = 4f * rangeFactor;
                    if (weight > 0.01f) {
                        picker.add("advancedoptics", weight);
                    }
                }
            }
            if (missMounts >= 1) {
                if (lotsOfMissMounts) {
                    if (stats == null || stats.getSkillLevel("missile_specialization") >= 7) {
                        float weight = 4f * missileFactor;
                        if (weight > 0.01f) {
                            picker.add("eccm", weight);
                        }
                    }
                }
                if (lotsOfLimitedAmmoMissMounts) {
                    if (stats == null || stats.getSkillLevel("missile_specialization") >= 3) {
                        float weight = 4f * missileFactor;
                        if (weight > 0.01f) {
                            picker.add("missleracks", weight);
                        }
                    }
                } else if (limitedAmmoMissMounts >= 1) {
                    if (stats == null || stats.getSkillLevel("missile_specialization") >= 3) {
                        float weight = 1f * missileFactor;
                        if (weight > 0.01f) {
                            picker.add("missleracks", weight);
                        }
                    }
                }
            }
            if (lotsOfIPDAIMounts && (stats == null || stats.getSkillLevel("computer_systems") >= 2)) {
                float weight = 3f * defenseFactor;
                if (faction.contentEquals("templars") || faction.contentEquals("SCY")) {
                    weight *= 2f;
                }
                if (weight > 0.01f) {
                    picker.add("pointdefenseai", weight);
                }
            }
            if (DSModPlugin.hasSWP && lotsOfTrackingMounts && (stats == null || stats.getSkillLevel("computer_systems") >= 5)) {
                float weight = 3f * defenseFactor * rangeFactor;
                if (faction.contentEquals("SCY")) {
                    weight *= 0.5f;
                }
                if (faction.contentEquals(Factions.LUDDIC_PATH) || faction.contentEquals(Factions.LUDDIC_CHURCH)) {
                    weight = 0f;
                }
                if (weight > 0.01f) {
                    picker.add("advanced_ai_core", weight);
                }
            }
            if ((stats == null || stats.getSkillLevel("computer_systems") >= 7) && !builtIn.contains("dedicated_targeting_core") && lotsOfITUMounts &&
                    (qualityFactor >= 0.4f) && !DS_Database.isHullModBlocked("targetingUnit", archetype, faction)) {
                float weight = 5f * rangeFactor;
                if (weight > 0.01f) {
                    picker.add("targetingunit", weight);
                }
            } else if ((ship.isCruiser() || ship.isCapital()) && !builtIn.contains("targetingunit") && lotsOfITUMounts) {
                float weight = 3f * rangeFactor;
                if (weight > 0.01f) {
                    picker.add("dedicated_targeting_core", weight);
                }
            }
            if (DSModPlugin.blackrockExists && hullId.startsWith("brdy_") && (stats == null || stats.getSkillLevel("gunnery_implants") >= 3) &&
                    !builtIn.contains("targetingunit") && !builtIn.contains("dedicated_targeting_core")) {
                float weight = 3f * weaponsFactor;
                if (weight > 0.01f) {
                    picker.add("brtarget", weight);
                }
            }
            if (DSModPlugin.imperiumExists && hullId.startsWith("ii_") && (stats == null || stats.getSkillLevel("computer_systems") >= 5) && lotsOfFCSMounts) {
                float weight = 3f * rangeFactor;
                if (faction.contentEquals("interstellarimperium")) {
                    weight *= 2f;
                }
                if (weight > 0.01f) {
                    picker.add("ii_fire_control", weight);
                }
            }
            if (lotsOfTurretMounts && (stats == null || stats.getSkillLevel("mechanical_engineering") >= 1)) {
                float weight = 1f * weaponsFactor;
                if (weight > 0.01f) {
                    picker.add("turretgyros", weight);
                }
            }
            if ((stats == null || stats.getSkillLevel("mechanical_engineering") >= 7) && !builtIn.contains("SCY_engineering") &&
                    !builtIn.contains("brdrive") && !builtIn.contains("unstable_injector") && (qualityFactor >= 0.4f) &&
                    !DS_Database.isHullModBlocked("augmentedengines", archetype, faction)) {
                float weight = 3f * enginesFactor;
                if (weight > 0.01f) {
                    picker.add("augmentedengines", weight);
                }
            } else if (!builtIn.contains("SCY_engineering") && !builtIn.contains("brdrive") && !builtIn.contains("augmentedengines")) {
                float weight = 3f * enginesFactor / (0.5f + armorFactor);
                if (weight > 0.01f) {
                    picker.add("unstable_injector", weight);
                }
            }
            if (DSModPlugin.blackrockExists && hullId.startsWith("brdy_") && (stats == null || stats.getSkillLevel("helmsmanship") >= 5) &&
                    !builtIn.contains("SCY_engineering") && !builtIn.contains("augmentedengines") && !builtIn.contains("unstable_injector")) {
                float weight = 3f * enginesFactor;
                if (weight > 0.01f) {
                    picker.add("brdrive", weight);
                }
            }
            if (lotsOfLimitedAmmoMounts) {
                float weight = 3f * weaponsFactor;
                if (weight > 0.01f) {
                    picker.add("magazines", weight);
                }
            }
            {
                float weight = 1f * logisticsFactor;
                if (weight > 0.01f) {
                    picker.add("hardened_subsystems", weight);
                }
            }
            {
                float weight = 1f * eliteFactor * Math.max(ventFactor, fluxFactor) * enginesFactor / ((0.5f + logisticsFactor) * (0.5f + rangeFactor));
                if (weight > 0.01f) {
                    picker.add("safetyoverrides", weight);
                }
            }
            if (stats == null || stats.getSkillLevel("flux_dynamics") >= 5) {
                float weight = 1.5f * fluxFactor * capacitorFactor * (float) Math.pow((newVariant.getNumFluxCapacitors() / (double) maxCapacitors), 2.0);
                if (!DSModPlugin.hasSWP) {
                    weight *= 0.35f;
                }
                if (weight > 0.01f) {
                    picker.add("fluxcoil", weight);
                }
            }
            if ((stats == null || stats.getSkillLevel("flux_dynamics") >= 7) && !neverVent) {
                float weight = 1.5f * fluxFactor * ventFactor * (float) Math.pow((newVariant.getNumFluxVents() / (double) maxVents), 2.0);
                if (!DSModPlugin.hasSWP) {
                    weight *= 0.35f;
                }
                if (weight > 0.01f) {
                    picker.add("fluxdistributor", weight);
                }
            }
            if (DSModPlugin.hasSWP && (stats != null && stats.getSkillLevel("logistical_oversight") >= 7) &&
                    !newVariant.getHullMods().contains("maximized_ordinance") && !builtIn.contains("mobile_resupplier") &&
                    !builtIn.contains("mobile_headquarters") && !builtIn.contains("mobile_starbase") && !builtIn.contains("mobile_autofactory")) {
                float weight = 1f * logisticsFactor;
                if (weight > 0.01f) {
                    picker.add("supply_conservation_program", weight);
                }
            }
            if (DSModPlugin.hasSWP && (stats == null || stats.getSkillLevel("wing_command") >= 3)) {
                if (isCarrier) {
                    float weight = 3f * logisticsFactor;
                    if (weight > 0.01f) {
                        picker.add("flight_deck_expansion", weight);
                    }
                } else if (isLiteCarrier) {
                    float weight = 1f * logisticsFactor;
                    if (weight > 0.01f) {
                        picker.add("flight_deck_expansion", weight);
                    }
                }
            }
            if (DSModPlugin.hasSWP && hullSpec.getHints().contains(ShipTypeHints.CIVILIAN) &&
                    (stats == null || stats.getSkillLevel("logistical_oversight") >= 5)) {
                float weight = 5f * logisticsFactor;
                if (weight > 0.01f) {
                    picker.add("cargo_expansion", weight);
                }
            }
            if (DSModPlugin.hasSWP && hullSpec.getHints().contains(ShipTypeHints.CIVILIAN) &&
                    (stats == null || stats.getSkillLevel("logistical_oversight") >= 3)) {
                float weight = 2f * logisticsFactor;
                if (weight > 0.01f) {
                    picker.add("fuel_expansion", weight);
                }
            }
            if (DSModPlugin.hasSWP && hullSpec.getHints().contains(ShipTypeHints.CIVILIAN) &&
                    (stats == null || stats.getSkillLevel("logistical_oversight") >= 1) && !builtIn.contains("ilk_AICrew")) {
                float weight = 2f * logisticsFactor;
                if (weight > 0.01f) {
                    picker.add("additional_crew_quarters", weight);
                }
            }
            if (DSModPlugin.blackrockExists && hullId.startsWith("brdy_") && (stats == null || stats.getSkillLevel("damage_control") >= 3)) {
                float weight = 3f * armorFactor;
                if (weight > 0.01f) {
                    picker.add("brassaultop", weight);
                }
            }
            if (DSModPlugin.mayorateExists && (stats == null || stats.getSkillLevel("computer_systems") >= 4) && ituMounts > 0) {
                float weight;
                if (faction.contentEquals("mayorate")) {
                    weight = 2f * rangeFactor;
                } else {
                    weight = 1f * rangeFactor;
                }
                if (weight > 0.01f) {
                    picker.add("ilk_SensorSuite", weight);
                }
            }
            if (DSModPlugin.mayorateExists && (stats == null || stats.getSkillLevel("computer_systems") >= 10) && !builtIn.contains("additional_crew_quarters")) {
                float weight;
                if (faction.contentEquals("mayorate")) {
                    weight = 2f * (float) Math.sqrt(Math.max(weaponsFactor * enginesFactor * logisticsFactor, 0));
                } else {
                    weight = 0.5f * (float) Math.sqrt(Math.max(weaponsFactor * enginesFactor * logisticsFactor, 0));
                }
                if (faction.contentEquals(Factions.LUDDIC_PATH) || faction.contentEquals(Factions.LUDDIC_CHURCH)) {
                    weight = 0f;
                }
                if (weight > 0.01f) {
                    picker.add("ilk_AICrew", weight);
                }
            }

            while (newVariant.computeOPCost(stats) < totalOP) {
                if (picker.isEmpty()) {
                    int remainingOP = totalOP - newVariant.computeOPCost(stats);
                    int moreVents = (int) (remainingOP * ventFactor / (capacitorFactor + ventFactor));
                    int moreCapacitors = remainingOP - moreVents;
                    if (moreVents + newVariant.getNumFluxVents() > maxVents) {
                        moreCapacitors += moreVents + newVariant.getNumFluxVents() - maxVents;
                        moreVents = maxVents - newVariant.getNumFluxVents();
                    }
                    if (moreCapacitors + newVariant.getNumFluxCapacitors() > maxCapacitors) {
                        moreVents += moreCapacitors + newVariant.getNumFluxCapacitors() - maxCapacitors;
                        moreCapacitors = maxCapacitors - newVariant.getNumFluxCapacitors();
                    }
                    if (moreVents + newVariant.getNumFluxVents() > maxVents) {
                        moreVents = maxVents - newVariant.getNumFluxVents();
                    }
                    newVariant.setNumFluxVents(newVariant.getNumFluxVents() + moreVents);
                    newVariant.setNumFluxCapacitors(newVariant.getNumFluxCapacitors() + moreCapacitors);
                    break;
                } else {
                    String pick = picker.pickAndRemove();
                    if (DS_Database.isHullModBlocked(pick, archetype, faction)) {
                        continue;
                    }
                    switch (pick) {
                        case "augmentedengines":
                            if (newVariant.hasHullMod("unstable_injector") || newVariant.hasHullMod("brdrive")) {
                                continue;
                            }
                            break;
                        case "unstable_injector":
                            if (newVariant.hasHullMod("augmentedengines") || newVariant.hasHullMod("brdrive")) {
                                continue;
                            }
                            break;
                        case "brdrive":
                            if (newVariant.hasHullMod("augmentedengines") || newVariant.hasHullMod("unstable_injector")) {
                                continue;
                            }
                            break;
                        case "targetingunit":
                            if (newVariant.hasHullMod("dedicated_targeting_core") || newVariant.hasHullMod("brtarget")) {
                                continue;
                            }
                            break;
                        case "dedicated_targeting_core":
                            if (newVariant.hasHullMod("targetingunit") || newVariant.hasHullMod("brtarget")) {
                                continue;
                            }
                            break;
                        case "brtarget":
                            if (newVariant.hasHullMod("targetingunit") || newVariant.hasHullMod("dedicated_targeting_core")) {
                                continue;
                            }
                            break;
                        default:
                            break;
                    }
                    if (!builtIn.contains(pick)) {
                        newVariant.addMod(pick);
                    }
                    if (newVariant.computeOPCost(stats) >= totalOP) {
                        newVariant.removeMod(pick);
                    }
                    if (newVariant.hasHullMod("extendedshieldemitter") && newVariant.hasHullMod("frontemitter") && shieldArc >= 180f) {
                        newVariant.removeMod("extendedshieldemitter");
                    }
                }
            }

            for (int i = 0; i < 100; i++) {
                if (newVariant.computeOPCost(stats) > totalOP) {
                    if (Math.random() > 0.5) {
                        if (newVariant.getNumFluxVents() > 0) {
                            newVariant.setNumFluxVents(newVariant.getNumFluxVents() - 1);
                        }
                    } else {
                        if (newVariant.getNumFluxCapacitors() > 0) {
                            newVariant.setNumFluxCapacitors(newVariant.getNumFluxCapacitors() - 1);
                        }
                    }
                }
            }
        }

        return newVariant;
    }

    public static Collection<String> getBuiltInHullMods(FleetMemberAPI ship) {
        ShipVariantAPI tmp = ship.getVariant().clone();
        tmp.clearHullMods();
        return tmp.getHullMods();
    }

    public static String pickRandomSSPWeapon(String faction, Map<String, Float> weaponData, WeaponSize size, float qualityFactor, float vanillaFactor,
                                             int maxTier) {
        WeightedRandomPicker<String> randomizer = new WeightedRandomPicker<>();

        for (Map.Entry<String, Float> entry : weaponData.entrySet()) {
            String id = entry.getKey();
            float weight = entry.getValue();
            DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(id);

            if (weapon.size != size) {
                continue;
            }

            if (weapon.tier > maxTier) {
                continue;
            }

            if (weapon.tier == 0) { // qF 0 = wt 3, qF 0.5 = wt 1, qf 1 = wt 0.33
                if (qualityFactor < 0.5f) {
                    weight *= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
                } else {
                    weight /= 1f + (qualityFactor - 0.5f) / 0.25f;
                }
            } else if (weapon.tier == 1) { // qF 0 = wt 2, qF 0.5 = wt 1, qf 1 = wt 0.5
                if (qualityFactor < 0.5f) {
                    weight *= 1f / (Math.max(qualityFactor, -0.45f) + 0.5f);
                } else {
                    weight /= 1f + (qualityFactor - 0.5f) / 0.5f;
                }
            } else if (weapon.tier == 2) { // qF 0 = wt 0.33, qF 0.5 = wt 1, qf 1 = wt 2
                if (qualityFactor < 0.5f) {
                    weight /= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
                } else {
                    weight *= 1f + (qualityFactor - 0.5f) / 0.5f;
                }
            } else if (weapon.tier == 3) { // qF 0 = wt 0.05, qF 0.5 = wt 1, qf 1 = wt 3
                if (qualityFactor < 0.5f) {
                    weight /= 1f / (Math.max(qualityFactor, 0.05f));
                } else {
                    weight *= 1f + (qualityFactor - 0.5f) / 0.25f;
                }
            }

            if (!id.startsWith("ssp_")) {
                weight *= vanillaFactor;
            }

            if (weight <= 0f) {
                continue;
            }

            randomizer.add(id, weight);
        }

        if (randomizer.isEmpty() && !faction.contentEquals("exigency") && !faction.contentEquals("templars")) {
            switch (faction) {
                case "everything":
                    return null;
                case "sector":
                    return pickRandomSSPWeapon("everything", DS_Database.factionWeapons.get("everything"), size, qualityFactor, vanillaFactor, maxTier);
                case "domain":
                    return pickRandomSSPWeapon("sector", DS_Database.factionWeapons.get("sector"), size, qualityFactor, vanillaFactor, maxTier);
                default:
                    return pickRandomSSPWeapon("domain", DS_Database.factionWeapons.get("domain"), size, qualityFactor, vanillaFactor, maxTier);
            }
        }

        return randomizer.pick();
    }

    public static ShipVariantAPI upgradeVariant(FleetMemberAPI ship, String faction, MutableCharacterStatsAPI stats,
                                                Archetype archetype, float qualityFactor, float bonusOP) {
        if (!DS_Database.factionWeapons.containsKey(faction) || ship.isFighterWing()) {
            return ship.getVariant();
        }

        ShipVariantAPI newVariant = doUpgrades(ship, ship.getVariant(), faction, stats, archetype, true, qualityFactor, bonusOP);

        return newVariant;
    }

    private static float calculateWeaponWeight(String id, float defaultWeight, float targetOP, String faction, EnumMap<RandomizerWeaponType, Float> coefficients,
                                               String preferredWeapon, SlotType slotType, boolean front, boolean turret, WeaponPreference preference,
                                               WeaponSize size, WeaponType type, DamageType preferredDamageType, float qualityFactor, int remainingOP,
                                               int lastResortOP, float attack, float standoff, float alpha, float defense, float closeRange, float longRange,
                                               float disable, boolean asymmetric, boolean broadside, boolean noobFriendly) {
        float weight = defaultWeight;
        float opTarget = targetOP;
        DS_WeaponEntry weapon = DS_Database.masterWeaponList.get(id);

        if (faction.contentEquals("SCY") && id.startsWith("SCY_")) {
            weight *= 4f;
        }

        if (preference == WeaponPreference.BALLISTIC) {
            if (weapon.type == WeaponType.BALLISTIC) {
                weight *= 5f;
                opTarget *= 1.25f;
            }
        } else if (preference == WeaponPreference.ENERGY) {
            if (weapon.type == WeaponType.ENERGY) {
                weight *= 5f;
                opTarget *= 1.25f;
            }
        } else if (preference == WeaponPreference.MISSILE) {
            if (weapon.type == WeaponType.MISSILE) {
                weight *= 5f;
                opTarget *= 1.25f;
            }
        } else if (preference == WeaponPreference.STRIKE) {
            if (weapon.alpha) {
                weight *= 2f;
                opTarget *= 1.25f;
            }
        }

        if (type == WeaponType.UNIVERSAL) {
            if (weapon.size != size) {
                return -1f;
            }
        } else if (type == WeaponType.HYBRID) {
            if (weapon.size != size) {
                return -1f;
            }
            if (weapon.type != WeaponType.BALLISTIC && weapon.type != WeaponType.ENERGY && weapon.type != WeaponType.HYBRID) {
                return -1f;
            }
        } else if (type == WeaponType.SYNERGY) {
            if (weapon.size != size) {
                return -1f;
            }
            if (weapon.type != WeaponType.MISSILE && weapon.type != WeaponType.ENERGY && weapon.type != WeaponType.SYNERGY) {
                return -1f;
            }
        } else if (type == WeaponType.COMPOSITE) {
            if (weapon.size != size) {
                return -1f;
            }
            if (weapon.type != WeaponType.BALLISTIC && weapon.type != WeaponType.MISSILE && weapon.type != WeaponType.COMPOSITE) {
                return -1f;
            }
        } else {
            if (weapon.type != type) {
                return -1f;
            }
            if (weapon.size == WeaponSize.MEDIUM && size == WeaponSize.LARGE) {
                weight *= 0.05f;
            } else if (weapon.size == WeaponSize.SMALL && size == WeaponSize.MEDIUM) {
                weight *= 0.05f;
            } else if (weapon.size != size) {
                return -1f;
            }
        }

        float diff = opTarget - weapon.OP;
        if (size == WeaponSize.SMALL) {
            diff *= 1.5f;
        } else if (size == WeaponSize.LARGE) {
            diff /= 1.5f;
        }
        if (diff < 0f) {
            weight *= Math.exp(-diff * diff / 10f);
        } else {
            if (diff < 5f) {
                weight *= Math.exp(-diff * diff / 20f);
            } else if (diff < 10f) {
                diff = Math.min(diff, 10f);
                weight *= Math.exp(-diff * diff / 40f - 0.625f);
            } else {
                diff = Math.min(diff, 10f);
                weight *= Math.exp(-diff * diff / 80f - 1.875f);
            }
        }

        if (weapon.OP > remainingOP) {
            if (weapon.OP <= lastResortOP && preferredWeapon != null && id.contentEquals(preferredWeapon)) {
                weight *= 0.001f;
            } else {
                return -1f;
            }
        } else {
            weight *= 0.5f;
        }

        if (preferredWeapon != null && id.contentEquals(preferredWeapon)) {
            if (asymmetric) {
                weight *= 5f;
            } else {
                weight *= 200000f;
            }
        }

        if (noobFriendly) {
            weight /= weapon.leetness;
        }

        if (weapon.tier == 0) { // qF 0 = wt 3, qF 0.5 = wt 1, qf 1 = wt 0.33
            if (qualityFactor < 0.5f) {
                weight *= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
            } else {
                weight /= 1f + (qualityFactor - 0.5f) / 0.25f;
            }
        } else if (weapon.tier == 1) { // qF 0 = wt 2, qF 0.5 = wt 1, qf 1 = wt 0.5
            if (qualityFactor < 0.5f) {
                weight *= 1f / (Math.max(qualityFactor, -0.45f) + 0.5f);
            } else {
                weight /= 1f + (qualityFactor - 0.5f) / 0.5f;
            }
        } else if (weapon.tier == 2) { // qF 0 = wt 0.33, qF 0.5 = wt 1, qf 1 = wt 2
            if (qualityFactor < 0.5f) {
                weight /= 0.75f / (Math.max(qualityFactor, -0.2f) + 0.25f);
            } else {
                weight *= 1f + (qualityFactor - 0.5f) / 0.5f;
            }
        } else if (weapon.tier == 3) { // qF 0 = wt 0.05, qF 0.5 = wt 0.5, qf 1 = wt 3
            if (qualityFactor < 0.5f) {
                weight /= 1f / (Math.max(qualityFactor, 0.05f));
            } else {
                weight *= 0.5f + (qualityFactor - 0.5f) / 0.2f;
            }
        } else if (weapon.tier == 4) { // qF 0 = wt 0.033, qF 0.5 = wt 0.33, qf 1 = wt 4
            if (qualityFactor < 0.5f) {
                weight /= 1.5f / (Math.max(qualityFactor, 0.05f));
            } else {
                weight *= 0.33f + (qualityFactor - 0.5f) / 0.136f;
            }
        }

        if (preferredDamageType != null) {
            if (weapon.damageType == preferredDamageType) {
                if (weapon.type == WeaponType.BALLISTIC) {
                    weight *= 2f;
                } else if (weapon.type == WeaponType.MISSILE) {
                    weight *= 1.5f;
                } else if (weapon.type == WeaponType.ENERGY) {
                    weight *= 1.25f;
                }
            }
        }

        float boost = 1f;
        if (weapon.attack) {
            boost += attack / boost;
        }
        if (weapon.standoff) {
            boost += standoff / boost;
        }
        if (weapon.alpha) {
            boost += alpha / boost;
        }
        if (weapon.defense) {
            boost += defense / boost;
        }
        if (weapon.longRange) {
            boost += longRange / boost;
        }
        if (weapon.closeRange) {
            boost += closeRange / boost;
        }
        if (weapon.disable) {
            boost += disable / boost;
        }
        boost -= 1f;
        weight *= boost;

        if (slotType == SlotType.FRONT) {
            if (weapon.defense) {
                weight *= 0.67f;
            }
        } else if (slotType == SlotType.SIDE) {
            if (!broadside) {
                if (weapon.standoff) {
                    weight *= 0.75f;
                }
                if (weapon.alpha) {
                    weight *= 0.5f;
                }
                if (weapon.defense) {
                    weight *= 1.5f;
                }
                if (weapon.closeRange) {
                    weight *= 0.5f;
                }
            } else {
                if (weapon.alpha) {
                    weight *= 0.75f;
                }
                if (weapon.closeRange) {
                    weight *= 0.75f;
                }
            }
        } else if (slotType == SlotType.BACK) {
            if (weapon.attack) {
                weight *= 0.5f;
            }
            if (weapon.standoff) {
                weight *= 0.75f;
            }
            if (weapon.alpha) {
                weight *= 0.5f;
            }
            if (weapon.defense) {
                weight *= 2f;
            }
            if (weapon.closeRange) {
                weight *= 0.25f;
            }
        } else if (slotType == SlotType.HEMI) {
            if (weapon.attack) {
                weight *= 0.75f;
            }
            if (weapon.alpha) {
                weight *= 0.5f;
            }
            if (weapon.standoff) {
                weight *= 1.25f;
            }
            if (weapon.longRange) {
                weight *= 1.25f;
            }
            if (weapon.defense) {
                weight *= 1.5f;
            }
            if (weapon.type == WeaponType.MISSILE) {
                weight *= 0.5f;
            }
        } else if (slotType == SlotType.WIDE) {
            if (weapon.defense) {
                weight *= 1.5f;
            }
            if (weapon.type == WeaponType.MISSILE) {
                weight *= 0.25f;
            }
        } else if (slotType == SlotType.BORESIGHT) {
            if (weapon.alpha) {
                weight *= 2f;
            }
            if (weapon.defense && weapon.type != WeaponType.MISSILE) {
                weight *= 0.33f;
            }
            if (weapon.closeRange) {
                weight *= 0.75f;
            }
            if (weapon.longRange) {
                weight *= 1.33f;
            }
            if (weapon.type == WeaponType.MISSILE) {
                weight *= 1.5f;
            }
        } else if (slotType == SlotType.CROOKED) {
            if (weapon.closeRange) {
                weight *= 0.5f;
            }
            if (weapon.longRange) {
                weight *= 2f;
            }
            if (weapon.type != WeaponType.MISSILE) {
                weight *= 0.25f;
            }
        }

        if (!front && weapon.frontOnly) {
            weight *= 0f;
        }
        if (!turret && weapon.turretOnly) {
            weight *= 0f;
        }

        if (coefficients != null) {
            if (weapon.alpha) {
                weight *= coefficients.get(RandomizerWeaponType.ALPHA);
            }
            if (weapon.attack) {
                weight *= coefficients.get(RandomizerWeaponType.ATTACK);
            }
            if (weapon.defense) {
                weight *= coefficients.get(RandomizerWeaponType.DEFENSE);
            }
            if (weapon.disable) {
                weight *= coefficients.get(RandomizerWeaponType.DISABLE);
            }
            if (weapon.standoff) {
                weight *= coefficients.get(RandomizerWeaponType.STANDOFF);
            }
            if (weapon.closeRange) {
                weight *= coefficients.get(RandomizerWeaponType.CLOSE_RANGE);
            }
            if (weapon.longRange) {
                weight *= coefficients.get(RandomizerWeaponType.LONG_RANGE);
            }
//            if (weapon.frontOnly) {
//                weight *= coefficients.get(RandomizerWeaponType.FRONT_ONLY);
//            }
//            if (weapon.turretOnly) {
//                weight *= coefficients.get(RandomizerWeaponType.TURRET_ONLY);
//            }
        }

        return weight;
    }

    private static String chooseWeapon(String faction, EnumMap<RandomizerWeaponType, Float> coefficients, Map<String, Float> weaponData,
                                       Map<String, Float> weaponDataAlt, float otherFactionWeight, String preferredWeapon, SlotType slotType, boolean front,
                                       boolean turret, WeaponPreference preference, WeaponSize size, WeaponType type, DamageType preferredDamageType,
                                       float qualityFactor, int remainingOP, int lastResortOP, float attack, float standoff, float alpha, float defense,
                                       float closeRange, float longRange, float disable, float budgetFactor, boolean asymmetric, boolean broadside,
                                       boolean noobFriendly) {
        WeightedRandomPicker<String> randomizer = new WeightedRandomPicker<>();

        float targetOP = getOPTargetForSlot(faction, size, type) * budgetFactor;

        for (Map.Entry<String, Float> entry : weaponData.entrySet()) {
            String id = entry.getKey();
            float weight = calculateWeaponWeight(id, entry.getValue(), targetOP, faction, coefficients, preferredWeapon, slotType, front, turret, preference,
                                                 size, type, preferredDamageType, qualityFactor, remainingOP, lastResortOP, attack, standoff, alpha, defense,
                                                 closeRange, longRange, disable, asymmetric, broadside, noobFriendly);

            if (weight <= 0f) {
                continue;
            }

            if (weaponDataAlt != null) {
                weight *= 1f - otherFactionWeight;
            }
            randomizer.add(id, weight);
        }

        if (weaponDataAlt != null) {
            for (Map.Entry<String, Float> entry : weaponDataAlt.entrySet()) {
                String id = entry.getKey();
                float weight = calculateWeaponWeight(id, entry.getValue(), targetOP, faction, coefficients, preferredWeapon, slotType, front, turret,
                                                     preference, size, type, preferredDamageType, qualityFactor, remainingOP, lastResortOP, attack, standoff,
                                                     alpha, defense, closeRange, longRange, disable, asymmetric, broadside, noobFriendly);

                if (weight <= 0f) {
                    continue;
                }

                weight *= otherFactionWeight;
                randomizer.add(id, weight);
            }
        }

        if (randomizer.isEmpty() && !DS_FleetInjector.NO_SHARING_WEAPONS.contains(faction)) {
            switch (faction) {
                case "everything":
                    return null;
                case "sector":
                    return chooseWeapon("everything", coefficients, DS_Database.factionWeapons.get("everything"), null, 0f, preferredWeapon, slotType, front,
                                        turret, preference, size, type, preferredDamageType, qualityFactor, remainingOP, lastResortOP, attack, standoff, alpha,
                                        defense, closeRange, longRange, disable, budgetFactor, asymmetric, broadside, noobFriendly);
                case "domain":
                    return chooseWeapon("sector", coefficients, DS_Database.factionWeapons.get("sector"), null, 0f, preferredWeapon, slotType, front, turret,
                                        preference, size, type, preferredDamageType, qualityFactor, remainingOP, lastResortOP, attack, standoff, alpha, defense,
                                        closeRange, longRange, disable, budgetFactor, asymmetric, broadside, noobFriendly);
                default:
                    return chooseWeapon("domain", coefficients, DS_Database.factionWeapons.get("domain"), null, 0f, preferredWeapon, slotType, front, turret,
                                        preference, size, type, preferredDamageType, qualityFactor, remainingOP, lastResortOP, attack, standoff, alpha, defense,
                                        closeRange, longRange, disable, budgetFactor, asymmetric, broadside, noobFriendly);
            }
        }

        return randomizer.pick();
    }

    private static float getOPTargetForSlot(String faction, WeaponSize size, WeaponType type) {
        if (type == WeaponType.BUILT_IN || type == WeaponType.DECORATIVE || type == WeaponType.LAUNCH_BAY || type == WeaponType.SYSTEM) {
            return 0f;
        }

        Map<DS_Database.SlotType, Float> weaponAverages = DS_Database.factionWeaponAverages.get(faction);
        if (weaponAverages == null) {
            if (size == WeaponSize.SMALL) {
                return 5f;
            } else if (size == WeaponSize.MEDIUM) {
                return 10f;
            } else {
                return 20f;
            }
        }

        DS_Database.SlotType slot = DS_Database.SlotType.getSlotType(size, type);
        if (slot == null) {
            return 0f;
        }
        Float op = weaponAverages.get(slot);
        if (op == null) {
            if (size == WeaponSize.SMALL) {
                return 5f;
            } else if (size == WeaponSize.MEDIUM) {
                return 10f;
            } else {
                return 20f;
            }
        }

        return op;
    }

    public static enum Archetype {

        ARCADE, BALANCED, STRIKE, ELITE, ASSAULT, SKIRMISH, ARTILLERY, CLOSE_SUPPORT, SUPPORT, ESCORT, FLEET, ULTIMATE
    }

    private static enum SlotType {

        FRONT, SIDE, BACK, HEMI, WIDE, BORESIGHT, CROOKED
    }

    private static enum WeaponPreference {

        ENERGY, BALLISTIC, MISSILE, STRIKE, PD
    }
}
