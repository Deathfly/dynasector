package data.scripts.variants;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.loading.WeaponSlotAPI;

public class DS_RandomizerTester implements EveryFrameScript {

    @Override
    public void advance(float amount) {
        CampaignFleetAPI player = Global.getSector().getPlayerFleet();
        if (player == null) {
            return;
        }

        FleetDataAPI data = player.getFleetData();
        for (FleetMemberAPI member : data.getMembersListCopy()) {
            ShipVariantAPI variant = member.getVariant();

            String name = member.getShipName();
            if (name == null || name.length() <= 0) {
                continue;
            }
            if (name.indexOf('-') <= 0) {
                continue;
            }
            String faction = name.substring(0, name.indexOf('-'));
            if (Global.getSector().getFaction(faction) == null) {
                continue;
            }

            variant.clearHullMods();
            for (WeaponSlotAPI slot : variant.getHullSpec().getAllWeaponSlotsCopy()) {
                if (!slot.isBuiltIn() && !slot.isDecorative() && !slot.isSystemSlot()) {
                    variant.clearSlot(slot.getId());
                }
            }
            variant.setNumFluxCapacitors(0);
            variant.setNumFluxVents(0);
            variant.autoGenerateWeaponGroups();

            if (name.endsWith("-R")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.ARTILLERY,
                                                                      0.5f, 0f, false), false, true);
            } else if (name.endsWith("-A")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.ASSAULT, 0.5f,
                                                                      0f, false), false, true);
            } else if (name.endsWith("-B")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.BALANCED,
                                                                      0.5f, 0f, false), false, true);
            } else if (name.endsWith("-C")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.CLOSE_SUPPORT,
                                                                      0.5f, 0f, false), false, true);
            } else if (name.endsWith("-L")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.ELITE, 0.5f,
                                                                      0f, false), false, true);
            } else if (name.endsWith("-E")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.ESCORT, 0.5f,
                                                                      0f, false), false, true);
            } else if (name.endsWith("-F")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.FLEET, 0.5f,
                                                                      0f, false), false, true);
            } else if (name.endsWith("-K")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.SKIRMISH,
                                                                      0.5f, 0f, false), false, true);
            } else if (name.endsWith("-S")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.STRIKE, 0.5f,
                                                                      0f, false), false, true);
            } else if (name.endsWith("-P")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.SUPPORT, 0.5f,
                                                                      0f, false), false, true);
            } else if (name.endsWith("-U")) {
                member.setVariant(DS_VariantRandomizer.createVariant(member, faction, null, 0f, player.getCommanderStats(),
                                                                      DS_VariantRandomizer.Archetype.ULTIMATE,
                                                                      0.5f, 0f, false), false, true);
            }
        }
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }
}
