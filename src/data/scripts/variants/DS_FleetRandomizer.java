package data.scripts.variants;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.fleet.ShipRolePick;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.campaign.fleets.DS_FleetInjector.FactionStyle;
import data.scripts.variants.DS_VariantRandomizer.Archetype;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DS_FleetRandomizer {

    public static final Comparator<FleetMemberAPI> PRIORITY = new Comparator<FleetMemberAPI>() {
        // -1 means member1 is first, 1 means member2 is first
        @Override
        public int compare(FleetMemberAPI member1, FleetMemberAPI member2) {
            if (SPECIAL_SHIPS.contains(member1.getHullId())) {
                if (!SPECIAL_SHIPS.contains(member2.getHullId())) {
                    return -1;
                }
            } else {
                if (SPECIAL_SHIPS.contains(member2.getHullId())) {
                    return 1;
                }
            }
            if (member1.isFlagship()) {
                if (!member2.isFlagship()) {
                    return -1;
                }
            } else {
                if (member2.isFlagship()) {
                    return 1;
                }
            }
            if (!member1.isCivilian()) {
                if (member2.isCivilian()) {
                    return -1;
                }
            } else {
                if (!member2.isCivilian()) {
                    return 1;
                }
            }
            if (!member1.isFighterWing()) {
                if (member2.isFighterWing()) {
                    return -1;
                }
            } else {
                if (!member2.isFighterWing()) {
                    return 1;
                }
            }
            if (member1.getFleetPointCost() > member2.getFleetPointCost()) {
                return -1;
            } else if (member1.getFleetPointCost() < member2.getFleetPointCost()) {
                return 1;
            }
            if (member1.getCaptain() != null) {
                if (member2.getCaptain() == null) {
                    return -1;
                }
            } else {
                if (member2.getCaptain() != null) {
                    return 1;
                }
            }
            if (!member1.isFrigate()) {
                if (member2.isFrigate()) {
                    return -1;
                }
            } else {
                if (!member2.isFrigate()) {
                    return 1;
                }
            }
            if (!member1.isDestroyer()) {
                if (member2.isDestroyer()) {
                    return -1;
                }
            } else {
                if (!member2.isDestroyer()) {
                    return 1;
                }
            }
            if (!member1.isCruiser()) {
                if (member2.isCruiser()) {
                    return -1;
                }
            } else {
                if (!member2.isCruiser()) {
                    return 1;
                }
            }
            if (!member1.isCapital()) {
                if (member2.isCapital()) {
                    return -1;
                }
            } else {
                if (!member2.isCapital()) {
                    return 1;
                }
            }
            return member1.getSpecId().compareTo(member2.getSpecId());
        }
    };

    public static final Set<String> SPECIAL_SHIPS = new HashSet<>(36);

    private static final float ALMOST_ZERO = 0.00001f;

    static {
        SPECIAL_SHIPS.add("ii_boss_praetorian");
        SPECIAL_SHIPS.add("ii_boss_olympus");
        SPECIAL_SHIPS.add("ii_boss_dominus");
        SPECIAL_SHIPS.add("msp_boss_potniaBis");
        SPECIAL_SHIPS.add("ms_boss_charybdis");
        SPECIAL_SHIPS.add("ms_boss_mimir");
        SPECIAL_SHIPS.add("tem_boss_paladin");
        SPECIAL_SHIPS.add("tem_boss_archbishop");
        SPECIAL_SHIPS.add("ssp_boss_phaeton");
        SPECIAL_SHIPS.add("ssp_boss_hammerhead");
        SPECIAL_SHIPS.add("ssp_boss_sunder");
        SPECIAL_SHIPS.add("ssp_boss_tarsus");
        SPECIAL_SHIPS.add("ssp_boss_medusa");
        SPECIAL_SHIPS.add("ssp_boss_falcon");
        SPECIAL_SHIPS.add("ssp_boss_hyperion");
        SPECIAL_SHIPS.add("ssp_boss_paragon");
        SPECIAL_SHIPS.add("ssp_boss_mule");
        SPECIAL_SHIPS.add("ssp_boss_aurora");
        SPECIAL_SHIPS.add("ssp_boss_odyssey");
        SPECIAL_SHIPS.add("ssp_boss_atlas");
        SPECIAL_SHIPS.add("ssp_boss_afflictor");
        SPECIAL_SHIPS.add("ssp_boss_brawler");
        SPECIAL_SHIPS.add("ssp_boss_cerberus");
        SPECIAL_SHIPS.add("ssp_boss_dominator");
        SPECIAL_SHIPS.add("ssp_boss_doom");
        SPECIAL_SHIPS.add("ssp_boss_euryale");
        SPECIAL_SHIPS.add("ssp_boss_lasher_b");
        SPECIAL_SHIPS.add("ssp_boss_lasher_r");
        SPECIAL_SHIPS.add("ssp_boss_onslaught");
        SPECIAL_SHIPS.add("ssp_boss_shade");
        SPECIAL_SHIPS.add("ssp_boss_eagle");
        SPECIAL_SHIPS.add("ssp_boss_beholder");
        SPECIAL_SHIPS.add("ssp_boss_dominator_luddic_path");
        SPECIAL_SHIPS.add("ssp_boss_onslaught_luddic_path");
        SPECIAL_SHIPS.add("ssp_boss_astral");
        SPECIAL_SHIPS.add("ssp_boss_conquest");
        SPECIAL_SHIPS.add("ssp_boss_wasp_wing");
        SPECIAL_SHIPS.add("tiandong_boss_wuzhang");
        SPECIAL_SHIPS.add("pack_bulldog_bullseye");
        SPECIAL_SHIPS.add("pack_pitbull_bullseye");
        SPECIAL_SHIPS.add("pack_komondor_bullseye");
        SPECIAL_SHIPS.add("pack_schnauzer_bullseye");
        SPECIAL_SHIPS.add("diableavionics_IBBgulf");
    }

//    public static boolean addRandomVariantToFleet(CampaignFleetAPI fleet, String faction, String role, float qualityFactor,
//                                                  Map<Archetype, Float> archetypeWeights) {
//        return addRandomVariantToFleet(fleet, faction, role, qualityFactor, null, archetypeWeights);
//    }
//
//    public static boolean addRandomVariantToFleet(CampaignFleetAPI fleet, String faction, String role, float qualityFactor, Archetype preferredArchetype,
//                                                  Map<Archetype, Float> archetypeWeights) {
//        float shipQualityFactor = qualityFactor;
//        FactionStyle style = FactionStyle.getStyle(faction);
//        if (style != null) {
//            if (qualityFactor < style.lowQF) {
//                if (style.lowQF - style.minQF <= ALMOST_ZERO) {
//                    shipQualityFactor = style.minQF;
//                } else {
//                    float a = (style.lowQF - qualityFactor) / (style.lowQF - style.minQF);
//                    shipQualityFactor = (style.lowQF * (1f - a)) + (style.minQF * a);
//                }
//            }
//            if (qualityFactor > style.highQF) {
//                if (style.maxQF - style.highQF <= ALMOST_ZERO) {
//                    shipQualityFactor = style.maxQF;
//                } else {
//                    float a = (qualityFactor - style.highQF) / (style.maxQF - style.highQF);
//                    shipQualityFactor = (style.highQF * (1f - a)) + (style.maxQF * a);
//                }
//            }
//        }
//
//        List<FleetMemberAPI> members = getRandomHull(faction, role, shipQualityFactor);
//        for (FleetMemberAPI member : members) {
//            member.setVariant(SSP_VariantRandomizer.createVariant(member, faction, fleet.getCommanderStats(),
//                                                                  getArchetypeFromRole(role, qualityFactor, preferredArchetype, archetypeWeights), qualityFactor,
//                                                                  0f), false, true);
//            fleet.getFleetData().addFleetMember(member);
//        }
//        return !members.isEmpty();
//    }
//
//    public static int addRandomVariantToFleetRandomBattle(List<FleetMemberAPI> fleet, String faction, String role, float qualityFactor,
//                                                          Map<Archetype, Float> archetypeWeights) {
//        int totalPts = 0;
//        float shipQualityFactor = qualityFactor;
//        FactionStyle style = FactionStyle.getStyle(faction);
//        if (style != null) {
//            if (qualityFactor < style.lowQF) {
//                if (style.lowQF - style.minQF <= ALMOST_ZERO) {
//                    shipQualityFactor = style.minQF;
//                } else {
//                    float a = (style.lowQF - qualityFactor) / style.lowQF;
//                    shipQualityFactor = (style.lowQF * (1f - a)) + (style.minQF * a);
//                }
//            }
//            if (qualityFactor > style.highQF) {
//                if (style.maxQF - style.highQF <= ALMOST_ZERO) {
//                    shipQualityFactor = style.maxQF;
//                } else {
//                    float a = (qualityFactor - style.highQF) / (1f - style.highQF);
//                    shipQualityFactor = (style.highQF * (1f - a)) + (style.maxQF * a);
//                }
//            }
//        }
//
//        List<FleetMemberAPI> members = getRandomHull(faction, role, shipQualityFactor);
//        for (FleetMemberAPI member : members) {
//            Archetype archetype = SSP_FleetRandomizer.getArchetypeFromRole(role, qualityFactor, null, archetypeWeights);
//            if (archetype == Archetype.ULTIMATE || archetype == Archetype.FLEET) {
//                archetype = Archetype.BALANCED;
//            }
//            member.setVariant(SSP_VariantRandomizer.createVariant(member, faction, null, archetype, qualityFactor, 0f), false, true);
//            fleet.add(member);
//            if (member.isFighterWing() || member.isFrigate()) {
//                totalPts += 1;
//            } else if (member.isDestroyer()) {
//                totalPts += 2;
//            } else if (member.isCruiser()) {
//                totalPts += 4;
//            } else if (member.isCapital()) {
//                totalPts += 8;
//            }
//        }
//        return totalPts;
//    }
    public static int countFPInFleetData(List<FleetMemberAPI> data) {
        int count = 0;
        for (FleetMemberAPI member : data) {
            count += member.getFleetPointCost();
        }

        return count;
    }

    public static int countPtsInFleetData(List<FleetMemberAPI> data) {
        int pts = 0;
        for (FleetMemberAPI member : data) {
            if (member.isFighterWing() || member.isFrigate()) {
                pts += 1;
            } else if (member.isDestroyer()) {
                pts += 2;
            } else if (member.isCruiser()) {
                pts += 4;
            } else if (member.isCapital()) {
                pts += 8;
            }
        }

        return pts;
    }

    public static int countRolePtsInFleet(String faction, CampaignFleetAPI fleet, String... roles) {
        Map<String, List<DS_FactionVariant>> factionData = DS_Database.factionVariants.get(faction);
        if (factionData == null) {
            return 0;
        }

        Set<String> hulls = new HashSet<>(100);
        for (String role : roles) {
            List<DS_FactionVariant> roleVariants = factionData.get(role);
            if (roleVariants == null || roleVariants.isEmpty()) {
                continue;
            }

            for (DS_FactionVariant variant : roleVariants) {
                hulls.add(variant.variantId);
            }
        }

        if (hulls.isEmpty()) {
            return 0;
        }

        int pts = 0;
        List<FleetMemberAPI> members = fleet.getFleetData().getMembersListCopy();
        for (FleetMemberAPI member : members) {
            String hull = member.getHullId();
            if (member.isFighterWing()) {
                hull += "_wing";
            } else if (!hull.endsWith("_Hull")) {
                hull += "_Hull";
            }

            if (hulls.contains(hull)) {
                if (member.isFighterWing() || member.isFrigate()) {
                    pts += 1;
                } else if (member.isDestroyer()) {
                    pts += 2;
                } else if (member.isCruiser()) {
                    pts += 4;
                } else if (member.isCapital()) {
                    pts += 8;
                }
            }
        }

        return pts;
    }

    public static List<String> deconstructFleet(CampaignFleetAPI fleet, String faction) {
        List<String> roleList = new ArrayList<>(25);
        WeightedRandomPicker<String> roleListFractions = new WeightedRandomPicker<>();

        Map<String, List<DS_FactionRole>> factionRoles = DS_Database.factionRoles.get(faction);
        if (factionRoles == null) {
            return roleList; // welp.
        }

        Collection keepShips;
        if (fleet.getMemoryWithoutUpdate().get("$keepShips") instanceof Collection) {
            keepShips = (Collection) fleet.getMemoryWithoutUpdate().get("$keepShips");
        } else {
            keepShips = null;
        }

        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            if (keepShips != null && keepShips.contains(member)) {
                continue;
            }

            String id = member.getHullId();
            if (member.isFighterWing()) {
                id += "_wing";
            }

            List<DS_FactionRole> roles = factionRoles.get(id);
            if (roles == null) {
                id = member.getHullSpec().getBaseHullId();
                if (member.isFighterWing()) {
                    id += "_wing";
                }

                roles = factionRoles.get(id);
            }
            if (roles == null) {
                continue; // guess we couldn't find anything...
            }

            if (SPECIAL_SHIPS.contains(id)) {
                continue;
            }

            for (DS_FactionRole role : roles) {
                roleListFractions.add(role.role, role.val);
            }
        }

        roleList.add(roleListFractions.pick());

        return roleList;
    }

    public static List<String> deconstructShip(FleetMemberAPI member, String faction) {
        List<String> roleList = new ArrayList<>(25);
        Map<String, Float> roleListFractions = new LinkedHashMap<>(55);

        Map<String, List<DS_FactionRole>> factionRoles = DS_Database.factionRoles.get(faction);
        if (factionRoles == null) {
            return roleList; // welp.
        }

        String id = member.getHullId();
        if (member.isFighterWing()) {
            id += "_wing";
        }

        List<DS_FactionRole> roles = factionRoles.get(id);
        if (roles == null) {
            id = member.getHullSpec().getBaseHullId();
            if (member.isFighterWing()) {
                id += "_wing";
            }

            roles = factionRoles.get(id);
        }
        if (roles == null) {
            return roleList; // guess we couldn't find anything...
        }

        for (DS_FactionRole role : roles) {
            Float fraction = roleListFractions.get(role.role);
            if (fraction == null) {
                roleListFractions.put(role.role, role.val);
            } else {
                roleListFractions.put(role.role, role.val + fraction);
            }
        }

        float fracRemainder = 0f;
        for (Map.Entry<String, Float> fraction : roleListFractions.entrySet()) {
            String role = fraction.getKey();
            float frac = fraction.getValue();

            int count = (int) Math.floor(frac);
            frac -= count;

            if ((float) Math.random() < frac + fracRemainder) {
                count++;
                fracRemainder = 0f;
            } else {
                fracRemainder += frac;
            }

            for (int i = 0; i < count; i++) {
                roleList.add(role);
            }
        }

        return roleList;
    }

    public static Archetype getArchetypeFromRole(String role, float qualityFactor, Archetype preferredArchetype, Map<Archetype, Float> archetypeWeights) {
        WeightedRandomPicker<Archetype> archetypes = new WeightedRandomPicker<>();
        if (role != null) {
            switch (role) {
                case ShipRoles.FAST_ATTACK:
                    archetypes.add(Archetype.ASSAULT, 3f);
                    archetypes.add(Archetype.BALANCED, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.SKIRMISH, 2f);
                    archetypes.add(Archetype.STRIKE, 2f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case ShipRoles.ESCORT_SMALL:
                    archetypes.add(Archetype.BALANCED, 2f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.ESCORT, 3f);
                    archetypes.add(Archetype.SUPPORT, 2f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case ShipRoles.ESCORT_MEDIUM:
                    archetypes.add(Archetype.BALANCED, 2f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.ESCORT, 3f);
                    archetypes.add(Archetype.SUPPORT, 2f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case ShipRoles.COMBAT_SMALL:
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 2f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.SKIRMISH, 0.5f);
                    archetypes.add(Archetype.STRIKE, 1f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case ShipRoles.COMBAT_MEDIUM:
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 2f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.SKIRMISH, 0.5f);
                    archetypes.add(Archetype.STRIKE, 1f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case ShipRoles.COMBAT_LARGE:
                    archetypes.add(Archetype.ARTILLERY, 1f);
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 2f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.STRIKE, 0.5f);
                    archetypes.add(Archetype.ESCORT, 0.5f);
                    archetypes.add(Archetype.SUPPORT, 0.5f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case ShipRoles.COMBAT_CAPITAL:
                    archetypes.add(Archetype.ARTILLERY, 1f);
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 2f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.ESCORT, 0.5f);
                    archetypes.add(Archetype.SUPPORT, 0.5f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case "miningSmall":
                case ShipRoles.COMBAT_FREIGHTER_SMALL:
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 2f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.ESCORT, 1f);
                    archetypes.add(Archetype.SKIRMISH, 0.5f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case "miningMedium":
                case ShipRoles.COMBAT_FREIGHTER_MEDIUM:
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 2f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.ESCORT, 1f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case "miningLarge":
                case ShipRoles.COMBAT_FREIGHTER_LARGE:
                    archetypes.add(Archetype.ARTILLERY, 1f);
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 2f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.ESCORT, 1f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.CIV_RANDOM:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ESCORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SKIRMISH, 1f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.CARRIER_SMALL:
                    archetypes.add(Archetype.ARTILLERY, 1f);
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.ESCORT, 1f);
                    archetypes.add(Archetype.FLEET, 1f);
                    archetypes.add(Archetype.STRIKE, 1f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case ShipRoles.CARRIER_MEDIUM:
                    archetypes.add(Archetype.ARTILLERY, 1f);
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.ESCORT, 1f);
                    archetypes.add(Archetype.FLEET, 1f);
                    archetypes.add(Archetype.STRIKE, 1f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case ShipRoles.CARRIER_LARGE:
                    archetypes.add(Archetype.ARTILLERY, 1f);
                    archetypes.add(Archetype.ASSAULT, 1f);
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.ELITE, 1f * qualityFactor);
                    archetypes.add(Archetype.ESCORT, 1f);
                    archetypes.add(Archetype.FLEET, 1f);
                    archetypes.add(Archetype.STRIKE, 1f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    archetypes.add(Archetype.ULTIMATE, 0.5f * qualityFactor * qualityFactor);
                    break;
                case ShipRoles.FREIGHTER_SMALL:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.FREIGHTER_MEDIUM:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.FREIGHTER_LARGE:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.TANKER_SMALL:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.TANKER_MEDIUM:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.TANKER_LARGE:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.PERSONNEL_SMALL:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.PERSONNEL_MEDIUM:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.PERSONNEL_LARGE:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.CLOSE_SUPPORT, 1f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.LINER_SMALL:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.LINER_MEDIUM:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.LINER_LARGE:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.TUG:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.CRIG:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                case ShipRoles.UTILITY:
                    archetypes.add(Archetype.BALANCED, 3f);
                    archetypes.add(Archetype.FLEET, 3f);
                    archetypes.add(Archetype.SUPPORT, 1f);
                    break;
                default:
            }
        }

        if (archetypes.isEmpty()) {
            if (preferredArchetype != null) {
                return preferredArchetype;
            }
            return Archetype.BALANCED;
        }

        if (archetypeWeights != null) {
            for (Map.Entry<Archetype, Float> entry : archetypeWeights.entrySet()) {
                Archetype archetype = entry.getKey();
                float weight = entry.getValue();
                if (archetypes.getItems().contains(archetype)) {
                    float currWeight = archetypes.getWeight(archetypes.getItems().indexOf(archetype));
                    archetypes.remove(archetype);
                    archetypes.add(archetype, currWeight * weight);
                }
            }
        }

        if (preferredArchetype != null) {
            for (int i = 0; i < archetypes.getItems().size(); i++) {
                Archetype archetype = archetypes.getItems().get(i);
                if (archetype == preferredArchetype) {
                    archetypes.add(archetype, archetypes.getWeight(i) * 9f + 5f);
                    break;
                }
            }
            archetypes.add(preferredArchetype, 5f);
        }

        return archetypes.pick();
    }

    public static List<FleetMemberAPI> getRandomHull(String faction, String role, float qualityFactor) {
        return getRandomHull(faction, role, new ArrayList<String>(0), qualityFactor, 0, true);
    }

    public static void randomizeVariant(FleetMemberAPI member, String faction, String otherFaction, float otherFactionWeight, String role, float qualityFactor,
                                        Archetype preferredArchetype, Map<Archetype, Float> archetypeWeights, float opBonus, boolean noobFriendly) {
        float variantQF = qualityFactor;
        FactionStyle style = FactionStyle.getStyle(faction);
        if (style != null) {
            if (qualityFactor < style.lowQF) {
                if (style.lowQF - style.minQF <= ALMOST_ZERO) {
                    variantQF = style.minQF;
                } else {
                    float a = (style.lowQF - qualityFactor) / (style.lowQF - style.minQF);
                    variantQF = (style.lowQF * (1f - a)) + (style.minQF * a);
                }
            }
            if (qualityFactor > style.highQF) {
                if (style.maxQF - style.highQF <= ALMOST_ZERO) {
                    variantQF = style.maxQF;
                } else {
                    float a = (qualityFactor - style.highQF) / (style.maxQF - style.highQF);
                    variantQF = (style.highQF * (1f - a)) + (style.maxQF * a);
                }
            }
        }

        if (!member.isFighterWing()) {
            member.setVariant(Global.getSettings().getVariant(member.getHullId() + "_Hull"), false, true);
        }
        member.setVariant(DS_VariantRandomizer.createVariant(member, faction, otherFaction, otherFactionWeight, null,
                                                              getArchetypeFromRole(role, qualityFactor, preferredArchetype, archetypeWeights), variantQF,
                                                              opBonus, noobFriendly), false, true);
    }

    private static List<FleetMemberAPI> getRandomHull(String faction, String role, List<String> previousRoles, float qualityFactor, int iterations,
                                                      boolean original) {
        if (role == null) {
            return new ArrayList<>(0);
        }

        String shipRole = role;

        Map<String, List<DS_FactionVariant>> factionData = DS_Database.factionVariants.get(faction);
        if (factionData == null) {
            return new ArrayList<>(0);
        }

        // Tiandong mixes combat ships with their freighters
        if (faction.contentEquals("tiandong")) {
            if (shipRole.contentEquals(ShipRoles.COMBAT_LARGE) && Math.random() < 0.05) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_LARGE;
            } else if (shipRole.contentEquals(ShipRoles.COMBAT_MEDIUM) && Math.random() < 0.1) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_MEDIUM;
            } else if (shipRole.contentEquals(ShipRoles.ESCORT_MEDIUM) && Math.random() < 0.1) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_MEDIUM;
            } else if (shipRole.contentEquals(ShipRoles.COMBAT_SMALL) && Math.random() < 0.15) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_SMALL;
            } else if (shipRole.contentEquals(ShipRoles.ESCORT_SMALL) && Math.random() < 0.15) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_SMALL;
            } else if (shipRole.contentEquals(ShipRoles.COMBAT_FREIGHTER_LARGE) && Math.random() < 0.05) {
                shipRole = ShipRoles.COMBAT_LARGE;
            } else if (shipRole.contentEquals(ShipRoles.COMBAT_FREIGHTER_MEDIUM) && Math.random() < 0.1) {
                if (Math.random() > 0.5) {
                    shipRole = ShipRoles.COMBAT_MEDIUM;
                } else {
                    shipRole = ShipRoles.ESCORT_MEDIUM;
                }
            } else if (shipRole.contentEquals(ShipRoles.COMBAT_FREIGHTER_SMALL) && Math.random() < 0.15) {
                if (Math.random() > 0.5) {
                    shipRole = ShipRoles.COMBAT_SMALL;
                } else {
                    shipRole = ShipRoles.ESCORT_SMALL;
                }
            } else if (shipRole.contentEquals(ShipRoles.FREIGHTER_LARGE) && Math.random() < 0.3) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_LARGE;
            } else if (shipRole.contentEquals(ShipRoles.FREIGHTER_MEDIUM) && Math.random() < 0.3) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_MEDIUM;
            } else if (shipRole.contentEquals(ShipRoles.FREIGHTER_SMALL) && Math.random() < 0.3) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_SMALL;
            } else if ((shipRole.contentEquals(ShipRoles.TANKER_LARGE) ||
                        shipRole.contentEquals(ShipRoles.PERSONNEL_LARGE) ||
                        shipRole.contentEquals(ShipRoles.LINER_LARGE)) && Math.random() < 0.15) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_LARGE;
            } else if ((shipRole.contentEquals(ShipRoles.TANKER_MEDIUM) ||
                        shipRole.contentEquals(ShipRoles.PERSONNEL_MEDIUM) ||
                        shipRole.contentEquals(ShipRoles.LINER_MEDIUM)) && Math.random() < 0.15) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_MEDIUM;
            } else if ((shipRole.contentEquals(ShipRoles.TANKER_SMALL) ||
                        shipRole.contentEquals(ShipRoles.PERSONNEL_SMALL) ||
                        shipRole.contentEquals(ShipRoles.LINER_SMALL)) && Math.random() < 0.15) {
                shipRole = ShipRoles.COMBAT_FREIGHTER_SMALL;
            }
        }

        /* Fallback if no defined role */
        List<DS_FactionVariant> variants = factionData.get(shipRole);
        if (variants == null) {
            FactionAPI fac = Global.getSector().getFaction(faction);
            if (fac != null) {
                List<ShipRolePick> picks = fac.pickShip(role, qualityFactor);
                List<FleetMemberAPI> members = new ArrayList<>(picks.size());
                for (ShipRolePick pick : picks) {
                    FleetMemberAPI member;
                    try {
                        if (pick.isFighterWing()) {
                            member = Global.getFactory().createFleetMember(FleetMemberType.FIGHTER_WING, pick.variantId);
                        } else {
                            member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, pick.variantId);
                        }
                    } catch (Exception e) {
                        member = null;
                    }
                    if (member != null) {
                        members.add(member);
                    }
                }
                return members;
            } else {
                return new ArrayList<>(0);
            }
        }

        float totalWeight = 0f;
        float totalRarity = 0f;
        WeightedRandomPicker<String> randomizer = new WeightedRandomPicker<>();
        for (DS_FactionVariant variant : variants) {
            String id = variant.variantId;
            float weight = variant.weight;
            float rarity = variant.rarity;

            if (qualityFactor < variant.qualityFactorMin - ALMOST_ZERO || qualityFactor > variant.qualityFactorMax + ALMOST_ZERO) {
                continue;
            }

            weight *= (Math.max(0.1f, 2f * Math.min(qualityFactor - variant.qualityFactorMin, variant.qualityFactorMax - qualityFactor)) /
                       Math.max(0.1f, variant.qualityFactorMax - variant.qualityFactorMin));

            if (weight <= 0f) {
                continue;
            }

            totalWeight += weight;
            totalRarity += rarity;
            randomizer.add(id, weight);
        }

        int index = 0;
        float effectiveSize = 0f;
        for (String variant : randomizer.getItems()) {
            effectiveSize += Math.min(1f, randomizer.getItems().size() * randomizer.getWeight(index) / totalWeight);
            index++;
        }

        FleetMemberAPI member = null;

        float unaryCheck;
        switch (shipRole) {
            case ShipRoles.CARRIER_LARGE:
            case ShipRoles.COMBAT_CAPITAL:
                unaryCheck = 1f;
                break;
            case ShipRoles.CARRIER_MEDIUM:
            case ShipRoles.COMBAT_LARGE:
            case ShipRoles.COMBAT_FREIGHTER_LARGE:
                unaryCheck = 1.5f;
                break;
            case ShipRoles.FREIGHTER_LARGE:
            case ShipRoles.TANKER_LARGE:
            case ShipRoles.PERSONNEL_LARGE:
            case ShipRoles.LINER_LARGE:
            case "miningLarge":
                unaryCheck = 1f;
                break;
            case ShipRoles.ESCORT_MEDIUM:
            case ShipRoles.COMBAT_MEDIUM:
            case ShipRoles.COMBAT_FREIGHTER_MEDIUM:
                unaryCheck = 2f;
                break;
            case ShipRoles.FREIGHTER_MEDIUM:
            case ShipRoles.TANKER_MEDIUM:
            case ShipRoles.PERSONNEL_MEDIUM:
            case ShipRoles.LINER_MEDIUM:
            case "miningMedium":
                unaryCheck = 1.5f;
                break;
            default:
                unaryCheck = 0f;
                break;
        }

        /* Kill occasional unary categories */
        if (!faction.contentEquals("domain") && !faction.contentEquals("sector") && !faction.contentEquals("everything") && !faction.contentEquals("templars") &&
                !faction.contentEquals("exigency") && (effectiveSize < (float) Math.random() * unaryCheck)) {
            randomizer.clear();
        }

        /* Kill categories filled by "rare" ships */
        if (effectiveSize < (float) Math.random() * totalRarity) {
            randomizer.clear();
        }

        if (!randomizer.isEmpty()) {
            int i = 0;
            while (i < 20) {
                String ship = randomizer.pick();

                try {
                    if (ship.endsWith("_wing")) {
                        member = Global.getFactory().createFleetMember(FleetMemberType.FIGHTER_WING, ship);
                    } else {
                        member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, ship);
                    }
                    break;
                } catch (Exception e) {
                    member = null;
                    i++;
                }
            }
        }

        if (member == null) {
            if (iterations >= 20) {
                return new ArrayList<>(1);
            }

            if (shipRole.contentEquals(ShipRoles.INTERCEPTOR)) {
                if (!previousRoles.contains(ShipRoles.FIGHTER)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FIGHTER, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.BOMBER)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.BOMBER, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.FIGHTER)) {
                if (!previousRoles.contains(ShipRoles.INTERCEPTOR)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.INTERCEPTOR, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.BOMBER)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.BOMBER, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.FAST_ATTACK)) {
                if (!previousRoles.contains(ShipRoles.COMBAT_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.ESCORT_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.ESCORT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.ESCORT_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.ESCORT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_CAPITAL) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_CAPITAL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.ESCORT_SMALL)) {
                if (!previousRoles.contains(ShipRoles.COMBAT_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.FAST_ATTACK)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FAST_ATTACK, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.ESCORT_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.ESCORT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_CAPITAL) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_CAPITAL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.COMBAT_SMALL)) {
                if (!previousRoles.contains(ShipRoles.ESCORT_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.ESCORT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.FAST_ATTACK)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FAST_ATTACK, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.ESCORT_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.ESCORT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_CAPITAL) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_CAPITAL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.ESCORT_MEDIUM)) {
                if (!previousRoles.contains(ShipRoles.COMBAT_MEDIUM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.COMBAT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.COMBAT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.ESCORT_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.ESCORT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.ESCORT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.FAST_ATTACK)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.FAST_ATTACK, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.FAST_ATTACK, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.COMBAT_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_CAPITAL) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_CAPITAL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.COMBAT_MEDIUM)) {
                if (!previousRoles.contains(ShipRoles.ESCORT_MEDIUM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.ESCORT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.COMBAT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.COMBAT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.ESCORT_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.ESCORT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.ESCORT_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.FAST_ATTACK)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.FAST_ATTACK, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.FAST_ATTACK, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.COMBAT_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_CAPITAL) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_CAPITAL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.COMBAT_LARGE)) {
                if (!previousRoles.contains(ShipRoles.COMBAT_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.COMBAT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.COMBAT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.ESCORT_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.ESCORT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.ESCORT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.COMBAT_CAPITAL) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_CAPITAL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.COMBAT_CAPITAL)) {
                if (!previousRoles.contains(ShipRoles.COMBAT_LARGE)) {
                    previousRoles.add(shipRole);
                    if (faction.contentEquals("SCY") && !previousRoles.contains(ShipRoles.COMBAT_MEDIUM)) {
                        List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.COMBAT_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                                   1, original);
                        List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.COMBAT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                                   1, false);
                        List<FleetMemberAPI> list3 = getRandomHull(faction, ShipRoles.COMBAT_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                                   1, false);
                        list1.addAll(list2);
                        list1.addAll(list3);
                        return list1;
                    } else {
                        List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.COMBAT_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                                   1, original);
                        List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.COMBAT_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                                   1, false);
                        list1.addAll(list2);
                        return list1;
                    }
                }
            }

            if (shipRole.contentEquals(ShipRoles.COMBAT_FREIGHTER_SMALL)) {
                if (!previousRoles.contains(ShipRoles.FREIGHTER_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.COMBAT_FREIGHTER_MEDIUM)) {
                if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.COMBAT_FREIGHTER_LARGE)) {
                if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_LARGE)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.CIV_RANDOM)) {
                if (!previousRoles.contains(ShipRoles.FREIGHTER_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.PERSONNEL_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.PERSONNEL_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.TANKER_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.TANKER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.LINER_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.LINER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.PERSONNEL_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.PERSONNEL_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.TANKER_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.TANKER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.LINER_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.LINER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.PERSONNEL_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.PERSONNEL_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.TANKER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.TANKER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.LINER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.LINER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.CARRIER_SMALL)) {
                if (!previousRoles.contains(ShipRoles.CARRIER_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.CARRIER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.CARRIER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.CARRIER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.CARRIER_MEDIUM)) {
                if (!previousRoles.contains(ShipRoles.CARRIER_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.CARRIER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.CARRIER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.CARRIER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.CARRIER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.CARRIER_LARGE)) {
                if (!previousRoles.contains(ShipRoles.CARRIER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.CARRIER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.CARRIER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                }
            }

            if (shipRole.contentEquals(ShipRoles.FREIGHTER_SMALL)) {
                if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.CIV_RANDOM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.CIV_RANDOM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.FREIGHTER_MEDIUM)) {
                if (!previousRoles.contains(ShipRoles.FREIGHTER_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.CIV_RANDOM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.CIV_RANDOM, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.CIV_RANDOM, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                }
            }

            if (shipRole.contentEquals(ShipRoles.FREIGHTER_LARGE)) {
                if (!previousRoles.contains(ShipRoles.FREIGHTER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                               1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                               1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_LARGE)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.COMBAT_FREIGHTER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.COMBAT_FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                }
            }

            if (shipRole.contentEquals(ShipRoles.TANKER_SMALL)) {
                if (!previousRoles.contains(ShipRoles.TANKER_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.TANKER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.TANKER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.TANKER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.CIV_RANDOM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.CIV_RANDOM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.TANKER_MEDIUM)) {
                if (!previousRoles.contains(ShipRoles.TANKER_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.TANKER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.TANKER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.TANKER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.TANKER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.TANKER_LARGE)) {
                if (!previousRoles.contains(ShipRoles.TANKER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.TANKER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.TANKER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1,
                                                               false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.FREIGHTER_LARGE)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.FREIGHTER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.PERSONNEL_SMALL)) {
                if (!previousRoles.contains(ShipRoles.PERSONNEL_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.PERSONNEL_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.PERSONNEL_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.PERSONNEL_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.CIV_RANDOM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.CIV_RANDOM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.LINER_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.LINER_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.PERSONNEL_MEDIUM)) {
                if (!previousRoles.contains(ShipRoles.PERSONNEL_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.PERSONNEL_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.PERSONNEL_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.PERSONNEL_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.PERSONNEL_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.LINER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.LINER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.PERSONNEL_LARGE)) {
                if (!previousRoles.contains(ShipRoles.PERSONNEL_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.PERSONNEL_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                               1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.PERSONNEL_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                               1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.LINER_LARGE)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.LINER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.LINER_SMALL)) {
                if (!previousRoles.contains(ShipRoles.LINER_MEDIUM) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.LINER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.LINER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.LINER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.CIV_RANDOM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.CIV_RANDOM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.PERSONNEL_SMALL)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.PERSONNEL_SMALL, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.LINER_MEDIUM)) {
                if (!previousRoles.contains(ShipRoles.LINER_SMALL)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.LINER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.LINER_SMALL, new ArrayList<>(previousRoles), qualityFactor,
                                                               iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.LINER_LARGE) && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.LINER_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.PERSONNEL_MEDIUM)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.PERSONNEL_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.LINER_LARGE)) {
                if (!previousRoles.contains(ShipRoles.LINER_MEDIUM)) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, ShipRoles.LINER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                               1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, ShipRoles.LINER_MEDIUM, new ArrayList<>(previousRoles), qualityFactor, iterations +
                                                               1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains(ShipRoles.PERSONNEL_LARGE)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.PERSONNEL_LARGE, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.TUG)) {
                if (!previousRoles.contains(ShipRoles.UTILITY)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.UTILITY, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.CRIG)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.CRIG, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.CRIG)) {
                if (!previousRoles.contains(ShipRoles.UTILITY)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.UTILITY, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.TUG)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.TUG, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals(ShipRoles.UTILITY)) {
                if (!previousRoles.contains(ShipRoles.CRIG)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.CRIG, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains(ShipRoles.TUG)) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, ShipRoles.TUG, new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals("miningSmall")) {
                if (!previousRoles.contains("miningMedium") && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, "miningMedium", new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                } else if (!previousRoles.contains("miningLarge") && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, "miningLarge", new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals("miningMedium")) {
                if (!previousRoles.contains("miningSmall")) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, "miningSmall", new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, "miningSmall", new ArrayList<>(previousRoles), qualityFactor, iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                } else if (!previousRoles.contains("miningLarge") && original) {
                    previousRoles.add(shipRole);
                    return getRandomHull(faction, "miningLarge", new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                }
            }

            if (shipRole.contentEquals("miningLarge")) {
                if (!previousRoles.contains("miningMedium")) {
                    previousRoles.add(shipRole);
                    List<FleetMemberAPI> list1 = getRandomHull(faction, "miningMedium", new ArrayList<>(previousRoles), qualityFactor, iterations + 1, original);
                    List<FleetMemberAPI> list2 = getRandomHull(faction, "miningMedium", new ArrayList<>(previousRoles), qualityFactor, iterations + 1, false);
                    list1.addAll(list2);
                    return list1;
                }
            }

            if (!original) {
                return new ArrayList<>(1);
            }

            if (!faction.contentEquals("exigency") && !faction.contentEquals("templars")) {
                String originalRole;
                if (!previousRoles.isEmpty()) {
                    originalRole = previousRoles.get(0);
                } else {
                    originalRole = role;
                }
                switch (faction) {
                    case "everything":
                        return new ArrayList<>(1);
                    case "sector":
                        return getRandomHull("everything", originalRole, new ArrayList<String>(0), qualityFactor, (iterations + 1) / 2, original);
                    case "domain":
                        return getRandomHull("sector", originalRole, new ArrayList<String>(0), qualityFactor, (iterations + 1) / 2, original);
                    default:
                        return getRandomHull("domain", originalRole, new ArrayList<String>(0), qualityFactor, (iterations + 1) / 2, original);
                }
            } else {
                return new ArrayList<>(1);
            }
        }

        List<FleetMemberAPI> list = new ArrayList<>(1);
        list.add(member);
        return list;
    }
}
