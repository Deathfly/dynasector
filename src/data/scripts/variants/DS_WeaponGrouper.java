package data.scripts.variants;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.loading.Description;
import com.fs.starfarer.api.loading.WeaponGroupSpec;
import com.fs.starfarer.api.loading.WeaponGroupType;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DS_WeaponGrouper {

    private static final Set<String> DO_NOT_SPLIT = new HashSet<>(15);

    static final Map<String, List<String>> weaponTypeOverride = new HashMap<>(15);

    static {
        weaponTypeOverride.put("Missile",
                               createWeaponList(new String[]{
                                   "ii_titan_w", "ii_apocalypse_w", "ii_supertitan_w", "exigency_drumlauncher", "exigency_stinger", "fox_supernova",
                                   "SCY_singularityTorpedoLauncher", "SCY_manticoreSwarm", "SCY_manticoreMirvTorpedo", "homing_laser", "ii_javelinlarge",
                                   "diableavionics_micromissile", "diableavionics_microarray"
                               }));
        weaponTypeOverride.put("No Aim",
                               createWeaponList(new String[]{
                                   "ii_titan_w", "ii_apocalypse_w", "ii_supertitan_w"
                               }));
        weaponTypeOverride.put("Anti-Fighter",
                               createWeaponList(new String[]{
                                   "phasecl"
                               }));
        weaponTypeOverride.put("Point Defense",
                               createWeaponList(new String[]{
                                   "phasecl"
                               }));
        weaponTypeOverride.put("Strike",
                               createWeaponList(new String[]{
                                   "fox_zeus", "tem_joyeuse", "ilk_nuke", "ilk_nuke_large", "ssp_gungnir", "ssp_gungnir_r", "exigency_repulsor_blaster",
                                   "SCY_focusBeam_mkii", "SCY_scpb_mkiii", "SCY_singularityTorpedoLauncher"
                               }));
        weaponTypeOverride.put("Assault",
                               createWeaponList(new String[]{
                                   "heavymauler", "phasecl", "brdy_squallgun", "ms_cepc", "ms_chaingang", "exigency_stinger", "ii_javelinlarge",
                                   "tiandong_mauler_battery"
                               }));
        weaponTypeOverride.put("Close Support",
                               createWeaponList(new String[]{
                                   "tem_secace", "tem_arondight", "heavymg", "exigency_drumlauncher", "ilk_graser_light", "fox_rotunda", "fox_rotunda_large",
                                   "SCY_area_mkiii", "SCY_manticoreSwarm"
                               }));
        weaponTypeOverride.put("Fire Support",
                               createWeaponList(new String[]{
                                   "ssp_redeemer", "SCY_manticoreMirvTorpedo", "SCY_manticorePhaseMissile"
                               }));
        weaponTypeOverride.put("Special",
                               createWeaponList(new String[]{
                                   "fox_supernova", "diableavionics_micromissile", "diableavionics_microarray"
                               }));
        weaponTypeOverride.put("Low Flux", // Cannot be autodetected, but is automatically flagged on for missiles
                               createWeaponList(new String[]{
                                   "lightmg", "lightdualmg", "lightmortar", "vulcan", "fragbomb", "clusterbomb", "bomb", "flak", "heavymg", "dualflak",
                                   "mininglaser", "pdlaser", "taclaser", "lrpdlaser", "pdburst", "gravitonbeam", "heavyburst", "guardian", "phasecl", "brvulcan",
                                   "br_iwbattery", "brdy_argussmall", "brdy_miniargus", "brdy_miniargusb", "brburst", "ii_hailfire", "ssp_reliant",
                                   "ssp_contender", "tem_carnwennan", "tem_interdictor", "fox_pd2", "exigency_stinger", "SCY_autonail_mki", "SCY_flak_mki",
                                   "SCY_vibratingBeam_mki", "SCY_minigun_mki", "SCY_singularityTorpedoLauncher", "ssp_tripleflak", "ms_trishula",
                                   "diableavionics_raptor", "diableavionics_ibis", "diableavionics_micromissile", "diableavionics_microarray"
                               }));
        weaponTypeOverride.put("Separate Fire", // Cannot be autodetected
                               createWeaponList(new String[]{
                                   "miningblaster", "plasma", "exigency_rr", "exipirated_rr_p", "fox_seigelaser", "tem_longinus", "tem_arondight",
                                   "tem_juger", "tem_joyeuse", "ii_gravitycannon", "br_xlpde", "ms_hempGun", "ms_rhpbc", "ilk_fluxtorp", "ilk_driver",
                                   "ilk_thermal_lance", "ms_slowbeamM", "ms_slowbeamH", "junk_pirates_welder", "tachyonlance", "ssp_klutotekhnes",
                                   "ssp_hellfire", "exigency_repulsor_blaster", "SCY_kacc_mkiii", "SCY_enb_mkii", "SCY_enb_mkiii", "SCY_pierce_mkii",
                                   "SCY_focusBeam_mkii", "SCY_scpb_mkiii", "SCY_sirenMainGun", "SCY_manticoreSwarm", "SCY_manticoreMirvTorpedo",
                                   "SCY_telchineMainGun", "SCY_ketoMainGun", "brdy_superscalarbeam", "tiandong_triphammer", "tiandong_macrocannon",
                                   "diableavionics_uhlan", "diableavionics_glaux", "diableavionics_heavythermalpulse"
                               }));
        weaponTypeOverride.put("Sustained Beam", // Cannot be autodetected
                               createWeaponList(new String[]{
                                   "hil", "gravitonbeam", "lrpdlaser", "pdlaser", "mininglaser", "phasebeam", "taclaser", "exigency_repulsor_beam",
                                   "exigency_lightning_gun", "fox_seigelaser", "tem_rhon", "tem_longinus", "tem_interdictor", "ii_gravitycannon",
                                   "br_xlpde", "ms_phasebeam", "junk_pirates_welder", "SCY_hackingCommLink", "SCY_minigun_mki", "SCY_minigun_mkii",
                                   "SCY_enhancedFocusBeam", "SCY_superFocusBeam", "SCY_khalkotauroiMainGun", "SCY_telchineMainGun", "brdy_superscalarbeam",
                                   "tiandong_heavy_mininglaser", "ms_trishula", "ilk_thermal_lance", "diableavionics_state", "diableavionics_versant_head"
                               }));
        weaponTypeOverride.put("Burst Beam", // Cannot be autodetected
                               createWeaponList(new String[]{
                                   "pdburst", "heavyburst", "guardian", "tachyonlance", "brdy_miniargusb", "brdy_miniargus", "br_fpde", "br_pde", "brburst",
                                   "brdy_argussmall", "fox_kineticcpl", "fox_antifrigatelaser", "ii_photonbeam", "tem_carnwennan", "fox_pd2", "ms_slowbeamM",
                                   "ms_pbc", "ms_bitgun", "ms_pdburstCustom", "ilk_graser", "ilk_graser_light", "ilk_graser_pd", "ms_slowbeamH",
                                   "junk_pirates_lexcimer", "pack_affenpinscher", "SCY_vibratingBeam_mki", "SCY_slasherBeam_mkii", "SCY_focusBeam_mkii",
                                   "SCY_scpb_mkiii", "diableavionics_burchel", "diableavionics_glaux", "brdy_plance", "ssp_lightphaselance"
                               }));
        weaponTypeOverride.put("Limited Ammo", // Cannot be autodetected, but is gathered from SS+ csv
                               createWeaponList(new String[]{
                                   "exigency_drumlauncher", "ms_cepc", "ms_chaingang", "SCY_singularityTorpedoLauncher", "SCY_manticoreSwarm",
                                   "SCY_manticoreMirvTorpedo", "SCY_manticorePhaseMissile", "ii_javelinlarge"
                               }));
        weaponTypeOverride.put("Override", // Disables autodetection (and csv data)
                               createWeaponList(new String[]{
                                   "ssp_redeemer", "phasecl", "heavymauler", "ii_titan_w", "ii_apocalypse_w", "ii_supertitan_w", "brdy_squallgun",
                                   "exigency_drumlauncher", "ms_cepc", "ms_chaingang", "ilk_graser_light", "exigency_stinger", "fox_supernova", "fox_rotunda",
                                   "fox_rotunda_large", "exigency_repulsor_blaster", "SCY_focusBeam_mkii", "SCY_area_mkiii", "SCY_scpb_mkiii",
                                   "SCY_singularityTorpedoLauncher", "SCY_manticoreSwarm", "SCY_manticoreMirvTorpedo", "SCY_manticorePhaseMissile",
                                   "ii_javelinlarge", "tiandong_mauler_battery", "diableavionics_micromissile", "diableavionics_microarray"
                               }));
        DO_NOT_SPLIT.add("ii_maximus");
        DO_NOT_SPLIT.add("diableavionics_versant");
    }

    public static ShipVariantAPI generateWeaponGroups(ShipVariantAPI variant, WeaponGroupConfig config) {
        // Clean up the existing groups
        // We go through this whole song and dance rather than just flushing and making new groups for reasons of maximum compatability
        for (WeaponGroupSpec group : variant.getWeaponGroups()) {
            WeaponGroupSpec clone = group.clone();
            for (String slot : clone.getSlots()) {
                group.removeSlot(slot);
            }
        }

        // These are programmable variables passed from the WeaponGroupConfig
        // This will change how the algorithm will behave
        boolean splitMissiles = false;
        boolean enforceSides = false;
        boolean linkedStrike = false;
        boolean splitBeams = false;
        boolean exigency = false;

        switch (config) {
            case MISSILE:
                splitMissiles = true;
                break;
            case BROADSIDE:
                enforceSides = true;
                break;
            case MISSILE_BROADSIDE:
                splitMissiles = true;
                enforceSides = true;
                break;
            case EXIGENCY:
                linkedStrike = true;
                exigency = true;
                break;
            case LINKED_STRIKE:
                linkedStrike = true;
                break;
            case MORPHEUS:
                splitBeams = true;
                linkedStrike = true;
                break;
            case STANDARD:
            default:
        }

        // Now we define all of the possible weapon groups that the variant can use
        List<WeaponGroupSpec> groupList = new ArrayList<>(18);

        WeaponGroupSpec PDGroup = new WeaponGroupSpec();
        PDGroup.setType(WeaponGroupType.LINKED);
        PDGroup.setAutofireOnByDefault(true);
        groupList.add(PDGroup);

        WeaponGroupSpec AFGroup = new WeaponGroupSpec();
        AFGroup.setType(WeaponGroupType.LINKED);
        AFGroup.setAutofireOnByDefault(true);
        groupList.add(AFGroup);

        WeaponGroupSpec AssHGroup = new WeaponGroupSpec();
        AssHGroup.setType(WeaponGroupType.LINKED);
        AssHGroup.setAutofireOnByDefault(false);
        groupList.add(AssHGroup);

        WeaponGroupSpec AssTGroup = new WeaponGroupSpec();
        AssTGroup.setType(WeaponGroupType.LINKED);
        AssTGroup.setAutofireOnByDefault(true);
        groupList.add(AssTGroup);

        WeaponGroupSpec CSHGroup = new WeaponGroupSpec();
        CSHGroup.setType(WeaponGroupType.LINKED);
        CSHGroup.setAutofireOnByDefault(true);
        groupList.add(CSHGroup);

        WeaponGroupSpec CSTGroup = new WeaponGroupSpec();
        CSTGroup.setType(WeaponGroupType.LINKED);
        CSTGroup.setAutofireOnByDefault(true);
        groupList.add(CSTGroup);

        WeaponGroupSpec SupGroup = new WeaponGroupSpec();
        SupGroup.setType(WeaponGroupType.LINKED);
        SupGroup.setAutofireOnByDefault(true);
        groupList.add(SupGroup);

        WeaponGroupSpec HvyHGroup = new WeaponGroupSpec();
        if (linkedStrike || enforceSides) {
            HvyHGroup.setType(WeaponGroupType.LINKED);
        } else {
            HvyHGroup.setType(WeaponGroupType.ALTERNATING);
        }
        HvyHGroup.setAutofireOnByDefault(false);
        groupList.add(HvyHGroup);

        WeaponGroupSpec HvyTGroup = new WeaponGroupSpec();
        if (linkedStrike || enforceSides) {
            HvyTGroup.setType(WeaponGroupType.LINKED);
        } else {
            HvyTGroup.setType(WeaponGroupType.ALTERNATING);
        }
        HvyTGroup.setAutofireOnByDefault(false);
        groupList.add(HvyTGroup);

        WeaponGroupSpec BeaHGroup = new WeaponGroupSpec();
        BeaHGroup.setType(WeaponGroupType.LINKED);
        BeaHGroup.setAutofireOnByDefault(true);
        groupList.add(BeaHGroup);

        WeaponGroupSpec BeaTGroup = new WeaponGroupSpec();
        BeaTGroup.setType(WeaponGroupType.LINKED);
        BeaTGroup.setAutofireOnByDefault(false);
        groupList.add(BeaTGroup);

        WeaponGroupSpec StrHGroup = new WeaponGroupSpec();
        if (linkedStrike) {
            StrHGroup.setType(WeaponGroupType.LINKED);
        } else {
            StrHGroup.setType(WeaponGroupType.ALTERNATING);
        }
        StrHGroup.setAutofireOnByDefault(false);
        groupList.add(StrHGroup);

        WeaponGroupSpec StrTGroup = new WeaponGroupSpec();
        if (linkedStrike || variant.getHullSize() == HullSize.CRUISER || variant.getHullSize() == HullSize.CAPITAL_SHIP) {
            StrTGroup.setType(WeaponGroupType.LINKED);
        } else {
            StrTGroup.setType(WeaponGroupType.ALTERNATING);
        }
        StrTGroup.setAutofireOnByDefault(false);
        groupList.add(StrTGroup);

        WeaponGroupSpec MisGroup = new WeaponGroupSpec();
        if (linkedStrike) {
            MisGroup.setType(WeaponGroupType.LINKED);
        } else {
            MisGroup.setType(WeaponGroupType.ALTERNATING);
        }
        if (exigency) {
            MisGroup.setAutofireOnByDefault(true);
        } else {
            MisGroup.setAutofireOnByDefault(false);
        }
        groupList.add(MisGroup);

        WeaponGroupSpec SpamMisGroup = new WeaponGroupSpec();
        if (exigency) {
            SpamMisGroup.setType(WeaponGroupType.ALTERNATING);
        } else {
            SpamMisGroup.setType(WeaponGroupType.LINKED);
        }
        SpamMisGroup.setAutofireOnByDefault(true);
        groupList.add(SpamMisGroup);

        WeaponGroupSpec StrMisGroup = new WeaponGroupSpec();
        if (linkedStrike) {
            StrMisGroup.setType(WeaponGroupType.LINKED);
        } else {
            StrMisGroup.setType(WeaponGroupType.ALTERNATING);
        }
        StrMisGroup.setAutofireOnByDefault(false);
        groupList.add(StrMisGroup);

        WeaponGroupSpec LGroup = new WeaponGroupSpec();
        LGroup.setType(WeaponGroupType.LINKED);
        LGroup.setAutofireOnByDefault(false);
        groupList.add(LGroup);

        WeaponGroupSpec RGroup = new WeaponGroupSpec();
        RGroup.setType(WeaponGroupType.LINKED);
        RGroup.setAutofireOnByDefault(false);
        groupList.add(RGroup);

        // This loops through all of the weapons and individually assigns them to initial groups
        // Most weapon groupings are auto-detected based on AI hints, weapon data, and description strings
        // The remaning groupings are enforced via overrides and configuration parameters
        List<WeaponSlotAPI> slots = variant.getHullSpec().getAllWeaponSlotsCopy();
        Collection<String> fittedSlots = variant.getFittedWeaponSlots();
        for (String fittedSlot : fittedSlots) {
            String weapon = variant.getWeaponId(fittedSlot);
            String type = Global.getSettings().getDescription(weapon, Description.Type.WEAPON).getText2();
            WeaponSlotAPI slot = null;
            for (WeaponSlotAPI slott : slots) {
                if (slott.getId().contentEquals(fittedSlot)) {
                    slot = slott;
                }
            }

            // These are the various weapon properties that are checked when making the groupings
            boolean antiFighter = false;
            boolean pointDefense = false;
            boolean strike = false;
            boolean noAim = false;
            boolean assault = false;
            boolean closeSupport = false;
            boolean fireSupport = false;
            boolean missile = false;
            boolean lowFlux = false;
            boolean highFlux = false;
            boolean hardpoint = false;
            boolean limitedAmmo = false;
            boolean beam = false;
            boolean special = false;
            boolean front = false;
            boolean back = false;
            boolean left = false;
            boolean right = false;
            WeaponSize size = WeaponSize.SMALL;
            DS_WeaponEntry entry = DS_Database.masterWeaponList.get(weapon);

            if (slot != null) {
                if (slot.getArc() <= 15f) {
                    hardpoint = true;
                }
                float angle = slot.getAngle();
                if (angle < -180f) {
                    angle += 360f;
                }
                if (angle > 180f) {
                    angle -= 360f;
                }
                if (angle <= 45f && angle >= -45f) {
                    front = true;
                } else if (angle >= 45f && angle <= 135f) {
                    left = true;
                } else if (angle > 135f || angle < -135f) {
                    back = true;
                } else {
                    right = true;
                }
                size = slot.getSlotSize();
            }
            if (weaponTypeOverride.get("Missile").contains(weapon)) {
                missile = true;
            }
            if (weaponTypeOverride.get("No Aim").contains(weapon)) {
                noAim = true;
            }
            if (weaponTypeOverride.get("Anti-Fighter").contains(weapon)) {
                antiFighter = true;
            }
            if (weaponTypeOverride.get("Point Defense").contains(weapon)) {
                pointDefense = true;
            }
            if (weaponTypeOverride.get("Strike").contains(weapon)) {
                strike = true;
            }
            if (weaponTypeOverride.get("Assault").contains(weapon)) {
                assault = true;
            }
            if (weaponTypeOverride.get("Close Support").contains(weapon)) {
                closeSupport = true;
            }
            if (weaponTypeOverride.get("Fire Support").contains(weapon)) {
                fireSupport = true;
            }
            if (weaponTypeOverride.get("Special").contains(weapon)) {
                special = true;
            }
            if (weaponTypeOverride.get("Low Flux").contains(weapon)) {
                lowFlux = true;
            }
            if (weaponTypeOverride.get("Separate Fire").contains(weapon)) {
                highFlux = true;
            }
            if (weaponTypeOverride.get("Sustained Beam").contains(weapon)) {
                beam = true;
            }
            /*
             if (getWeaponList("Burst Beam").contains(weapon)) { }
             */
            if (weaponTypeOverride.get("Limited Ammo").contains(weapon)) {
                limitedAmmo = true;
            }

            // If "Override" is set for the weapon, skip this auto-detection stuff and only use the overrides
            // Weapons without "Override" set will use both auto-detected stuff and overrides
            if (!weaponTypeOverride.get("Override").contains(weapon)) {
                if (entry != null && entry.type == WeaponType.MISSILE) {
                    missile = true;
                    lowFlux = true;
                }
                if (entry != null && entry.limitedAmmo && missile) {
                    limitedAmmo = true;
                }
                EnumSet<AIHints> hints = variant.getWeaponSpec(fittedSlot).getAIHints();
                for (AIHints hint : hints) {
                    if (hint == AIHints.ANTI_FTR) {
                        antiFighter = true;
                    }
                    if (hint == AIHints.PD || hint == AIHints.PD_ONLY) {
                        pointDefense = true;
                    }
                    if (hint == AIHints.STRIKE) {
                        strike = true;
                    }
                    if (hint == AIHints.DO_NOT_AIM || hint == AIHints.HEATSEEKER) {
                        noAim = true;
                    }
                }
                if (type.contentEquals("Anti-Fighter")) {
                    antiFighter = true;
                }
                if (type.contentEquals("Point Defense")) {
                    pointDefense = true;
                }
                if (type.contentEquals("Strike")) {
                    strike = true;
                }
                if (type.contentEquals("Assault")) {
                    assault = true;
                }
                if (type.contentEquals("Close Support") || type.contentEquals("Suppression")) {
                    closeSupport = true;
                }
                if (type.contentEquals("Fire Support")) {
                    fireSupport = true;
                }
                if (type.contentEquals("Special")) {
                    special = true;
                }
            }

            // This is the logic for assigning weapons to general groups
            // Make sure you know what you are doing before changing this stuff around
            // The order of operations is important!
            if (hardpoint && ((highFlux && !((variant.getHullSize() == HullSize.CRUISER && size == WeaponSize.SMALL) ||
                                             (variant.getHullSize() == HullSize.CAPITAL_SHIP && (size == WeaponSize.SMALL || size == WeaponSize.MEDIUM)))) ||
                              (size == WeaponSize.LARGE && (variant.getHullSize() == HullSize.FRIGATE || variant.getHullSize() == HullSize.DESTROYER)))) {
                if (!beam) {
                    HvyHGroup.addSlot(fittedSlot);
                } else {
                    BeaHGroup.addSlot(fittedSlot);
                }
            } else if (!hardpoint && ((highFlux && !((variant.getHullSize() == HullSize.CRUISER && size == WeaponSize.SMALL) ||
                                                     (variant.getHullSize() == HullSize.CAPITAL_SHIP && (size == WeaponSize.SMALL || size == WeaponSize.MEDIUM)))) ||
                                      (size == WeaponSize.LARGE && (variant.getHullSize() == HullSize.FRIGATE || variant.getHullSize() == HullSize.DESTROYER)))) {
                if (!beam) {
                    HvyTGroup.addSlot(fittedSlot);
                } else {
                    BeaTGroup.addSlot(fittedSlot);
                }
            } else if (missile && left && enforceSides && !noAim && !limitedAmmo) {
                LGroup.addSlot(fittedSlot);
            } else if (missile && right && enforceSides && !noAim && !limitedAmmo) {
                RGroup.addSlot(fittedSlot);
            } else if (missile && !limitedAmmo && !strike && !antiFighter && !pointDefense && (splitMissiles || hardpoint)) {
                SpamMisGroup.addSlot(fittedSlot);
            } else if (missile && (limitedAmmo || hardpoint) && !strike && !pointDefense && !antiFighter) {
                MisGroup.addSlot(fittedSlot);
            } else if (missile && strike) {
                StrMisGroup.addSlot(fittedSlot);
            } else if (pointDefense && (!hardpoint || noAim) && !limitedAmmo) {
                PDGroup.addSlot(fittedSlot);
            } else if (antiFighter) {
                AFGroup.addSlot(fittedSlot);
            } else if (left && (hardpoint || enforceSides) && (!lowFlux || missile)) {
                LGroup.addSlot(fittedSlot);
            } else if (right && (hardpoint || enforceSides) && (!lowFlux || missile)) {
                RGroup.addSlot(fittedSlot);
            } else if (assault && hardpoint && (!lowFlux || missile)) {
                AssHGroup.addSlot(fittedSlot);
            } else if (assault && !hardpoint && (!lowFlux || missile)) {
                AssTGroup.addSlot(fittedSlot);
            } else if (closeSupport && hardpoint && (!lowFlux || missile)) {
                CSHGroup.addSlot(fittedSlot);
            } else if (closeSupport && !hardpoint && (!lowFlux || missile)) {
                CSTGroup.addSlot(fittedSlot);
            } else if (strike || limitedAmmo) {
                if (!beam) {
                    if (hardpoint) {
                        StrHGroup.addSlot(fittedSlot);
                    } else {
                        StrTGroup.addSlot(fittedSlot);
                    }
                } else {
                    if (hardpoint) {
                        BeaHGroup.addSlot(fittedSlot);
                    } else {
                        BeaTGroup.addSlot(fittedSlot);
                    }
                }
            } else if (fireSupport || lowFlux || special) {
                SupGroup.addSlot(fittedSlot);
            } else {
                SupGroup.addSlot(fittedSlot); // This should always be at the end
            }
        }

        // Now we consolidate the general weapon groups into five final weapon groups
        // This is only done if the number of general weapon groups is greater than 5
        // If Alex ever increases the maximum number of weapon groups, this number can be changed
        int groupCount = 0;
        for (WeaponGroupSpec group : groupList) {
            if (!group.getSlots().isEmpty()) {
                groupCount++;
            }
        }

        // Combine groups that the AI can't handle separately
        if (!AssHGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!CSHGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : AssHGroup.getSlots()) {
                    CSHGroup.addSlot(toAdd);
                }
                AssHGroup = null;
            }
        } else {
            AssHGroup = null;
        }
        if (!HvyHGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!StrHGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : HvyHGroup.getSlots()) {
                    StrHGroup.addSlot(toAdd);
                }
                HvyHGroup = null;
            }
        } else {
            HvyHGroup = null;
        }

        // This section is the most difficult part of the algorithm and can make or break large ships like the Onslaught
        // Make sure you know what you are doing when adding logic here
        // Do not pass weapons into a group that may have already become null; NetBeans should warn you about this
        // Order of operations is extremely important
        if (!AFGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!PDGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : AFGroup.getSlots()) {
                    PDGroup.addSlot(toAdd);
                }
                AFGroup = null;
            }
        } else {
            AFGroup = null;
        }
        if (!splitMissiles) {
            if (!StrMisGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!MisGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : StrMisGroup.getSlots()) {
                        MisGroup.addSlot(toAdd);
                    }
                    StrMisGroup = null;
                }
            } else {
                StrMisGroup = null;
            }
        }
        if (!PDGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!SupGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : PDGroup.getSlots()) {
                    SupGroup.addSlot(toAdd);
                }
                PDGroup = null;
            }
        } else {
            PDGroup = null;
        }
        if (!splitMissiles) {
            if (!SpamMisGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!SupGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : SpamMisGroup.getSlots()) {
                        SupGroup.addSlot(toAdd);
                    }
                    SpamMisGroup = null;
                }
            } else {
                SpamMisGroup = null;
            }
        }
        if (!AssTGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!CSTGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : AssTGroup.getSlots()) {
                    CSTGroup.addSlot(toAdd);
                }
                AssTGroup = null;
            }
        } else {
            AssTGroup = null;
        }
        if (!splitBeams) {
            if (!BeaTGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!CSTGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : BeaTGroup.getSlots()) {
                        CSTGroup.addSlot(toAdd);
                    }
                    BeaTGroup = null;
                }
            } else {
                BeaTGroup = null;
            }
            if (!BeaHGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!CSHGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : BeaHGroup.getSlots()) {
                        CSHGroup.addSlot(toAdd);
                    }
                    BeaHGroup = null;
                }
            } else {
                BeaHGroup = null;
            }
        }
        if (!enforceSides) {
            if (!LGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!RGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : LGroup.getSlots()) {
                        RGroup.addSlot(toAdd);
                    }
                    LGroup = null;
                }
            } else {
                LGroup = null;
            }
        }
        if (!CSTGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!SupGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : CSTGroup.getSlots()) {
                    SupGroup.addSlot(toAdd);
                }
                CSTGroup = null;
            }
        } else {
            CSTGroup = null;
        }
        if (!HvyTGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!StrTGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : HvyTGroup.getSlots()) {
                    StrTGroup.addSlot(toAdd);
                }
                HvyTGroup = null;
            }
        } else {
            HvyTGroup = null;
        }
        if (splitMissiles && StrMisGroup != null) {
            if (!StrMisGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!MisGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : StrMisGroup.getSlots()) {
                        MisGroup.addSlot(toAdd);
                    }
                    StrMisGroup = null;
                }
            } else {
                StrMisGroup = null;
            }
        }
        if (splitMissiles && SpamMisGroup != null) {
            if (!SpamMisGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!SupGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : SpamMisGroup.getSlots()) {
                        SupGroup.addSlot(toAdd);
                    }
                    SpamMisGroup = null;
                }
            } else {
                SpamMisGroup = null;
            }
        }
        if (splitBeams && BeaTGroup != null) {
            if (!BeaTGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!StrHGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : BeaTGroup.getSlots()) {
                        StrHGroup.addSlot(toAdd);
                    }
                    BeaTGroup = null;
                }
            } else {
                BeaTGroup = null;
            }
        }
        if (splitBeams && BeaHGroup != null) {
            if (!BeaHGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!CSHGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : BeaHGroup.getSlots()) {
                        CSHGroup.addSlot(toAdd);
                    }
                    BeaHGroup = null;
                }
            } else {
                BeaHGroup = null;
            }
        }
        if (!StrTGroup.getSlots().isEmpty()) {
            if (groupCount > 5) {
                if (!StrHGroup.getSlots().isEmpty()) {
                    groupCount--;
                }
                for (String toAdd : StrTGroup.getSlots()) {
                    StrHGroup.addSlot(toAdd);
                }
                StrTGroup = null;
            }
        } else {
            StrTGroup = null;
        }
        if (enforceSides) {
            if (!CSHGroup.getSlots().isEmpty()) {
                if (groupCount > 5) {
                    if (!SupGroup.getSlots().isEmpty()) {
                        groupCount--;
                    }
                    for (String toAdd : CSHGroup.getSlots()) {
                        SupGroup.addSlot(toAdd);
                    }
                    CSHGroup = null;
                }
            } else {
                CSHGroup = null;
            }
        } else {
            if (CSHGroup.getSlots().isEmpty()) {
                CSHGroup = null;
            }
        }
        if (enforceSides && LGroup != null) {
            if (LGroup.getSlots().isEmpty()) {
                LGroup = null;
            }
        }
        if (RGroup.getSlots().isEmpty()) {
            RGroup = null;
        }
        if (StrHGroup.getSlots().isEmpty()) {
            StrHGroup = null;
        }
        if (SupGroup.getSlots().isEmpty()) {
            SupGroup = null;
        }
        if (MisGroup.getSlots().isEmpty()) {
            MisGroup = null;
        }

        if (variant.getWeaponGroups().size() < 5) {
            for (int i = variant.getWeaponGroups().size(); i < 5; i++) {
                variant.addWeaponGroup(new WeaponGroupSpec());
            }
        }

        // I didn't make a loop for this because the order in which you put these functions determines their order on the ship
        int currentGroup = 0;
        int tries = 0;
        while (groupCount < 5 && tries < 5) {
            if (HvyTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(HvyTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (StrTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(StrTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (BeaTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(BeaTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (AssTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(AssTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (CSTGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(CSTGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (StrMisGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(StrMisGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (MisGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(MisGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (SpamMisGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(SpamMisGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (SupGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(SupGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (AFGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(AFGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            if (PDGroup != null && groupCount < 5) {
                WeaponGroupSpec newSpec = splitGroup(PDGroup, variant);
                if (newSpec != null) {
                    integrateGroup(newSpec, variant.getGroup(currentGroup));
                    currentGroup++;
                    groupCount++;
                }
            }
            tries++;
        }

        if (HvyHGroup != null) {
            integrateGroup(HvyHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (HvyTGroup != null) {
            integrateGroup(HvyTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (StrHGroup != null) {
            integrateGroup(StrHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (StrTGroup != null) {
            integrateGroup(StrTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (BeaHGroup != null) {
            integrateGroup(BeaHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (BeaTGroup != null) {
            integrateGroup(BeaTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (LGroup != null) {
            integrateGroup(LGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (RGroup != null) {
            integrateGroup(RGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (AssHGroup != null) {
            integrateGroup(AssHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (CSHGroup != null) {
            integrateGroup(CSHGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (AssTGroup != null) {
            integrateGroup(AssTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (CSTGroup != null) {
            integrateGroup(CSTGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (StrMisGroup != null) {
            integrateGroup(StrMisGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (MisGroup != null) {
            integrateGroup(MisGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (SpamMisGroup != null) {
            integrateGroup(SpamMisGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (SupGroup != null) {
            integrateGroup(SupGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (AFGroup != null) {
            integrateGroup(AFGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }
        if (PDGroup != null) {
            integrateGroup(PDGroup, variant.getGroup(currentGroup));
            currentGroup++;
        }

        return variant;
    }

    private static List<String> createWeaponList(String[] variants) {
        return Collections.unmodifiableList(Arrays.asList(variants));
    }

    private static void integrateGroup(WeaponGroupSpec source, WeaponGroupSpec destination) {
        for (String slot : source.getSlots()) {
            destination.addSlot(slot);
        }
        destination.setType(source.getType());
        destination.setAutofireOnByDefault(source.isAutofireOnByDefault());
    }

    private static WeaponGroupSpec splitGroup(WeaponGroupSpec spec, ShipVariantAPI variant) {
        if (DO_NOT_SPLIT.contains(variant.getHullSpec().getHullId())) {
            return null;
        }

        Set<String> idSet = new LinkedHashSet<>(20);
        for (String slot : spec.getSlots()) {
            idSet.add(variant.getWeaponId(slot));
        }
        if (idSet.size() > 1) {
            int i = (int) Math.ceil(idSet.size() / 2f);
            Iterator<String> iter = idSet.iterator();
            while (iter.hasNext()) {
                iter.next();
                if (i > 0) {
                    i--;
                } else {
                    iter.remove();
                }
            }

            WeaponGroupSpec newSpec = new WeaponGroupSpec();
            newSpec.setType(spec.getType());
            newSpec.setAutofireOnByDefault(spec.isAutofireOnByDefault());

            iter = spec.getSlots().iterator();
            while (iter.hasNext()) {
                String slot = iter.next();
                if (idSet.contains(variant.getWeaponId(slot))) {
                    newSpec.addSlot(slot);
                    iter.remove();
                }
            }

            return newSpec;
        } else {
            return null;
        }
    }

    public enum WeaponGroupConfig {

        STANDARD, MISSILE, BROADSIDE, MISSILE_BROADSIDE, EXIGENCY, LINKED_STRIKE, MORPHEUS
    }
}
