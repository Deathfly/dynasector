package data.scripts.util;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI.ShipEngineAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import db.twiglib.TwigUtils;
import java.util.ArrayList;
import java.util.List;

public class DS_Twig {

    public static ShipAPI empTargetTwig(ShipAPI ship) {
        if (DSModPlugin.hasTwigLib) {
            if (!TwigUtils.isMultiShip(ship)) {
                return ship;
            }

            ShipAPI root = TwigUtils.getRoot(ship);
            List<ShipAPI> children = TwigUtils.getChildNodesAsShips(root);
            children.add(root);

            WeightedRandomPicker<ShipAPI> targetPicker = new WeightedRandomPicker<>();
            for (ShipAPI child : children) {
                if (!child.isAlive()) {
                    continue;
                }
                int weight = 0;
                for (WeaponAPI weapon : child.getAllWeapons()) {
                    if (!weapon.getSlot().isHidden() && !weapon.getSlot().isDecorative()) {
                        weight++;
                    }
                }
                for (ShipEngineAPI engine : child.getEngineController().getShipEngines()) {
                    if (!engine.isSystemActivated()) {
                        weight++;
                    }
                }
                targetPicker.add(child, weight);
            }

            if (targetPicker.getItems().size() > 0) {
                return targetPicker.pick();
            } else {
                return ship;
            }
        } else {
            return ship;
        }
    }

    public static List<ShipAPI> getChildren(ShipAPI ship) {
        if (DSModPlugin.hasTwigLib) {
            List<ShipAPI> children = TwigUtils.getChildNodesAsShips(ship);
            if (children == null) {
                return new ArrayList<>(0);
            } else {
                return children;
            }
        } else {
            return new ArrayList<>(0);
        }
    }

    public static boolean hasChildren(ShipAPI ship) {
        if (DSModPlugin.hasTwigLib) {
            return TwigUtils.getNumberOfChildNodes(ship) > 0;
        } else {
            return true;
        }
    }
}
