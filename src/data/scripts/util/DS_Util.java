package data.scripts.util;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import data.scripts.DSModPlugin;
import java.awt.Color;
import java.util.List;
import java.util.TreeMap;

public class DS_Util {

    static final TreeMap<Integer, String> romanMap = new TreeMap<>();

    static {
        romanMap.put(1000, "M");
        romanMap.put(900, "CM");
        romanMap.put(500, "D");
        romanMap.put(400, "CD");
        romanMap.put(100, "C");
        romanMap.put(90, "XC");
        romanMap.put(50, "L");
        romanMap.put(40, "XL");
        romanMap.put(10, "X");
        romanMap.put(9, "IX");
        romanMap.put(5, "V");
        romanMap.put(4, "IV");
        romanMap.put(1, "I");
    }

    public static int calculatePowerLevel(CampaignFleetAPI fleet) {
        int power = fleet.getFleetPoints();
        int offLvl = 0;
        int cdrLvl = 0;
        boolean commander = false;
        for (OfficerDataAPI officer : fleet.getFleetData().getOfficersCopy()) {
            if (officer.getPerson() == fleet.getCommander()) {
                commander = true;
                cdrLvl = officer.getPerson().getStats().getLevel();
            } else {
                offLvl += officer.getPerson().getStats().getLevel();
            }
        }
        if (!commander) {
            cdrLvl = fleet.getCommanderStats().getLevel();
        }
        power *= Math.sqrt(cdrLvl / 100f + 1f);
        int flatBonus = cdrLvl + offLvl + 10;
        if (power < flatBonus * 2) {
            flatBonus *= power / (float) (flatBonus * 2);
        }
        power += flatBonus;
        return power;
    }

    public static Color colorBlend(Color a, Color b, float amount) {
        float conjAmount = 1f - amount;
        return new Color((int) Math.max(0, Math.min(255, a.getRed() * conjAmount + b.getRed() * amount)),
                         (int) Math.max(0, Math.min(255, a.getGreen() * conjAmount + b.getGreen() * amount)),
                         (int) Math.max(0, Math.min(255, a.getBlue() * conjAmount + b.getBlue() * amount)),
                         (int) Math.max(0, Math.min(255, a.getAlpha() * conjAmount + b.getAlpha() * amount)));
    }

    public static Color colorJitter(Color color, float amount) {
        return new Color(Math.max(0, Math.min(255, color.getRed() + (int) (((float) Math.random() - 0.5f) * amount))),
                         Math.max(0, Math.min(255, color.getGreen() + (int) (((float) Math.random() - 0.5f) * amount))),
                         Math.max(0, Math.min(255, color.getBlue() + (int) (((float) Math.random() - 0.5f) * amount))),
                         color.getAlpha());
    }

    public static float getEconAvailability(String commodityId) {
        float commodityDemand = 0f;
        float commoditySupply = 0f;

        List<MarketAPI> allMarkets = Global.getSector().getEconomy().getMarketsCopy();
        for (MarketAPI market : allMarkets) {
            for (CommodityOnMarketAPI commodity : market.getAllCommodities()) {
                String id = commodity.getId();
                if (!id.contentEquals(commodityId)) {
                    continue;
                }

                commodity.getCommodity().isExotic();
                commodityDemand += commodity.getDemand().getDemandValue();
                commoditySupply += commodity.getSupplyValue() * commodity.getUtilityOnMarket();
            }
        }

        if (commodityDemand > 1f) {
            return commoditySupply / commodityDemand;
        } else {
            return 0f;
        }
    }

    public static int getNonCivilianFleetPoints(FleetDataAPI fleetData) {
        int fp = 0;
        for (FleetMemberAPI member : fleetData.getMembersListCopy()) {
            if (member.isMothballed() || member.isCivilian() || member.getHullSpec().getHints().contains(ShipTypeHints.CIVILIAN)) {
                continue;
            }
            fp += member.getFleetPointCost();
        }
        return Math.max(fp, 1);
    }

    public static boolean isValidRoman(String num) {
        for (int k = 0; k < num.length(); k++) {
            if (num.charAt(k) != 'I' && num.charAt(k) != 'V' && num.charAt(k) != 'X' && num.charAt(k) != 'L' && num.charAt(k) != 'C' && num.charAt(k) != 'D' &&
                    num.charAt(k) != 'M') {
                return false;
            }
        }
        return true;
    }

    public static float lerp(float x, float y, float alpha) {
        return (1f - alpha) * x + alpha * y;
    }

    /* Pyrolistical on StackExchange */
    public static double roundToSignificantFigures(double num, int n) {
        if (num == 0) {
            return 0;
        }

        final double d = Math.ceil(Math.log10(num < 0 ? -num : num));
        final int power = n - (int) d;

        final double magnitude = Math.pow(10, power);
        final long shifted = Math.round(num * magnitude);
        return shifted / magnitude;
    }

    public static long roundToSignificantFiguresLong(double num, int n) {
        return Math.round(roundToSignificantFigures(num, n));
    }

    public static final String toRoman(int number) {
        int lower = romanMap.floorKey(number);
        if (number == lower) {
            return romanMap.get(number);
        }
        return romanMap.get(lower) + toRoman(number - lower);
    }

    public static enum RequiredFaction {

        NONE, JUNK_PIRATES, TIANDONG, SHADOWYARDS, IMPERIUM, TEMPLARS, DIABLE, BLACKROCK, CITADEL, EXIGENCY, MAYORATE, SCY;

        public boolean isLoaded() {
            switch (this) {
                case JUNK_PIRATES:
                    return DSModPlugin.junkPiratesExists;
                case TIANDONG:
                    return DSModPlugin.tiandongExists;
                case SHADOWYARDS:
                    return DSModPlugin.shadowyardsExists;
                case IMPERIUM:
                    return DSModPlugin.imperiumExists;
                case TEMPLARS:
                    return DSModPlugin.templarsExists;
                case DIABLE:
                    return DSModPlugin.diableExists;
                case BLACKROCK:
                    return DSModPlugin.blackrockExists;
                case CITADEL:
                    return DSModPlugin.citadelExists;
                case EXIGENCY:
                    return DSModPlugin.exigencyExists;
                case MAYORATE:
                    return DSModPlugin.mayorateExists;
                case SCY:
                    return DSModPlugin.scyExists;
                case NONE:
                default:
                    return true;
            }
        }
    }
}
