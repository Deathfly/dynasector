package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.thoughtworks.xstream.XStream;
import data.scripts.campaign.DS_Profiler;
import data.scripts.campaign.fleets.DS_FleetInjector;
import data.scripts.campaign.submarkets.DS_BlackMarketPlugin;
import data.scripts.campaign.submarkets.DS_MilitarySubmarketPlugin;
import data.scripts.campaign.submarkets.DS_OpenMarketPlugin;
import data.scripts.variants.DS_Database;
import java.io.IOException;
import org.apache.log4j.Level;
import org.json.JSONException;
import org.json.JSONObject;

public class DSModPlugin extends BaseModPlugin {

    public static boolean Module_FleetIntegration = true;
    public static boolean Module_LeveledAICommanders = true;
    public static boolean Module_MarketIntegration = true;
    public static boolean Module_NPCCrewVeterancy = false;
    public static boolean Module_ProceduralVariants = false;
    public static boolean Module_RareShips = false;
    public static boolean Module_ScaledVariants = true;

    public static boolean SHOW_DEBUG_INFO = false;

    public static boolean blackrockExists = false;
    public static boolean checkMemory = false;
    public static boolean citadelExists = false;
    public static boolean diableExists = false;
    public static boolean exigencyExists = false;
    public static boolean hasSSP = false;
    public static boolean hasSWP = false;
    public static boolean hasStarsectorPlus = false;
    public static boolean hasTwigLib = false;
    public static boolean hasUnderworld = false;
    public static boolean imperiumExists = false;
    public static boolean isExerelin = false;
    public static boolean junkPiratesExists = false;
    public static boolean mayorateExists = false;
    public static boolean scyExists = false;
    public static boolean shadowyardsExists = false;
    public static boolean templarsExists = false;
    public static boolean tiandongExists = false;

    private static final String SETTINGS_FILE = "DYNASECTOR_OPTIONS.ini";

    public static void syncDSScripts() {
        if (!Global.getSector().hasScript(DS_Profiler.class)) {
            Global.getSector().addScript(new DS_Profiler());
        }
    }

    public static void syncDSScriptsExerelin() {
        if (!Global.getSector().hasScript(DS_Profiler.class)) {
            Global.getSector().addScript(new DS_Profiler());
        }
    }

    private static void loadSettings() throws IOException, JSONException {
        JSONObject settings = Global.getSettings().loadJSON(SETTINGS_FILE);

        Module_FleetIntegration = settings.getBoolean("fleetIntegration");
        Module_LeveledAICommanders = settings.getBoolean("leveledAICommanders");
        Module_MarketIntegration = settings.getBoolean("marketIntegration");
        Module_NPCCrewVeterancy = settings.getBoolean("npcCrewVeterancy");
        Module_ProceduralVariants = settings.getBoolean("proceduralVariants");
        Module_RareShips = settings.getBoolean("rareShips");
        Module_ScaledVariants = settings.getBoolean("scaledVariants");

        checkMemory = settings.getBoolean("checkMemory");
    }

    @Override
    public void configureXStream(XStream x) {
        x.alias("DS_FleetInjector", DS_FleetInjector.class);
        x.alias("DS_BlackMarketPlugin", DS_BlackMarketPlugin.class);
        x.alias("DS_MilitarySubmarketPlugin", DS_MilitarySubmarketPlugin.class);
        x.alias("DS_OpenMarketPlugin", DS_OpenMarketPlugin.class);
        x.alias("DS_FleetProfiler", DS_Profiler.class);
        DS_ModPluginAlt.configureModdedXStream(x);
    }

    @Override
    public void onApplicationLoad() throws Exception {
        boolean hasLazyLib = Global.getSettings().getModManager().isModEnabled("lw_lazylib");
        if (!hasLazyLib) {
            throw new RuntimeException("DynaSector requires LazyLib!");
        }

        isExerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        hasTwigLib = Global.getSettings().getModManager().isModEnabled("ztwiglib");
        hasSSP = Global.getSettings().getModManager().isModEnabled("dr_ssp");
        hasSWP = Global.getSettings().getModManager().isModEnabled("swp");
        hasUnderworld = Global.getSettings().getModManager().isModEnabled("underworld");

        try {
            loadSettings();
        } catch (IOException | JSONException e) {
            Global.getLogger(DSModPlugin.class).log(Level.ERROR, "Settings loading failed! " + e.getMessage());
        }

        if (Global.getSettings().getModManager().isModEnabled("dr_ssp")) {
            String[] verString = Global.getSettings().getModManager().getModSpec("dr_ssp").getVersion().split("\\.");
            int major = Integer.parseInt(verString[0]);
            int minor = Integer.parseInt(verString[1]);
            if (major <= 3 && minor <= 4) {
                throw new RuntimeException("Ship/Weapon Pack is not compatible with Starsector+ " +
                        Global.getSettings().getModManager().getModSpec("dr_ssp").getVersion());
            }
        }

        DS_Database.init();

        imperiumExists = Global.getSettings().getModManager().isModEnabled("Imperium");
        templarsExists = Global.getSettings().getModManager().isModEnabled("Templars");
        blackrockExists = Global.getSettings().getModManager().isModEnabled("blackrock_driveyards");
        exigencyExists = Global.getSettings().getModManager().isModEnabled("exigency");
        citadelExists = Global.getSettings().getModManager().isModEnabled("Citadel");
        shadowyardsExists = Global.getSettings().getModManager().isModEnabled("shadow_ships");
        mayorateExists = Global.getSettings().getModManager().isModEnabled("mayorate");
        junkPiratesExists = Global.getSettings().getModManager().isModEnabled("junk_pirates_release");
        scyExists = Global.getSettings().getModManager().isModEnabled("SCY");
        tiandongExists = Global.getSettings().getModManager().isModEnabled("THI");
        diableExists = Global.getSettings().getModManager().isModEnabled("diableavionics");
    }

    @Override
    public void onGameLoad(boolean newGame) {
        if (!newGame) {
            Boolean inUse = (Boolean) Global.getSector().getPersistentData().get("DS_in_use");
            if (inUse == null || inUse == false) {
                throw new RuntimeException("You cannot add DynaSector to an existing campaign!");
            }
        }

        if (isExerelin) {
            syncDSScriptsExerelin();
        } else {
            syncDSScripts();
        }

        if (Global.getSector().getPersistentData().containsKey("DS_initialized")) {
            Global.getSector().getPersistentData().remove("DS_initialized");
        } else {
            Global.getSector().addTransientListener(new DS_FleetInjector());
        }

        DS_Database.checkForUnusedFighters(Global.getSettings());
        DS_Database.checkForUnusedHulls(Global.getSettings());
        DS_Database.checkForUnusedWeapons(Global.getSettings());
    }

    @Override
    public void onNewGame() {
        Global.getSector().getPersistentData().put("DS_in_use", true);
        Global.getSector().addTransientListener(new DS_FleetInjector());
        Global.getSector().getPersistentData().put("DS_initialized", true);

        if (isExerelin) {
            syncDSScriptsExerelin();
        } else {
            syncDSScripts();
        }
    }
}
