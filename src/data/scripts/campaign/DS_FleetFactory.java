package data.scripts.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParams;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import com.fs.starfarer.api.loading.FleetCompositionDoctrineAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.DS_Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.add12PointGroup;
import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.add16PointGroup;
import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.add8PointGroup;
import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.addCommanderAndOfficers;
import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.addRandomCombatFreighters;
import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.addRandomCombatShips;
import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.addRandomShips;
import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.createEmptyFleet;
import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.pickMarket;
import static com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2.prune;
import static data.scripts.variants.DS_FleetRandomizer.PRIORITY;

public class DS_FleetFactory extends FleetFactoryV2 {

    public static final Set<String> BANNED_HULLS = new HashSet<>(12);

    static {
        //BANNED_HULLS.add("ssp_cathedral");
        //BANNED_HULLS.add("ssp_infernalmachine");
        BANNED_HULLS.add("ssp_superhyperion");
        BANNED_HULLS.add("ssp_oberon");
        BANNED_HULLS.add("ssp_ultron");
        BANNED_HULLS.add("ssp_zeus");
        BANNED_HULLS.add("ssp_ezekiel");
        BANNED_HULLS.add("ssp_cristarium");
        BANNED_HULLS.add("ssp_zero");
        BANNED_HULLS.add("ssp_superzero");
        BANNED_HULLS.add("ssp_hyperzero");
        BANNED_HULLS.add("ssp_archangel");
    }

    public static float add12PointGroupCarrier(float qf, CampaignFleetAPI fleet, Random random, MarketAPI market, boolean capitalOk, float allowedOverflow,
                                               FleetParams params) {
        FleetCompositionDoctrineAPI doctrine = fleet.getFaction().getCompositionDoctrine();

        WeightedRandomPicker<Integer> carrierSize = new WeightedRandomPicker<>(random);
        carrierSize.add(1, doctrine.getSmallCarrierProbability() * .33f);
        carrierSize.add(2, doctrine.getMediumCarrierProbability());
        Integer size = carrierSize.pick();

        float cost = 0;
        if (size >= 2) {
            cost += addToFleet(ShipRoles.CARRIER_MEDIUM, market, qf, random, fleet);
        } else {
            float added = 0;
            int attempts = 0;
            while (added < 4 && attempts < 4) {
                added += addToFleet(ShipRoles.CARRIER_SMALL, market, qf, random, fleet);
                attempts++;
            }
            cost += added;
        }
        cost += addRandomFighters(9f - cost, qf, fleet, random, market, params);
        return cost + addRandomCombatShips(12f - cost, qf, fleet, random, market, capitalOk, false, allowedOverflow, params);
    }

    public static float add16PointGroupCarrier(float qf, CampaignFleetAPI fleet, Random random, MarketAPI market, boolean capitalOk, boolean largeCarrierOk,
                                               float allowedOverflow, FleetParams params) {
        FleetCompositionDoctrineAPI doctrine = fleet.getFaction().getCompositionDoctrine();

        WeightedRandomPicker<Integer> carrierSize = new WeightedRandomPicker<>(random);
        if (doctrine.getSmallCarrierProbability() > 0) {
            carrierSize.add(1, doctrine.getSmallCarrierProbability() * .17f);
        }
        if (doctrine.getMediumCarrierProbability() > 0) {
            carrierSize.add(2, doctrine.getMediumCarrierProbability() * .33f);
        }
        if (largeCarrierOk && doctrine.getLargeCarrierProbability() > 0) {
            carrierSize.add(3, doctrine.getLargeCarrierProbability());
        }

        if (!carrierSize.isEmpty()) {
            Integer size = carrierSize.pick();
            float cost = 0f;
            if (size >= 3 && largeCarrierOk) {
                cost += addToFleet(ShipRoles.CARRIER_LARGE, market, qf, random, fleet);
            } else {
                if (size >= 2) {
                    float added = 0;
                    int attempts = 0;
                    while (added < 8 && attempts < 4) {
                        added += addToFleet(ShipRoles.CARRIER_MEDIUM, market, qf, random, fleet);
                        attempts++;
                    }
                    cost += added;
                } else {
                    float added = 0;
                    int attempts = 0;
                    added += addToFleet(ShipRoles.CARRIER_MEDIUM, market, qf, random, fleet);
                    while (added < 8 && attempts < 4) {
                        added += addToFleet(ShipRoles.CARRIER_SMALL, market, qf, random, fleet);
                        attempts++;
                    }
                    cost += added;
                }
            }
            return cost + addRandomFighters(16f - cost, qf, fleet, random, market, params);
        } else {
            return addRandomCombatShips(16f, qf, fleet, random, market, capitalOk, true, allowedOverflow, params);
        }
    }

    public static float add8PointGroupCarrier(float qf, CampaignFleetAPI fleet, Random random, MarketAPI market, boolean capitalOk, float allowedOverflow,
                                              FleetParams params) {
        float cost = addToFleet(ShipRoles.CARRIER_SMALL, market, qf, random, fleet);
        cost += addRandomFighters(5f - cost, qf, fleet, random, market, params);
        return cost + addRandomCombatShips(8f - cost, qf, fleet, random, market, capitalOk, false, allowedOverflow, params);
    }

    public static CampaignFleetAPI createCarrierFleet(FleetParams params) {
        Global.getSettings().profilerBegin("FleetFactoryV2.createCarrierFleet()");
        try {

            MarketAPI market = pickMarket(params);
            if (market == null) {
                return null;
            }

            String factionId = params.factionId;
            if (params.factionIdForShipPicking != null) {
                factionId = params.factionIdForShipPicking;
            }
            CampaignFleetAPI fleet = createEmptyFleet(factionId, params.fleetType, market);

            FleetCompositionDoctrineAPI doctrine = fleet.getFaction().getCompositionDoctrine();

            float qf = market.getShipQualityFactor();
            qf += params.qualityBonus;
            if (params.qualityOverride >= 0) {
                qf = params.qualityOverride;
            }

            Random random = new Random();

            float combatPts = params.combatPts;
            float carrierPts = 0f;
            float freighterPts = params.freighterPts;
            float tankerPts = params.tankerPts;
            float transportPts = params.transportPts;
            float linerPts = params.linerPts;
            float civilianPts = params.civilianPts;
            float utilityPts = params.utilityPts;

            boolean capitalOk = combatPts >= doctrine.getMinPointsForCombatCapital();
            boolean largeCarrierOk = combatPts >= doctrine.getMinPointsForLargeCarrier();

            boolean cfFail = false;
            while (combatPts > 0) {
                if (!cfFail && freighterPts > 0 && random.nextFloat() < doctrine.getCombatFreighterProbability()) {
                    float pts = Math.min(freighterPts, combatPts);
                    if (pts > 8) {
                        pts = 8;
                    }
                    float added = addRandomCombatFreighters(pts, qf, fleet, random, market);
                    combatPts -= added / 2f;
                    freighterPts -= added / 2f;
                    cfFail |= added < pts / 2f;
                    continue;
                }
                if (combatPts >= 16f) {
                    float pts;
                    if (carrierPts < combatPts * 0.5f) {
                        pts = add16PointGroupCarrier(qf, fleet, random, market, capitalOk, largeCarrierOk, combatPts - 16f, params);
                        carrierPts += pts;
                    } else {
                        pts = add16PointGroup(qf, fleet, random, market, capitalOk, largeCarrierOk, combatPts - 16f, params);
                    }
                    combatPts -= pts;
                } else if (combatPts >= 12f) {
                    float pts;
                    if (carrierPts < combatPts * 0.5f) {
                        pts = add12PointGroupCarrier(qf, fleet, random, market, capitalOk, combatPts - 12f, params);
                        carrierPts += pts;
                    } else {
                        pts = add12PointGroup(qf, fleet, random, market, capitalOk, combatPts - 12f, params);
                    }
                    combatPts -= pts;
                } else if (combatPts >= 8f) {
                    float pts;
                    if (carrierPts < combatPts * 0.5f) {
                        pts = add8PointGroupCarrier(qf, fleet, random, market, capitalOk, combatPts - 8f, params);
                        carrierPts += pts;
                    } else {
                        pts = add8PointGroup(qf, fleet, random, market, capitalOk, combatPts - 8f, params);
                    }
                    combatPts -= pts;
                } else {
                    float pts = addRandomCombatShips(combatPts, qf, fleet, random, market, capitalOk, true, 0f, params);
                    combatPts -= pts;
                }
            }

            addRandomShips(freighterPts, qf, fleet, random, market,
                           ShipRoles.FREIGHTER_SMALL,
                           ShipRoles.FREIGHTER_MEDIUM,
                           ShipRoles.FREIGHTER_LARGE);

            addRandomShips(tankerPts, qf, fleet, random, market,
                           ShipRoles.TANKER_SMALL,
                           ShipRoles.TANKER_MEDIUM,
                           ShipRoles.TANKER_LARGE);

            addRandomShips(transportPts, qf, fleet, random, market,
                           ShipRoles.PERSONNEL_SMALL,
                           ShipRoles.PERSONNEL_MEDIUM,
                           ShipRoles.PERSONNEL_LARGE);

            addRandomShips(linerPts, qf, fleet, random, market,
                           ShipRoles.LINER_SMALL,
                           ShipRoles.LINER_MEDIUM,
                           ShipRoles.LINER_LARGE);

            addRandomShips(utilityPts, qf, fleet, random, market, ShipRoles.CIV_RANDOM);
            addRandomShips(civilianPts, qf, fleet, random, market, ShipRoles.UTILITY);

            fleet.getFleetData().sort();

            float minLevel = doctrine.getOfficerLevelBase() + doctrine.getOfficerLevelPerPoint() * params.getTotalPts();
            minLevel *= market.getStats().getDynamic().getValue(Stats.OFFICER_LEVEL_MULT);
            minLevel += params.officerLevelBonus;
            minLevel -= minLevel * doctrine.getOfficerLevelVariance() * 0.5f;
            minLevel = (int) minLevel;
            if (minLevel < 1) {
                minLevel = 1;
            }

            float maxLevel = minLevel + minLevel * doctrine.getOfficerLevelVariance();
            maxLevel = (int) maxLevel;
            if (maxLevel < minLevel) {
                maxLevel = minLevel;
            }
            if (maxLevel > params.levelLimit) {
                maxLevel = params.levelLimit;
            }
            if (minLevel > params.levelLimit) {
                minLevel = params.levelLimit;
            }

            float count = (doctrine.getOfficersPerPoint() * params.getTotalPts()) * params.officerNumMult;
            count *= market.getStats().getDynamic().getValue(Stats.OFFICER_NUM_MULT);
            if (params.officerNumMult > 1 && count < 1) {
                count = 1;
            }
            if (count > 0 && count < 1) {
                count = 1;
            }
            int max = (int) Global.getSettings().getFloat("officerPlayerMax");
            if (count > max) {
                count = max;
            }

            prune(fleet, (int) (params.getTotalPts() * 6), random);

            List<FleetMemberAPI> members = fleet.getFleetData().getMembersListCopy();
            boolean hasShip = false;
            for (FleetMemberAPI member : members) {
                if (!member.isFighterWing()) {
                    hasShip = true;
                    break;
                }
            }
            if (!hasShip) {
                addRandomShips(1f, qf, fleet, random, market, ShipRoles.COMBAT_SMALL);
            }

            addCommanderAndOfficers((int) count, minLevel, maxLevel, fleet, params.commander, random);

            fleet.forceSync();

            if (fleet.getFleetData().getNumMembers() <= 0 ||
                    fleet.getFleetData().getNumMembers() == fleet.getNumFighters()) {
                return null;
            }

            for (FleetMemberAPI member : members) {
                member.getRepairTracker().setCR(member.getRepairTracker().getMaxCR());
            }

            return fleet;
        } finally {
            Global.getSettings().profilerEnd();
        }
    }

    public static CampaignFleetAPI enhancedCreateFleet(FactionAPI faction, int fleetSize, FleetFactoryDelegate delegate) {
        FleetCompositionDoctrineAPI doctrine = faction.getCompositionDoctrine();
        float preInterceptors = doctrine.getInterceptors();
        float preFighters = doctrine.getFighters();
        float preBombers = doctrine.getBombers();
        float preSmall = doctrine.getSmall();
        float preFast = doctrine.getFast();
        float preMedium = doctrine.getMedium();
        float preLarge = doctrine.getLarge();
        float preCapital = doctrine.getCapital();
        float preSmallCarrierProbability = doctrine.getSmallCarrierProbability();
        float preMediumCarrierProbability = doctrine.getMediumCarrierProbability();
        float preLargeCarrierProbability = doctrine.getLargeCarrierProbability();

        if (fleetSize > 25 && fleetSize <= 50) {
            doctrine.setInterceptors(preInterceptors * 0.5f);
            doctrine.setFighters(preFighters * 0.5f);
            doctrine.setBombers(preBombers * 0.5f);
            doctrine.setSmall(preSmall * 0.5f);
            doctrine.setFast(preFast * 0.5f);
            doctrine.setMedium(preMedium);
            doctrine.setLarge(preLarge * 1.25f);
            doctrine.setCapital(preCapital * 1.5f);
            doctrine.setSmallCarrierProbability(preSmallCarrierProbability * 0.8f);
            doctrine.setMediumCarrierProbability(preMediumCarrierProbability * 0.9f);
            doctrine.setLargeCarrierProbability(preLargeCarrierProbability);
        } else if (fleetSize > 50 && fleetSize <= 100) {
            doctrine.setInterceptors(preInterceptors * 0.25f);
            doctrine.setFighters(preFighters * 0.25f);
            doctrine.setBombers(preBombers * 0.25f);
            doctrine.setSmall(preSmall * 0.25f);
            doctrine.setFast(preFast * 0.25f);
            doctrine.setMedium(preMedium * 0.75f);
            doctrine.setLarge(preLarge);
            doctrine.setCapital(preCapital * 1.25f);
            doctrine.setSmallCarrierProbability(preSmallCarrierProbability * 0.5f);
            doctrine.setMediumCarrierProbability(preMediumCarrierProbability * 0.65f);
            doctrine.setLargeCarrierProbability(preLargeCarrierProbability * 0.8f);
        } else if (fleetSize > 100) {
            doctrine.setInterceptors(preInterceptors * 0.125f);
            doctrine.setFighters(preFighters * 0.125f);
            doctrine.setBombers(preBombers * 0.125f);
            doctrine.setSmall(preSmall * 0.125f);
            doctrine.setFast(preFast * 0.125f);
            doctrine.setMedium(preMedium * 0.375f);
            doctrine.setLarge(preLarge * 0.75f);
            doctrine.setCapital(preCapital);
            doctrine.setSmallCarrierProbability(preSmallCarrierProbability * 0.2f);
            doctrine.setMediumCarrierProbability(preMediumCarrierProbability * 0.4f);
            doctrine.setLargeCarrierProbability(preLargeCarrierProbability * 0.6f);
        }

        CampaignFleetAPI fleet = delegate.createFleet();

        doctrine.setInterceptors(preInterceptors);
        doctrine.setFighters(preFighters);
        doctrine.setBombers(preBombers);
        doctrine.setSmall(preSmall);
        doctrine.setFast(preFast);
        doctrine.setMedium(preMedium);
        doctrine.setLarge(preLarge);
        doctrine.setCapital(preCapital);
        doctrine.setSmallCarrierProbability(preSmallCarrierProbability);
        doctrine.setMediumCarrierProbability(preMediumCarrierProbability);
        doctrine.setLargeCarrierProbability(preLargeCarrierProbability);

        return fleet;
    }

    public static void finishFleet(CampaignFleetAPI fleet, String faction, FleetMemberAPI... preserve) {
        Set<FleetMemberAPI> preserveShips = new HashSet<>(0);
        preserveShips.addAll(Arrays.asList(preserve));
        if (fleet.isValidPlayerFleet()) {
            List<FleetMemberAPI> members = fleet.getFleetData().getMembersListCopy();
            Collections.sort(members, PRIORITY);
            FleetMemberAPI flagship = fleet.getFlagship();
            fleet.getFleetData().clear();
            for (FleetMemberAPI member : members) {
                fleet.getFleetData().addFleetMember(member);
            }
            fleet.getFleetData().setFlagship(flagship);

            for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
                if (BANNED_HULLS.contains(member.getHullId()) && !preserveShips.contains(member)) {
                    fleet.getFleetData().removeFleetMember(member);
                    continue;
                }

                if (member.getShipName() != null) {
                    String[] words = member.getShipName().split(" ");
                    if (words.length > 1) {
                        String lastWord = words[words.length - 1];
                        if (DS_Util.isValidRoman(lastWord)) {
                            String name = words[0];
                            if (name.contentEquals(fleet.getFaction().getEntityNamePrefix())) {
                                name = "";
                            }
                            for (int i = 1; i < words.length - 1; i++) {
                                if (name.isEmpty()) {
                                    name += words[i];
                                } else {
                                    name += " " + words[i];
                                }
                            }
                            name += getNumerals(fleet);
                            member.setShipName(name);
                        }
                    }
                }

                member.getRepairTracker().setCR(member.getRepairTracker().getMaxCR());
            }
            fleet.updateCounts();
        }
    }

    public static void finishFleetNonIntrusive(CampaignFleetAPI fleet, String faction) {
        List<FleetMemberAPI> members = new ArrayList<>(fleet.getFleetData().getNumMembers());
        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            members.add(member);
        }
        for (FleetMemberAPI member : members) {
            fleet.getFleetData().removeFleetMember(member);
        }
        Collections.sort(members, PRIORITY);
        for (FleetMemberAPI member : members) {
            fleet.getFleetData().addFleetMember(member);
        }

        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            if (member.getShipName() != null) {
                String[] words = member.getShipName().split(" ");
                if (words.length > 1) {
                    String lastWord = words[words.length - 1];
                    if (DS_Util.isValidRoman(lastWord)) {
                        String name = words[0];
                        if (name.contentEquals(fleet.getFaction().getEntityNamePrefix())) {
                            name = "";
                        }
                        for (int i = 1; i < words.length - 1; i++) {
                            if (name.isEmpty()) {
                                name += words[i];
                            } else {
                                name += " " + words[i];
                            }
                        }
                        name += getNumerals(fleet);
                        member.setShipName(name);
                    }
                }
            }

            member.getRepairTracker().setCR(member.getRepairTracker().getMaxCR());
        }
    }

    public static String getNumerals(CampaignFleetAPI fleet) {
        String numerals = "";
        int number = 1;
        switch (fleet.getFaction().getId()) {
            case Factions.TRITACHYON:
            case "diableavionics":
            case "citadeldefenders":
            case "blackrock_driveyards":
            case "exigency":
            case "shadow_industry": {
                while (Math.random() <= 0.33) {
                    number++;
                }
                if (number <= 1) {
                    return numerals;
                }
                numerals = "-" + String.format("%03d", number);
                break;
            }
            case "tiandong": {
                while (Math.random() <= 0.33 || number == 4) {
                    number++;
                }
                if (number <= 1) {
                    return numerals;
                }
                numerals = "-" + String.format("%03d", number);
                break;
            }
            default: {
                while (Math.random() <= 0.33) {
                    number++;
                }
                if (number <= 1) {
                    return numerals;
                }
                numerals = " " + DS_Util.toRoman(number);
                break;
            }
        }
        return numerals;
    }

    public interface FleetFactoryDelegate {

        public CampaignFleetAPI createFleet();
    }
}
