package data.scripts.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.characters.SkillSpecAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.plugins.OfficerLevelupPlugin;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.variants.DS_FleetRandomizer;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;

public final class DS_LevelUpper {

    public static Logger log = Global.getLogger(DS_LevelUpper.class);

    public static PersonAPI createCommander(FactionAPI faction, int level, PersonAPI commander) {
        PersonAPI person;
        if (commander != null) {
            person = commander;
            person.getStats().addXP(-person.getStats().getXP());
            for (String skillId : Global.getSettings().getSortedSkillIds()) {
                SkillSpecAPI skill = Global.getSettings().getSkillSpec(skillId);
                if (skill.isAptitudeEffect()) {
                    person.getStats().setAptitudeLevel(skillId.replace("aptitude_", ""), 0);
                } else {
                    person.getStats().setSkillLevel(skillId, 0);
                }
            }
        } else {
            person = faction.createRandomPerson();
        }

        OfficerLevelupPlugin plugin = (OfficerLevelupPlugin) Global.getSettings().getPlugin("SSP_commanderLevelUp");

        long xp = plugin.getXPForLevel(level);
        person.getStats().addXP(xp);
        person.getStats().levelUpIfNeeded();
        person.getStats().addAptitudePoints(2);
        person.getStats().addSkillPoints(4);

        DS_LevelupPluginImpl lvPlugin = new DS_LevelupPluginImpl();

        int currLevel = 2;
        while (currLevel < level) {
            person.getStats().addAptitudePoints(lvPlugin.getAptitudePointsAtLevel(currLevel));
            person.getStats().addSkillPoints(lvPlugin.getSkillPointsAtLevel(currLevel));
            currLevel++;
        }

        WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
        while (person.getStats().getAptitudePoints() > 0 || person.getStats().getSkillPoints() > 0) {
            picker.clear();
            picker.addAll(plugin.pickLevelupSkills(person));
            String skill = picker.pick();
            log.info("picked " + skill);
            if (skill != null) {
                if (Global.getSettings().getSkillSpec(skill).isAptitudeEffect()) {
                    person.getStats().increaseAptitude(skill.replace("aptitude_", ""));
                    person.getStats().addAptitudePoints(-1);
                } else {
                    person.getStats().increaseSkill(skill);
                    person.getStats().addSkillPoints(-1);
                }
            } else {
                break;
            }
        }

        if (commander == null) {
            if (level < 20) {
                person.setRankId(Ranks.SPACE_COMMANDER);
            } else if (level < 40) {
                person.setRankId(Ranks.SPACE_CAPTAIN);
            } else {
                person.setRankId(Ranks.SPACE_ADMIRAL);
            }
            person.setPostId(Ranks.POST_FLEET_COMMANDER);

            String personality = faction.pickPersonality();
            person.setPersonality(personality);
        }

        StringBuilder skillString = new StringBuilder(100);
        for (String skillId : Global.getSettings().getSortedSkillIds()) {
            SkillSpecAPI skill = Global.getSettings().getSkillSpec(skillId);
            if (skill.isAptitudeEffect()) {
                skillString.append(person.getStats().getAptitudeLevel(skillId.replace("aptitude_", ""))).append(", ");
            } else {
                skillString.append(person.getStats().getSkillLevel(skillId)).append(", ");
            }
        }
        log.info("Leveled up " + person.getRank() + " " + person.getName().getFullName() + " to " + person.getStats().getLevel() + " with skills: " +
                skillString.toString());
        return person;
    }

    public static void levelCrew(CampaignFleetAPI fleet, int totalCrew, int grn, int reg, int vet, int eli) {
        if (!DSModPlugin.Module_NPCCrewVeterancy) {
            return;
        }

        int green = grn;
        int regular = reg;
        int veteran = vet;
        int elite = eli;
        List<FleetMemberAPI> members = fleet.getFleetData().getMembersListCopy();
        Collections.sort(members, DS_FleetRandomizer.PRIORITY);
        for (FleetMemberAPI member : members) {
            float crew = member.getMinCrew();

            float crewLevelIfElite = Math.min(4f * elite / crew, 4f);
            if (crewLevelIfElite >= 3.5f && elite >= crew) {
                elite -= (int) crew;
                member.setCrewXPLevel(CrewXPLevel.ELITE);
                continue;
            }

            float crewLevelIfVeteran = Math.min(3f * veteran / crew, 3f * (crew - elite) / crew);
            if (crewLevelIfElite + crewLevelIfVeteran >= 3.5f && elite + veteran >= crew) {
                elite = 0;
                veteran -= (int) crew - elite;
                member.setCrewXPLevel(CrewXPLevel.ELITE);
                continue;
            }

            float crewLevelIfRegular = Math.min(2f * regular / crew, 2f * (crew - (elite + veteran)) / crew);
            if (crewLevelIfElite + crewLevelIfVeteran + crewLevelIfRegular >= 3.5f && elite + veteran + regular >= crew) {
                elite = 0;
                veteran = 0;
                regular -= (int) crew - (elite + veteran);
                member.setCrewXPLevel(CrewXPLevel.ELITE);
                continue;
            }

            float crewLevelIfGreen = Math.min(1f * green / crew, 1f * (crew - (elite + veteran + regular)) / crew);
            if (crewLevelIfElite + crewLevelIfVeteran + crewLevelIfRegular + crewLevelIfGreen >= 3.5f) {
                elite = 0;
                veteran = 0;
                regular = 0;
                green -= (int) crew - (elite + veteran + regular);
                member.setCrewXPLevel(CrewXPLevel.ELITE);
                continue;
            }

            crewLevelIfVeteran = Math.min(3f * veteran / crew, 3f);
            if (crewLevelIfVeteran >= 2.5f && veteran >= crew) {
                veteran -= (int) crew;
                member.setCrewXPLevel(CrewXPLevel.VETERAN);
                continue;
            }

            crewLevelIfRegular = Math.min(2f * regular / crew, 2f * (crew - veteran) / crew);
            if (crewLevelIfVeteran + crewLevelIfRegular >= 2.5f && veteran + regular >= crew) {
                veteran = 0;
                regular -= (int) crew - veteran;
                member.setCrewXPLevel(CrewXPLevel.VETERAN);
                continue;
            }

            crewLevelIfGreen = Math.min(1f * green / crew, 1f * (crew - (veteran + regular)) / crew);
            if (crewLevelIfVeteran + crewLevelIfRegular + crewLevelIfGreen >= 2.5f) {
                veteran = 0;
                regular = 0;
                green -= (int) crew - (veteran + regular);
                member.setCrewXPLevel(CrewXPLevel.VETERAN);
                continue;
            }

            crewLevelIfRegular = Math.min(2f * regular / crew, 2f);
            if (crewLevelIfRegular >= 1.5f && regular >= crew) {
                regular -= (int) crew;
                member.setCrewXPLevel(CrewXPLevel.REGULAR);
                continue;
            }

            crewLevelIfGreen = Math.min(1f * green / crew, 1f * (crew - regular) / crew);
            if (crewLevelIfRegular + crewLevelIfGreen >= 1.5f) {
                regular = 0;
                green -= (int) crew - regular;
                member.setCrewXPLevel(CrewXPLevel.REGULAR);
                continue;
            }

            green -= (int) crew;
            member.setCrewXPLevel(CrewXPLevel.GREEN);
        }
    }

    public static void levelCrewRandomBattle(List<FleetMemberAPI> fleet, int totalCrew, int grn, int reg, int vet, int eli) {
        if (!DSModPlugin.Module_NPCCrewVeterancy) {
            return;
        }

        int green = grn;
        int regular = reg;
        int veteran = vet;
        int elite = eli;
        Collections.sort(fleet, DS_FleetRandomizer.PRIORITY);
        for (FleetMemberAPI member : fleet) {
            float crew = member.getMinCrew();

            float crewLevelIfElite = Math.min(4f * elite / crew, 4f);
            if (crewLevelIfElite >= 3.5f && elite >= crew) {
                elite -= (int) crew;
                member.setCrewXPLevel(CrewXPLevel.ELITE);
                continue;
            }

            float crewLevelIfVeteran = Math.min(3f * veteran / crew, 3f * (crew - elite) / crew);
            if (crewLevelIfElite + crewLevelIfVeteran >= 3.5f && elite + veteran >= crew) {
                elite = 0;
                veteran -= (int) crew - elite;
                member.setCrewXPLevel(CrewXPLevel.ELITE);
                continue;
            }

            float crewLevelIfRegular = Math.min(2f * regular / crew, 2f * (crew - (elite + veteran)) / crew);
            if (crewLevelIfElite + crewLevelIfVeteran + crewLevelIfRegular >= 3.5f && elite + veteran + regular >= crew) {
                elite = 0;
                veteran = 0;
                regular -= (int) crew - (elite + veteran);
                member.setCrewXPLevel(CrewXPLevel.ELITE);
                continue;
            }

            float crewLevelIfGreen = Math.min(1f * green / crew, 1f * (crew - (elite + veteran + regular)) / crew);
            if (crewLevelIfElite + crewLevelIfVeteran + crewLevelIfRegular + crewLevelIfGreen >= 3.5f) {
                elite = 0;
                veteran = 0;
                regular = 0;
                green -= (int) crew - (elite + veteran + regular);
                member.setCrewXPLevel(CrewXPLevel.ELITE);
                continue;
            }

            crewLevelIfVeteran = Math.min(3f * veteran / crew, 3f);
            if (crewLevelIfVeteran >= 2.5f && veteran >= crew) {
                veteran -= (int) crew;
                member.setCrewXPLevel(CrewXPLevel.VETERAN);
                continue;
            }

            crewLevelIfRegular = Math.min(2f * regular / crew, 2f * (crew - veteran) / crew);
            if (crewLevelIfVeteran + crewLevelIfRegular >= 2.5f && veteran + regular >= crew) {
                veteran = 0;
                regular -= (int) crew - veteran;
                member.setCrewXPLevel(CrewXPLevel.VETERAN);
                continue;
            }

            crewLevelIfGreen = Math.min(1f * green / crew, 1f * (crew - (veteran + regular)) / crew);
            if (crewLevelIfVeteran + crewLevelIfRegular + crewLevelIfGreen >= 2.5f) {
                veteran = 0;
                regular = 0;
                green -= (int) crew - (veteran + regular);
                member.setCrewXPLevel(CrewXPLevel.VETERAN);
                continue;
            }

            crewLevelIfRegular = Math.min(2f * regular / crew, 2f);
            if (crewLevelIfRegular >= 1.5f && regular >= crew) {
                regular -= (int) crew;
                member.setCrewXPLevel(CrewXPLevel.REGULAR);
                continue;
            }

            crewLevelIfGreen = Math.min(1f * green / crew, 1f * (crew - regular) / crew);
            if (crewLevelIfRegular + crewLevelIfGreen >= 1.5f) {
                regular = 0;
                green -= (int) crew - regular;
                member.setCrewXPLevel(CrewXPLevel.REGULAR);
                continue;
            }

            green -= (int) crew;
            member.setCrewXPLevel(CrewXPLevel.GREEN);
        }
    }
}
