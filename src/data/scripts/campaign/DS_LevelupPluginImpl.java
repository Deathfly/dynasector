package data.scripts.campaign;

import com.fs.starfarer.api.plugins.LevelupPlugin;

public class DS_LevelupPluginImpl implements LevelupPlugin {

    @Override
    public int getAptitudePointsAtLevel(int level) {
        if (level % 2 == 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public int getSkillPointsAtLevel(int level) {
        // player starts w/ 2 skill 4 aptitude points, not 2/8; add 4 extra skill points along the way
        if (level == 10) {
            return 3;
        }
        if (level == 20) {
            return 3;
        }
        if (level == 30) {
            return 3;
        }
        if (level == 40) {
            return 3;
        }
        return 2;
    }

    @Override
    public long getXPForLevel(int level) {

        if (level <= 1) {
            return 0;
        }

        float p1 = 10;
        float p2 = 35;

        float f1 = 1f;
        float f2 = Math.min(1, Math.max(0, level - p1) / 5f);
        float f3 = Math.max(0, level - p2);

        float p1level = Math.max(0, level - p1 + 1);
        float p2level = Math.max(0, level - p2 + 1);
        float mult1 = (1f + level) * 0.5f * level * 1f;
        float mult2 = (1f + p1level) * 0.5f * p1level * 0.25f;
        float mult3 = (1f + p2level) * 0.5f * p2level * 2f;

        float base = 1500;

        float r = f1 * mult1 * base +
              f2 * mult2 * base +
              f3 * mult3 * base;

        return (long) r;
    }
}
