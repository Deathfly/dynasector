package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoAPI.CargoItemType;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.impl.campaign.submarkets.OpenMarketPlugin;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Util;

import static data.scripts.campaign.submarkets.DS_SubmarketUtils.AFFECTED_FACTIONS;
import static data.scripts.campaign.submarkets.DS_SubmarketUtils.IGNORE_FACTION_FILE;

public class DS_OpenMarketPlugin extends OpenMarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(2f / 3f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();

        float stability = market.getStabilityValue();
        float openMarketFraction = 0.1f + 0.01f * stability;

        for (CommodityOnMarketAPI com : market.getAllCommodities()) {
            String id = com.getId();
            if (market.isIllegal(id)) {
                continue;
            }

            float desired = com.getMaxPlayerFacingStockpile(openMarketFraction, com.getAverageStockpileAfterDemand());
            desired = (int) desired;

            float current = cargo.getQuantity(CargoItemType.RESOURCES, id);

            if (desired > current) {
                cargo.addItems(CargoItemType.RESOURCES, id, (int) (desired - current));
            } else if (current > desired) {
                cargo.removeItems(CargoItemType.RESOURCES, id, (int) (current - desired));
            }
        }

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);

            WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
            factionPicker.add(market.getFaction(), 7.5f);
            factionPicker.add(Global.getSector().getFaction(Factions.INDEPENDENT), 2.5f);
            if (IGNORE_FACTION_FILE.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addWeaponsBasedOnMarketSize(DS_Util.lerp(1.5f, 5f, stability / 10f), 0, factionPicker, submarket, 1f);
            } else {
                if (AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                    DS_SubmarketUtils.addWeaponsBasedOnMarketSize(DS_Util.lerp(1.5f, 5f, stability / 10f), 0, factionPicker, submarket, 1f);
                } else {
                    addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(1.5f, 5f, stability / 10f)), 0, factionPicker);
                }
            }
            addRandomWeapons(Math.max(1, market.getSize() - 3), 0);

            addShips(pruneAmount);
        }

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            countScale = 0.5f;
        }

        // 52
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.CRIG, 10f); //20
        rolePicker.add(ShipRoles.TUG, 10f); //20
        rolePicker.add(ShipRoles.CIV_RANDOM, 10f); //26
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 10f); //26
        rolePicker.add(ShipRoles.TANKER_SMALL, 3f); //26
        rolePicker.add(ShipRoles.PERSONNEL_SMALL, 3f); //26
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_SMALL, 5f); //6
        rolePicker.add(ShipRoles.LINER_SMALL, 1f); //1

        if (marketSize >= 4) { // 67.5
            rolePicker.add(ShipRoles.UTILITY, 5f * mediumShipRarity); //25
            rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.TANKER_MEDIUM, 1f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.PERSONNEL_MEDIUM, 1f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM, 3f * mediumShipRarity); //3
            rolePicker.add(ShipRoles.LINER_MEDIUM, 0.5f * mediumShipRarity); //0.5
        }

        if (marketSize >= 6) { // 77.75
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //28
            rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.TANKER_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.PERSONNEL_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 2f * largeShipRarity); //2
            rolePicker.add(ShipRoles.LINER_LARGE, 0.25f * largeShipRarity); //0.25
        }

        float stability = market.getStabilityValue();
        countScale *= DS_Util.lerp(0.5f, 1.25f, stability / 10f);

        // 3/4/5/6/7/8/9/10 [1/2/2/3/3/4/4/5]
        if (AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
            DS_SubmarketUtils.addShipsForRoles(Math.round((2 + marketSize) * countScale), rolePicker, null, submarket, 1f);
        } else {
            addShipsForRoles(Math.round((2 + marketSize) * countScale), rolePicker, null);
        }
    }
}
