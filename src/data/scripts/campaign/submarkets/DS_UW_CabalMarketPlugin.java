package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoAPI.CargoItemType;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Util;

import static data.scripts.campaign.submarkets.DS_SubmarketUtils.AFFECTED_FACTIONS;
import static data.scripts.campaign.submarkets.DS_SubmarketUtils.IGNORE_FACTION_FILE;

public class DS_UW_CabalMarketPlugin extends UW_CabalMarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(0.5f, Math.max(14f, sinceLastCargoUpdate) / 14f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();
        float stability = market.getStabilityValue();
        float cabalMarketLegalFraction = 0.1f + 0.01f * stability;
        float cabalMarketIllegalFraction = 0.2f + 0.01f * stability;

        for (CommodityOnMarketAPI com : market.getAllCommodities()) {
            String id = com.getId();
            boolean illegal = market.isIllegal(id);

            float desired;
            float basedOnSupply;
            if (illegal) {
                desired = com.getMaxPlayerFacingStockpile(cabalMarketIllegalFraction, com.getAverageStockpileAfterDemand());
                basedOnSupply = com.getMaxPlayerFacingStockpile(cabalMarketIllegalFraction, com.getSupplyValue());
            } else {
                desired = com.getMaxPlayerFacingStockpile(cabalMarketLegalFraction, com.getAverageStockpileAfterDemand());
                basedOnSupply = com.getMaxPlayerFacingStockpile(cabalMarketLegalFraction, com.getSupplyValue());
            }

            desired = (int) Math.max(desired, basedOnSupply);

            switch (id) {
                case Commodities.FUEL:
                    desired = Math.max(desired, Global.getSettings().getFloat("blackMarketMinFuel"));
                    break;
                case Commodities.SUPPLIES:
                    desired = Math.max(desired, Global.getSettings().getFloat("blackMarketMinSupplies"));
                    break;
                case Commodities.MARINES:
                    desired = Math.max(desired, Global.getSettings().getFloat("blackMarketMinMarines"));
                    break;
            }

            float current = cargo.getQuantity(CargoItemType.RESOURCES, id);
            if (desired > current) {
                cargo.addItems(CargoItemType.RESOURCES, id, (int) (desired - current));
            } else if (current > desired) {
                cargo.removeItems(CargoItemType.RESOURCES, id, (int) (current - desired));
            }
        }

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 14f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);

            if (IGNORE_FACTION_FILE.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addBlackMarketWeapons(DS_Util.lerp(2f, 5f, stability / 10f), 4, null, submarket, 1f);
            } else {
                if (AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                    DS_SubmarketUtils.addBlackMarketWeapons(DS_Util.lerp(2f, 5f, stability / 10f), 4, null, submarket, 1f);
                } else {
                    addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(2f, 5f, stability / 10f)), 4, null);
                }
            }

            addRandomWeapons(Math.max(1, market.getSize() - 2), 4);

            addShips(pruneAmount);
        }

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float veryLargeShipRarity = 1f;
        float militaryRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            veryLargeShipRarity = 0.33f;
            militaryRarity = 0.67f;
            countScale = 0.5f;
        }

        // 63
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 3f); //5
        rolePicker.add(ShipRoles.TANKER_SMALL, 1f); //5
        rolePicker.add(ShipRoles.PERSONNEL_SMALL, 1f); //5
        rolePicker.add(ShipRoles.COMBAT_SMALL, 15f * militaryRarity); //25
        rolePicker.add(ShipRoles.ESCORT_SMALL, 10f * militaryRarity); //25
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 15f * mediumShipRarity * militaryRarity); //15
        rolePicker.add(ShipRoles.CARRIER_SMALL, 3f * mediumShipRarity * militaryRarity); //3
        rolePicker.add(ShipRoles.INTERCEPTOR, 5f * militaryRarity); //15
        rolePicker.add(ShipRoles.FIGHTER, 5f * militaryRarity); //15
        rolePicker.add(ShipRoles.BOMBER, 5f * militaryRarity); //15

        if (marketSize >= 4) { // 140
            rolePicker.add(ShipRoles.UTILITY, 5f * mediumShipRarity); //5
            rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity); //9
            rolePicker.add(ShipRoles.TANKER_MEDIUM, 2f * mediumShipRarity); //9
            rolePicker.add(ShipRoles.PERSONNEL_MEDIUM, 2f * mediumShipRarity); //9
            rolePicker.add(ShipRoles.COMBAT_MEDIUM, 10f * mediumShipRarity * militaryRarity); //35
            rolePicker.add(ShipRoles.ESCORT_MEDIUM, 10f * mediumShipRarity * militaryRarity); //35
            rolePicker.add(ShipRoles.CARRIER_SMALL, 10f * mediumShipRarity * militaryRarity); //13
            rolePicker.add(ShipRoles.COMBAT_LARGE, 10f * largeShipRarity * militaryRarity); //10
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f * largeShipRarity * militaryRarity); //5
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 3f * veryLargeShipRarity * militaryRarity); //3
            rolePicker.add(ShipRoles.INTERCEPTOR, 5f * militaryRarity); //30
            rolePicker.add(ShipRoles.FIGHTER, 5f * militaryRarity); //30
            rolePicker.add(ShipRoles.BOMBER, 5f * militaryRarity); //30
        }

        if (marketSize >= 5) { // 174.5
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //8
            rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.TANKER_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.PERSONNEL_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 5f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * largeShipRarity * militaryRarity); //15
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 5f * largeShipRarity * militaryRarity); //10
            rolePicker.add(ShipRoles.COMBAT_CAPITAL, 4f * veryLargeShipRarity * militaryRarity); //5
            rolePicker.add(ShipRoles.CARRIER_LARGE, 2f * veryLargeShipRarity * militaryRarity); //2
            rolePicker.add(ShipRoles.INTERCEPTOR, 2.5f * militaryRarity); //37.5
            rolePicker.add(ShipRoles.FIGHTER, 2.5f * militaryRarity); //37.5
            rolePicker.add(ShipRoles.BOMBER, 2.5f * militaryRarity); //37.5
        }

        float stability = market.getStabilityValue();
        countScale *= DS_Util.lerp(0.5f, 1.1f, stability / 10f);

        if (AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
            DS_SubmarketUtils.addShipsForRoles(Math.round((1.5f + marketSize * 1.5f) * countScale), rolePicker, null, submarket, 1f);
        } else {
            addShipsForRoles(Math.round((1.5f + marketSize * 1.5f) * countScale), rolePicker, null);
        }
    }
}
