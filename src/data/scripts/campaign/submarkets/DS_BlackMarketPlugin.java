package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoAPI.CargoItemType;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.impl.campaign.submarkets.BlackMarketPlugin;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.util.DS_Util;

import static data.scripts.campaign.submarkets.DS_SubmarketUtils.AFFECTED_FACTIONS;
import static data.scripts.campaign.submarkets.DS_SubmarketUtils.IGNORE_FACTION_FILE;

public class DS_BlackMarketPlugin extends BlackMarketPlugin {

    private boolean firstVisit = true;

    public float getPruneAmount() {
        return (float) Math.pow(0.5f, Math.max(30f, sinceLastCargoUpdate) / 30f);
    }

    @Override
    public void updateCargoPrePlayerInteraction() {
        if (!DSModPlugin.Module_MarketIntegration) {
            super.updateCargoPrePlayerInteraction();
            return;
        }

        if (!okToUpdateCargo()) {
            return;
        }
        float pruneAmount = getPruneAmount();
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();
        float stability = market.getStabilityValue();
        float blackMarketLegalFraction = 0.11f - 0.01f * stability;
        float blackMarketIllegalFraction = 0.2f - 0.01f * stability;

        for (CommodityOnMarketAPI com : market.getAllCommodities()) {
            String id = com.getId();
            boolean illegal = market.isIllegal(id);

            float desired;
            float basedOnSupply;
            if (illegal) {
                desired = com.getMaxPlayerFacingStockpile(blackMarketIllegalFraction, com.getAverageStockpileAfterDemand());
                basedOnSupply = com.getMaxPlayerFacingStockpile(blackMarketIllegalFraction, com.getSupplyValue());
            } else {
                desired = com.getMaxPlayerFacingStockpile(blackMarketLegalFraction, com.getAverageStockpileAfterDemand());
                basedOnSupply = com.getMaxPlayerFacingStockpile(blackMarketLegalFraction, com.getSupplyValue());
            }

            desired = (int) Math.max(desired, basedOnSupply);

            switch (id) {
                case Commodities.FUEL:
                    desired = Math.max(desired, Global.getSettings().getFloat("blackMarketMinFuel"));
                    break;
                case Commodities.SUPPLIES:
                    desired = Math.max(desired, Global.getSettings().getFloat("blackMarketMinSupplies"));
                    break;
                case Commodities.MARINES:
                    desired = Math.max(desired, Global.getSettings().getFloat("blackMarketMinMarines"));
                    break;
            }

            float current = cargo.getQuantity(CargoItemType.RESOURCES, id);
            if (desired > current) {
                cargo.addItems(CargoItemType.RESOURCES, id, (int) (desired - current));
            } else if (current > desired) {
                cargo.removeItems(CargoItemType.RESOURCES, id, (int) (current - desired));
            }
        }

        int iterations = 1;
        if (firstVisit || pruneAmount <= 0.2f) {
            iterations++;
            firstVisit = false;
        }
        for (int i = 0; i < iterations; i++) {
            if (i > 0) {
                sinceLastCargoUpdate = 30f;
                pruneAmount = getPruneAmount();
                sinceLastCargoUpdate = 0f;
            }

            DS_SubmarketUtils.pruneWeapons(pruneAmount, cargo);

            WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
            factionPicker.add(market.getFaction(), 11f - stability);
            if (!submarket.getFaction().getId().contentEquals(Factions.PIRATES)) {
                factionPicker.add(Global.getSector().getFaction(Factions.INDEPENDENT), 4f);
            }
            factionPicker.add(submarket.getFaction(), 6f);
            if (IGNORE_FACTION_FILE.contains(submarket.getFaction().getId())) {
                DS_SubmarketUtils.addBlackMarketWeapons(DS_Util.lerp(4f, 1.5f, stability / 10f), 3, factionPicker, submarket, 1f);
            } else {
                if (AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
                    DS_SubmarketUtils.addBlackMarketWeapons(DS_Util.lerp(4f, 1.5f, stability / 10f), 3, factionPicker, submarket, 1f);
                } else {
                    addWeaponsBasedOnMarketSize(Math.round(DS_Util.lerp(4f, 1.5f, stability / 10f)), 3, factionPicker);
                }
            }

            addRandomWeapons(Math.max(1, market.getSize() - 3), 3);

            addShips(pruneAmount);
        }

        cargo.sort();
        cargo.getMothballedShips().sort();
        DS_SubmarketUtils.finish(cargo, submarket.getFaction());
    }

    private void addShips(float prune) {
        int marketSize = market.getSize();

        pruneShips(prune);

        float mediumShipRarity = 1f;
        float largeShipRarity = 1f;
        float militaryRarity = 1f;
        float countScale = 1f;
        if (DSModPlugin.Module_RareShips) {
            mediumShipRarity = 0.75f;
            largeShipRarity = 0.5f;
            militaryRarity = 0.67f;
            countScale = 0.5f;
        }

        // 71
        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.CIV_RANDOM, 5f); //10
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 3f); //10
        rolePicker.add(ShipRoles.TANKER_SMALL, 1f); //10
        rolePicker.add(ShipRoles.PERSONNEL_SMALL, 1f); //10
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_SMALL, 15f); //15
        rolePicker.add(ShipRoles.COMBAT_SMALL, 15f * militaryRarity); //15
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 15f * militaryRarity * mediumShipRarity); //15
        rolePicker.add(ShipRoles.INTERCEPTOR, 5f * militaryRarity); //15
        rolePicker.add(ShipRoles.FIGHTER, 5f * militaryRarity); //15
        rolePicker.add(ShipRoles.BOMBER, 5f * militaryRarity); //15
        rolePicker.add(ShipRoles.LINER_SMALL, 1f); //1

        if (marketSize >= 4) { // 129
            rolePicker.add(ShipRoles.UTILITY, 5f * mediumShipRarity); //5
            rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.TANKER_MEDIUM, 1f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.PERSONNEL_MEDIUM, 1f * mediumShipRarity); //7
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM, 10f * mediumShipRarity); //10
            rolePicker.add(ShipRoles.COMBAT_MEDIUM, 10f * militaryRarity * mediumShipRarity); //10
            rolePicker.add(ShipRoles.CARRIER_SMALL, 10f * militaryRarity * mediumShipRarity); //10
            rolePicker.add(ShipRoles.INTERCEPTOR, 5f * militaryRarity); //30
            rolePicker.add(ShipRoles.FIGHTER, 5f * militaryRarity); //30
            rolePicker.add(ShipRoles.BOMBER, 5f * militaryRarity); //30
            rolePicker.add(ShipRoles.LINER_MEDIUM, 1f); //1
        }

        if (marketSize >= 6) { // 173
            rolePicker.add(ShipRoles.UTILITY, 3f * largeShipRarity); //9
            rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f * largeShipRarity); //5
            rolePicker.add(ShipRoles.TANKER_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.PERSONNEL_LARGE, 1f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 5f * largeShipRarity); //5
            rolePicker.add(ShipRoles.COMBAT_LARGE, 5f * militaryRarity * largeShipRarity); //5
            rolePicker.add(ShipRoles.CARRIER_MEDIUM, 10f * militaryRarity * largeShipRarity); //10
            rolePicker.add(ShipRoles.INTERCEPTOR, 5f * militaryRarity); //45
            rolePicker.add(ShipRoles.FIGHTER, 5f * militaryRarity); //45
            rolePicker.add(ShipRoles.BOMBER, 5f * militaryRarity); //45
            rolePicker.add(ShipRoles.LINER_LARGE, 1f); //1
        }

        WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
        float stability = market.getStabilityValue();
        factionPicker.add(market.getFaction(), 11f - stability);
        if (!submarket.getFaction().getId().contentEquals(Factions.PIRATES)) {
            factionPicker.add(Global.getSector().getFaction(Factions.INDEPENDENT), 4f);
        }
        factionPicker.add(submarket.getFaction(), 6f);
        countScale *= DS_Util.lerp(1.25f, 0.75f, stability / 10f);

        // 2/4/6/8/10/12/14/16 [1/2/3/4/5/6/7/8]
        if (AFFECTED_FACTIONS.contains(submarket.getFaction().getId())) {
            DS_SubmarketUtils.addShipsForRoles(Math.round(Math.max(1, (marketSize * 2) * countScale)), rolePicker, factionPicker, submarket, 1f);
        } else {
            addShipsForRoles(Math.round(Math.max(1, (marketSize * 2) * countScale)), rolePicker, factionPicker);
        }
    }
}
