package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.fleet.ShipRolePick;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.DS_Util;
import data.scripts.variants.DS_Database;
import data.scripts.variants.DS_VariantRandomizer;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import static data.scripts.campaign.DS_FleetFactory.BANNED_HULLS;

public class DS_SubmarketUtils {

    static final Set<String> AFFECTED_FACTIONS = new HashSet<>(21);
    static final Set<String> IGNORE_FACTION_FILE = new HashSet<>(1);

    static {
        AFFECTED_FACTIONS.add(Factions.HEGEMONY);
        AFFECTED_FACTIONS.add(Factions.DIKTAT);
        AFFECTED_FACTIONS.add(Factions.INDEPENDENT);
        AFFECTED_FACTIONS.add(Factions.KOL);
        AFFECTED_FACTIONS.add(Factions.LIONS_GUARD);
        AFFECTED_FACTIONS.add(Factions.LUDDIC_CHURCH);
        AFFECTED_FACTIONS.add(Factions.LUDDIC_PATH);
        AFFECTED_FACTIONS.add(Factions.NEUTRAL);
        AFFECTED_FACTIONS.add(Factions.PIRATES);
        AFFECTED_FACTIONS.add(Factions.TRITACHYON);
        AFFECTED_FACTIONS.add(Factions.PERSEAN);
        AFFECTED_FACTIONS.add("cabal");
        AFFECTED_FACTIONS.add("interstellarimperium");
        AFFECTED_FACTIONS.add("citadeldefenders");
        AFFECTED_FACTIONS.add("exipirated");
        AFFECTED_FACTIONS.add("shadow_industry");
        AFFECTED_FACTIONS.add("mayorate");
        AFFECTED_FACTIONS.add("junk_pirates");
        AFFECTED_FACTIONS.add("pack");
        AFFECTED_FACTIONS.add("syndicate_asp");
        AFFECTED_FACTIONS.add("SCY");
        AFFECTED_FACTIONS.add("tiandong");
        AFFECTED_FACTIONS.add("diableavionics");
        AFFECTED_FACTIONS.add("player_npc");
        IGNORE_FACTION_FILE.add("player_npc");
    }

    private static void addShip(FleetMemberAPI member, SubmarketAPI submarket) {
        member.getRepairTracker().setMothballed(true);
        submarket.getCargo().getMothballedShips().addFleetMember(member);
    }

    private static void addWeaponsForRolePicks(int maxCount, int maxTier, WeightedRandomPicker<FactionAPI> factionPicker, int maxSize, int clustering,
                                               SubmarketAPI submarket, float vanillaFactor) {
        WeightedRandomPicker<WeaponSize> sizePicker = new WeightedRandomPicker<>();
        int countLarge, countMedium, countSmall;
        if (maxSize == 1) {
            sizePicker.add(WeaponSize.SMALL, 0.6f);
            sizePicker.add(WeaponSize.MEDIUM, 0.4f);
        } else if (maxSize == 2) {
            sizePicker.add(WeaponSize.SMALL, 0.533f);
            sizePicker.add(WeaponSize.MEDIUM, 0.433f);
            sizePicker.add(WeaponSize.LARGE, 0.034f);
        } else if (maxSize == 3) {
            sizePicker.add(WeaponSize.SMALL, 0.4f);
            sizePicker.add(WeaponSize.MEDIUM, 0.45f);
            sizePicker.add(WeaponSize.LARGE, 0.15f);
        } else if (maxSize == 4) {
            sizePicker.add(WeaponSize.SMALL, 0.275f);
            sizePicker.add(WeaponSize.MEDIUM, 0.4f);
            sizePicker.add(WeaponSize.LARGE, 0.325f);
        } else {
            sizePicker.add(WeaponSize.SMALL, 0.25f);
            sizePicker.add(WeaponSize.MEDIUM, 0.4f);
            sizePicker.add(WeaponSize.LARGE, 0.35f);
        }
        if (clustering == 7) { // 56 -> 80
            countLarge = 6;
            countMedium = 9;
            countSmall = 14;
        } else if (clustering == 6) { // 49 -> 70
            countLarge = 5;
            countMedium = 8;
            countSmall = 13;
        } else if (clustering == 5) { // 42 -> 60
            countLarge = 4;
            countMedium = 8;
            countSmall = 10;
        } else if (clustering == 4) { // 35 -> 50
            countLarge = 3;
            countMedium = 7;
            countSmall = 9;
        } else if (clustering == 3) { // 28 -> 40
            countLarge = 3;
            countMedium = 5;
            countSmall = 6;
        } else if (clustering == 2) { // 21 -> 30
            countLarge = 2;
            countMedium = 4;
            countSmall = 5;
        } else if (clustering == 1) { // 14 -> 20
            countLarge = 2;
            countMedium = 2;
            countSmall = 2;
        } else { // 7 -> 10
            countLarge = 1;
            countMedium = 1;
            countSmall = 1;
        }

        CargoAPI cargo = submarket.getCargo();
        int picks = 0;
        while (picks < maxCount) {
            String faction;
            if (factionPicker == null) {
                faction = submarket.getFaction().getId();
            } else {
                faction = factionPicker.pick().getId();
            }
            Map<String, Float> weaponData = DS_Database.factionWeapons.get(faction);
            if (weaponData == null) {
                weaponData = DS_Database.factionWeapons.get("independent");
            }

            String id = DS_VariantRandomizer.pickRandomSSPWeapon(faction, weaponData, sizePicker.pick(), submarket.getMarket().getShipQualityFactor(),
                                                                  vanillaFactor, maxTier);
            WeaponSpecAPI spec = Global.getSettings().getWeaponSpec(id);
            int count;
            if (spec != null && spec.getSize() == WeaponSize.SMALL) {
                count = countSmall;
            } else if (spec != null && spec.getSize() == WeaponSize.MEDIUM) {
                count = countMedium;
            } else {
                count = countLarge;
            }
            cargo.addWeapons(id, count);
            picks++;
        }
    }

    static void addBlackMarketWeapons(float baseCount, int maxTier, WeightedRandomPicker<FactionAPI> factionPicker, SubmarketAPI submarket, float vanillaFactor) {
        int marketSize = submarket.getMarket().getSize();
        if (marketSize >= 8) { // 100
            int remainingCount = Math.round(Math.max(1, baseCount * 4f)) - ((int) Math.max(1, baseCount * 1f) * 3);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1f), maxTier, factionPicker, 5, 3, submarket, vanillaFactor); // 40 * 1 = 40
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1f), maxTier, factionPicker, 5, 2, submarket, vanillaFactor); // 30 * 1 = 30
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1f), maxTier, factionPicker, 5, 1, submarket, vanillaFactor); // 20 * 1 = 20
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 5, 0, submarket, vanillaFactor); // 10 * 1 = 10
        } else if (marketSize >= 7) { // 90
            int remainingCount = Math.round(Math.max(1, baseCount * 3.6f)) - ((int) Math.max(1, baseCount * 0.9f) * 3);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.9f), maxTier, factionPicker, 4, 3, submarket, vanillaFactor); // 40 * 0.9 = 36
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.9f), maxTier, factionPicker, 4, 2, submarket, vanillaFactor); // 30 * 0.9 = 27
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.9f), maxTier, factionPicker, 4, 1, submarket, vanillaFactor); // 20 * 0.9 = 18
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 4, 0, submarket, vanillaFactor); // 10 * 0.9 = 9
        } else if (marketSize >= 6) { // 80
            int remainingCount = Math.round(Math.max(1, baseCount * 3.99f)) - ((int) Math.max(1, baseCount * 1.33f) * 2);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1.33f), maxTier, factionPicker, 4, 2, submarket, vanillaFactor); // 30 * 1.33 = 39.9
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1.33f), maxTier, factionPicker, 4, 1, submarket, vanillaFactor); // 20 * 1.33 = 26.6
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 4, 0, submarket, vanillaFactor); // 10 * 1.33 = 13.3
        } else if (marketSize >= 5) { // 70
            int remainingCount = Math.round(Math.max(1, baseCount * 3.51f)) - ((int) Math.max(1, baseCount * 1.17f) * 2);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1.17f), maxTier, factionPicker, 3, 2, submarket, vanillaFactor); // 30 * 1.17 = 35.1
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1.17f), maxTier, factionPicker, 3, 1, submarket, vanillaFactor); // 20 * 1.17 = 23.4
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 3, 0, submarket, vanillaFactor); // 10 * 1.17 = 11.7
        } else if (marketSize >= 4) { // 60
            int remainingCount = Math.round(Math.max(1, baseCount * 4f)) - (int) Math.max(1, baseCount * 2f);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 2f), maxTier, factionPicker, 3, 1, submarket, vanillaFactor); // 20 * 2 = 40
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 3, 0, submarket, vanillaFactor); // 10 * 2 = 20
        } else if (marketSize >= 3) { // 50
            int remainingCount = Math.round(Math.max(1, baseCount * 3.32f)) - (int) Math.max(1, baseCount * 1.66f);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1.66f), maxTier, factionPicker, 2, 1, submarket, vanillaFactor); // 20 * 1.66 = 33.2
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 2, 0, submarket, vanillaFactor); // 10 * 1.66 = 16.6
        } else if (marketSize >= 2) { // 40
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 4f), maxTier, factionPicker, 2, 0, submarket, vanillaFactor); // 10 * 4 = 40
        } else if (marketSize >= 1) { // 30
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 3f), maxTier, factionPicker, 1, 0, submarket, vanillaFactor); // 10 * 3 = 30
        } else { // 20
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 2f), maxTier, factionPicker, 1, 0, submarket, vanillaFactor); // 10 * 2 = 20
        }
    }

    static void addShipsForRoles(int maxTotal, WeightedRandomPicker<String> rolePicker, WeightedRandomPicker<FactionAPI> factionPicker, SubmarketAPI submarket,
                                 float vanillaFactor) {
        if (rolePicker.isEmpty()) {
            return;
        }

        Random rand = new Random();
        int curr = submarket.getCargo().getMothballedShips().getMembersListCopy().size();
        int toAdd = maxTotal - curr;
        int added = 0;
        while (added < toAdd) {
            String faction;
            if (factionPicker == null) {
                faction = submarket.getFaction().getId();
            } else {
                faction = factionPicker.pick().getId();
            }
            String role = rolePicker.pick();
            List<ShipRolePick> picks = Global.getSector().getFaction(faction).pickShip(role, submarket.getMarket().getShipQualityFactor(), rand);
            if (picks != null && !picks.isEmpty()) {
                for (ShipRolePick pick : picks) {
                    if (!pick.variantId.startsWith("ssp_")) {
                        if ((float) Math.random() > vanillaFactor) {
                            continue;
                        }
                    }
                    if (!pick.isFighterWing()) {
                        String hullId = Global.getSettings().getVariant(pick.variantId).getHullSpec().getHullId();
                        if (BANNED_HULLS.contains(hullId)) {
                            continue;
                        }
                    }
                    FleetMemberType type = FleetMemberType.SHIP;
                    String variantId = pick.variantId;
                    if (pick.isFighterWing()) {
                        type = FleetMemberType.FIGHTER_WING;
                    } else {
                        FleetMemberAPI member = Global.getFactory().createFleetMember(type, pick.variantId);
                        variantId = member.getHullId() + "_Hull";
                    }
                    FleetMemberAPI member = Global.getFactory().createFleetMember(type, variantId);
                    addShip(member, submarket);
                }
                added++;
            }
        }
    }

    static void addWeaponsBasedOnMarketSize(float baseCount, int maxTier, WeightedRandomPicker<FactionAPI> factionPicker, SubmarketAPI submarket,
                                            float vanillaFactor) {
        int marketSize = submarket.getMarket().getSize();
        if (marketSize >= 8) { // 100
            int remainingCount = Math.round(Math.max(1, baseCount * 1.65f)) - ((int) Math.max(1, baseCount * 0.33f) * 4);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.33f), maxTier, factionPicker, 4, 7, submarket, vanillaFactor); // 80 * 0.33 = 26.4
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.33f), maxTier, factionPicker, 4, 6, submarket, vanillaFactor); // 70 * 0.33 = 23.1
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.33f), maxTier, factionPicker, 4, 5, submarket, vanillaFactor); // 60 * 0.33 = 19.8
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.33f), maxTier, factionPicker, 4, 4, submarket, vanillaFactor); // 50 * 0.33 = 16.5
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 4, 3, submarket, vanillaFactor); // 40 * 0.33 = 13.2
        } else if (marketSize >= 7) { // 90
            int remainingCount = Math.round(Math.max(1, baseCount * 1.64f)) - ((int) Math.max(1, baseCount * 0.41f) * 3);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.41f), maxTier, factionPicker, 4, 6, submarket, vanillaFactor); // 70 * 0.41 = 28.7
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.41f), maxTier, factionPicker, 4, 5, submarket, vanillaFactor); // 60 * 0.41 = 24.6
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.41f), maxTier, factionPicker, 4, 4, submarket, vanillaFactor); // 50 * 0.41 = 20.5
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 4, 3, submarket, vanillaFactor); // 40 * 0.41 = 16.4
        } else if (marketSize >= 6) { // 80
            int remainingCount = Math.round(Math.max(1, baseCount * 1.76f)) - ((int) Math.max(1, baseCount * 0.44f) * 3);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.44f), maxTier, factionPicker, 4, 5, submarket, vanillaFactor); // 60 * 0.44 = 26.4
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.44f), maxTier, factionPicker, 4, 4, submarket, vanillaFactor); // 50 * 0.44 = 22
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.44f), maxTier, factionPicker, 4, 3, submarket, vanillaFactor); // 40 * 0.44 = 17.6
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 4, 2, submarket, vanillaFactor); // 30 * 0.44 = 13.2
        } else if (marketSize >= 5) { // 70
            int remainingCount = Math.round(Math.max(1, baseCount * 1.74f)) - ((int) Math.max(1, baseCount * 0.58f) * 2);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.58f), maxTier, factionPicker, 3, 4, submarket, vanillaFactor); // 50 * 0.58 = 29
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.58f), maxTier, factionPicker, 3, 3, submarket, vanillaFactor); // 40 * 0.58 = 23.2
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 3, 2, submarket, vanillaFactor); // 30 * 0.58 = 17.4
        } else if (marketSize >= 4) { // 60
            int remainingCount = Math.round(Math.max(1, baseCount * 2.01f)) - ((int) Math.max(1, baseCount * 0.67f) * 2);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.67f), maxTier, factionPicker, 3, 3, submarket, vanillaFactor); // 40 * 0.67 = 26.8
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 0.67f), maxTier, factionPicker, 3, 2, submarket, vanillaFactor); // 30 * 0.67 = 20.1
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 3, 1, submarket, vanillaFactor); // 20 * 0.67 = 13.4
        } else if (marketSize >= 3) { // 50
            int remainingCount = Math.round(Math.max(1, baseCount * 2f)) - (int) Math.max(1, baseCount * 1f);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1f), maxTier, factionPicker, 2, 2, submarket, vanillaFactor); // 30 * 1 = 30
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 2, 1, submarket, vanillaFactor); // 20 * 1 = 20
        } else if (marketSize >= 2) { // 40
            int remainingCount = Math.round(Math.max(1, baseCount * 2.66f)) - (int) Math.max(1, baseCount * 1.33f);
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 1.33f), maxTier, factionPicker, 2, 1, submarket, vanillaFactor); // 20 * 1.33 = 26.6
            addWeaponsForRolePicks(Math.max(1, remainingCount), maxTier, factionPicker, 2, 0, submarket, vanillaFactor); // 10 * 1.33 = 13.3
        } else if (marketSize >= 1) { // 30
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 3f), maxTier, factionPicker, 1, 0, submarket, vanillaFactor); // 10 * 3 = 30
        } else { // 20
            addWeaponsForRolePicks((int) Math.max(1, baseCount * 2f), maxTier, factionPicker, 1, 0, submarket, vanillaFactor); // 10 * 2 = 20
        }
    }

    static void finish(CargoAPI cargo, FactionAPI faction) {
        for (FleetMemberAPI member : cargo.getMothballedShips().getMembersListCopy()) {
            if (member.getShipName() != null) {
                String[] words = member.getShipName().split(" ");
                if (words.length > 1) {
                    String lastWord = words[words.length - 1];
                    if (DS_Util.isValidRoman(lastWord)) {
                        String name = words[0];
                        if (name.contentEquals(faction.getEntityNamePrefix())) {
                            name = "";
                        }
                        for (int i = 1; i < words.length - 1; i++) {
                            if (name.isEmpty()) {
                                name += words[i];
                            } else {
                                name += " " + words[i];
                            }
                        }
                        if (Math.random() < 0.25) {
                            if (Math.random() < 0.3) {
                                if (Math.random() < 0.35) {
                                    if (Math.random() < 0.4) {
                                        name += " V";
                                    } else {
                                        name += " IV";
                                    }
                                } else {
                                    name += " III";
                                }
                            } else {
                                name += " II";
                            }
                        }
                        member.setShipName(name);
                    }
                }
            }
        }
    }

    static void pruneWeapons(float mult, CargoAPI cargo) {
        for (CargoStackAPI stack : cargo.getStacksCopy()) {
            if (!stack.isWeaponStack()) {
                continue;
            }

            float qty = stack.getSize();
            if (qty <= 1.5f && (float) Math.random() < mult) {
                cargo.addItems(stack.getType(), stack.getData(), 2f - qty);
            } else if (qty <= 3.5f && (float) Math.random() >= mult) {
                cargo.removeItems(stack.getType(), stack.getData(), qty);
            } else if (qty <= 1.5f) {
                cargo.removeItems(stack.getType(), stack.getData(), qty);
            } else if (Math.round(qty * (1f - mult)) > 0.5f) {
                cargo.removeItems(stack.getType(), stack.getData(), Math.round(qty * (1f - mult)));
            }
        }
    }
}
