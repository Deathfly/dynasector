package data.scripts.campaign.fleets;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.CampaignEventListener;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.characters.SkillSpecAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.events.BaseEventPlugin;
import com.fs.starfarer.api.impl.campaign.events.PersonBountyEvent;
import com.fs.starfarer.api.impl.campaign.events.PersonBountyEvent.BountyType;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.DSModPlugin;
import data.scripts.campaign.DS_FleetFactory;
import data.scripts.campaign.DS_LevelUpper;
import data.scripts.campaign.DS_LevelupPluginImpl;
import data.scripts.variants.DS_FleetRandomizer;
import data.scripts.variants.DS_VariantRandomizer.Archetype;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.MathUtils;

public class DS_FleetInjector extends BaseCampaignEventListener implements EveryFrameScript {

    public static final Set<String> AFFECTED_FACTIONS = new HashSet<>(31);
    public static final Set<String> NO_SHARING_WEAPONS = new HashSet<>(5);

    public static final Logger log = Global.getLogger(DS_FleetInjector.class);

    private static WeakReference<DS_FleetInjector> thisInjector;

    static {
        AFFECTED_FACTIONS.add(Factions.HEGEMONY);
        AFFECTED_FACTIONS.add(Factions.DIKTAT);
        AFFECTED_FACTIONS.add(Factions.INDEPENDENT);
        AFFECTED_FACTIONS.add(Factions.KOL);
        AFFECTED_FACTIONS.add(Factions.LIONS_GUARD);
        AFFECTED_FACTIONS.add(Factions.LUDDIC_CHURCH);
        AFFECTED_FACTIONS.add(Factions.LUDDIC_PATH);
        AFFECTED_FACTIONS.add(Factions.NEUTRAL);
        AFFECTED_FACTIONS.add(Factions.PIRATES);
        AFFECTED_FACTIONS.add(Factions.TRITACHYON);
        AFFECTED_FACTIONS.add(Factions.PERSEAN);
        AFFECTED_FACTIONS.add("cabal");
        AFFECTED_FACTIONS.add("interstellarimperium");
        AFFECTED_FACTIONS.add("citadeldefenders");
        AFFECTED_FACTIONS.add("blackrock_driveyards");
        AFFECTED_FACTIONS.add("exigency");
        AFFECTED_FACTIONS.add("exipirated");
        AFFECTED_FACTIONS.add("templars");
        AFFECTED_FACTIONS.add("shadow_industry");
        AFFECTED_FACTIONS.add("mayorate");
        AFFECTED_FACTIONS.add("junk_pirates");
        AFFECTED_FACTIONS.add("pack");
        AFFECTED_FACTIONS.add("syndicate_asp");
        AFFECTED_FACTIONS.add("SCY");
        AFFECTED_FACTIONS.add("tiandong");
        AFFECTED_FACTIONS.add("diableavionics");
        AFFECTED_FACTIONS.add("player_npc");
        AFFECTED_FACTIONS.add("famous_bounty");
        AFFECTED_FACTIONS.add("domain");
        AFFECTED_FACTIONS.add("sector");
        AFFECTED_FACTIONS.add("everything");
        NO_SHARING_WEAPONS.add("exigency");
        NO_SHARING_WEAPONS.add("templars");
        NO_SHARING_WEAPONS.add("SCY");
        NO_SHARING_WEAPONS.add("diableavionics");
        NO_SHARING_WEAPONS.add("cabal");
    }

    public static MarketAPI findNearestLocalMarketWithSameFaction(SectorEntityToken token) {
        List<MarketAPI> localMarkets = Misc.getMarketsInLocation(token.getContainingLocation());
        float distToLocalMarket = Float.MAX_VALUE;
        MarketAPI closest = null;
        for (MarketAPI market : localMarkets) {
            if (!market.getFaction().getId().contentEquals(token.getFaction().getId())) {
                continue;
            }

            if (market.getPrimaryEntity() == null) {
                continue;
            }
            if (market.getPrimaryEntity().getContainingLocation() != token.getContainingLocation()) {
                continue;
            }

            float currDist = Misc.getDistance(market.getPrimaryEntity().getLocation(), token.getLocation());
            if (currDist < distToLocalMarket) {
                distToLocalMarket = currDist;
                closest = market;
            }
        }
        return closest;
    }

    public static Map<Archetype, Float> getArchetypeWeights(FleetStyle fleetStyle, String faction) {
        FactionStyle factionStyle = FactionStyle.getStyle(faction);

        Map<Archetype, Float> archetypeWeights = new HashMap<>(factionStyle.archetypeWeights.size() + fleetStyle.archetypeWeights.size());
        for (Map.Entry<Archetype, Float> entry : factionStyle.archetypeWeights.entrySet()) {
            Archetype archetype = entry.getKey();
            float weight = entry.getValue();
            archetypeWeights.put(archetype, weight);
        }
        for (Map.Entry<Archetype, Float> entry : fleetStyle.archetypeWeights.entrySet()) {
            Archetype archetype = entry.getKey();
            float weight = entry.getValue();
            Float currWeight = archetypeWeights.get(archetype);
            if (currWeight != null) {
                archetypeWeights.put(archetype, weight * currWeight);
            } else {
                archetypeWeights.put(archetype, weight);
            }
        }

        return archetypeWeights;
    }

    public static DS_FleetInjector getInjector() {
        return thisInjector.get();
    }

    public static void injectFleet(CampaignFleetAPI fleet) {
        String faction = fleet.getFaction().getId();
        MemoryAPI memory = fleet.getMemoryWithoutUpdate();

        MarketAPI market = Misc.getSourceMarket(fleet);
        if (market == null) {
            market = findNearestLocalMarketWithSameFaction(fleet);
        }

        float stability;
        float qualityFactor;
        if (market != null) {
            stability = memory.contains("$stability") ? memory.getFloat("$stability") : market.getStabilityValue();
            qualityFactor = memory.contains("$qualityFactor") ? memory.getFloat("$qualityFactor") : market.getShipQualityFactor();
        } else {
            stability = memory.contains("$stability") ? memory.getFloat("$stability") : 5f;
            qualityFactor = memory.contains("$qualityFactor") ? memory.getFloat("$qualityFactor") : 0.5f;
        }

        String type = memory.contains(MemFlags.MEMORY_KEY_FLEET_TYPE) ? memory.getString(MemFlags.MEMORY_KEY_FLEET_TYPE) : null;
        if (type != null && !type.isEmpty()) {
            if (!AFFECTED_FACTIONS.contains(faction)) {
                return;
            }
            switch (type) {
                case FleetTypes.TRADE: {
                    // Trade Fleet
                    if (NO_SHARING_WEAPONS.contains(faction)) {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, null, 0f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.CIVILIAN, faction), CommanderType.CIVILIAN, -1f, true);
                    } else {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, Factions.INDEPENDENT, 0.5f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.CIVILIAN, faction), CommanderType.CIVILIAN, -1f, true);
                    }
                    levelFleet(fleet, CrewType.CIVILIAN, faction, CommanderType.CIVILIAN);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case FleetTypes.TRADE_SMUGGLER: {
                    // Smuggler Fleet
                    if (NO_SHARING_WEAPONS.contains(faction)) {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, null, 0f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.STANDARD, faction), CommanderType.STANDARD, -1f, true);
                    } else {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, Factions.PIRATES, 0.5f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.STANDARD, faction), CommanderType.STANDARD, -1f, true);
                    }
                    levelFleet(fleet, CrewType.STANDARD, faction, CommanderType.STANDARD);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case FleetTypes.TRADE_SMALL: {
                    // Small Trade Fleet
                    if (market == null) {
                        break;
                    }

                    String fromFaction = market.getFactionId();
                    if (!AFFECTED_FACTIONS.contains(fromFaction)) {
                        break;
                    }

                    if (NO_SHARING_WEAPONS.contains(fromFaction)) {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), fromFaction, null, 0f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.CIVILIAN, faction), CommanderType.CIVILIAN, -1f, true);
                    } else {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), fromFaction, Factions.INDEPENDENT, 0.5f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.CIVILIAN, faction), CommanderType.CIVILIAN, -1f, true);
                    }
                    levelFleet(fleet, CrewType.CIVILIAN, fromFaction, CommanderType.CIVILIAN);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, fromFaction);
                    break;
                }
                case FleetTypes.TRADE_LINER: {
                    // Liner Fleet
                    if (NO_SHARING_WEAPONS.contains(faction)) {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, null, 0f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.CIVILIAN, faction), CommanderType.CIVILIAN, -1f, true);
                    } else {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, Factions.INDEPENDENT, 0.5f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.CIVILIAN, faction), CommanderType.CIVILIAN, -1f, true);
                    }
                    levelFleet(fleet, CrewType.CIVILIAN, faction, CommanderType.CIVILIAN);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case FleetTypes.FOOD_RELIEF_FLEET: {
                    // Relief Fleet from Food Shortage Event
                    if (NO_SHARING_WEAPONS.contains(faction)) {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, null, 0f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.CIVILIAN, faction), CommanderType.CIVILIAN, -1f, true);
                    } else {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, Factions.INDEPENDENT, 0.5f, qualityFactor, null, null,
                                          getArchetypeWeights(FleetStyle.CIVILIAN, faction), CommanderType.CIVILIAN, -1f, true);
                    }
                    levelFleet(fleet, CrewType.CIVILIAN, faction, CommanderType.CIVILIAN);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case FleetTypes.PATROL_SMALL: {
                    // Small Patrol Fleet
                    if (fleet.getNameWithFaction().startsWith(Global.getSector().getFaction(Factions.LIONS_GUARD).getDisplayName())) {
                        Archetype theme = pickTheme(Factions.LIONS_GUARD);
                        setThemeName(fleet, theme);
                        randomizeVariants(fleet, Factions.LIONS_GUARD, qualityFactor, theme, getArchetypeWeights(FleetStyle.ELITE, Factions.LIONS_GUARD),
                                          CommanderType.ELITE);
                        levelFleet(fleet, CrewType.ELITE, Factions.LIONS_GUARD, CommanderType.STANDARD);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, Factions.LIONS_GUARD);
                        break;
                    } else if (fleet.getFaction().getId().contentEquals(Factions.LUDDIC_PATH)) {
                        Archetype theme = pickTheme(faction);
                        setThemeName(fleet, theme);
                        randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.RAIDER, faction), CommanderType.STANDARD, true);
                        levelFleet(fleet, CrewType.STANDARD, faction, CommanderType.STANDARD);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                        break;
                    }

                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    boolean useKOL = false;
                    if (fleet.getFaction().getId().contentEquals(Factions.LUDDIC_CHURCH) && theme == null) {
                        if (Math.random() < qualityFactor / 2f) {
                            useKOL = true;
                        }
                    }

                    if (useKOL) {
                        fleet.setNoFactionInName(true);
                        fleet.setName("Knights of Ludd " + fleet.getName());
                        randomizeVariants(fleet, Factions.KOL, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, Factions.KOL),
                                          CommanderType.ELITE);
                        levelFleet(fleet, CrewType.MILITARY, Factions.KOL, CommanderType.ELITE);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, Factions.KOL);
                    } else {
                        randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, faction), CommanderType.MILITARY);
                        levelFleet(fleet, CrewType.MILITARY, faction, CommanderType.MILITARY);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    }
                    break;
                }
                case FleetTypes.PATROL_MEDIUM:
                case FleetTypes.PATROL_LARGE: {
                    // Medium/Large Patrol Fleets
                    if (fleet.getNameWithFaction().startsWith(Global.getSector().getFaction(Factions.LIONS_GUARD).getDisplayName())) {
                        Archetype theme = pickTheme(Factions.LIONS_GUARD);
                        setThemeName(fleet, theme);
                        randomizeVariants(fleet, Factions.LIONS_GUARD, qualityFactor, theme, getArchetypeWeights(FleetStyle.ELITE, Factions.LIONS_GUARD),
                                          CommanderType.ELITE);
                        levelFleet(fleet, CrewType.ELITE, Factions.LIONS_GUARD, CommanderType.ELITE);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, Factions.LIONS_GUARD);
                        break;
                    }

                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    boolean useKOL = false;
                    if (fleet.getFaction().getId().contentEquals(Factions.LUDDIC_CHURCH) && theme == null) {
                        if (Math.random() < qualityFactor / 2f) {
                            useKOL = true;
                        }
                    }

                    if (useKOL) {
                        fleet.setNoFactionInName(true);
                        fleet.setName("Knights of Ludd " + fleet.getName());
                        randomizeVariants(fleet, Factions.KOL, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, Factions.KOL),
                                          CommanderType.ELITE);
                        levelFleet(fleet, CrewType.ELITE, Factions.KOL, CommanderType.ELITE);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, Factions.KOL);
                    } else {
                        randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, faction), CommanderType.MILITARY);
                        levelFleet(fleet, CrewType.MILITARY, faction, CommanderType.MILITARY);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    }
                    break;
                }
                case FleetTypes.MERC_SCOUT:
                case FleetTypes.MERC_BOUNTY_HUNTER:
                case FleetTypes.MERC_PRIVATEER:
                case FleetTypes.MERC_PATROL:
                case FleetTypes.MERC_ARMADA: {
                    // Merc / Pirate / Avesta Fleets
                    boolean isPirate = memory.contains(MemFlags.MEMORY_KEY_PIRATE) ? memory.getBoolean(MemFlags.MEMORY_KEY_PIRATE) : false;
                    boolean isAvestan = fleet.getFaction().getId().contentEquals("exipirated");

                    if (isPirate) {
                        if (isAvestan) {
                            Archetype theme = pickTheme("exipirated");
                            setThemeName(fleet, theme);
                            switch (type) {
                                case FleetTypes.MERC_SCOUT:
                                case FleetTypes.MERC_BOUNTY_HUNTER:
                                    randomizeVariants(fleet, "exipirated", qualityFactor, theme, getArchetypeWeights(FleetStyle.RAIDER, "exipirated"),
                                                      CommanderType.MILITARY);
                                    break;
                                case FleetTypes.MERC_PRIVATEER:
                                case FleetTypes.MERC_PATROL:
                                case FleetTypes.MERC_ARMADA:
                                    randomizeVariants(fleet, "exipirated", qualityFactor, theme, getArchetypeWeights(FleetStyle.PROFESSIONAL, "exipirated"),
                                                      CommanderType.MILITARY);
                                    break;
                                default:
                                    break;
                            }
                            levelFleet(fleet, CrewType.PROFESSIONAL, "exipirated", CommanderType.MILITARY);
                            DS_FleetFactory.finishFleetNonIntrusive(fleet, "exipirated");
                        } else {
                            Archetype theme = pickTheme(Factions.PIRATES);
                            setThemeName(fleet, theme);
                            switch (type) {
                                case FleetTypes.MERC_SCOUT:
                                case FleetTypes.MERC_BOUNTY_HUNTER: {
                                    String extendedTheme = null;
                                    if (theme == null && Math.random() >= 0.7) {
                                        extendedTheme = pickExtendedTheme(Factions.PIRATES);
                                    }
                                    setExtendedThemeName(fleet, extendedTheme);
                                    if (extendedTheme != null) {
                                        randomizeVariants(fleet, extendedTheme, qualityFactor, theme, getArchetypeWeights(FleetStyle.RAIDER, extendedTheme),
                                                          CommanderType.STANDARD, true);
                                    } else {
                                        randomizeVariants(fleet, Factions.PIRATES, qualityFactor, theme,
                                                          getArchetypeWeights(FleetStyle.RAIDER, Factions.PIRATES), CommanderType.STANDARD, true);
                                    }
                                    break;
                                }
                                case FleetTypes.MERC_PRIVATEER:
                                case FleetTypes.MERC_PATROL:
                                case FleetTypes.MERC_ARMADA: {
                                    String extendedTheme = null;
                                    if (theme == null && Math.random() >= 0.7) {
                                        extendedTheme = pickExtendedTheme(Factions.PIRATES);
                                    }
                                    setExtendedThemeName(fleet, extendedTheme);
                                    if (extendedTheme != null) {
                                        randomizeVariants(fleet, extendedTheme, qualityFactor, theme, getArchetypeWeights(FleetStyle.STANDARD, extendedTheme),
                                                          CommanderType.STANDARD, true);
                                    } else {
                                        randomizeVariants(fleet, Factions.PIRATES, qualityFactor, theme,
                                                          getArchetypeWeights(FleetStyle.STANDARD, Factions.PIRATES), CommanderType.STANDARD, true);
                                    }
                                    break;
                                }
                                default:
                                    break;
                            }
                            levelFleet(fleet, CrewType.STANDARD, Factions.PIRATES, CommanderType.STANDARD);
                            DS_FleetFactory.finishFleetNonIntrusive(fleet, Factions.PIRATES);
                        }
                    } else {
                        Archetype theme = pickTheme(faction);
                        setThemeName(fleet, theme);
                        switch (type) {
                            case FleetTypes.MERC_SCOUT:
                            case FleetTypes.MERC_BOUNTY_HUNTER: {
                                String extendedTheme = null;
                                if (theme == null && Math.random() >= 0.7) {
                                    extendedTheme = pickExtendedTheme(faction);
                                }
                                setExtendedThemeName(fleet, extendedTheme);
                                if (extendedTheme != null) {
                                    if (NO_SHARING_WEAPONS.contains(faction)) {
                                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), extendedTheme, null, 0f, qualityFactor, null, theme,
                                                          getArchetypeWeights(FleetStyle.RAIDER, extendedTheme), CommanderType.STANDARD, -1f, false);
                                    } else {
                                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), extendedTheme, Factions.INDEPENDENT, 0.33f, qualityFactor,
                                                          null, theme, getArchetypeWeights(FleetStyle.RAIDER, extendedTheme), CommanderType.STANDARD, -1f, false);
                                    }
                                } else {
                                    if (NO_SHARING_WEAPONS.contains(faction)) {
                                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, null, 0f, qualityFactor, null, theme,
                                                          getArchetypeWeights(FleetStyle.RAIDER, faction), CommanderType.STANDARD, -1f, false);
                                    } else {
                                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, Factions.INDEPENDENT, 0.33f, qualityFactor, null,
                                                          theme, getArchetypeWeights(FleetStyle.RAIDER, faction), CommanderType.STANDARD, -1f, false);
                                    }
                                }
                                break;
                            }
                            case FleetTypes.MERC_PRIVATEER:
                            case FleetTypes.MERC_PATROL:
                            case FleetTypes.MERC_ARMADA: {
                                String extendedTheme = null;
                                if (theme == null && Math.random() >= 0.7) {
                                    extendedTheme = pickExtendedTheme(faction);
                                }
                                setExtendedThemeName(fleet, extendedTheme);
                                if (extendedTheme != null) {
                                    if (NO_SHARING_WEAPONS.contains(faction)) {
                                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), extendedTheme, null, 0f, qualityFactor, null, theme,
                                                          getArchetypeWeights(FleetStyle.PROFESSIONAL, extendedTheme), CommanderType.STANDARD, -1f, false);
                                    } else {
                                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), extendedTheme, Factions.INDEPENDENT, 0.33f, qualityFactor,
                                                          null, theme, getArchetypeWeights(FleetStyle.PROFESSIONAL, extendedTheme), CommanderType.STANDARD, -1f,
                                                          false);
                                    }
                                } else {
                                    if (NO_SHARING_WEAPONS.contains(faction)) {
                                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, null, 0f, qualityFactor, null, theme,
                                                          getArchetypeWeights(FleetStyle.PROFESSIONAL, faction), CommanderType.STANDARD, -1f, false);
                                    } else {
                                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, Factions.INDEPENDENT, 0.33f, qualityFactor, null,
                                                          theme, getArchetypeWeights(FleetStyle.PROFESSIONAL, faction), CommanderType.STANDARD, -1f, false);
                                    }
                                }
                                break;
                            }
                            default:
                                break;
                        }
                        levelFleet(fleet, CrewType.PROFESSIONAL, faction, CommanderType.STANDARD);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    }

                    break;
                }
                case FleetTypes.PERSON_BOUNTY_FLEET: {
                    // Bounty Fleet from Person Bounty Event
                    String bountyFaction;
                    SSPBountyType bountyType;
                    float level;

                    if (DSModPlugin.hasSSP) {
                        bountyFaction = memory.contains("$personBountyFaction") ? memory.getString("$personBountyFaction") : Factions.PIRATES;
                        int bountyTypeOrd = memory.contains("$personBountyType") ?
                                            ((Enum) memory.get("$personBountyType")).ordinal() :
                                            SSPBountyType.PIRATE.ordinal();
                        bountyType = bountyTypeOrd >= SSPBountyType.values().length ?
                                     SSPBountyType.PIRATE :
                                     SSPBountyType.values()[bountyTypeOrd];
                        level = memory.contains("$level") ? memory.getFloat("$level") : -1f;
                    } else {
                        bountyFaction = Factions.PIRATES;
                        bountyType = SSPBountyType.PIRATE;
                        level = -1f;

                        /* Oh boy, here we go... */
                        for (CampaignEventListener listener : Global.getSector().getAllListeners()) {
                            if (listener instanceof PersonBountyEvent) {
                                PersonBountyEvent eventPlugin = (PersonBountyEvent) listener;

                                /* Okay, now that we have an event, it's time to use voodoo magic! */
                                CampaignFleetAPI bountyFleet;
                                try {
                                    Field f = PersonBountyEvent.class.getDeclaredField("fleet");
                                    f.setAccessible(true);
                                    bountyFleet = (CampaignFleetAPI) f.get(eventPlugin);
                                } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                                    throw new RuntimeException(ex);
                                }

                                if (bountyFleet != fleet) {
                                    continue;
                                }

                                /* Finish up our witchcraft... */
                                BountyType vanillaBountyType;
                                try {
                                    Field f = PersonBountyEvent.class.getDeclaredField("bountyType");
                                    f.setAccessible(true);
                                    vanillaBountyType = (BountyType) f.get(eventPlugin);
                                } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                                    throw new RuntimeException(ex);
                                }
                                MarketAPI spawnMarket;
                                try {
                                    Field f = BaseEventPlugin.class.getDeclaredField("market");
                                    f.setAccessible(true);
                                    spawnMarket = (MarketAPI) f.get(eventPlugin);
                                } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                                    throw new RuntimeException(ex);
                                }

                                if (spawnMarket == null) {
                                    continue;
                                }

                                bountyFaction = spawnMarket.getFactionId();
                                switch (vanillaBountyType) {
                                    default:
                                    case PIRATE:
                                        bountyType = SSPBountyType.PIRATE;
                                        break;
                                    case DESERTER:
                                        bountyType = SSPBountyType.DESERTER;
                                        break;
                                }
                                break;
                            }
                        }
                    }

                    if (!AFFECTED_FACTIONS.contains(bountyFaction)) {
                        return;
                    }

                    Archetype theme = pickTheme(bountyFaction);
                    setThemeName(fleet, theme);
                    if (bountyType == SSPBountyType.PIRATE) {
                        String extendedTheme = null;
                        if (theme == null && Math.random() >= 0.7) {
                            extendedTheme = pickExtendedTheme(bountyFaction);
                        }
                        setExtendedThemeName(fleet, extendedTheme);
                        if (extendedTheme != null) {
                            randomizeVariants(fleet.getFleetData().getMembersListCopy(), extendedTheme, qualityFactor, theme,
                                              getArchetypeWeights(FleetStyle.STANDARD, extendedTheme), CommanderType.ELITE, level, false);
                        } else {
                            randomizeVariants(fleet.getFleetData().getMembersListCopy(), bountyFaction, qualityFactor, theme,
                                              getArchetypeWeights(FleetStyle.STANDARD, bountyFaction), CommanderType.ELITE, level, false);
                        }
                    } else if (bountyType == SSPBountyType.PATHER) {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), bountyFaction, qualityFactor, theme,
                                          getArchetypeWeights(FleetStyle.RAIDER, bountyFaction), CommanderType.ELITE, level, false);
                    } else if (bountyType == SSPBountyType.DESERTER) {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), bountyFaction, Factions.PIRATES, 0.33f, qualityFactor, null,
                                          theme, getArchetypeWeights(FleetStyle.MILITARY, bountyFaction), CommanderType.ELITE, level, false);
                    } else if (bountyType == SSPBountyType.MERCENARY) {
                        String extendedTheme = null;
                        if (theme == null && Math.random() >= 0.7) {
                            extendedTheme = pickExtendedTheme(bountyFaction);
                        }
                        setExtendedThemeName(fleet, extendedTheme);
                        if (extendedTheme != null) {
                            randomizeVariants(fleet.getFleetData().getMembersListCopy(), extendedTheme, Factions.INDEPENDENT, 0.33f, qualityFactor, null,
                                              theme, getArchetypeWeights(FleetStyle.PROFESSIONAL, extendedTheme), CommanderType.ELITE, level, false);
                        } else {
                            randomizeVariants(fleet.getFleetData().getMembersListCopy(), bountyFaction, Factions.INDEPENDENT, 0.33f, qualityFactor, null,
                                              theme, getArchetypeWeights(FleetStyle.PROFESSIONAL, bountyFaction), CommanderType.ELITE, level, false);
                        }
                    } else if (bountyType == SSPBountyType.ASSASSINATION) {
                        randomizeVariants(fleet.getFleetData().getMembersListCopy(), bountyFaction, qualityFactor, theme,
                                          getArchetypeWeights(FleetStyle.ELITE, bountyFaction), CommanderType.ELITE, level, false);
                    }
                    levelFleet(fleet, CrewType.PROFESSIONAL, bountyFaction, CommanderType.ELITE, level);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, bountyFaction);
                    break;
                }
                case "famousBounty": {
                    // Famous Bounty Fleet from Famous Bounty Event
                    String bountyFaction = memory.contains("$famousBountyFaction") ? memory.getString("$famousBountyFaction") : Factions.PIRATES;
                    float level = memory.contains("$level") ? memory.getFloat("$level") : -1f;

                    if (!AFFECTED_FACTIONS.contains(bountyFaction)) {
                        return;
                    }

                    randomizeVariants(fleet.getFleetData().getMembersListCopy(), bountyFaction, qualityFactor, null,
                                      getArchetypeWeights(FleetStyle.ELITE, bountyFaction), CommanderType.ACE, level, false);
                    levelFleet(fleet, CrewType.ELITE, bountyFaction, CommanderType.ACE, level);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, bountyFaction);
                    break;
                }
                case "vengeanceFleet": {
                    // Faction Vengeance Event Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, faction), CommanderType.MILITARY);
                    levelFleet(fleet, CrewType.MILITARY, faction, CommanderType.MILITARY);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "cabalFleet": {
                    // Starlight Cabal Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.RAIDER, faction), CommanderType.ELITE, true);
                    levelFleet(fleet, CrewType.PROFESSIONAL, faction, CommanderType.ELITE);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "templarAttack1":
                case "templarDefense1":
                case "templarAttack2":
                case "templarDefense2":
                case "templarAttack3":
                case "templarDefense3":
                case "templarAttack4":
                case "templarDefense4": {
                    // Templar Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, faction), CommanderType.ELITE);
                    levelFleet(fleet, CrewType.SUPER_ELITE, faction, CommanderType.ELITE);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "tem_responseFleet": {
                    // Crusade Response Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, faction), CommanderType.MILITARY);
                    levelFleet(fleet, CrewType.MILITARY, faction, CommanderType.MILITARY);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "imperiumISA": {
                    // ISA Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.PROFESSIONAL, faction), CommanderType.ELITE);
                    levelFleet(fleet, CrewType.PROFESSIONAL, faction, CommanderType.ELITE);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "imperiumSiegeFleet": {
                    // Imperial Siege Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.ELITE, faction), CommanderType.MILITARY);
                    levelFleet(fleet, CrewType.ELITE, faction, CommanderType.MILITARY);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "imperiumSiegeEscort": {
                    // Imperial Blockade Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.PROFESSIONAL, faction), CommanderType.ELITE);
                    levelFleet(fleet, CrewType.PROFESSIONAL, faction, CommanderType.ELITE);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "imperialDefense": {
                    // Imperial Defense Fleet
                    randomizeVariants(fleet, faction, qualityFactor, null, getArchetypeWeights(FleetStyle.ELITE, faction), CommanderType.ELITE);
                    levelFleet(fleet, CrewType.ELITE, faction, CommanderType.ELITE);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "imperiumHegWarFleet": {
                    // Hegemony War Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, faction), CommanderType.MILITARY);
                    levelFleet(fleet, CrewType.MILITARY, faction, CommanderType.MILITARY);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "churchPurificationFleet": {
                    // Luddic Church Purification Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, faction), CommanderType.MILITARY);
                    levelFleet(fleet, CrewType.MILITARY, faction, CommanderType.MILITARY);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "churchEscortFleet": {
                    // Luddic Church Escort Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, faction), CommanderType.MILITARY);
                    levelFleet(fleet, CrewType.MILITARY, faction, CommanderType.MILITARY);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "exigencyAttack":
                case "exigencyDefense": {
                    // Exigency Fleet
                    Archetype theme = pickTheme(faction);
                    setThemeName(fleet, theme);
                    randomizeVariants(fleet, faction, qualityFactor, theme, getArchetypeWeights(FleetStyle.MILITARY, faction), CommanderType.ELITE);
                    levelFleet(fleet, CrewType.ELITE, faction, CommanderType.ELITE);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "SCYspyFleet": {
                    // Scy Spy Fleet
                    randomizeVariants(fleet, faction, qualityFactor, null, getArchetypeWeights(FleetStyle.ELITE, faction), CommanderType.ELITE);
                    levelFleet(fleet, CrewType.ELITE, faction, CommanderType.ELITE);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                case "tiandongMining": {
                    // Tiandong Mining Fleet
                    randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, Factions.INDEPENDENT, 0.33f, qualityFactor, null, null,
                                      getArchetypeWeights(FleetStyle.STANDARD, faction), CommanderType.STANDARD, -1f, false);
                    levelFleet(fleet, CrewType.STANDARD, faction, CommanderType.STANDARD);
                    DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    break;
                }
                default: {
                    if (DSModPlugin.isExerelin) {
                        try {
                            Class<?> def = Global.getSettings().getScriptClassLoader().loadClass("exerelin.utilities.ExerelinUtilsFleet");
                            Class<?>[] args = new Class<?>[5];
                            args[0] = CampaignFleetAPI.class;
                            args[1] = MarketAPI.class;
                            args[2] = Float.class;
                            args[3] = Float.class;
                            args[4] = String.class;
                            Method method;
                            try {
                                method = def.getMethod("injectFleet", args);
                                method.invoke(def, fleet, market, stability, qualityFactor, type);
                            } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException |
                                     InvocationTargetException ex) {
                                log.warn(ex.getMessage());
                            }
                        } catch (ClassNotFoundException ex) {
                            // Shouldn't happen
                        }
                    } else {
                        randomizeVariants(fleet, faction, qualityFactor, null, getArchetypeWeights(FleetStyle.STANDARD, faction), CommanderType.STANDARD);
                        levelFleet(fleet, CrewType.STANDARD, faction, CommanderType.STANDARD);
                        DS_FleetFactory.finishFleetNonIntrusive(fleet, faction);
                    }
                }
            }
        }
    }

    public static void levelFleet(CampaignFleetAPI fleet, CrewType crewType, String faction, CommanderType commanderType) {
        levelFleet(fleet, crewType, faction, commanderType, -1f);
    }

    public static void levelFleet(CampaignFleetAPI fleet, CrewType crewType, String faction, CommanderType commanderType, float commanderLevel) {
        if (DSModPlugin.Module_LeveledAICommanders) {
            float pts = DS_FleetRandomizer.countPtsInFleetData(fleet.getFleetData().getMembersListCopy());
            float level = ((pts * commanderType.levelPerPoint + commanderType.baseLevel) *
                           (1f + MathUtils.getRandomNumberInRange(-0.5f, 0.5f) * commanderType.levelVariance));
            level = Math.min(commanderType.levelMax, level);
            if (commanderLevel >= 0f) {
                level = commanderLevel * 2f;
            }

            PersonAPI commander = fleet.getCommander();
            if (commander != null) {
                int lvl = Math.round(level);
                DS_LevelupPluginImpl plugin = new DS_LevelupPluginImpl();
                int aptitudes = 2;
                int skills = 4;
                for (int i = 1; i <= lvl; i++) {
                    aptitudes += plugin.getAptitudePointsAtLevel(i);
                    skills += plugin.getSkillPointsAtLevel(i);
                }

                List<String> allowedAptitudes = new ArrayList<>(Global.getSettings().getAptitudeIds().size());
                for (String aptitudeId : Global.getSettings().getAptitudeIds()) {
                    if (!aptitudeId.contentEquals("combat")) {
                        allowedAptitudes.add(aptitudeId);
                    }
                }
                List<String> allowedSkills = new ArrayList<>(Global.getSettings().getSkillIds().size());
                for (String skillId : Global.getSettings().getSkillIds()) {
                    SkillSpecAPI skill = Global.getSettings().getSkillSpec(skillId);
                    if (!skill.isCombatOfficerSkill()) {
                        allowedSkills.add(skillId);
                    }
                }

                for (int i = 0, j = 0; i < aptitudes; i++, j++) {
                    if (j >= allowedAptitudes.size()) {
                        j = 0;
                    }
                    String aptitude = allowedAptitudes.get(j);
                    commander.getStats().setAptitudeLevel(aptitude, commander.getStats().getAptitudeLevel(aptitude) + 1);
                }
                for (int i = 0, j = 0; i < skills; i++, j++) {
                    if (j >= allowedSkills.size()) {
                        j = 0;
                    }
                    String skill = allowedSkills.get(j);
                    commander.getStats().setSkillLevel(skill, commander.getStats().getSkillLevel(skill) + 1);
                }
                fleet.forceSync();
            }
        }

        if (!DSModPlugin.Module_NPCCrewVeterancy) {
            return;
        }

        FactionStyle factionStyle = FactionStyle.getStyle(faction);
        List<FleetMemberAPI> members = fleet.getFleetData().getMembersListCopy();
        int crew = 0;
        for (FleetMemberAPI member : members) {
            crew += (int) ((member.getMinCrew() + member.getMaxCrew() * 0.25f) / 1.25f);
        }
        CrewType.CrewData data = crewType.getCrewData(crew, factionStyle);
        DS_LevelUpper.levelCrew(fleet, crew, data.green, data.regular, data.veteran, data.elite);
        fleet.forceSync();
    }

    public static void levelFleetRandomBattle(List<FleetMemberAPI> fleetData, CrewType crewType, String faction) {
        if (!DSModPlugin.Module_NPCCrewVeterancy) {
            return;
        }

        FactionStyle factionStyle = FactionStyle.getStyle(faction);
        int crew = 0;
        for (FleetMemberAPI member : fleetData) {
            crew += (int) ((member.getMinCrew() + member.getMaxCrew() * 0.25f) / 1.25f);
        }
        CrewType.CrewData data = crewType.getCrewData(crew, factionStyle);
        DS_LevelUpper.levelCrewRandomBattle(fleetData, crew, data.green, data.regular, data.veteran, data.elite);
    }

    public static String pickExtendedTheme(String faction) {
        WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
        picker.add(Factions.HEGEMONY, 2f);
        picker.add(Factions.DIKTAT, 2f);
        picker.add(Factions.TRITACHYON, 3f);
        picker.add("cabal", 0.5f);
        if (DSModPlugin.imperiumExists) {
            picker.add("interstellarimperium", 2f);
        }
        if (DSModPlugin.citadelExists) {
            picker.add("citadeldefenders", 2f);
        }
        if (DSModPlugin.blackrockExists) {
            picker.add("blackrock_driveyards", 3f);
        }
        if (DSModPlugin.exigencyExists) {
            picker.add("exigency", 3f);
            picker.add("exipirated", 1f);
        }
        if (DSModPlugin.shadowyardsExists) {
            picker.add("shadow_industry", 3f);
        }
        if (DSModPlugin.mayorateExists) {
            picker.add("mayorate", 2f);
        }
        if (DSModPlugin.junkPiratesExists) {
            picker.add("junk_pirates", 2f);
        }
        if (DSModPlugin.scyExists) {
            picker.add("SCY", 3f);
        }
        if (DSModPlugin.tiandongExists) {
            picker.add("tiandong", 2f);
        }
        if (DSModPlugin.diableExists) {
            picker.add("diableavionics", 1.5f);
        }
        picker.add("domain", 2f);
        picker.add("sector", 1f);
        picker.add("everything", 0.5f);
        picker.remove(faction);
        return picker.pick();
    }

    public static Archetype pickTheme(String faction) {
        WeightedRandomPicker<Archetype> picker = new WeightedRandomPicker<>();
        if (faction.contentEquals("templars")) {
            picker.add(null, 10f);
        } else {
            picker.add(null, 40f);
        }
        picker.add(Archetype.ARTILLERY, 3f);
        picker.add(Archetype.ASSAULT, 3f);
        picker.add(Archetype.CLOSE_SUPPORT, 3f);
        picker.add(Archetype.ELITE, 1f);
        picker.add(Archetype.SKIRMISH, 3f);
        picker.add(Archetype.STRIKE, 3f);
        picker.add(Archetype.ULTIMATE, 0.25f);
        return picker.pick();
    }

    public static void randomizeVariants(CampaignFleetAPI fleet, String faction, float qualityFactor, Archetype theme, Map<Archetype, Float> archetypeWeights,
                                         CommanderType commanderType) {
        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, null, 0f, qualityFactor, null, theme, archetypeWeights, commanderType, -1f, false);
    }

    public static void randomizeVariants(CampaignFleetAPI fleet, String faction, float qualityFactor, Archetype theme, Map<Archetype, Float> archetypeWeights,
                                         CommanderType commanderType, boolean noobFriendly) {
        randomizeVariants(fleet.getFleetData().getMembersListCopy(), faction, null, 0f, qualityFactor, null, theme, archetypeWeights, commanderType, -1f,
                          noobFriendly);
    }

    public static void randomizeVariants(List<FleetMemberAPI> fleet, String faction, float qualityFactor, Archetype theme,
                                         Map<Archetype, Float> archetypeWeights, CommanderType commanderType, float commanderLevel, boolean noobFriendly) {
        randomizeVariants(fleet, faction, null, 0f, qualityFactor, null, theme, archetypeWeights, commanderType, commanderLevel, noobFriendly);
    }

    public static void randomizeVariants(List<FleetMemberAPI> fleet, String faction, String otherFaction, float otherFactionWeight, float qualityFactor,
                                         Float opBonus, Archetype theme, Map<Archetype, Float> archetypeWeights, CommanderType commanderType,
                                         float commanderLevel, boolean noobFriendly) {
        if (!DSModPlugin.Module_ProceduralVariants) {
            return;
        }

        float pts = DS_FleetRandomizer.countPtsInFleetData(fleet);
        float level = ((pts * commanderType.levelPerPoint + commanderType.baseLevel) *
                       (1f + MathUtils.getRandomNumberInRange(-0.5f, 0.5f) * commanderType.levelVariance));
        level = Math.min(commanderType.levelMax, level);
        if (commanderLevel >= 0f) {
            level = commanderLevel * 2f;
        }
        float aptitudeTotal = 2f + level / 2f;
        float combatAptitude = aptitudeTotal / 3f;
        float technologyAptitude = aptitudeTotal / 3f;
        float ordnanceBonus = technologyAptitude * 3f + combatAptitude * 1.25f;
        if (commanderType == CommanderType.DUNCE || !DSModPlugin.Module_ScaledVariants) {
            ordnanceBonus = 0f;
        }
        if (opBonus != null) {
            ordnanceBonus += opBonus;
        }

        for (FleetMemberAPI member : fleet) {
            if (DS_FleetRandomizer.SPECIAL_SHIPS.contains(member.getHullId()) && !member.getVariant().isEmptyHullVariant()) {
                continue;
            }
            List<String> roleList = DS_FleetRandomizer.deconstructShip(member, faction);
            if (roleList.isEmpty()) {
                if (DS_FleetRandomizer.SPECIAL_SHIPS.contains(member.getHullId())) {
                    DS_FleetRandomizer.randomizeVariant(member, faction, otherFaction, otherFactionWeight, null, qualityFactor, Archetype.ULTIMATE,
                                                        archetypeWeights, ordnanceBonus, noobFriendly);
                } else {
                    DS_FleetRandomizer.randomizeVariant(member, faction, otherFaction, otherFactionWeight, null, qualityFactor, theme, archetypeWeights,
                                                        ordnanceBonus, noobFriendly);
                }
            } else {
                DS_FleetRandomizer.randomizeVariant(member, faction, otherFaction, otherFactionWeight, roleList.get(0), qualityFactor, theme, archetypeWeights,
                                                    ordnanceBonus, noobFriendly);
            }
        }
    }

    public static void setExtendedThemeName(CampaignFleetAPI fleet, String theme) {
        if (!DSModPlugin.Module_ProceduralVariants) {
            return;
        }

        if (theme == null) {
            return;
        }
        String name;
        switch (theme) {
            case Factions.DIKTAT:
                name = "Diktat";
                break;
            case Factions.INDEPENDENT:
                name = "Stock";
                break;
            case Factions.LUDDIC_CHURCH:
                name = "Luddic";
                break;
            case Factions.LUDDIC_PATH:
                name = "Pather";
                break;
            case "cabal":
                name = "Starlight";
                break;
            case "interstellarimperium":
                name = "Imperial";
                break;
            case "shadow_industry":
                name = "Yardie";
                break;
            case "blackrock_driveyards":
                name = "Blackrock";
                break;
            case "junk_pirates":
                name = "Junk";
                break;
            case "pack":
                name = "Pack";
                break;
            case "syndicate_asp":
                name = "Syndicate";
                break;
            case "SCY":
                name = "Scyan";
                break;
            case "diableavionics":
                name = "Diable";
                break;
            case "domain":
                name = "Mixed";
                break;
            case "sector":
                name = "Exotic";
                break;
            case "everything":
                name = "Jackpot";
                break;
            default:
                name = Global.getSector().getFaction(theme).getDisplayName();
                break;
        }
        fleet.setName(fleet.getName() + " (" + name + ")");
    }

    public static void setThemeName(CampaignFleetAPI fleet, Archetype archetype) {
        if (!DSModPlugin.Module_ProceduralVariants) {
            return;
        }

        if (archetype == null) {
            return;
        }
        String name;
        switch (archetype) {
            case ARTILLERY:
                name = "Artillery";
                break;
            case ASSAULT:
                name = "Assault";
                break;
            case CLOSE_SUPPORT:
                name = "Standoff";
                break;
            case ELITE:
                name = "Elite";
                break;
            case SKIRMISH:
                name = "Skirmish";
                break;
            case STRIKE:
                name = "Strike";
                break;
            case ULTIMATE:
                name = "Ace";
                break;
            default:
                return;
        }
        fleet.setName(fleet.getName() + " (" + name + ")");
    }

    private final List<CampaignFleetAPI> newFleets = new LinkedList<>();

    @SuppressWarnings("LeakingThisInConstructor")
    public DS_FleetInjector() {
        super(false);
        Global.getSector().addTransientScript(this);
        thisInjector = new WeakReference<>(this);
    }

    @Override
    public void advance(float amount) {
        Iterator<CampaignFleetAPI> iter = newFleets.iterator();
        while (iter.hasNext()) {
            CampaignFleetAPI fleet = iter.next();
            injectFleet(fleet);
            iter.remove();
        }
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public void reportFleetSpawned(CampaignFleetAPI fleet) {
        newFleets.add(fleet);
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    public static enum CommanderType {

        CIVILIAN(0f, 0.5f, 0.4f, 30f),
        STANDARD(2.5f, 0.75f, 0.2f, 50f),
        MILITARY(5f, 0.9f, 0.1f, 60f),
        ELITE(10f, 1f, 0.05f, 80f),
        ACE(15f, 1.1f, 0f, 100f),
        DUNCE(0f, 0f, 0f, 0f);

        public final float baseLevel;
        public final float levelPerPoint;
        public final float levelVariance;
        public final float levelMax;

        private CommanderType(float baseLevel, float levelPerPoint, float levelVariance, float levelMax) {
            this.baseLevel = baseLevel;
            this.levelPerPoint = levelPerPoint;
            this.levelVariance = levelVariance;
            this.levelMax = levelMax;
        }
    }

    public static enum CrewType {

        CIVILIAN(0.5f, 0.5f, 0.4f, 0f, 0.1f, 0f, 0f, 0f),
        STANDARD(0.25f, 0.2f, 0.6f, 0.4f, 0.1f, 0f, 0.05f, 0f),
        MILITARY(0.2f, 0.1f, 0.6f, 0.4f, 0.15f, 0f, 0.05f, 0f),
        PROFESSIONAL(0.1f, 0f, 0.6f, 0.4f, 0.2f, 0.1f, 0.1f, 0f),
        ELITE(0f, 0f, 0.3f, 0f, 0.5f, 0.2f, 0.2f, 0.1f),
        SUPER_ELITE(0f, 0f, 0f, 0f, 0.5f, 0f, 0.5f, 0.5f);

        public final float greenWeight;
        public final float minimumGreen;
        public final float regularWeight;
        public final float minimumRegular;
        public final float veteranWeight;
        public final float minimumVeteran;
        public final float eliteWeight;
        public final float minimumElite;

        private CrewType(float greenWeight, float minimumGreen, float regularWeight, float minimumRegular, float veteranWeight, float minimumVeteran,
                         float eliteWeight, float minimumElite) {
            this.greenWeight = greenWeight;
            this.minimumGreen = minimumGreen;
            this.regularWeight = regularWeight;
            this.minimumRegular = minimumRegular;
            this.veteranWeight = veteranWeight;
            this.minimumVeteran = minimumVeteran;
            this.eliteWeight = eliteWeight;
            this.minimumElite = minimumElite;
        }

        public static class CrewData {

            int elite = 0;
            int green = 0;
            int regular = 0;
            int veteran = 0;
        }

        public CrewData getCrewData(int crw, FactionStyle style) {
            CrewData data = new CrewData();
            int crew = crw;

            while (crew > 0) {
                int greenNotMet = Math.max((int) (minimumGreen * crew) - data.green, 0);
                int regularNotMet = Math.max((int) (minimumRegular * crew) - data.regular, 0);
                int veteranNotMet = Math.max((int) (minimumVeteran * crew) - data.veteran, 0);
                int eliteNotMet = Math.max((int) (minimumElite * crew) - data.veteran, 0);

                if (greenNotMet + regularNotMet + veteranNotMet + eliteNotMet >= crew) {
                    if (eliteNotMet > 0) {
                        data.elite += 10;
                    } else if (veteranNotMet > 0) {
                        data.veteran += 10;
                    } else if (regularNotMet > 0) {
                        data.regular += 10;
                    } else {
                        data.green += 10;
                    }
                } else {
                    float rand = (float) Math.random() * (greenWeight * style.green + regularWeight * style.regular + veteranWeight * style.veteran +
                                                          eliteWeight * style.elite);
                    if (rand < greenWeight * style.green) {
                        data.green += 10;
                    } else if (rand < greenWeight * style.green + regularWeight * style.regular) {
                        data.regular += 10;
                    } else if (rand < greenWeight * style.green + regularWeight * style.regular + veteranWeight * style.veteran) {
                        data.veteran += 10;
                    } else {
                        data.elite += 10;
                    }
                }

                crew -= 10;
            }

            return data;
        }
    }

    public static enum FactionStyle {

        HEGEMONY(Factions.HEGEMONY,
                 1f, 0.2f, // xpMult, xpVariance
                 1f, 2f, 1f, 1f, // green, regular, veteran, elite
                 0.2f, 0.4f, 0.5f, 0.7f), // minQF, lowQF, highQF, maxQF
        DIKTAT(Factions.DIKTAT,
               1f, 0.3f, // xpMult, xpVariance
               0.5f, 1f, 1f, 1f, // green, regular, veteran, elite
               0f, 0f, 0.6f, 0.8f, // minQF, lowQF, highQF, maxQF
               new ArchetypeWeight(Archetype.BALANCED, 1.5f),
               new ArchetypeWeight(Archetype.ASSAULT, 2f),
               new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 2f),
               new ArchetypeWeight(Archetype.FLEET, 2f)),
        INDEPENDENT(Factions.INDEPENDENT,
                    0.8f, 0.6f, // xpMult, xpVariance
                    1f, 1f, 1f, 1f, // green, regular, veteran, elite
                    0f, 0f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                    new ArchetypeWeight(Archetype.BALANCED, 2f),
                    new ArchetypeWeight(Archetype.SKIRMISH, 0.75f),
                    new ArchetypeWeight(Archetype.ARTILLERY, 0.5f),
                    new ArchetypeWeight(Archetype.ESCORT, 1.5f),
                    new ArchetypeWeight(Archetype.FLEET, 1.5f)),
        NEUTRAL(Factions.NEUTRAL,
                1f, 0.2f, // xpMult, xpVariance
                1f, 1f, 1f, 1f, // green, regular, veteran, elite
                0f, 0f, 1f, 1f), // minQF, lowQF, highQF, maxQF
        LIONS_GUARD(Factions.LIONS_GUARD,
                    1.1f, 0.1f, // xpMult, xpVariance
                    0.25f, 0.75f, 1f, 1.25f, // green, regular, veteran, elite
                    0.3f, 0.5f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                    new ArchetypeWeight(Archetype.BALANCED, 0.5f),
                    new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                    new ArchetypeWeight(Archetype.ELITE, 2f),
                    new ArchetypeWeight(Archetype.ASSAULT, 0.75f),
                    new ArchetypeWeight(Archetype.ARTILLERY, 0.75f),
                    new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 0.75f),
                    new ArchetypeWeight(Archetype.SUPPORT, 0.75f),
                    new ArchetypeWeight(Archetype.ESCORT, 0.75f),
                    new ArchetypeWeight(Archetype.FLEET, 0.5f),
                    new ArchetypeWeight(Archetype.ULTIMATE, 1.5f)),
        KOL(Factions.KOL,
            1.1f, 0.15f, // xpMult, xpVariance
            0.75f, 1f, 1.25f, 1f, // green, regular, veteran, elite
            0f, 0f, 0.4f, 0.6f, // minQF, lowQF, highQF, maxQF
            new ArchetypeWeight(Archetype.ELITE, 1.5f),
            new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
            new ArchetypeWeight(Archetype.SKIRMISH, 1.5f)),
        LUDDIC_CHURCH(Factions.LUDDIC_CHURCH,
                      0.75f, 0.3f, // xpMult, xpVariance
                      1.5f, 1.25f, 1f, 0.75f, // green, regular, veteran, elite
                      0f, 0f, 0.3f, 0.5f, // minQF, lowQF, highQF, maxQF
                      new ArchetypeWeight(Archetype.BALANCED, 2f),
                      new ArchetypeWeight(Archetype.ELITE, 0.5f),
                      new ArchetypeWeight(Archetype.SUPPORT, 2f),
                      new ArchetypeWeight(Archetype.ESCORT, 2f),
                      new ArchetypeWeight(Archetype.FLEET, 2f)),
        LUDDIC_PATH(Factions.LUDDIC_PATH,
                    0.9f, 0.5f, // xpMult, xpVariance
                    1f, 1f, 1f, 1f, // green, regular, veteran, elite
                    0f, 0f, 0.2f, 0.4f, // minQF, lowQF, highQF, maxQF
                    new ArchetypeWeight(Archetype.BALANCED, 0.25f),
                    new ArchetypeWeight(Archetype.STRIKE, 2f),
                    new ArchetypeWeight(Archetype.ELITE, 0.25f),
                    new ArchetypeWeight(Archetype.ASSAULT, 2f),
                    new ArchetypeWeight(Archetype.SKIRMISH, 3f),
                    new ArchetypeWeight(Archetype.ARTILLERY, 0.5f),
                    new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 0.5f),
                    new ArchetypeWeight(Archetype.SUPPORT, 0.25f),
                    new ArchetypeWeight(Archetype.ESCORT, 0.25f),
                    new ArchetypeWeight(Archetype.FLEET, 0.25f),
                    new ArchetypeWeight(Archetype.ULTIMATE, 0.25f)),
        PIRATES(Factions.PIRATES,
                0.8f, 0.4f, // xpMult, xpVariance
                1f, 1f, 1f, 1f, // green, regular, veteran, elite
                0f, 0f, 0f, 0.5f, // minQF, lowQF, highQF, maxQF
                new ArchetypeWeight(Archetype.BALANCED, 0.75f),
                new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                new ArchetypeWeight(Archetype.SKIRMISH, 1.5f),
                new ArchetypeWeight(Archetype.ARTILLERY, 0.5f),
                new ArchetypeWeight(Archetype.SUPPORT, 0.5f),
                new ArchetypeWeight(Archetype.ESCORT, 0.25f),
                new ArchetypeWeight(Archetype.FLEET, 0.25f)),
        TRITACHYON(Factions.TRITACHYON,
                   1.2f, 0.25f, // xpMult, xpVariance
                   0.75f, 1f, 1.25f, 1.25f, // green, regular, veteran, elite
                   0.5f, 1f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                   new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                   new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                   new ArchetypeWeight(Archetype.ELITE, 1.5f)),
        PERSEAN(Factions.PERSEAN,
                0.8f, 0.4f, // xpMult, xpVariance
                1f, 1f, 1f, 1f, // green, regular, veteran, elite
                0f, 0f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                new ArchetypeWeight(Archetype.BALANCED, 2f),
                new ArchetypeWeight(Archetype.SKIRMISH, 0.75f),
                new ArchetypeWeight(Archetype.ARTILLERY, 0.5f),
                new ArchetypeWeight(Archetype.ESCORT, 1.5f),
                new ArchetypeWeight(Archetype.FLEET, 1.5f)),
        CABAL("cabal",
              1.2f, 0.4f, // xpMult, xpVariance
              0.75f, 1f, 1.25f, 1.5f, // green, regular, veteran, elite
              0.5f, 1f, 1f, 1f, // minQF, lowQF, highQF, maxQF
              new ArchetypeWeight(Archetype.BALANCED, 1.5f),
              new ArchetypeWeight(Archetype.STRIKE, 1.5f),
              new ArchetypeWeight(Archetype.ELITE, 1.5f),
              new ArchetypeWeight(Archetype.ULTIMATE, 1.5f)),
        IMPERIUM("interstellarimperium",
                 1f, 0.2f, // xpMult, xpVariance
                 1f, 1.5f, 1.5f, 1f, // green, regular, veteran, elite
                 0f, 0f, 0.6f, 0.8f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.STRIKE, 0.75f),
                 new ArchetypeWeight(Archetype.ELITE, 0.5f),
                 new ArchetypeWeight(Archetype.SKIRMISH, 0.5f),
                 new ArchetypeWeight(Archetype.ARTILLERY, 2f),
                 new ArchetypeWeight(Archetype.ULTIMATE, 0.5f)),
        CITADEL("citadeldefenders",
                1f, 0.3f, // xpMult, xpVariance
                1f, 1f, 1.5f, 1f, // green, regular, veteran, elite
                0.2f, 0.5f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                new ArchetypeWeight(Archetype.ELITE, 1.5f),
                new ArchetypeWeight(Archetype.SKIRMISH, 1.5f)),
        BLACKROCK("blackrock_driveyards",
                  1.1f, 0.15f, // xpMult, xpVariance
                  0.75f, 1f, 1f, 1.25f, // green, regular, veteran, elite
                  0.2f, 0.4f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                  new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                  new ArchetypeWeight(Archetype.ELITE, 1.5f),
                  new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
                  new ArchetypeWeight(Archetype.SKIRMISH, 2f),
                  new ArchetypeWeight(Archetype.ARTILLERY, 0.5f)),
        EXIGENCY("exigency",
                 1.2f, 0.1f, // xpMult, xpVariance
                 0.5f, 1f, 1.5f, 2f, // green, regular, veteran, elite
                 0.5f, 1f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                 new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
                 new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 2f),
                 new ArchetypeWeight(Archetype.SUPPORT, 0.5f),
                 new ArchetypeWeight(Archetype.ESCORT, 0.5f),
                 new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        AHRIMAN("exipirated",
                0.8f, 0.4f, // xpMult, xpVariance
                1f, 1f, 1f, 1f, // green, regular, veteran, elite
                0f, 0f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                new ArchetypeWeight(Archetype.STRIKE, 1.5f),
                new ArchetypeWeight(Archetype.ELITE, 1.5f),
                new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
                new ArchetypeWeight(Archetype.ARTILLERY, 1.5f),
                new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 1.5f),
                new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        TEMPLARS("templars",
                 1.3f, 0.15f, // xpMult, xpVariance
                 0.1f, 0.5f, 2f, 6f, // green, regular, veteran, elite
                 1f, 1f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.BALANCED, 2f),
                 new ArchetypeWeight(Archetype.ELITE, 2f),
                 new ArchetypeWeight(Archetype.SUPPORT, 0.25f),
                 new ArchetypeWeight(Archetype.ESCORT, 0.25f),
                 new ArchetypeWeight(Archetype.FLEET, 0.25f),
                 new ArchetypeWeight(Archetype.ULTIMATE, 2f)),
        SHADOWYARDS("shadow_industry",
                    1.1f, 0.3f, // xpMult, xpVariance
                    1f, 1f, 1f, 1f, // green, regular, veteran, elite
                    0.2f, 0.4f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                    new ArchetypeWeight(Archetype.BALANCED, 2f),
                    new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 1.5f)),
        MAYORATE("mayorate",
                 0.85f, 0.35f, // xpMult, xpVariance
                 1.5f, 1f, 0.75f, 0.5f, // green, regular, veteran, elite
                 0.2f, 0.3f, 1f, 1f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.BALANCED, 0.5f),
                 new ArchetypeWeight(Archetype.ELITE, 1.5f),
                 new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        JUNK_PIRATES("junk_pirates",
                     0.9f, 0.5f, // xpMult, xpVariance
                     1f, 1f, 1f, 1f, // green, regular, veteran, elite
                     0.2f, 0.3f, 0.7f, 0.8f, // minQF, lowQF, highQF, maxQF
                     new ArchetypeWeight(Archetype.BALANCED, 0.5f),
                     new ArchetypeWeight(Archetype.ELITE, 2f),
                     new ArchetypeWeight(Archetype.ARTILLERY, 0.5f),
                     new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 0.5f),
                     new ArchetypeWeight(Archetype.SUPPORT, 0.25f),
                     new ArchetypeWeight(Archetype.ESCORT, 0.5f),
                     new ArchetypeWeight(Archetype.FLEET, 0.25f),
                     new ArchetypeWeight(Archetype.ULTIMATE, 1.5f)),
        PACK("pack",
             1f, 0.5f, // xpMult, xpVariance
             1f, 0.75f, 0.9f, 1.1f, // green, regular, veteran, elite
             0.2f, 0.3f, 1f, 1f, // minQF, lowQF, highQF, maxQF
             new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        ASP("syndicate_asp",
            1f, 0.4f, // xpMult, xpVariance
            0.75f, 1f, 2f, 1f, // green, regular, veteran, elite
            0f, 0f, 1f, 1f, // minQF, lowQF, highQF, maxQF
            new ArchetypeWeight(Archetype.BALANCED, 2f),
            new ArchetypeWeight(Archetype.STRIKE, 0.5f),
            new ArchetypeWeight(Archetype.ELITE, 0.5f),
            new ArchetypeWeight(Archetype.ESCORT, 2f),
            new ArchetypeWeight(Archetype.FLEET, 2f),
            new ArchetypeWeight(Archetype.ULTIMATE, 0.5f)),
        SCY("SCY",
            1f, 0.2f, // xpMult, xpVariance
            1f, 1.25f, 1.5f, 1.25f, // green, regular, veteran, elite
            0f, 0f, 1f, 1f, // minQF, lowQF, highQF, maxQF
            new ArchetypeWeight(Archetype.STRIKE, 1.5f),
            new ArchetypeWeight(Archetype.ELITE, 1.5f),
            new ArchetypeWeight(Archetype.ASSAULT, 1.5f),
            new ArchetypeWeight(Archetype.SKIRMISH, 0.5f)),
        TIANDONG("tiandong",
                 1f, 0.3f, // xpMult, xpVariance
                 1f, 2f, 1f, 0.75f, // green, regular, veteran, elite
                 0.2f, 0.4f, 0.6f, 0.8f, // minQF, lowQF, highQF, maxQF
                 new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                 new ArchetypeWeight(Archetype.CLOSE_SUPPORT, 1.5f),
                 new ArchetypeWeight(Archetype.ESCORT, 1.5f)),
        DIABLE("diableavionics",
               1.2f, 0.5f, // xpMult, xpVariance
               0.5f, 1f, 1.5f, 1f, // green, regular, veteran, elite
               0.2f, 0.6f, 1f, 1f, // minQF, lowQF, highQF, maxQF
               new ArchetypeWeight(Archetype.SKIRMISH, 1.5f),
               new ArchetypeWeight(Archetype.ESCORT, 1.5f)),
        DOMAIN("domain",
               1f, 0.2f, // xpMult, xpVariance
               1f, 1f, 1f, 1f, // green, regular, veteran, elite
               0f, 0f, 1f, 1f), // minQF, lowQF, highQF, maxQF
        SECTOR("sector",
               1f, 0.2f, // xpMult, xpVariance
               1f, 1f, 1f, 1f, // green, regular, veteran, elite
               0f, 0f, 1f, 1f), // minQF, lowQF, highQF, maxQF
        EVERYTHING("everything",
                   1f, 0.2f, // xpMult, xpVariance
                   1f, 1f, 1f, 1f, // green, regular, veteran, elite
                   0f, 0f, 1f, 1f), // minQF, lowQF, highQF, maxQF
        PLAYER_NPC("player_npc",
                   1f, 0.2f, // xpMult, xpVariance
                   1f, 1f, 1f, 1f, // green, regular, veteran, elite
                   0f, 0f, 1f, 1f); // minQF, lowQF, highQF, maxQF

        public final String faction;

        public final float xpMult;
        public final float xpVariance;

        public final float green;
        public final float regular;
        public final float veteran;
        public final float elite;

        public final float minQF;
        public final float lowQF;
        public final float highQF;
        public final float maxQF;

        public final Map<Archetype, Float> archetypeWeights;

        private FactionStyle(String faction, float xpMult, float xpVariance, float green, float regular, float veteran,
                             float elite, float minQF, float lowQF, float highQF, float maxQF, ArchetypeWeight... weights) {
            this.faction = faction;
            this.xpMult = xpMult;
            this.xpVariance = xpVariance;
            this.green = green;
            this.regular = regular;
            this.veteran = veteran;
            this.elite = elite;
            this.minQF = minQF;
            this.lowQF = lowQF;
            this.highQF = highQF;
            this.maxQF = maxQF;
            archetypeWeights = new HashMap<>(weights.length);
            for (ArchetypeWeight archetypeWeight : weights) {
                archetypeWeights.put(archetypeWeight.archetype, archetypeWeight.weight);
            }
        }

        public static FactionStyle getStyle(String faction) {
            for (FactionStyle style : FactionStyle.values()) {
                if (style.faction.contentEquals(faction)) {
                    return style;
                }
            }
            return INDEPENDENT;
        }
    }

    public static enum FleetStyle {

        CIVILIAN(new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                 new ArchetypeWeight(Archetype.STRIKE, 0.5f),
                 new ArchetypeWeight(Archetype.ELITE, 0.25f),
                 new ArchetypeWeight(Archetype.SUPPORT, 1.5f),
                 new ArchetypeWeight(Archetype.ESCORT, 2f),
                 new ArchetypeWeight(Archetype.ULTIMATE, 0f)),
        STANDARD(new ArchetypeWeight(Archetype.SUPPORT, 0.5f)),
        MILITARY(new ArchetypeWeight(Archetype.BALANCED, 1f),
                 new ArchetypeWeight(Archetype.SKIRMISH, 0.75f),
                 new ArchetypeWeight(Archetype.ESCORT, 0.75f),
                 new ArchetypeWeight(Archetype.FLEET, 0.5f)),
        RAIDER(new ArchetypeWeight(Archetype.BALANCED, 0.5f),
               new ArchetypeWeight(Archetype.STRIKE, 1.5f),
               new ArchetypeWeight(Archetype.SKIRMISH, 2f),
               new ArchetypeWeight(Archetype.ASSAULT, 1.25f),
               new ArchetypeWeight(Archetype.SUPPORT, 0.5f),
               new ArchetypeWeight(Archetype.ESCORT, 0.5f),
               new ArchetypeWeight(Archetype.FLEET, 0.25f)),
        ELITE(new ArchetypeWeight(Archetype.BALANCED, 0.5f),
              new ArchetypeWeight(Archetype.ELITE, 1.5f),
              new ArchetypeWeight(Archetype.SUPPORT, 0.5f),
              new ArchetypeWeight(Archetype.ESCORT, 0.5f),
              new ArchetypeWeight(Archetype.FLEET, 0.5f),
              new ArchetypeWeight(Archetype.ULTIMATE, 1.25f)),
        PROFESSIONAL(new ArchetypeWeight(Archetype.BALANCED, 1.5f),
                     new ArchetypeWeight(Archetype.ELITE, 1.25f),
                     new ArchetypeWeight(Archetype.FLEET, 0.75f));

        public final Map<Archetype, Float> archetypeWeights;

        private FleetStyle(ArchetypeWeight... weights) {
            archetypeWeights = new HashMap<>(weights.length);
            for (ArchetypeWeight archetypeWeight : weights) {
                archetypeWeights.put(archetypeWeight.archetype, archetypeWeight.weight);
            }
        }
    }

    /* Must match the SSP_PersonBountyEvent.BountyType enum exactly! */
    private static enum SSPBountyType {

        PIRATE, DESERTER, ASSASSINATION, MERCENARY, PATHER, CABAL
    }

    private static class ArchetypeWeight {

        final Archetype archetype;
        final float weight;

        ArchetypeWeight(Archetype archetype, float weight) {
            this.archetype = archetype;
            this.weight = weight;
        }
    }
}
