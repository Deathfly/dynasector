package data.missions;

import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.missions.DS_FleetGenerator.GeneratorFleetTypes;
import data.scripts.DSModPlugin;
import data.scripts.campaign.fleets.DS_FleetInjector;
import data.scripts.campaign.fleets.DS_FleetInjector.FleetStyle;
import data.scripts.variants.DS_FleetRandomizer;
import data.scripts.variants.DS_VariantRandomizer.Archetype;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class DS_BaseRandomBattle implements MissionDefinitionPlugin {

    private static boolean first = true;

    protected static final WeightedRandomPicker<String> FACTIONS = new WeightedRandomPicker<>();
    protected static final String[] OBJECTIVE_TYPES = {
        "sensor_array", "sensor_array", "nav_buoy", "nav_buoy", "comm_relay"
    };
    protected static final List<String> PLANETS = new ArrayList<>(35);
    protected static final List<Color> STAR_COLORS = new ArrayList<>(13);

    protected static final Random rand = new Random();

    public static void inflateFleet(List<FleetMemberAPI> fleet, FleetSide side, MissionDefinitionAPI api) {
        boolean flagship = true;
        for (FleetMemberAPI fleetMember : fleet) {
            FleetMemberAPI member;
            if (fleetMember.isFighterWing()) {
                member = api.addToFleet(side, fleetMember.getSpecId(), FleetMemberType.FIGHTER_WING, false, fleetMember.getCrewXPLevel());
            } else {
                member = api.addToFleet(side, fleetMember.getHullId() + "_Hull", FleetMemberType.SHIP, flagship, fleetMember.getCrewXPLevel());
                member.setVariant(fleetMember.getVariant(), false, true);

                flagship = false;
            }

            if (member.getCrewXPLevel() == CrewXPLevel.GREEN) {
                member.getRepairTracker().setCR(0.5f);
            } else if (member.getCrewXPLevel() == CrewXPLevel.REGULAR) {
                member.getRepairTracker().setCR(0.6f);
            } else if (member.getCrewXPLevel() == CrewXPLevel.VETERAN) {
                member.getRepairTracker().setCR(0.7f);
            } else if (member.getCrewXPLevel() == CrewXPLevel.ELITE) {
                member.getRepairTracker().setCR(0.8f);
            }

            if (member.getMaxCrew() <= 0f) {
                member.setCrewXPLevel(CrewXPLevel.GREEN);
                member.getRepairTracker().setCR(0.5f);
            }
        }
    }

    protected static FleetData generateFleet(int maxPts, float qualityFactor, float opBonus, FleetSide side, String faction, String fleetType,
                                             MissionDefinitionAPI api) {
        GeneratorFleetTypes type;
        Map<Archetype, Float> archetypeWeights;
        switch (fleetType) {
            case "raiders":
                type = GeneratorFleetTypes.RAIDERS;
                archetypeWeights = DS_FleetInjector.getArchetypeWeights(FleetStyle.RAIDER, faction);
                break;
            case "patrol fleet":
                type = GeneratorFleetTypes.PATROL;
                archetypeWeights = DS_FleetInjector.getArchetypeWeights(FleetStyle.MILITARY, faction);
                break;
            case "hunter-killers":
                type = GeneratorFleetTypes.HUNTERS;
                archetypeWeights = DS_FleetInjector.getArchetypeWeights(FleetStyle.RAIDER, faction);
                break;
            case "carrier fleet":
                type = GeneratorFleetTypes.CARRIER;
                archetypeWeights = DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction);
                break;
            case "war fleet":
                type = GeneratorFleetTypes.WAR;
                archetypeWeights = DS_FleetInjector.getArchetypeWeights(FleetStyle.MILITARY, faction);
                break;
            case "defense fleet":
                type = GeneratorFleetTypes.DEFENSE;
                archetypeWeights = DS_FleetInjector.getArchetypeWeights(FleetStyle.MILITARY, faction);
                break;
            case "convoy":
                type = GeneratorFleetTypes.CONVOY;
                archetypeWeights = DS_FleetInjector.getArchetypeWeights(FleetStyle.CIVILIAN, faction);
                break;
            case "blockade-runners":
                type = GeneratorFleetTypes.BLOCKADE;
                archetypeWeights = DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction);
                break;
            case "invasion fleet":
                type = GeneratorFleetTypes.INVASION;
                archetypeWeights = DS_FleetInjector.getArchetypeWeights(FleetStyle.MILITARY, faction);
                break;
            default:
                return null;
        }

        float realMax = maxPts;
        switch (faction) {
            case "exipirated":
                break;
            case "blackrock_driveyards":
                realMax *= 0.9;
                break;
            case "citadeldefenders":
                break;
            case "exigency":
                break;
            case Factions.HEGEMONY:
                break;
            case "interstellarimperium":
                break;
            case Factions.INDEPENDENT:
                break;
            case Factions.KOL:
                break;
            case "mayorate":
                break;
            case Factions.LUDDIC_CHURCH:
                realMax *= 1.1;
                break;
            case Factions.LUDDIC_PATH:
                realMax *= 1.2;
                break;
            case Factions.PIRATES:
                realMax *= 1.1;
                break;
            case Factions.DIKTAT:
                break;
            case Factions.LIONS_GUARD:
                realMax *= 0.9;
                break;
            case "shadow_industry":
                realMax *= 0.9;
                break;
            case "templars":
                realMax *= 0.85;
                break;
            case Factions.TRITACHYON:
                realMax *= 0.9;
                break;
            case Factions.PERSEAN:
                break;
            case "junk_pirates":
                break;
            case "pack":
                break;
            case "syndicate_asp":
                realMax *= 1.1;
                break;
            case "SCY":
                break;
            case "tiandong":
                break;
            case "diableavionics":
                realMax *= 0.95;
                break;
            case "cabal":
                realMax *= 0.85;
                break;
            default:
        }

        return type.generate(api, side, faction, qualityFactor, opBonus, (int) realMax, archetypeWeights);
    }

    static FleetData finishFleet(List<FleetMemberAPI> fleet, FleetSide side, String faction, MissionDefinitionAPI api) {
        Collections.sort(fleet, DS_FleetRandomizer.PRIORITY);

        inflateFleet(fleet, side, api);

        return new FleetData(fleet);
    }

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        if (first) {
            first = false;
            init();
        }
    }

    private void init() {
        PLANETS.add("gas_giant");
        PLANETS.add("ice_giant");
        PLANETS.add("lava");
        PLANETS.add("frozen");
        PLANETS.add("barren");
        PLANETS.add("toxic");
        PLANETS.add("jungle");
        PLANETS.add("terran");
        PLANETS.add("desert");
        PLANETS.add("arid");
        PLANETS.add("cryovolcanic");
        PLANETS.add("rocky_metallic");
        PLANETS.add("rocky_unstable");
        PLANETS.add("water");
        PLANETS.add("rocky_ice");
        PLANETS.add("irradiated");
        PLANETS.add("barren-bombarded");
        PLANETS.add("toxic_cold");
        PLANETS.add("tundra");
        PLANETS.add("barren-desert");
        PLANETS.add("terran-eccentric");
        PLANETS.add("star_yellow");
        PLANETS.add("star_red");
        PLANETS.add("star_orange");
        PLANETS.add("star_blue");
        PLANETS.add("star_white");
        PLANETS.add("wormholeUnder");
        STAR_COLORS.add(new Color(255, 255, 150, 60)); // star_yellow
        STAR_COLORS.add(new Color(255, 255, 255, 60)); // star_white
        STAR_COLORS.add(new Color(127, 190, 255, 60)); // star_blue
        STAR_COLORS.add(new Color(255, 240, 150, 60)); // star_orange
        STAR_COLORS.add(new Color(255, 0, 0, 100)); // star_red
        STAR_COLORS.add(new Color(255, 150, 225, 128)); // SCY_star
        STAR_COLORS.add(new Color(255, 255, 150, 60)); // SCY_companionStar
        STAR_COLORS.add(new Color(255, 0, 0, 100)); // star_red_dwarf
        STAR_COLORS.add(new Color(255, 233, 148, 30)); // brown_dwarf_star
        STAR_COLORS.add(new Color(255, 155, 155, 145)); // exigency_black_hole
        STAR_COLORS.add(new Color(255, 255, 255, 150)); // star_antioch
        STAR_COLORS.add(new Color(255, 240, 40, 60)); // star_brstar
        FACTIONS.add(Factions.PIRATES, 1f);
        FACTIONS.add(Factions.HEGEMONY, 1f);
        FACTIONS.add(Factions.DIKTAT, 0.5f);
        FACTIONS.add(Factions.LIONS_GUARD, 0.25f);
        FACTIONS.add(Factions.INDEPENDENT, 1f);
        FACTIONS.add(Factions.TRITACHYON, 1f);
        //FACTIONS.add(Factions.PERSEAN, 1f);
        FACTIONS.add(Factions.LUDDIC_PATH, 0.5f);
        FACTIONS.add(Factions.LUDDIC_CHURCH, 1f);
        //FACTIONS.add(Factions.KOL, 0.25f);
        if (DSModPlugin.hasUnderworld) {
            FACTIONS.add("cabal", 0.25f);
        }
        if (DSModPlugin.imperiumExists) {
            PLANETS.add("ii_cobalt");
            PLANETS.add("ii_cydonia");
            PLANETS.add("ii_auric");
            PLANETS.add("ii_irradiated-bombarded");
            FACTIONS.add("interstellarimperium", 1f);
        }
        if (DSModPlugin.citadelExists) {
            FACTIONS.add("citadeldefenders", 1f);
        }
        if (DSModPlugin.blackrockExists) {
            PLANETS.add("star_brstar");
            PLANETS.add("br_blackrockplanet");
            PLANETS.add("br_lodestone");
            PLANETS.add("br_nanoplanet");
            FACTIONS.add("blackrock_driveyards", 1f);
        }
        if (DSModPlugin.exigencyExists) {
            PLANETS.add("exigency_black_hole");
            PLANETS.add("exigency_planetoid");
            FACTIONS.add("exipirated", 0.5f);
            FACTIONS.add("exigency", 0.75f);
        }
        if (DSModPlugin.templarsExists) {
            PLANETS.add("star_antioch");
            FACTIONS.add("templars", 0.25f);
        }
        if (DSModPlugin.shadowyardsExists) {
            PLANETS.add("planet_euripides");
            FACTIONS.add("shadow_industry", 1f);
        }
        if (DSModPlugin.mayorateExists) {
            PLANETS.add("star_red_dwarf");
            PLANETS.add("ilk_ilkhanna");
            FACTIONS.add("mayorate", 1f);
        }
        if (DSModPlugin.junkPiratesExists) {
            PLANETS.add("planet_glory");
            FACTIONS.add("junk_pirates", 1f);
            FACTIONS.add("pack", 0.75f);
            FACTIONS.add("syndicate_asp", 0.5f);
        }
        if (DSModPlugin.scyExists) {
            PLANETS.add("SCY_star");
            PLANETS.add("SCY_homePlanet");
            PLANETS.add("SCY_miningColony");
            PLANETS.add("SCY_companionStar");
            PLANETS.add("SCY_burntPlanet");
            PLANETS.add("SCY_acid");
            PLANETS.add("SCY_moon");
            PLANETS.add("SCY_redRock");
            FACTIONS.add("SCY", 1f);
        }
        if (DSModPlugin.tiandongExists) {
            PLANETS.add("tiandong_shaanxi");
            PLANETS.add("tiandong_tiexiu");
            PLANETS.add("tiandong_zaolei");
            PLANETS.add("tiandong_gan");
            FACTIONS.add("tiandong", 0.75f);
        }
        if (DSModPlugin.diableExists) {
            FACTIONS.add("diableavionics", 0.75f);
        }
    }

    protected String getFactionName(String faction) {
        switch (faction) {
            case Factions.PIRATES:
                return "Pirate";
            case Factions.HEGEMONY:
                return "Hegemony";
            case Factions.DIKTAT:
                return "Sindrian Diktat";
            case Factions.LIONS_GUARD:
                return "Lion's Guard";
            case Factions.INDEPENDENT:
                return "Independent";
            case Factions.TRITACHYON:
                return "Tri-Tachyon";
            case Factions.LUDDIC_PATH:
                return "Pather";
            case Factions.LUDDIC_CHURCH:
                return "Luddic Church";
            case Factions.KOL:
                return "Knights of Ludd";
            case Factions.PERSEAN:
                return "Persean League";
            case "cabal":
                return "Starlight";
            case "interstellarimperium":
                return "Imperial";
            case "citadeldefenders":
                return "Citadel";
            case "blackrock_driveyards":
                return "Blackrock";
            case "exipirated":
                return "Association";
            case "exigency":
                return "ExigencyCorp";
            case "templars":
                return "Knights Templar";
            case "shadow_industry":
                return "Shadowyards";
            case "mayorate":
                return "Mayorate";
            case "junk_pirates":
                return "Junk Pirate";
            case "pack":
                return "P.A.C.K.";
            case "syndicate_asp":
                return "Syndicate";
            case "SCY":
                return "Scy";
            case "tiandong":
                return "Tiandong";
            case "diableavionics":
                return "Diable";
            default:
                return "Unknown";
        }
    }

    protected String getFactionPrefix(String faction) {
        switch (faction) {
            case Factions.PIRATES:
                return "ISS";
            case Factions.HEGEMONY:
                return "HSS";
            case Factions.DIKTAT:
                return "SDS";
            case Factions.LIONS_GUARD:
                return "LGS";
            case Factions.INDEPENDENT:
                return "ISS";
            case Factions.TRITACHYON:
                return "TTS";
            case Factions.LUDDIC_PATH:
                return "";
            case Factions.LUDDIC_CHURCH:
                return "CGR";
            case Factions.KOL:
                return "LSS";
            case Factions.PERSEAN:
                return "FUCK";
            case "cabal":
                return "STAR";
            case "interstellarimperium":
                return "ISA";
            case "citadeldefenders":
                return "DCS";
            case "blackrock_driveyards":
                return "BRS";
            case "exipirated":
                return "AA";
            case "exigency":
                return "EXI";
            case "templars":
                return "";
            case "shadow_industry":
                return "SYS";
            case "mayorate":
                return "MNS";
            case "junk_pirates":
                return "RNS";
            case "pack":
                return "PSS";
            case "syndicate_asp":
                return "ASP";
            case "SCY":
                return "SNS";
            case "tiandong":
                return "THI";
            case "diableavionics":
                return "UNS";
            default:
                return "ISS";
        }
    }

    public static class FleetData {

        public final List<FleetMemberAPI> fleet;

        FleetData(List<FleetMemberAPI> fleet) {
            this.fleet = fleet;
        }
    };

    protected static class Plugin extends BaseEveryFrameCombatPlugin {

        private final float range;

        @SuppressWarnings(value = "PublicConstructorInNonPublicClass")
        public Plugin(float range) {
            this.range = range;
        }

        @Override
        public void advance(float amount, List<InputEventAPI> events) {
        }

        @Override
        public void init(CombatEngineAPI engine) {
            engine.getContext().setStandoffRange(range);
            engine.getContext().setInitialEscapeRange(range);
            engine.getContext().setFlankDeploymentDistance(range * 2f);
        }
    }
}
