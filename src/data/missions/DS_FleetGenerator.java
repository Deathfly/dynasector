package data.missions;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParams;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import data.missions.DS_BaseRandomBattle.FleetData;
import data.scripts.campaign.DS_FleetFactory;
import data.scripts.campaign.fleets.DS_FleetInjector;
import data.scripts.campaign.fleets.DS_FleetInjector.CommanderType;
import data.scripts.campaign.fleets.DS_FleetInjector.CrewType;
import data.scripts.campaign.fleets.DS_FleetInjector.FleetStyle;
import data.scripts.variants.DS_VariantRandomizer.Archetype;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public interface DS_FleetGenerator {

    static enum GeneratorFleetTypes {

        RAIDERS(new RaidersGen()),
        PATROL(new PatrolGen()),
        HUNTERS(new HuntersGen()),
        CARRIER(new CarrierGen()),
        WAR(new WarGen()),
        DEFENSE(new DefenseGen()),
        CONVOY(new ConvoyGen()),
        BLOCKADE(new BlockadeGen()),
        INVASION(new InvasionGen());

        private final DS_FleetGenerator gen;

        private GeneratorFleetTypes(DS_FleetGenerator gen) {
            this.gen = gen;
        }

        FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts, Map<Archetype, Float> archetypeWeights) {
            return gen.generate(api, side, faction, qf, opBonus, maxPts, archetypeWeights);
        }
    }

    FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts, Map<Archetype, Float> archetypeWeights);

    static class RaidersGen implements DS_FleetGenerator {

        @Override
        public FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts,
                                  Map<Archetype, Float> archetypeWeights) {
            List<FleetMemberAPI> fleet = new ArrayList<>(50);

            MarketAPI market = Global.getFactory().createMarket(String.valueOf((new Random()).nextLong()), String.valueOf((new Random()).nextLong()), 4);
            market.setFactionId(faction);
            market.setBaseSmugglingStabilityValue(0);
            CampaignFleetAPI fleetEntity = DS_FleetFactory.createFleet(new FleetParams(null,
                                                                                        market,
                                                                                        faction,
                                                                                        FleetTypes.PATROL_LARGE,
                                                                                        maxPts * 0.75f, // combatPts
                                                                                        maxPts * 0.25f, // freighterPts
                                                                                        0f, // tankerPts
                                                                                        0f, // transportPts
                                                                                        0f, // linerPts
                                                                                        0f, // civilianPts
                                                                                        0f, // utilityPts
                                                                                        0f, // qualityBonus
                                                                                        qf // qualityOverride
                     ));

            if (fleetEntity == null) {
                return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
            }

            fleet.addAll(fleetEntity.getFleetData().getMembersListCopy());

            DS_FleetInjector.randomizeVariants(fleet, faction, null, 0f, qf, opBonus, null, DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction),
                                                CommanderType.DUNCE, -1f, false);

            DS_FleetInjector.levelFleetRandomBattle(fleet, CrewType.STANDARD, faction);

            return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
        }
    }

    static class PatrolGen implements DS_FleetGenerator {

        @Override
        public FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts,
                                  Map<Archetype, Float> archetypeWeights) {
            List<FleetMemberAPI> fleet = new ArrayList<>(50);

            MarketAPI market = Global.getFactory().createMarket(String.valueOf((new Random()).nextLong()), String.valueOf((new Random()).nextLong()), 4);
            market.setFactionId(faction);
            market.setBaseSmugglingStabilityValue(0);
            CampaignFleetAPI fleetEntity = DS_FleetFactory.createFleet(new FleetParams(null,
                                                                                        market,
                                                                                        faction,
                                                                                        FleetTypes.PATROL_LARGE,
                                                                                        maxPts, // combatPts
                                                                                        0f, // freighterPts
                                                                                        0f, // tankerPts
                                                                                        0f, // transportPts
                                                                                        0f, // linerPts
                                                                                        0f, // civilianPts
                                                                                        0f, // utilityPts
                                                                                        0f, // qualityBonus
                                                                                        qf // qualityOverride
                     ));

            if (fleetEntity == null) {
                return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
            }

            fleet.addAll(fleetEntity.getFleetData().getMembersListCopy());

            DS_FleetInjector.randomizeVariants(fleet, faction, null, 0f, qf, opBonus, null, DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction),
                                                CommanderType.DUNCE, -1f, false);

            DS_FleetInjector.levelFleetRandomBattle(fleet, CrewType.MILITARY, faction);

            return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
        }
    }

    static class HuntersGen implements DS_FleetGenerator {

        @Override
        public FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts,
                                  Map<Archetype, Float> archetypeWeights) {
            List<FleetMemberAPI> fleet = new ArrayList<>(50);

            MarketAPI market = Global.getFactory().createMarket(String.valueOf((new Random()).nextLong()), String.valueOf((new Random()).nextLong()), 5);
            market.setFactionId(faction);
            market.setBaseSmugglingStabilityValue(0);
            CampaignFleetAPI fleetEntity = DS_FleetFactory.createFleet(new FleetParams(null,
                                                                                        market,
                                                                                        faction,
                                                                                        FleetTypes.PATROL_LARGE,
                                                                                        maxPts, // combatPts
                                                                                        0f, // freighterPts
                                                                                        0f, // tankerPts
                                                                                        0f, // transportPts
                                                                                        0f, // linerPts
                                                                                        0f, // civilianPts
                                                                                        0f, // utilityPts
                                                                                        0f, // qualityBonus
                                                                                        qf // qualityOverride
                     ));

            if (fleetEntity == null) {
                return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
            }

            fleet.addAll(fleetEntity.getFleetData().getMembersListCopy());

            DS_FleetInjector.randomizeVariants(fleet, faction, null, 0f, qf, opBonus, null, DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction),
                                                CommanderType.DUNCE, -1f, false);

            DS_FleetInjector.levelFleetRandomBattle(fleet, CrewType.PROFESSIONAL, faction);

            return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
        }
    }

    static class CarrierGen implements DS_FleetGenerator {

        @Override
        public FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts,
                                  Map<Archetype, Float> archetypeWeights) {
            List<FleetMemberAPI> fleet = new ArrayList<>(50);

            MarketAPI market = Global.getFactory().createMarket(String.valueOf((new Random()).nextLong()), String.valueOf((new Random()).nextLong()), 6);
            market.setFactionId(faction);
            market.setBaseSmugglingStabilityValue(0);
            CampaignFleetAPI fleetEntity = DS_FleetFactory.createCarrierFleet(new FleetParams(null,
                                                                                               market,
                                                                                               faction,
                                                                                               FleetTypes.PATROL_LARGE,
                                                                                               maxPts, // combatPts
                                                                                               0f, // freighterPts
                                                                                               0f, // tankerPts
                                                                                               0f, // transportPts
                                                                                               0f, // linerPts
                                                                                               0f, // civilianPts
                                                                                               0f, // utilityPts
                                                                                               0f, // qualityBonus
                                                                                               qf // qualityOverride
                     ));

            if (fleetEntity == null) {
                return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
            }

            fleet.addAll(fleetEntity.getFleetData().getMembersListCopy());

            DS_FleetInjector.randomizeVariants(fleet, faction, null, 0f, qf, opBonus, null, DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction),
                                                CommanderType.DUNCE, -1f, false);

            DS_FleetInjector.levelFleetRandomBattle(fleet, CrewType.MILITARY, faction);

            return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
        }
    }

    static class WarGen implements DS_FleetGenerator {

        @Override
        public FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts,
                                  Map<Archetype, Float> archetypeWeights) {
            List<FleetMemberAPI> fleet = new ArrayList<>(50);

            MarketAPI market = Global.getFactory().createMarket(String.valueOf((new Random()).nextLong()), String.valueOf((new Random()).nextLong()), 6);
            market.setFactionId(faction);
            market.setBaseSmugglingStabilityValue(0);
            CampaignFleetAPI fleetEntity = DS_FleetFactory.createFleet(new FleetParams(null,
                                                                                        market,
                                                                                        faction,
                                                                                        FleetTypes.PATROL_LARGE,
                                                                                        maxPts, // combatPts
                                                                                        0f, // freighterPts
                                                                                        0f, // tankerPts
                                                                                        0f, // transportPts
                                                                                        0f, // linerPts
                                                                                        0f, // civilianPts
                                                                                        0f, // utilityPts
                                                                                        0f, // qualityBonus
                                                                                        qf // qualityOverride
                     ));

            if (fleetEntity == null) {
                return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
            }

            fleet.addAll(fleetEntity.getFleetData().getMembersListCopy());

            DS_FleetInjector.randomizeVariants(fleet, faction, null, 0f, qf, opBonus, null, DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction),
                                                CommanderType.DUNCE, -1f, false);

            DS_FleetInjector.levelFleetRandomBattle(fleet, CrewType.MILITARY, faction);

            return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
        }
    }

    static class DefenseGen implements DS_FleetGenerator {

        @Override
        public FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts,
                                  Map<Archetype, Float> archetypeWeights) {
            List<FleetMemberAPI> fleet = new ArrayList<>(50);

            MarketAPI market = Global.getFactory().createMarket(String.valueOf((new Random()).nextLong()), String.valueOf((new Random()).nextLong()), 6);
            market.setFactionId(faction);
            market.setBaseSmugglingStabilityValue(0);
            CampaignFleetAPI fleetEntity = DS_FleetFactory.createFleet(new FleetParams(null,
                                                                                        market,
                                                                                        faction,
                                                                                        FleetTypes.PATROL_LARGE,
                                                                                        maxPts, // combatPts
                                                                                        0f, // freighterPts
                                                                                        0f, // tankerPts
                                                                                        0f, // transportPts
                                                                                        0f, // linerPts
                                                                                        0f, // civilianPts
                                                                                        0f, // utilityPts
                                                                                        0f, // qualityBonus
                                                                                        qf // qualityOverride
                     ));

            if (fleetEntity == null) {
                return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
            }

            fleet.addAll(fleetEntity.getFleetData().getMembersListCopy());

            DS_FleetInjector.randomizeVariants(fleet, faction, null, 0f, qf, opBonus, null, DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction),
                                                CommanderType.DUNCE, -1f, false);

            DS_FleetInjector.levelFleetRandomBattle(fleet, CrewType.STANDARD, faction);

            return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
        }
    }

    static class ConvoyGen implements DS_FleetGenerator {

        @Override
        public FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts,
                                  Map<Archetype, Float> archetypeWeights) {
            List<FleetMemberAPI> fleet = new ArrayList<>(50);

            MarketAPI market = Global.getFactory().createMarket(String.valueOf((new Random()).nextLong()), String.valueOf((new Random()).nextLong()), 4);
            market.setFactionId(faction);
            market.setBaseSmugglingStabilityValue(0);
            CampaignFleetAPI fleetEntity = DS_FleetFactory.createFleet(new FleetParams(null,
                                                                                        market,
                                                                                        faction,
                                                                                        FleetTypes.PATROL_LARGE,
                                                                                        maxPts * 0.6f, // combatPts
                                                                                        maxPts * 0.1f, // freighterPts
                                                                                        maxPts * 0.1f, // tankerPts
                                                                                        maxPts * 0.1f, // transportPts
                                                                                        maxPts * 0.1f, // linerPts
                                                                                        maxPts * 0.1f, // civilianPts
                                                                                        0f, // utilityPts
                                                                                        0f, // qualityBonus
                                                                                        qf // qualityOverride
                     ));

            if (fleetEntity == null) {
                return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
            }

            fleet.addAll(fleetEntity.getFleetData().getMembersListCopy());

            DS_FleetInjector.randomizeVariants(fleet, faction, null, 0f, qf, opBonus, null, DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction),
                                                CommanderType.DUNCE, -1f, false);

            DS_FleetInjector.levelFleetRandomBattle(fleet, CrewType.CIVILIAN, faction);

            return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
        }
    }

    static class BlockadeGen implements DS_FleetGenerator {

        @Override
        public FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts,
                                  Map<Archetype, Float> archetypeWeights) {
            List<FleetMemberAPI> fleet = new ArrayList<>(50);

            MarketAPI market = Global.getFactory().createMarket(String.valueOf((new Random()).nextLong()), String.valueOf((new Random()).nextLong()), 5);
            market.setFactionId(faction);
            market.setBaseSmugglingStabilityValue(0);
            CampaignFleetAPI fleetEntity = DS_FleetFactory.createFleet(new FleetParams(null,
                                                                                        market,
                                                                                        faction,
                                                                                        FleetTypes.PATROL_LARGE,
                                                                                        maxPts * 0.8f, // combatPts
                                                                                        maxPts * 0.1f, // freighterPts
                                                                                        0f, // tankerPts
                                                                                        maxPts * 0.1f, // transportPts
                                                                                        0f, // linerPts
                                                                                        0f, // civilianPts
                                                                                        0f, // utilityPts
                                                                                        0f, // qualityBonus
                                                                                        qf // qualityOverride
                     ));

            if (fleetEntity == null) {
                return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
            }

            fleet.addAll(fleetEntity.getFleetData().getMembersListCopy());

            DS_FleetInjector.randomizeVariants(fleet, faction, null, 0f, qf, opBonus, null, DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction),
                                                CommanderType.DUNCE, -1f, false);

            DS_FleetInjector.levelFleetRandomBattle(fleet, CrewType.PROFESSIONAL, faction);

            return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
        }
    }

    static class InvasionGen implements DS_FleetGenerator {

        @Override
        public FleetData generate(MissionDefinitionAPI api, FleetSide side, String faction, float qf, float opBonus, int maxPts,
                                  Map<Archetype, Float> archetypeWeights) {
            List<FleetMemberAPI> fleet = new ArrayList<>(50);

            MarketAPI market = Global.getFactory().createMarket(String.valueOf((new Random()).nextLong()), String.valueOf((new Random()).nextLong()), 6);
            market.setFactionId(faction);
            market.setBaseSmugglingStabilityValue(0);
            CampaignFleetAPI fleetEntity = DS_FleetFactory.createFleet(new FleetParams(null,
                                                                                        market,
                                                                                        faction,
                                                                                        FleetTypes.PATROL_LARGE,
                                                                                        maxPts * 0.7f, // combatPts
                                                                                        0f, // freighterPts
                                                                                        0f, // tankerPts
                                                                                        maxPts * 0.2f, // transportPts
                                                                                        0f, // linerPts
                                                                                        maxPts * 0.1f, // civilianPts
                                                                                        0f, // utilityPts
                                                                                        0f, // qualityBonus
                                                                                        qf // qualityOverride
                     ));

            if (fleetEntity == null) {
                return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
            }

            fleet.addAll(fleetEntity.getFleetData().getMembersListCopy());

            DS_FleetInjector.randomizeVariants(fleet, faction, null, 0f, qf, opBonus, null, DS_FleetInjector.getArchetypeWeights(FleetStyle.STANDARD, faction),
                                                CommanderType.DUNCE, -1f, false);

            DS_FleetInjector.levelFleetRandomBattle(fleet, CrewType.MILITARY, faction);

            return DS_BaseRandomBattle.finishFleet(fleet, side, faction, api);
        }
    }
}
