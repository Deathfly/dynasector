package data.missions.tester;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;
import data.scripts.variants.DS_Database;
import data.scripts.variants.DS_FactionVariant;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class MissionDefinition implements MissionDefinitionPlugin {

    public static final Comparator<FleetMemberAPI> PRIORITY = new Comparator<FleetMemberAPI>() {
        @Override
        public int compare(FleetMemberAPI member1, FleetMemberAPI member2) {
            if (ALWAYS_LAST.contains(member1.getHullId()) && !ALWAYS_LAST.contains(member2.getHullId())) {
                return 1;
            } else if (!ALWAYS_LAST.contains(member1.getHullId()) && ALWAYS_LAST.contains(member2.getHullId())) {
                return -1;
            }
            float wt1 = member1.getStats().getSuppliesPerMonth().getBaseValue() + member1.getMinCrew() / 100f;
            float wt2 = member2.getStats().getSuppliesPerMonth().getBaseValue() + member2.getMinCrew() / 100f;
            if (Float.compare(wt2, wt1) == 0) {
                if (member1.getHullSpec().getHullName().compareTo(member2.getHullSpec().getHullName()) != 0) {
                    return member1.getHullSpec().getHullName().compareTo(member2.getHullSpec().getHullName());
                } else {
                    return member1.getId().compareTo(member2.getId());
                }
            } else {
                return Float.compare(wt2, wt1);
            }
        }
    };

    private static final Set<String> ALWAYS_LAST = new HashSet<>(10);

    static {
        ALWAYS_LAST.add("ssp_hyperzero");
        ALWAYS_LAST.add("ssp_superzero");
        ALWAYS_LAST.add("ssp_zero");
        ALWAYS_LAST.add("ssp_cristarium");
        ALWAYS_LAST.add("ssp_ezekiel");
        ALWAYS_LAST.add("ssp_zeus");
        ALWAYS_LAST.add("ssp_archangel");
        ALWAYS_LAST.add("ssp_ultron");
        ALWAYS_LAST.add("ssp_oberon");
        ALWAYS_LAST.add("ssp_superhyperion");
        ALWAYS_LAST.add("ii_boss_praetorian");
        ALWAYS_LAST.add("ii_boss_olympus");
        ALWAYS_LAST.add("ii_boss_dominus");
        ALWAYS_LAST.add("msp_boss_potniaBis");
        ALWAYS_LAST.add("ms_boss_charybdis");
        ALWAYS_LAST.add("ms_boss_mimir");
        ALWAYS_LAST.add("tem_boss_paladin");
        ALWAYS_LAST.add("tem_boss_archbishop");
        ALWAYS_LAST.add("ssp_boss_phaeton");
        ALWAYS_LAST.add("ssp_boss_hammerhead");
        ALWAYS_LAST.add("ssp_boss_sunder");
        ALWAYS_LAST.add("ssp_boss_tarsus");
        ALWAYS_LAST.add("ssp_boss_medusa");
        ALWAYS_LAST.add("ssp_boss_falcon");
        ALWAYS_LAST.add("ssp_boss_hyperion");
        ALWAYS_LAST.add("ssp_boss_paragon");
        ALWAYS_LAST.add("ssp_boss_mule");
        ALWAYS_LAST.add("ssp_boss_aurora");
        ALWAYS_LAST.add("ssp_boss_odyssey");
        ALWAYS_LAST.add("ssp_boss_atlas");
        ALWAYS_LAST.add("ssp_boss_afflictor");
        ALWAYS_LAST.add("ssp_boss_brawler");
        ALWAYS_LAST.add("ssp_boss_cerberus");
        ALWAYS_LAST.add("ssp_boss_dominator");
        ALWAYS_LAST.add("ssp_boss_doom");
        ALWAYS_LAST.add("ssp_boss_euryale");
        ALWAYS_LAST.add("ssp_boss_lasher_b");
        ALWAYS_LAST.add("ssp_boss_lasher_r");
        ALWAYS_LAST.add("ssp_boss_onslaught");
        ALWAYS_LAST.add("ssp_boss_shade");
        ALWAYS_LAST.add("ssp_boss_eagle");
        ALWAYS_LAST.add("ssp_boss_beholder");
        ALWAYS_LAST.add("ssp_boss_dominator_luddic_path");
        ALWAYS_LAST.add("ssp_boss_onslaught_luddic_path");
        ALWAYS_LAST.add("ssp_boss_astral");
        ALWAYS_LAST.add("ssp_boss_conquest");
        ALWAYS_LAST.add("tiandong_boss_wuzhang");
        ALWAYS_LAST.add("pack_bulldog_bullseye");
        ALWAYS_LAST.add("pack_pitbull_bullseye");
        ALWAYS_LAST.add("pack_komondor_bullseye");
        ALWAYS_LAST.add("pack_schnauzer_bullseye");
        ALWAYS_LAST.add("diableavionics_IBBgulf");
    }

    private final Set<String> allShips = new HashSet<>(300);
    private final Set<FleetMemberAPI> ships = new TreeSet<>(PRIORITY);

    @Override
    public void defineMission(MissionDefinitionAPI api) {
        for (Map<String, List<DS_FactionVariant>> roleMap : DS_Database.factionVariants.values()) {
            for (List<DS_FactionVariant> variantList : roleMap.values()) {
                for (DS_FactionVariant variant : variantList) {
                    boolean added = allShips.add(variant.variantId);
                    if (added) {
                        try {
                            FleetMemberAPI member;
                            if (variant.variantId.endsWith("_wing")) {
                                member = Global.getFactory().createFleetMember(FleetMemberType.FIGHTER_WING, variant.variantId);
                            } else {
                                if (!variant.variantId.endsWith("_Hull")) {
                                    member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, variant.variantId + "_Hull");
                                } else {
                                    member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, variant.variantId);
                                }
                            }
                            ships.add(member);
                        } catch (Exception ex) {
                        }
                    }
                }
            }
        }

        addShip("ssp_hyperzero_Hull");
        addShip("ssp_superzero_Hull");
        addShip("ssp_zero_Hull");
        addShip("ssp_cristarium_Hull");
        addShip("ssp_ezekiel_Hull");
        addShip("ssp_zeus_Hull");
        //addShip("ssp_archangel_Hull");
        addShip("ssp_ultron_Hull");
        addShip("ssp_oberon_Hull");
        addShip("ssp_superhyperion_Hull");
        addShip("ii_boss_praetorian_Hull");
        addShip("ii_boss_olympus_Hull");
        addShip("ii_boss_dominus_Hull");
        addShip("msp_boss_potniaBis_Hull");
        addShip("ms_boss_charybdis_Hull");
        addShip("ms_boss_mimir_Hull");
        addShip("tem_boss_paladin_Hull");
        addShip("tem_boss_archbishop_Hull");
        addShip("ssp_boss_phaeton_Hull");
        addShip("ssp_boss_hammerhead_Hull");
        addShip("ssp_boss_sunder_Hull");
        addShip("ssp_boss_tarsus_Hull");
        addShip("ssp_boss_medusa_Hull");
        addShip("ssp_boss_falcon_Hull");
        addShip("ssp_boss_hyperion_Hull");
        addShip("ssp_boss_paragon_Hull");
        addShip("ssp_boss_mule_Hull");
        addShip("ssp_boss_aurora_Hull");
        addShip("ssp_boss_odyssey_Hull");
        addShip("ssp_boss_atlas_Hull");
        addShip("ssp_boss_afflictor_Hull");
        addShip("ssp_boss_brawler_Hull");
        addShip("ssp_boss_cerberus_Hull");
        addShip("ssp_boss_dominator_Hull");
        addShip("ssp_boss_doom_Hull");
        addShip("ssp_boss_euryale_Hull");
        addShip("ssp_boss_lasher_b_Hull");
        addShip("ssp_boss_lasher_r_Hull");
        addShip("ssp_boss_onslaught_Hull");
        addShip("ssp_boss_shade_Hull");
        addShip("ssp_boss_eagle_Hull");
        addShip("ssp_boss_beholder_Hull");
        addShip("ssp_boss_dominator_luddic_path_Hull");
        addShip("ssp_boss_onslaught_luddic_path_Hull");
        addShip("ssp_boss_astral_Hull");
        addShip("ssp_boss_conquest_Hull");
        addShip("tiandong_boss_wuzhang_Hull");
        addShip("pack_bulldog_bullseye_Hull");
        addShip("pack_pitbull_bullseye_Hull");
        addShip("pack_komondor_bullseye_Hull");
        addShip("pack_schnauzer_bullseye_Hull");
        addShip("diableavionics_IBBgulf_Hull");

        api.initFleet(FleetSide.PLAYER, "ISS", FleetGoal.ATTACK, false, 5);
        api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true, 5);

        api.setFleetTagline(FleetSide.PLAYER, "Your forces");
        api.setFleetTagline(FleetSide.ENEMY, "Enemy forces");

        api.addBriefingItem("Defeat all enemy forces");

        generateFleet(FleetSide.PLAYER, ships, api);
        generateFleet(FleetSide.ENEMY, ships, api);

        float width = 24000f;
        float height = 18000f;
        api.initMap(-width / 2f, width / 2f, -height / 2f, height / 2f);

        float minX = -width / 2;
        float minY = -height / 2;

        for (int i = 0; i < 50; i++) {
            float x = (float) Math.random() * width - width / 2;
            float y = (float) Math.random() * height - height / 2;
            float radius = 100f + (float) Math.random() * 400f;
            api.addNebula(x, y, radius);
        }

        api.addObjective(minX + width * 0.25f + 2000, minY + height * 0.25f + 2000, "nav_buoy");
        api.addObjective(minX + width * 0.75f - 2000, minY + height * 0.25f + 2000, "comm_relay");
        api.addObjective(minX + width * 0.75f - 2000, minY + height * 0.75f - 2000, "nav_buoy");
        api.addObjective(minX + width * 0.25f + 2000, minY + height * 0.75f - 2000, "comm_relay");
        api.addObjective(minX + width * 0.5f, minY + height * 0.5f, "sensor_array");

        String[] planets = {"barren", "terran", "gas_giant", "ice_giant", "cryovolcanic", "frozen", "jungle", "desert", "arid"};
        String planet = planets[(int) (Math.random() * planets.length)];
        float radius = 100f + (float) Math.random() * 150f;
        api.addPlanet(0, 0, radius, planet, 200f, true);

        ships.clear();
    }

    private void addShip(String variant) {
        boolean added = allShips.add(variant);
        if (added) {
            try {
                FleetMemberAPI member;
                if (variant.endsWith("_wing")) {
                    member = Global.getFactory().createFleetMember(FleetMemberType.FIGHTER_WING, variant);
                } else {
                    if (!variant.endsWith("_Hull")) {
                        member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, variant + "_Hull");
                    } else {
                        member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, variant);
                    }
                }
                ships.add(member);
            } catch (Exception ex) {
            }
        }
    }

    private void generateFleet(FleetSide side, Set<FleetMemberAPI> ships, MissionDefinitionAPI api) {
        for (FleetMemberAPI ship : ships) {
            try {
                String id = ship.getSpecId();
                if (id.endsWith("_wing")) {
                    api.addToFleet(side, id, FleetMemberType.FIGHTER_WING, false);
                } else {
                    if (!id.endsWith("_Hull")) {
                        id += "_Hull";
                    }
                    api.addToFleet(side, id, FleetMemberType.SHIP, false);
                }
            } catch (Exception ex) {
            }
        }
    }
}
