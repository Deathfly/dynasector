package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.impl.campaign.ids.Strings;
import com.fs.starfarer.api.impl.campaign.rulecmd.DS_NGCAddFactionOptions.FactionChoice;
import com.fs.starfarer.api.util.Misc.Token;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * SSP_NGCAddShipOptions
 */
public class DS_NGCAddShipOptions extends BaseCommandPlugin {

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap) {
        dialog.getOptionPanel().clearOptions();

        String param = params.get(0).getString(memoryMap);
        String group = params.get(1).getString(memoryMap);
        String faction = params.get(2).getString(memoryMap);
        switch (param) {
            default:
                return false;
            case "size": {
                dialog.getOptionPanel().clearOptions();
                ShipTypeChoice shipType = ShipTypeChoice.getShipTypeChoice(params.get(3).getString(memoryMap));
                int smallCredits = 5000 + shipType.bonusCashSmall;
                int mediumCredits = 35000 + shipType.bonusCashMedium;
                int largeCredits = 85000 + shipType.bonusCashLarge;
                String smallStart = "Classic start" + "\n";
                String mediumStart = "Standard start" + "\n";
                String largeStart = "Accelerated start" + "\n";
                smallStart += shipType.displayMap.get(SizeChoice.SMALL);
                mediumStart += shipType.displayMap.get(SizeChoice.MEDIUM);
                largeStart += shipType.displayMap.get(SizeChoice.LARGE);
                if (smallCredits > 0) {
                    smallStart += "\n" + "+ " + smallCredits + Strings.C;
                }
                if (mediumCredits > 0) {
                    mediumStart += "\n" + "+ " + mediumCredits + Strings.C;
                }
                if (largeCredits > 0) {
                    largeStart += "\n" + "+ " + largeCredits + Strings.C;
                }

                switch (group) {
                    case "civilian":
                        dialog.getTextPanel().addParagraph("Overall, your luck has been...");
                        dialog.getOptionPanel().addOption("Terrible", "ngcSizeSmall", smallStart);
                        dialog.getOptionPanel().addOption("Average", "ngcSizeMedium", mediumStart);
                        dialog.getOptionPanel().addOption("Amazing", "ngcSizeLarge", largeStart);
                        break;
                    case "military":
                        dialog.getTextPanel().addParagraph("You ultimately achieved the rank of...");
                        dialog.getOptionPanel().addOption("Lieutenant", "ngcSizeSmall", smallStart);
                        dialog.getOptionPanel().addOption("Commander", "ngcSizeMedium", mediumStart);
                        dialog.getOptionPanel().addOption("Captain", "ngcSizeLarge", largeStart);
                        break;
                    case "corporate":
                        dialog.getTextPanel().addParagraph("You were eventually promoted to...");
                        dialog.getOptionPanel().addOption("Supervisor", "ngcSizeSmall", smallStart);
                        dialog.getOptionPanel().addOption("Manager", "ngcSizeMedium", mediumStart);
                        dialog.getOptionPanel().addOption("General Manager", "ngcSizeLarge", largeStart);
                        break;
                    case "mercenary":
                        dialog.getTextPanel().addParagraph("Your long-term profits were...");
                        dialog.getOptionPanel().addOption("Poor", "ngcSizeSmall", smallStart);
                        dialog.getOptionPanel().addOption("Middling", "ngcSizeMedium", mediumStart);
                        dialog.getOptionPanel().addOption("Excellent", "ngcSizeLarge", largeStart);
                        break;
                    case "outlaw":
                        dialog.getTextPanel().addParagraph("All in all, your spoils have been...");
                        dialog.getOptionPanel().addOption("Paltry", "ngcSizeSmall", smallStart);
                        dialog.getOptionPanel().addOption("Adequate", "ngcSizeMedium", mediumStart);
                        dialog.getOptionPanel().addOption("Sizeable", "ngcSizeLarge", largeStart);
                        break;
                    case "outsider":
                        dialog.getTextPanel().addParagraph("Fortune has...");
                        dialog.getOptionPanel().addOption("Frowned upon you", "ngcSizeSmall", smallStart);
                        dialog.getOptionPanel().addOption("Ignored you", "ngcSizeMedium", mediumStart);
                        dialog.getOptionPanel().addOption("Smiled upon you", "ngcSizeLarge", largeStart);
                        break;
                    default:
                        return false;
                }
                break;
            }
            case "ship": {
                dialog.getOptionPanel().clearOptions();
                FactionChoice factionChoice = FactionChoice.getFactionChoice(faction);

                switch (group) {
                    case "civilian":
                        dialog.getTextPanel().addParagraph("For your career, you became...");
                        break;
                    case "military":
                        dialog.getTextPanel().addParagraph("In service, you took on the role of...");
                        break;
                    case "corporate":
                        dialog.getTextPanel().addParagraph("You settled into employment as...");
                        break;
                    case "mercenary":
                        dialog.getTextPanel().addParagraph("You found your specialty as...");
                        break;
                    case "outlaw":
                        dialog.getTextPanel().addParagraph("Your exploits led you to the path of...");
                        break;
                    case "outsider":
                        dialog.getTextPanel().addParagraph("Fate led to your calling as...");
                        break;
                    default:
                        return false;
                }

                for (ShipTypeChoice choice : factionChoice.shipTypeChoices) {
                    dialog.getOptionPanel().addOption(choice.flavorString, "ngcShipType" + choice.name());
                }
                break;
            }
        }

        return true;
    }

    static enum ShipTypeChoice {

        // CIV_RANDOM, FREIGHTER_*, TANKER_*, PERSONNEL_*, LINER_* are worth 1/2
        // FREIGHTER_LARGE, TANKER_LARGE, PERSONNEL_LARGE, LINER_LARGE are special and are worth 3 instead of 2
        // COMBAT_FREIGHTER_*, CARRIER_*, UTILITY, TUG, CRIG are worth 3/4
        // CARRIER_* size is actually one step up, so CARRIER_SMALL is 1.5, CARRIER_MEDIUM is 3, and CARRIER_LARGE is 6
        // Small: 1 pt
        // Medium: 4 pts
        // Large: 9 pts
        // Leftover points: 10000cr per pt
        TRADER(0, 0, 0,
               "An entrepreneur, trading legal goods and taking opportunities",
               Arrays.asList(
                       new RoleEntry(SizeChoice.SMALL, ShipRoles.FREIGHTER_MEDIUM),
                       new RoleEntry(SizeChoice.MEDIUM, ShipRoles.ESCORT_SMALL, ShipRoles.ESCORT_SMALL, ShipRoles.FREIGHTER_MEDIUM, ShipRoles.FREIGHTER_SMALL,
                                     ShipRoles.CIV_RANDOM),
                       new RoleEntry(SizeChoice.LARGE, ShipRoles.ESCORT_MEDIUM, ShipRoles.ESCORT_SMALL, ShipRoles.ESCORT_SMALL, ShipRoles.FREIGHTER_LARGE,
                                     ShipRoles.FREIGHTER_MEDIUM, ShipRoles.FREIGHTER_MEDIUM)
               ), Arrays.asList(
                       new DisplayEntry(SizeChoice.SMALL, "1 Medium Freighter"),
                       new DisplayEntry(SizeChoice.MEDIUM, "2 Escort Frigates" + "\n" + "1 Medium Freighter" + "\n" + "1 Small Freighter" + "\n" +
                                        "1 Civilian Extra"),
                       new DisplayEntry(SizeChoice.LARGE, "1 Escort Destroyer" + "\n" + "2 Escort Frigates" + "\n" + "1 Large Freighter" + "\n" +
                                        "2 Medium Freighters")
               )),
        SMUGGLER(2500, 0, 0,
                 "A smuggler, taking risks and dealing in restricted wares",
                 Arrays.asList(
                         new RoleEntry(SizeChoice.SMALL, ShipRoles.COMBAT_FREIGHTER_SMALL),
                         new RoleEntry(SizeChoice.MEDIUM, ShipRoles.COMBAT_FREIGHTER_MEDIUM, ShipRoles.COMBAT_FREIGHTER_SMALL, ShipRoles.COMBAT_FREIGHTER_SMALL,
                                       ShipRoles.ESCORT_SMALL),
                         new RoleEntry(SizeChoice.LARGE, ShipRoles.COMBAT_FREIGHTER_LARGE, ShipRoles.COMBAT_FREIGHTER_MEDIUM, ShipRoles.COMBAT_FREIGHTER_MEDIUM,
                                       ShipRoles.ESCORT_MEDIUM, ShipRoles.ESCORT_SMALL)
                 ), Arrays.asList(
                         new DisplayEntry(SizeChoice.SMALL, "1 Small Combat Freighter"),
                         new DisplayEntry(SizeChoice.MEDIUM, "1 Medium Combat Freighter" + "\n" + "2 Small Combat Freighters" + "\n" + "1 Escort Frigate"),
                         new DisplayEntry(SizeChoice.LARGE, "1 Large Combat Freighter" + "\n" + "2 Medium Combat Freighters" + "\n" + "1 Escort Destroyer" +
                                          "\n" + "1 Escort Frigate")
                 )),
        RAIDER(2500, 5000, 0,
               "A raider, swift to strike and keen to loot",
               Arrays.asList(
                       new RoleEntry(SizeChoice.SMALL, ShipRoles.COMBAT_FREIGHTER_SMALL),
                       new RoleEntry(SizeChoice.MEDIUM, ShipRoles.FAST_ATTACK, ShipRoles.FAST_ATTACK, ShipRoles.COMBAT_FREIGHTER_SMALL,
                                     ShipRoles.COMBAT_FREIGHTER_SMALL),
                       new RoleEntry(SizeChoice.LARGE, ShipRoles.COMBAT_MEDIUM, ShipRoles.COMBAT_FREIGHTER_MEDIUM, ShipRoles.COMBAT_FREIGHTER_MEDIUM,
                                     ShipRoles.FAST_ATTACK, ShipRoles.FAST_ATTACK, ShipRoles.FAST_ATTACK, ShipRoles.FAST_ATTACK)
               ), Arrays.asList(
                       new DisplayEntry(SizeChoice.SMALL, "1 Small Combat Freighter"),
                       new DisplayEntry(SizeChoice.MEDIUM, "2 Fast Frigates" + "\n" + "2 Small Combat Freighters"),
                       new DisplayEntry(SizeChoice.LARGE, "1 Destroyer" + "\n" + "2 Medium Combat Freighters" + "\n" + "4 Fast Frigates")
               )),
        PRIVATEER(0, 2500, 0,
                  "A privateer, dangerous but practical",
                  Arrays.asList(
                          new RoleEntry(SizeChoice.SMALL, ShipRoles.COMBAT_SMALL),
                          new RoleEntry(SizeChoice.MEDIUM, ShipRoles.COMBAT_MEDIUM, ShipRoles.COMBAT_SMALL, ShipRoles.COMBAT_FREIGHTER_SMALL),
                          new RoleEntry(SizeChoice.LARGE, ShipRoles.COMBAT_LARGE, ShipRoles.COMBAT_MEDIUM, ShipRoles.COMBAT_FREIGHTER_MEDIUM,
                                        ShipRoles.COMBAT_FREIGHTER_SMALL, ShipRoles.COMBAT_FREIGHTER_SMALL)
                  ), Arrays.asList(
                          new DisplayEntry(SizeChoice.SMALL, "1 Frigate"),
                          new DisplayEntry(SizeChoice.MEDIUM, "1 Destroyer" + "\n" + "1 Frigate" + "\n" + "1 Small Combat Freighter"),
                          new DisplayEntry(SizeChoice.LARGE, "1 Cruiser" + "\n" + "1 Destroyer" + "\n" + "1 Medium Combat Freighters" + "\n" +
                                           "2 Small Combat Freighters")
                  )),
        BOUNTY_HUNTER(0, 0, 0,
                      "A bounty hunter, always poised to take on the next quarry",
                      Arrays.asList(
                              new RoleEntry(SizeChoice.SMALL, ShipRoles.COMBAT_SMALL),
                              new RoleEntry(SizeChoice.MEDIUM, ShipRoles.COMBAT_MEDIUM, ShipRoles.COMBAT_SMALL, ShipRoles.COMBAT_SMALL),
                              new RoleEntry(SizeChoice.LARGE, ShipRoles.COMBAT_LARGE, ShipRoles.COMBAT_MEDIUM, ShipRoles.FAST_ATTACK, ShipRoles.COMBAT_SMALL,
                                            ShipRoles.COMBAT_SMALL)
                      ), Arrays.asList(
                              new DisplayEntry(SizeChoice.SMALL, "1 Frigate"),
                              new DisplayEntry(SizeChoice.MEDIUM, "1 Destroyer" + "\n" + "2 Frigates"),
                              new DisplayEntry(SizeChoice.LARGE, "1 Cruiser" + "\n" + "1 Destroyer" + "\n" + "1 Fast Frigate" + "\n" + "2 Frigates")
                      )),
        PATROL(0, 0, 0,
               "A patrol commander, focused on rooting out criminals and enemies",
               Arrays.asList(
                       new RoleEntry(SizeChoice.SMALL, ShipRoles.COMBAT_SMALL),
                       new RoleEntry(SizeChoice.MEDIUM, ShipRoles.COMBAT_MEDIUM, ShipRoles.ESCORT_SMALL, ShipRoles.ESCORT_SMALL),
                       new RoleEntry(SizeChoice.LARGE, ShipRoles.COMBAT_LARGE, ShipRoles.ESCORT_MEDIUM, ShipRoles.COMBAT_SMALL, ShipRoles.COMBAT_SMALL,
                                     ShipRoles.ESCORT_SMALL)
               ), Arrays.asList(
                       new DisplayEntry(SizeChoice.SMALL, "1 Frigate"),
                       new DisplayEntry(SizeChoice.MEDIUM, "1 Destroyer" + "\n" + "2 Escort Frigates"),
                       new DisplayEntry(SizeChoice.LARGE, "1 Cruiser" + "\n" + "1 Escort Destroyer" + "\n" + "2 Frigates" + "\n" + "1 Escort Frigate")
               )),
        CARRIER(0, -5000, 0,
                "A squadron leader, favoring strike craft to defeat the enemy",
                Arrays.asList(
                        new RoleEntry(SizeChoice.SMALL, ShipRoles.COMBAT_SMALL),
                        new RoleEntry(SizeChoice.MEDIUM, ShipRoles.CARRIER_SMALL, ShipRoles.ESCORT_SMALL, ShipRoles.FIGHTER, ShipRoles.FIGHTER),
                        new RoleEntry(SizeChoice.LARGE, ShipRoles.CARRIER_MEDIUM, ShipRoles.ESCORT_MEDIUM, ShipRoles.FAST_ATTACK, ShipRoles.BOMBER,
                                      ShipRoles.FIGHTER, ShipRoles.FIGHTER)
                ), Arrays.asList(
                        new DisplayEntry(SizeChoice.SMALL, "1 Frigate"),
                        new DisplayEntry(SizeChoice.MEDIUM, "1 Destroyer Carrier" + "\n" + "1 Escort Frigate" + "\n" + "2 Fighter Wings"),
                        new DisplayEntry(SizeChoice.LARGE, "1 Cruiser Carrier" + "\n" + "1 Escort Destroyer" + "\n" + "1 Fast Frigate" + "\n" +
                                         "1 Bomber Wing" + "\n" + "2 Fighter Wings")
                ));

        final int bonusCashSmall;
        final int bonusCashMedium;
        final int bonusCashLarge;
        final String flavorString;

        final Map<SizeChoice, String[]> roleMap;
        final Map<SizeChoice, String> displayMap;

        private ShipTypeChoice(int bonusCashSmall, int bonusCashMedium, int bonusCashLarge, String flavorString,
                               List<RoleEntry> roleEntryList, List<DisplayEntry> displayEntryList) {
            this.bonusCashSmall = bonusCashSmall;
            this.bonusCashMedium = bonusCashMedium;
            this.bonusCashLarge = bonusCashLarge;
            this.flavorString = flavorString;
            roleMap = new LinkedHashMap<>(roleEntryList.size());
            for (RoleEntry entry : roleEntryList) {
                roleMap.put(entry.size, entry.shipRoles);
            }
            displayMap = new LinkedHashMap<>(displayEntryList.size());
            for (DisplayEntry entry : displayEntryList) {
                displayMap.put(entry.size, entry.dislpay);
            }
        }

        static ShipTypeChoice getShipTypeChoice(String id) {
            for (ShipTypeChoice choice : ShipTypeChoice.values()) {
                if (choice.name().contentEquals(id)) {
                    return choice;
                }
            }
            return null;
        }

        private static final class DisplayEntry {

            final String dislpay;
            final SizeChoice size;

            DisplayEntry(SizeChoice size, String dislpay) {
                this.size = size;
                this.dislpay = dislpay;
            }
        }

        private static final class RoleEntry {

            final String[] shipRoles;
            final SizeChoice size;

            RoleEntry(SizeChoice size, String... shipRoles) {
                this.size = size;
                this.shipRoles = shipRoles;
            }
        }
    }

    static enum SizeChoice {

        SMALL, MEDIUM, LARGE;
    }
}
