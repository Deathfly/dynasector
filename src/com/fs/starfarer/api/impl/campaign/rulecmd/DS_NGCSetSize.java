package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.CharacterCreationData;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.fleet.ShipFilter;
import com.fs.starfarer.api.fleet.ShipRolePick;
import com.fs.starfarer.api.impl.campaign.rulecmd.DS_NGCAddFactionOptions.FactionChoice;
import com.fs.starfarer.api.impl.campaign.rulecmd.DS_NGCAddShipOptions.ShipTypeChoice;
import com.fs.starfarer.api.impl.campaign.rulecmd.DS_NGCAddShipOptions.SizeChoice;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.Misc.Token;
import data.scripts.campaign.fleets.DS_FleetInjector.FactionStyle;
import data.scripts.variants.DS_Database;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * SSP_NGCSetSize
 */
public class DS_NGCSetSize extends BaseCommandPlugin {

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap) {
        String param = params.get(0).getString(memoryMap);
        String faction = params.get(1).getString(memoryMap);
        ShipTypeChoice shipType = ShipTypeChoice.getShipTypeChoice(params.get(2).getString(memoryMap));
        CharacterCreationData data = (CharacterCreationData) memoryMap.get(MemKeys.LOCAL).get("$characterData");

        String size = Misc.lcFirst(param.substring("ngcSize".length()));
        memoryMap.get(MemKeys.LOCAL).set("$ngcSize", size, 7);
        SizeChoice sizeChoice;
        switch (size) {
            case "small":
                sizeChoice = SizeChoice.SMALL;
                break;
            case "medium":
                sizeChoice = SizeChoice.MEDIUM;
                break;
            case "large":
                sizeChoice = SizeChoice.LARGE;
                break;
            default:
                return false;
        }

        Random rand = new Random();
        MarketAPI market = Global.getFactory().createMarket(String.valueOf(rand.nextLong()), String.valueOf(rand.nextLong()), 6);
        market.setFactionId(faction);
        market.setBaseSmugglingStabilityValue(0);
        float QF = (FactionStyle.getStyle(faction).highQF + FactionStyle.getStyle(faction).lowQF) / 2f;
        float minQF = (FactionStyle.getStyle(faction).maxQF + FactionStyle.getStyle(faction).minQF) / 2f;
        minQF = Math.min(0.5f, minQF);

        List<String> shipList = new ArrayList<>(shipType.roleMap.get(sizeChoice).length);
        List<String> wingList = new ArrayList<>(shipType.roleMap.get(sizeChoice).length);
        NoShitShips filter = new NoShitShips(minQF, FactionChoice.getFactionChoice(faction));
        for (String role : shipType.roleMap.get(sizeChoice)) {
            List<ShipRolePick> picks = market.pickShipsForRole(role, faction, QF, rand, filter);
            if (picks.isEmpty()) {
                picks = market.pickShipsForRole(role, faction, QF, rand, null);
            }
            for (ShipRolePick pick : picks) {
                if (pick.isFighterWing()) {
                    wingList.add(pick.variantId);
                } else {
                    shipList.add(pick.variantId);
                }
            }
        }

        for (String ship : shipList) {
            ShipVariantAPI variant = Global.getSettings().getVariant(ship);
            dialog.getTextPanel().addParagraph("Added " + variant.getHullSpec().getHullName() + "-class " + variant.getDesignation(),
                                               Misc.getPositiveHighlightColor());
            data.addStartingFleetMember(ship, FleetMemberType.SHIP);
        }
        for (String wing : wingList) {
            FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.FIGHTER_WING, wing);
            dialog.getTextPanel().addParagraph("Added " + member.getHullSpec().getHullName() + " " + member.getVariant().getDisplayName() + " Wing",
                                               Misc.getPositiveHighlightColor());
            data.addStartingFleetMember(wing, FleetMemberType.FIGHTER_WING);
        }

        int credits;
        switch (sizeChoice) {
            case SMALL:
                credits = 5000 + shipType.bonusCashSmall;
                break;
            case MEDIUM:
                credits = 35000 + shipType.bonusCashMedium;
                dialog.getTextPanel().addParagraph("Added officer", Misc.getPositiveHighlightColor());
                break;
            case LARGE:
                credits = 85000 + shipType.bonusCashLarge;
                dialog.getTextPanel().addParagraph("Added 2 officers", Misc.getPositiveHighlightColor());
                break;
            default:
                return false;
        }
        data.getStartingCargo().getCredits().add(credits);
        dialog.getTextPanel().addParagraph("Added " + credits + " credits", Misc.getPositiveHighlightColor());
        if (shipType == ShipTypeChoice.TRADER) {
            dialog.getTextPanel().addParagraph("Added trade goods", Misc.getPositiveHighlightColor());
        }
        if (shipType == ShipTypeChoice.SMUGGLER) {
            dialog.getTextPanel().addParagraph("Added contraband", Misc.getPositiveHighlightColor());
        }

        return true;
    }

    private static class NoShitShips implements ShipFilter {

        private final FactionChoice faction;
        private final float minimumQF;

        NoShitShips(float minimumQF, FactionChoice faction) {
            this.minimumQF = minimumQF;
            this.faction = faction;
        }

        @Override
        public boolean isAvailable(String variantId) {
            if (faction.factionPrefix != null) {
                if (!variantId.startsWith(faction.factionPrefix)) {
                    return false;
                }
            }

            ShipVariantAPI variant;
            try {
                variant = Global.getSettings().getVariant(variantId);
            } catch (Exception e) {
                return true;
            }

            Float qf = DS_Database.variantQuality.get(variant.getHullSpec().getHullId());
            if (qf != null && qf < minimumQF) {
                return false;
            }

            if (variant.getQuality() < minimumQF) {
                return false;
            }

            return true;
        }
    }
}
